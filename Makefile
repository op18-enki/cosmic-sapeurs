build:
	cd fronts && npm run generate-interfaces
	docker-compose build

up:
	docker-compose up

down:
	docker-compose down --remove-orphans

ressources: # if you want to get access to postgres but still use your local python
	docker-compose -f docker-compose.ressources.yml up

test-ressources:
	docker-compose -f docker-compose.test.yml up
