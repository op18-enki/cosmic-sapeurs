from dataclasses import asdict
import json
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event_data,
)

custom_snapshot = {
    "topic": "adagio_snapshot",
    "data": {
        "vehicles": [
            asdict(
                make_adagio_vehicle_event_data(
                    id_mma=1,
                    id_intervention=None,
                    abrege_famille_mma_originelle="VSAV BSPP",
                    abrege_famille_mma_operationnelle="VSAV BSPP",
                    libelle_affectation_operationnelle="STOU",
                    abrege_statut_operationnel="DISPO",
                    omnibus=False,
                    omnibus_id_mma=None,
                )
            ),
            asdict(
                make_adagio_vehicle_event_data(
                    id_mma=2,
                    id_intervention=None,
                    abrege_famille_mma_originelle="VSAV BSPP",
                    abrege_famille_mma_operationnelle="VSAV BSPP",
                    libelle_affectation_operationnelle="STOU",
                    abrege_statut_operationnel="DISPO",
                    omnibus=False,
                    omnibus_id_mma=None,
                )
            ),
            asdict(
                make_adagio_vehicle_event_data(
                    id_mma=3,
                    id_intervention=None,
                    abrege_famille_mma_originelle="VSAV BSPP",
                    abrege_famille_mma_operationnelle="VSAV BSPP",
                    libelle_affectation_operationnelle="CHPT",
                    abrege_statut_operationnel="PARTI",
                    omnibus=False,
                    omnibus_id_mma=None,
                )
            ),
        ],
        "operations": [],
    },
}
with open("data/fake_adagio_responses/snapshot.json", "w") as file:
    json.dump(custom_snapshot, file)
