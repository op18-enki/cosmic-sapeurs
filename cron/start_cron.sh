#!/usr/bin/env bash

# Initialization script for the pipelines docker container, to be run at container startup.
echo "Running $0 $@ (pwd: $(pwd))"

# The script uses the following environment variables:
#  LOGDIR (required): the directory in which to keep the log files.
: "${LOGDIR:=/var/lib/pipelines/log}"

#  CRONFILE (required): The path at which to create the cronfile. Any existing file will be
#  overwritten.
: "${CRONFILE:=/var/lib/pipelines/crontab}"

#  pipeline in cron format. Default: every two days at 03:00.
: "${RESYNC_SCHEDULE:=0 3 */2 * *}"


# Create logdir if it doesn't already exist.
if [[ ! -d $LOGDIR ]]; then
  mkdir -p $LOGDIR && chmod 755 $LOGDIR
fi

echo "cronfile"
echo $CRONFILE

echo "logdir"
echo $LOGDIR

# Regenerate the cronfile. Any new pipeline schedules can be added here.
cat <<EOT > $CRONFILE
SHELL=/bin/bash
PATH=$PATH

$RESYNC_SCHEDULE curl -X POST -d '{"victim": [], "fire": []}' http://sapeurs-api:8080/api/reset >> $LOGDIR/cron.log 2>&1

EOT

# Register the cronfile with cron.
crontab $CRONFILE

echo "Cron configuration:"
crontab -l

echo "Starting cron in blocking mode"
cron -f
