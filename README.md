# Cosmic Sapeurs 🇬🇧

(Version française [ici](README.fr.md).)

Cosmic-Sapeurs is a project driven by [Paris Fire Fighter Brigade](https://www.pompiersparis.fr/fr/) and [Etalab](https://www.etalab.gouv.fr/) throw the [Entrepreneurs d'Intérêt Général Program](https://www.etalab.gouv.fr/entrepreneurs-d-interet-general). Together, we developed a real-time dashboard telling where are the resources (eg. pump vehicles) and the needs (eg. fires).

Read the project description (in french) on the [EIG Program website](https://entrepreneur-interet-general.etalab.gouv.fr/defis/2020/operations-18.html).

This repository hosts the code of the application.
The backend is in Python; the frontend in React/Tsx and Redux.

🤔 Why "cosmic-sapeurs" ? It comes from the book [Cosmic Python](https://www.cosmicpython.com/), which we followed to design our architecture, in a clean way. By the way, we'd encourage you to read it.

## Functionalities

<div align="center" >
<img src="readme-docs/fonctionnalites.png" alt="fonctionnalites" height="350px">
</div>

## Architecture

### Event-driven

This application is _event-driven_, which means we "listen" (ie. "subscribe", "plug") to an event queue and we trigger changes accordingly.
See [this chapter](https://www.cosmicpython.com/book/part2.html) of Cosmic-Python.
<br>

Concretely speaking: 🔥 ➡ 📞 ➡ 🧑🏾‍💻 ➡ 🚒 ➡ 🎙

- You call 18 to report a fire. When the operation is opened, we receive an event _"operation_changed_status"_ with the relevant information (operation id, status, address, cause, date, emergency level...)

- You call 18 to report a fire. When the operation is opened, we receive an event _"operation_changed_status"_ with the relevant information (operation id, status, address, cause, date, emergency level...)

- A vehicle is affected to the operation. When leaving the station, the officer changes the status of the vehicle and we receive an event _"vehicle_changed_status"_ with the relevant information (vehicle id, status, vehicle category, station, affected operation, date...)

 <br>

An event is an object with a uuid, a topic and a data object. See the definition of an [EventEntity](https://gitlab.com/op18-enki/cosmic-sapeurs/-/blob/develop/backs/shared/shared/events/event_entity.py).

<br>

<div align="center">
<img src="readme-docs/architecture-story.png" alt="architecture example" >
</div>

### Services

There are 4 services (3 in [backs](./backs), 1 [front](./front).

- **User Interface:** a frontend application in typescript / Redux / React. Docs [here](./front/README.md).
- **Sapeurs-Backend:** a Python application, where all the domain code (ie. business logic) happens. Docs [here](./backs/cover-ops/README.md)
- **Converters:** this service aims at converting the raw data (coming from the external system) into sapeurs-format. This is an independent Python application, deployed independently from Sapeurs-Backend. The converters stores the raw events before converting them and sending them to Sapeurs-Backend. Docs [here](./backs/converters/README.md)
- **Replayers:** this service allows us to "replay" events from CSV files, in order to mock the external system.
  We can choose the replay speed. Note that if the converters storage is in CSV mode, the files to replay are directly the stored CSV files. Docs [here](./backs/replayers/README.md)

<div align="center">
<img src="readme-docs/architecture-sapeurs.jpg" alt="architecture logicielle" >
</div>

### Event Flows

Here is a picture of how events are flowing through the app.

<div align="center">
<img src="readme-docs/event-flows.png" alt="event flows" >
</div>

### Interface contract

The interface contract is an independent utility [dataclass-to-interface](./dataclass-to-interface)). It ensures that the format of the objects exchanged between the backend and the frontend is defined only once. Hence, we define our objects as Python dataclass [here](./backs/shared/shared/data_transfert_objects) and we generate their corresponding Typescript Interfaces [there](./front/src/interfaces/generated).

For example :

This VehicleEventData python class :

```python
@dataclass
class VehicleEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    timestamp: str
    raw_vehicle_id: str
    raw_operation_id: str
    status: Literal[ "departed_to_intervention" , "arrived_on_intervention", "transport_to_hospital"]
    raw_status: str
    home_area: Literal["CHPT" , "STOU" , "PARM" ]
    role: Literal["vsav" , "ep" ]
    vehicle_name: str
```

... would become the following tsx interface :

```typescript
interface VehicleEventData {
  timestamp: string;
  raw_vehicle_id: string;
  raw_operation_id?: string;
  status:
    | "departed_to_intervention"
    | "arrived_on_intervention"
    | "transport_to_hospital";
  raw_status: string;
  home_area: "CHPT" | "STOU" | "PARM";
  role: "vsav" | "ep";
  vehicle_name: string;
}
```

### "Clean" organization within each service

If you open any of the three backend service, you'll notice the following organization :

- **DOMAIN**

  - **ports:** dependency API where we define how the application interacts with external "bricks". For example, a repository port would have a method "add". The actual implementation of those "bricks" are made by adapters. The ports are tested in tests/unit.

  - **use cases:** all the application specific business rules gets divided into list of use cases, each having a single responsability. For example: store the new event in a repository. The use-cases are tested in tests/unit.

  - ...

- **ADAPTERS**:
  The actual implementation of the ports. For example, a repository could be implemented with Postgres database, hence the "add" method would be an INSERT. The adapters are tested in tests/integration.

- **ENTRYPOINTS**:
  - ./server.py : the script to launch the application.
  - ./config.py : preparation of the application instances.
    The entrypoint is tested in tests/e2e.

## Installation

Clone the repository with git :

```
git clone https://gitlab.com/op18-enki/cosmic-sapeurs
```

If you don't have git, you can also download the .zip [here](https://gitlab.com/op18-enki/cosmic-sapeurs)

### With Docker

#### Pre-requisite

- Docker CE >= 1.13.0
- docker-compose >= 1.10.0

#### Steps

Copy the `front/.env.sample` in `front/.env` and the cstc.json file. Note that you can adapt the environment variables and the map geo-json, if needed.

```
cp ./front/.env.sample ./front/.env
cp ./front/src/ui/map/empty_cstc.json ./front/src/ui/map/cstc.json
```

Build and launch the services :

```
docker-compose up --build
```

Then visit http://localhost:4200/

If you want to replay events, follow the indications in the [replayers documentation](./backs/replayers/README.md).
If you use docker make sure you have the following environment variable: CONVERTERS_URL="http://localhost:4200/converters"

Note that a folder `./docker-data` has been created. It contains the volumes we want to make persistent. This goes the two ways around : if you have data in this folder, the container will also do. Hence, if you want a fresh start, you may delete this directory :

```
rm -rf docker-data
```

### Without Docker

You may also launch the backs and the front independently. See the respective guidelines: (([replayers docs](/backs/replayers/README.fr.md), [converters docs](/backs/converters/README.fr.md), [cover-ops docs](/backs/cover-ops/README.fr.md), [frontend docs](./front/README.md)).

To launch without docker, depending on your config you may need a redis, rabbitmq and postgres server running.
Than you must make sure you have the following env variables :

- in converter : COVER_OPS_URL="http://localhost:8080/api"
- in replayers : CONVERTERS_URL="http://localhost:8081"
- in front : REACT_APP_WITH_PROXY="TRUE"

## Tests

### Backs

- Follow the installation guidelines of each application (so that you create a virtual environment in each folder.)
- Up the test docker-compose:

```
docker-compose -f docker-compose.test.yml up --build
```

- Launch all tests:

```
 cd ./backs
 sh launch_all_tests.sh
```

### Front

```
cd ./front
npm install
npm run test
```
