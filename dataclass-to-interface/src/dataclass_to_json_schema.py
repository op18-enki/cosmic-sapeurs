import importlib
import json
import os
import shutil
import sys
from glob import glob
from pathlib import Path

import click


@click.command()
@click.option("--input-python-dir", help="Path to dataclasses folder to import")
@click.option(
    "--output-json-dir", help="Path where intermediate json schemas will be stored"
)
def convert(input_python_dir: str, output_json_dir: str):
    """
    Convert python dataclass in input directory into json--schemas
    """
    files = glob(os.path.join(input_python_dir, "*.py"))
    sys.path.append(input_python_dir)

    shutil.rmtree(output_json_dir, ignore_errors=True)
    Path(output_json_dir).mkdir(parents=True, exist_ok=True)
    for file in files:
        module_name = Path(file).stem
        module = importlib.import_module(module_name)
        for (dataclass_name, dataclass_object) in module.__dict__.items():
            if (
                dataclass_name
                not in [
                    "JsonSchemaMixin",
                    "TypeVar",
                    "DateStrField",
                ]
                and isinstance(dataclass_object, type)
            ):
                schema = dataclass_object.json_schema(additionalProperties=False)
                with open(
                    os.path.join(output_json_dir, f"{dataclass_name}.json"), "w"
                ) as f:
                    json.dump(schema, f)


if __name__ == "__main__":
    convert()
