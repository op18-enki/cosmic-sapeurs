export type {
  Area,
  VehicleRole,
  VehicleStatus,
  DomainTopic,
  AvailabilityKind,
  OperationCause,
  OmnibusBalanceKind,
  AvailabilityKindWithoutOmnibusUnavailable,
  AvailabilityKindWithoutUnavailable,
  OperationRelevantStatus,
} from "interfaces/stringLiterals";
export {
  bsppAreaOptions,
  omnibusBalanceKindOptions,
  availabilityKindOptionsWithoutOmnibus,
  availabilityKindOptionsWithoutUnavailable,
} from "interfaces/stringLiterals";
export type { OmnibusBalance } from "interfaces/generated/OmnibusBalance";
export type {
  OmnibusBalanceChangedEventData as OmnibusBalanceChangedEvent,
  OmnibusDetail,
} from "interfaces/generated/OmnibusBalanceChangedEventData";

export type { VehicleEventData as VehicleEvent } from "interfaces/generated/VehicleEventData";
export type {
  Availability,
  AvailabilityChangedEventData as AvailabilityChangedEvent,
} from "interfaces/generated/AvailabilityChangedEventData";
export type {
  OngoingOpChangedEventData as OngoingOpChangedEvent,
  OperationOpeningInfos,
} from "interfaces/generated/OngoingOpChangedEventData";
export type { ResetCoverOpsFinishedEventData as ResetCoverOpsFinishedEvent } from "interfaces/generated/ResetCoverOpsFinishedEventData";
export type { FrontLinesHistory } from "interfaces/generated/FrontLinesHistory";
