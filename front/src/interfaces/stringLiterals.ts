import { Literals } from "interfaces/generated/Literals";

export type VehicleStatus = Literals["vehicleStatus"];
export type VehicleRole = Literals["vehicleRole"];
export type AvailabilityKind = Literals["availabilityKind"];
export type DomainTopic = Literals["domainTopic"];
export type OperationCause = Literals["operationCause"];
export type OmnibusBalanceKind = Literals["omnibusBalanceKind"];
export type OperationRelevantStatus = Literals["operationRelevantStatus"];

export type Area = Literals["area"];

export type AvailabilityKindWithoutOmnibusUnavailable = Exclude<
  AvailabilityKind,
  "unavailable_omnibus"
>;

export type AvailabilityKindWithoutUnavailable = Exclude<
  AvailabilityKindWithoutOmnibusUnavailable,
  "unavailable"
>;

// // prettier-ignore
// export type BsppArea = "STMR" | "ANTO" | "CHPY" | "PLCL" | "STCL" | "CHLY" | "POIS" | "ROUS" | "CHTO" | "STOU" | "PARM" | "VIJF" | "PIER" | "SEVI" | "CHPT" |
// "PCDG" | "LEVA" | "AUBE" | "VISG" | "BOUL" | "NOIS" | "MALF" | "SUCY" | "VILC" | "CHOI" | "VITR" | "STDE" | "LACO" | "MTRL" | "STHO" | "VIMB" | "NEUI" |
// "COBI" | "CLIC" | "PLAI" | "AULN" | "MALA" | "BLME" | "DRAN" | "LAND" | "MENI" | "CHAR" | "PANT" | "BITC" | "BLAN" | "MTMA" | "PROY" | "AUTE" | "DAUP" |
// "MTGE" | "GREN" | "ISSY" | "MEUD" | "BGLR" | "CLAM" | "NATI" | "CBVE" | "PUTX" | "GENN" | "MASS" | "ASNI" | "IVRY" | "RUEI" | "VINC" | "NANT" | "COBE" |
// "NOGT" | "CRET" | "TREM" | "RUNG" | "ORLY" | "BOND" | "BSLT"

// prettier-ignore
export const bsppAreaOptions: Area[] = ["ANTO", "ASNI", "AUBE", "AULN", "AUTE", "BGLR", "BITC", "BLAN", "BLME", "BOND", "BOUL", "BSLT", "CBVE", 
"CHAR", "CHLY", "CHOI", "CHPT", "CHPY", "CHTO", "CLAM", "CLIC", "COBE", "COBI", "CRET", "DAUP", "DRAN", "GENN", "GREN", "ISSY", "IVRY", "LACO",
"LAND", "LEVA", "MALA", "MALF", "MASS", "MENI", "MEUD", "MTGE", "MTMA", "MTRL", "NANT", "NATI", "NEUI", "NOGT", "NOIS", "ORLY", "PANT", "PARM",
"PCDG", "PIER", "PLAI", "PLCL", "POIS", "PROY", "PUTX", "ROUS", "RUEI", "RUNG", "SEVI", "STCL", "STDE", "STHO", "STMR", "STOU", "SUCY", "TREM",
"VIJF", "VILC", "VIMB", "VINC", "VISG", "VITR"];

export const omnibusBalanceKindOptions: OmnibusBalanceKind[] = [
  "pse_on_inter_fire",
  "only_pse_available",
  "only_pse_on_inter_fire",
  "both_available",
  "pse_on_inter_san",
  "vsav_on_inter_san",
  "both_on_inter_san",
  "dangling",
];

export const availabilityKindOptionsWithoutOmnibus: AvailabilityKindWithoutOmnibusUnavailable[] =
  ["available", "on_operation", "recoverable", "unavailable"];

export const availabilityKindOptionsWithoutUnavailable: AvailabilityKindWithoutUnavailable[] =
  ["available", "on_operation", "recoverable"];
