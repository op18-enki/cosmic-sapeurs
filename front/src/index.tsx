import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { App } from "app/App";
import reportWebVitals from "./reportWebVitals";
import { getStore } from "core-logic/setup/initializeStore";
import { Provider } from "react-redux";
import { ENV } from "environmentVariables";
import { RouteProvider } from "app/Router";

const { httpClientKind, pubSubClientKind } = ENV;

ReactDOM.render(
  <React.StrictMode>
    <Provider
      store={getStore({
        httpClientKind,
        pubSubClientKind,
      })}
    >
      <RouteProvider>
        <App />
      </RouteProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root"),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
