import {
  FormControl,
  FormControlLabel,
  FormLabel,
  RadioGroup,
  Radio as MuiRadio,
} from "@material-ui/core";
import * as React from "react";

type Option<T> = {
  value: T;
  label: string;
  color?: string;
};

type UiRadioProps<T> = {
  title?: string;
  options: Option<T>[];
  value: T | null;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  className?: string;
};

export const UiRadio = <T extends unknown>({
  options,
  title,
  handleChange,
  value = null,
  className,
}: UiRadioProps<T>) => (
  <FormControl>
    <FormLabel>{title}</FormLabel>
    <RadioGroup value={value} onChange={handleChange} className={className}>
      {options.map(({ label, value: optionValue, color }) => {
        const style = { ...(color ? { color } : {}) };
        return (
          <FormControlLabel
            value={optionValue}
            control={<MuiRadio color="primary" style={style} />}
            style={style}
            label={label}
          />
        );
      })}
    </RadioGroup>
  </FormControl>
);
