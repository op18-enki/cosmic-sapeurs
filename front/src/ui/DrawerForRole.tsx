import { useSelector } from "app/redux-hooks";
import {
  latestVehicleEventByRawVehicleIdForRoleSelectors,
  vehiclesLabels,
} from "core-logic";
import { AvailabilityKind, VehicleRole } from "interfaces";
import * as R from "ramda";
import { vehicleTableHeaders } from "ui/vehicleTableHeaders";
import { addSinceField } from "utils";
import { UiDrawerForRole } from "./UiDrawerForRole";

const availabilityKindToLabel: Record<AvailabilityKind, string> = {
  available: "disponibles",
  on_operation: "sur intervention",
  recoverable: "récupérables",
  unavailable: "indisponibles",
  unavailable_omnibus: "indisponibles Omnibus",
};

type DrawerForRoleProps = {
  role: VehicleRole;
  children: React.ReactNode;
  className?: string;
};

export const DrawerForRole = ({
  role,
  children,
  className,
}: DrawerForRoleProps) => {
  // TODO : redux access should not be in a UI component...
  const vehiclesByIdForThisRole = useSelector(
    latestVehicleEventByRawVehicleIdForRoleSelectors[role],
  );
  const vehiclesForThisRole = addSinceField(R.values(vehiclesByIdForThisRole));

  const countByAvailabilityKind = R.countBy(
    ({ availability_kind }) => availability_kind!,
    vehiclesForThisRole,
  ) as Partial<Record<AvailabilityKind, number>>;
  const vehiclesInService = vehiclesForThisRole.filter(
    ({ availability_kind }) => availability_kind !== "unavailable",
  );

  const textForAvailabilityKind = (availabilityKind: AvailabilityKind) => {
    const count = countByAvailabilityKind[availabilityKind];
    return count
      ? `${count} ${availabilityKindToLabel[availabilityKind]}`
      : undefined;
  };

  const inServiceDetails = [
    textForAvailabilityKind("available"),
    textForAvailabilityKind("on_operation"),
    textForAvailabilityKind("recoverable"),
    textForAvailabilityKind("unavailable_omnibus"),
  ]
    .filter((str) => str !== undefined)
    .join(", ");

  const unavailableText = textForAvailabilityKind("unavailable");

  const title = (
    <p>
      <span>
        {vehiclesInService.length} {vehiclesLabels.roles[role]} en service{" "}
      </span>
      <span className="text-sm font-light">
        ({inServiceDetails} {unavailableText && "- " + unavailableText})
      </span>
    </p>
  );

  return (
    <UiDrawerForRole
      data={vehiclesForThisRole}
      headers={vehicleTableHeaders}
      title={title}
      className={className}
    >
      {children}
    </UiDrawerForRole>
  );
};
