import Fab from "@material-ui/core/Fab";
import { ReactChild } from "react";

type UiFabProps = {
  children: ReactChild;
  onClick?: () => void;
  disabled?: boolean;
  style?: React.CSSProperties;
};

export const UiFab = ({
  children,
  onClick,
  disabled = false,
  style,
}: UiFabProps) => (
  <Fab
    size="small"
    color="secondary"
    onClick={onClick}
    disabled={disabled}
    style={style}
  >
    {children}
  </Fab>
);
