import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";

const useStyle = makeStyles({
  formControl: {
    margin: 5,
    minWidth: 200,
  },
});

type UiNumberSelectProps = {
  label: string;
  defaultNumber: number;
  onChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
};

export const UiNumberSelect = ({
  label,
  defaultNumber,
  onChange,
}: UiNumberSelectProps) => {
  const classes = useStyle();

  return (
    <FormControl className={classes.formControl}>
      <TextField
        label={label}
        type="number"
        variant="outlined"
        defaultValue={defaultNumber}
        onChange={onChange}
        margin="normal"
      />
    </FormControl>
  );
};
