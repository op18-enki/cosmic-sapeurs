import { makeStyles } from "@material-ui/core/styles";
import Drawer, { DrawerProps } from "@material-ui/core/Drawer";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";

type UiDrawerProps = {
  children: React.ReactNode;
  className?: string;
  content: React.ReactNode;
  position?: DrawerProps["anchor"];
  variant?: DrawerProps["variant"];
};

const navBarHeight = "48px";

const useStyles = makeStyles((theme) => ({
  drawer: {
    maxWidth: "80%",
    top: navBarHeight,
    height: `calc(100% - ${navBarHeight} - 28px)`,
  },
  unavailable: {
    color: theme.palette.grey[400],
  },
}));

export const UiDrawer = ({
  children,
  content,
  position = "right",
  variant = "temporary",
  className,
}: UiDrawerProps) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <>
      <div
        className={`cursor-pointer ${className ?? ""}`}
        onClick={() => setIsOpen(!isOpen)}
      >
        {children}
      </div>
      <Drawer
        variant={variant}
        anchor={position}
        open={isOpen}
        onClose={() => setIsOpen(false)}
        classes={{
          paperAnchorRight: classes.drawer,
        }}
      >
        {variant === "persistent" && (
          <div className="w-full mr-2">
            <CloseIcon
              onClick={() => setIsOpen(false)}
              className="cursor-pointer float-right"
            />
          </div>
        )}
        {content}
      </Drawer>
    </>
  );
};
