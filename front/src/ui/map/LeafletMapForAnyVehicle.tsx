import { contrastedMapColors } from "app/theme";
import { Area, Availability } from "interfaces";
import { PathOptions } from "leaflet";
import * as R from "ramda";
import { useEffect, useState } from "react";
import { GeoJSON, MapContainer } from "react-leaflet";
import { areaGeoJSON } from "ui/map/getMapData";
import { GroupmentGeoJson } from "ui/map/GroupmentGeoJson";
import { LegendPosition, MapLegend } from "ui/map/MapLegend";
import { id, makeColorScale } from "utils";
import {
  commonStyle,
  onCountryHoverStyle,
  OnEachFeature,
  useWatchVariables,
} from "./LeafletMap";

type ScaleSize = "small" | "big";

const makeScaleAndColor = (scaleSize: ScaleSize) =>
  makeColorScale({
    colors: [
      contrastedMapColors.red,
      ...(scaleSize === "big" ? [contrastedMapColors.orange] : []),
      contrastedMapColors.green,
    ],
    max: scaleSize === "big" ? 2 : 1,
  });

const useScaleAndColor = (data: LeafletMapAvailabilityInputs) => {
  const [scaleAndColor, setScaleAndColor] = useState(
    makeScaleAndColor("small"),
  );

  useEffect(() => {
    const { LSL, LSO, ...rest } = data;
    const totals = R.values(rest).map((availability) => {
      const { unavailable, ...availabilityExceptUnavailable } =
        availability ?? {};
      return R.values(
        availabilityExceptUnavailable ?? ({} as Partial<Availability>),
      ).reduce((acc: number, v) => acc + (v ?? 0), 0);
    });
    const max = Math.max(...totals, 0);
    const scaleSize: ScaleSize = max > 1 ? "big" : "small";
    setScaleAndColor(makeScaleAndColor(scaleSize));
  }, [data]);

  return scaleAndColor;
};

type LeafletMapForAnyVehicleProps = {
  title: string;
  legendPosition: LegendPosition;
  data: LeafletMapAvailabilityInputs;
  makePopupText: OnMapHoverTextMakerWithAvailability;
  mapContainerStyle?: React.CSSProperties;
  mapZoom?: number;
  titleColor?: string;
};

export const LeafletMapForAnyVehicle = ({
  title,
  data,
  makePopupText,
  legendPosition,
  titleColor,
  mapContainerStyle = { height: "440px", width: "500px" },
  mapZoom = 10,
}: LeafletMapForAnyVehicleProps) => {
  const { getColor, scale } = useScaleAndColor(data);
  const key = useWatchVariables(data, getColor);

  return (
    <div className="relative">
      <MapLegend
        colors={scale}
        title={title}
        position={legendPosition}
        titleColor={titleColor}
      />
      <MapContainer
        style={mapContainerStyle}
        zoom={mapZoom}
        minZoom={mapZoom}
        maxZoom={mapZoom}
        scrollWheelZoom={false}
        touchZoom={false}
        zoomControl={false}
        center={[48.88, 2.4]}
        dragging={false}
        attributionControl={false}
      >
        <GeoJSON
          key={key}
          style={commonStyle}
          data={areaGeoJSON.features}
          onEachFeature={makeOnAreaForData({
            data,
            getColor: getColor,
            makePopupText,
          })}
        />
        <GroupmentGeoJson key={"gpt" + key} />
      </MapContainer>
    </div>
  );
};

type LeafletMapAvailabilityInputs = Partial<Record<Area, Availability>>;

export type OnMapHoverTextMakerWithAvailability = (props: {
  area: string;
  availability?: Availability;
}) => string;

type MakeOnAreaForDataProps = {
  data: LeafletMapAvailabilityInputs;
  getColor: (n?: number) => string;
  makePopupText: OnMapHoverTextMakerWithAvailability;
};

const makeOnAreaForData =
  ({ data, getColor, makePopupText }: MakeOnAreaForDataProps): OnEachFeature =>
  (feature, layer): void => {
    const area: Area = (feature as any).code;
    const availability = data[area];
    const popUpText = makePopupText({ area, availability });
    const fillColor = getColor(
      availability ? availability.available ?? 0 : undefined,
    );
    let lineColor = commonStyle.color!;
    let weight = commonStyle.weight!;

    layer.bindPopup(popUpText, {
      closeButton: false,
    });

    (layer as any).options.fillColor = fillColor;
    (layer as any).options.color = lineColor;
    (layer as any).options.weight = weight;

    layer.on({
      mouseover: (e) => {
        e.target.setStyle(onCountryHoverStyle);
        layer.togglePopup();
      },
      mouseout: (e) => {
        e.target.setStyle(
          id<PathOptions>({
            ...commonStyle,
            fillColor,
            weight,
            color: lineColor,
          }),
        );
        layer.togglePopup();
      },
    });
  };
