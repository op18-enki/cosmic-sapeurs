import * as R from "ramda";

export type LegendPosition = "top" | "bottom";

type MapLegendProps = {
  title: string;
  colors: {
    color: string;
    label: string;
  }[];
  position: LegendPosition;
  titleColor?: string;
};

export const MapLegend = ({
  colors,
  title,
  position,
  titleColor,
}: MapLegendProps) => {
  const renderTitle = () => (
    <div
      className="w-36"
      style={{ color: titleColor ?? R.last(colors)!.color }}
    >
      {title}
    </div>
  );
  const positionStyle =
    position === "top" ? "left-10 top-10" : "left-10 bottom-2";
  return (
    <div
      className={`flex flex-col text-sm font-normal leading-normal text-gray-400 absolute z-10 ${positionStyle}`}
    >
      {position === "top" && renderTitle()}
      <div>
        <ul className="flex m-0 p-0">
          {colors.map(({ color, label }) => (
            <li
              key={color}
              className={"flex flex-col items-center content-end"}
            >
              <div
                className={"border border-gray-400 h-6 w-6"}
                style={{ backgroundColor: color }}
              />
              <div className={"text-sm"}>{label}</div>
            </li>
          ))}
        </ul>
      </div>
      {position === "bottom" && renderTitle()}
    </div>
  );
};
