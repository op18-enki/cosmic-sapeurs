import { omnibusBothAvailableColor } from "app/theme";
import type {
  CoverOpsOperationCause,
  CoverOpsVehicleSubCategory,
} from "core-logic";
import type { Area, OmnibusBalance } from "interfaces";
import type { PathOptions } from "leaflet";
import "leaflet/dist/leaflet.css";
import { useEffect, useState } from "react";
import { GeoJSON, MapContainer } from "react-leaflet";
import { GroupmentGeoJson } from "ui/map/GroupmentGeoJson";

import { id, Scale } from "utils";
import { areaGeoJSON } from "./getMapData";
import "./LeafletMap.css";

import { LegendPosition, MapLegend } from "./MapLegend";

export type AreaCounts = {
  operations: Record<CoverOpsOperationCause, number | undefined>;
  availabilities: Record<CoverOpsVehicleSubCategory, number | undefined>;
  omnibus: OmnibusBalance;
};

export type LeafletMapInputs = Partial<Record<Area, AreaCounts>>;

export type OnMapHoverTextMaker = (props: {
  area: string;
  areaCounts?: AreaCounts;
}) => string;

export type GetColorForArea = (areaCounts?: AreaCounts) => string;

export type OmnibusDisplayOption =
  | "greenLines"
  | "includeWithOthers"
  | "notShown";

export type MapProps = {
  data: LeafletMapInputs;
  scale: Scale;
  onHoverTextMaker: OnMapHoverTextMaker;
  getColorForArea: GetColorForArea;
  title: string;
  titleColor?: string;
  legendPosition: LegendPosition;
  omnibusDisplay: OmnibusDisplayOption;
  omnibusColor?: string;
  mapContainerStyle?: React.CSSProperties;
  mapZoom?: number;
};

export const commonStyle: PathOptions = {
  fillOpacity: 1,
  color: "grey",
  weight: 0.5,
};

export const onCountryHoverStyle: PathOptions = {
  weight: 2,
};

export type OnEachFeature = NonNullable<
  React.ComponentProps<typeof GeoJSON>["onEachFeature"]
>;

export const useWatchVariables = (...watched: unknown[]) => {
  const [dataChangeCount, setDataChangedCount] = useState(0);

  useEffect(() => {
    setDataChangedCount(dataChangeCount + 1);
  }, watched);

  return dataChangeCount;
};

const makeOnAreaForData =
  (
    data: LeafletMapInputs,
    getColorForArea: GetColorForArea,
    makePopupText: OnMapHoverTextMaker,
    omnibusColor: string,
    omnibusDisplay: OmnibusDisplayOption,
  ): OnEachFeature =>
  (feature, layer): void => {
    const area: Area = (feature as any).code;
    const areaCounts = data[area];
    const popUpText = makePopupText({ area, areaCounts });
    const fillColor = getColorForArea(areaCounts);
    let lineColor = commonStyle.color!;
    let weight = commonStyle.weight!;

    layer.bindPopup(popUpText, {
      closeButton: false,
    });

    (layer as any).options.fillColor = fillColor;

    if (omnibusDisplay === "greenLines") {
      if (areaCounts?.omnibus.both_available) {
        lineColor = omnibusColor;
        weight = 3;
      }

      (layer as any).options.color = lineColor;
      (layer as any).options.weight = weight;
    }

    (layer as any).options.color = lineColor;
    (layer as any).options.weight = weight;

    layer.on({
      mouseover: (e) => {
        e.target.setStyle(onCountryHoverStyle);
        layer.togglePopup();
      },
      mouseout: (e) => {
        e.target.setStyle(
          id<PathOptions>({
            ...commonStyle,
            fillColor,
            weight,
            color: lineColor,
          }),
        );
        layer.togglePopup();
      },
    });
  };

export const LeafletMap = ({
  data,
  scale,
  onHoverTextMaker,
  getColorForArea,
  title,
  titleColor,
  legendPosition,
  omnibusDisplay,
  omnibusColor = omnibusBothAvailableColor,
  mapContainerStyle = { height: "440px", width: "500px" },
  mapZoom = 10,
}: MapProps) => {
  const key = useWatchVariables(data, getColorForArea, omnibusDisplay);

  return (
    <div className="relative -mx-10">
      <MapLegend
        colors={scale}
        title={title}
        position={legendPosition}
        titleColor={titleColor}
      />
      <MapContainer
        style={mapContainerStyle}
        zoom={mapZoom}
        minZoom={mapZoom}
        maxZoom={mapZoom}
        scrollWheelZoom={false}
        touchZoom={false}
        zoomControl={false}
        center={[48.88, 2.4]}
        dragging={false}
        attributionControl={false}
      >
        <GeoJSON
          key={key}
          style={commonStyle}
          data={areaGeoJSON.features}
          onEachFeature={makeOnAreaForData(
            data,
            getColorForArea,
            onHoverTextMaker,
            omnibusColor,
            omnibusDisplay,
          )}
        />
        <GroupmentGeoJson key={"gpt" + key} />
      </MapContainer>
    </div>
  );
};
