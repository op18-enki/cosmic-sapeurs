import { gptGeoJSON } from "ui/map/getMapData";
import { GeoJSON } from "react-leaflet";

export const GroupmentGeoJson = () => (
  <GeoJSON
    style={{
      color: "black",
      weight: 2,
      fill: false,
      opacity: 1,
    }}
    data={gptGeoJSON.features}
  />
);
