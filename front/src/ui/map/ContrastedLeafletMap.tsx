import { vehicleToLabel } from "app/pages/CoverOps/LineAndBar";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { onMapHoverAvailability } from "app/pages/CoverOps/onMapHoverAvailability";
import { CoverOpsVehicleSubCategory } from "core-logic";
import { LeafletMap, LeafletMapInputs } from "ui/index";
import type { OmnibusDisplayOption } from "ui/map/LeafletMap";
import { LegendPosition } from "ui/map/MapLegend";

type ContrastedLeafletMapProps = {
  data: LeafletMapInputs;
  omnibusDisplay: OmnibusDisplayOption;
  colors: ReturnType<typeof makeMapColorAndScale>;
  subCategory: CoverOpsVehicleSubCategory;
  titleColor: string;
  legendPosition: LegendPosition;
  mapContainerStyle?: React.CSSProperties;
  mapZoom?: number;
};

export const ContrastedLeafletMap = ({
  omnibusDisplay,
  data,
  colors,
  subCategory,
  titleColor,
  legendPosition,
  mapContainerStyle,
  mapZoom,
}: ContrastedLeafletMapProps) => (
  <LeafletMap
    data={data}
    onHoverTextMaker={onMapHoverAvailability}
    getColorForArea={colors.getColorForArea}
    scale={colors.scale}
    title={`Nbr de ${vehicleToLabel[subCategory]} disponibles par cstc`}
    titleColor={titleColor}
    legendPosition={legendPosition}
    omnibusDisplay={omnibusDisplay}
    omnibusColor="#53a611"
    mapContainerStyle={mapContainerStyle}
    mapZoom={mapZoom}
  />
);
