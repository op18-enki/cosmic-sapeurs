import { useEffect, useRef } from "react";
import Chart from "chart.js";

export const useChart = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const chartRef = useRef<Chart>();

  return {
    chartRef,
    canvasRef,
  };
};

export const useUpdateChartOnDataChange = (
  chartRef: React.MutableRefObject<Chart | undefined>,
  data: Chart.ChartData,
) => {
  useEffect(() => {
    if (chartRef.current) {
      chartRef.current.data = data;
      chartRef.current.update();
    }
  }, [data]);
};
