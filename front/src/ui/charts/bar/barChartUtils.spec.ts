import { inputDataToBarChartJSData } from "./barChartUtils";
import type { BarChartJSData, ColorsBy } from "./barChartUtils";
import type {
  AvailabilityKind,
  AvailabilityKindWithoutOmnibusUnavailable,
} from "interfaces";
import { availabilityKindOptionsWithoutUnavailable } from "interfaces";

const commonColorsAndLabels = {
  unavailable: { color: "lightgray", label: "indisponible" },
  recoverable: { color: "gray", label: "récupérable" },
};
const colorByAvailability: ColorsBy<AvailabilityKindWithoutOmnibusUnavailable> =
  {
    ...commonColorsAndLabels,
    available: { color: "green", label: "disponible" },
    on_operation: { color: "lightgreen", label: "en service" },
  };

describe("Bar Chart Utils", () => {
  describe("Convert input data sample to Chart.js data", () => {
    it("Returns empty datasets, when no data and empty role and count options", () => {
      expectConvertedDataToMatch(
        inputDataToBarChartJSData({
          data: {},
          variableOptions: [],
          colorByVariable: {},
          barLabel: "foo",
        }),
        {
          datasets: [],
        },
      );
    });

    it("Returns empty datasets, when no data but role and count options", () => {
      expectConvertedDataToMatch(
        inputDataToBarChartJSData({
          data: {},
          variableOptions: availabilityKindOptionsWithoutUnavailable,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),
        {
          datasets: [
            { variable: "available", data: [null] },
            { variable: "on_operation", data: [null] },
            { variable: "recoverable", data: [null] },
          ],
        },
      );
    });

    it("Returns correct format, when one data sample is given with colors", () => {
      expectConvertedDataToMatch(
        inputDataToBarChartJSData({
          data: { available: 10, recoverable: 4, on_operation: 90 },
          variableOptions: availabilityKindOptionsWithoutUnavailable,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),

        {
          datasets: [
            {
              variable: "available",
              data: [10],
              backgroundColor: "green",
            },
            {
              variable: "on_operation",
              data: [90],
              backgroundColor: "lightgreen",
            },
            {
              variable: "recoverable",
              data: [4],
              backgroundColor: "gray",
            },
          ],
          labels: ["foo"],
        },
      );
    });

    it("Returns correct format, when multiple data sample are given with colors", () => {
      expectConvertedDataToMatch(
        inputDataToBarChartJSData({
          data: {
            available: 10,
            recoverable: 4,
            on_operation: 90,
          },
          variableOptions: availabilityKindOptionsWithoutUnavailable,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),
        {
          datasets: [
            {
              variable: "available",
              data: [10],
              backgroundColor: "green",
            },
            {
              variable: "on_operation",
              data: [90],
              backgroundColor: "lightgreen",
            },
            {
              variable: "recoverable",
              data: [4],
              backgroundColor: "gray",
            },
          ],
          labels: ["foo"],
        },
      );
    });
  });

  const expectConvertedDataToMatch = (
    actual: BarChartJSData<AvailabilityKind>,
    expected: BarChartJSData<AvailabilityKind>,
  ) => {
    expect(actual).toMatchObject(expected);
  };
});
