import { ChartDataSets } from "chart.js";
import { draw } from "patternomaly";
import * as R from "ramda";

export type BarInput<T extends string> = Partial<Record<T, number>>;

type ColorAndStripes = { color: string; stripes?: string };

export type ColorsBy<K extends string> = Record<
  K,
  | (ColorAndStripes & { label: string; strongLabel?: string })
  | (ColorAndStripes & { label?: string; strongLabel: string })
>;

export type BarChartJSData<V extends string> = {
  labels?: string[];
  datasets: Array<ChartDataSets & { variable: V; data: (number | null)[] }>;
};

type InputDataToBarChartJSDataProps<T extends string> = {
  data: BarInput<T>;
  variableOptions: T[];
  colorByVariable: ColorsBy<T>;
  barLabel: string;
};

const colorWithStripes = (color: string, stripesColor: string) =>
  draw("diagonal-right-left", color, stripesColor, 8);

export const inputDataToBarChartJSData = <T extends string>({
  variableOptions,
  data: inputSample,
  colorByVariable,
  barLabel,
}: InputDataToBarChartJSDataProps<T>): BarChartJSData<T> => {
  const getBackgroundColor = (kind: T) => {
    const { color, stripes } = colorByVariable[kind];
    return stripes ? colorWithStripes(color, stripes) : color;
  };

  return {
    datasets: variableOptions.map((variable) => ({
      variable: variable,
      data: [inputSample[variable] ?? null],
      backgroundColor: getBackgroundColor(variable),
      borderRadius: 5,
      borderColor: "#303030",
      borderWidth: 1,
    })),
    labels: [barLabel],
  };
};

export type HorizontalBarChartJSData = {
  labels?: string[];
  datasets: [
    {
      data: Array<number | null>;
      backgroundColor: Array<string | CanvasPattern>;
      borderColor: string;
      borderRadius: number;
      borderWidth: number;
    },
  ];
};

export const inputDataToHorizontalBarChartJSData = <T extends string>({
  data: inputSample,
  colorByVariable,
}: InputDataToBarChartJSDataProps<T>): HorizontalBarChartJSData => {
  const getBackgroundColor = (kind: T) => {
    const { color, stripes } = colorByVariable[kind];
    return stripes ? colorWithStripes(color, stripes) : color;
  };

  const variableOptions = R.keys(colorByVariable);

  const labels: string[] = Array(variableOptions.length).fill("");

  return {
    datasets: [
      {
        data: variableOptions.map((variable) => inputSample[variable] ?? null),
        backgroundColor: variableOptions.map(getBackgroundColor),
        borderRadius: 2,
        borderColor: "#303030",
        borderWidth: 1,
      },
    ],
    labels,
  };
};
