import { UiTooltip } from "ui";
import { LegendText } from "ui/charts/bar/LegendText";

export type WithLegendHover<T extends string> = {
  LegendHover: React.ComponentType<{
    label: string;
    variable: T;
  }>;
};

const makeStripesBackground = (
  color: string,
  stripesColor: string,
) => `repeating-linear-gradient(
  -45deg,
  ${color},
  ${color} 4px,
  ${stripesColor} 3px,
  ${stripesColor} 6px
)`;

// Legend Element :
type LabelOrStrongLabel =
  | { label: string; strongLabel?: string }
  | { label?: string; strongLabel: string };

type LegendElementProps<T extends string> = WithLegendHover<T> &
  LabelOrStrongLabel & {
    color: string;
    stripes?: string;
    value?: number | null;
    variable: T;
  };

export const LegendElement = <T extends string>({
  color,
  stripes,
  label,
  strongLabel,
  value,
  variable,
  LegendHover,
}: LegendElementProps<T>) => {
  const background = stripes ? makeStripesBackground(color, stripes) : color;
  const fullLabel = `${label ? label : ""}${
    strongLabel ? " " + strongLabel : ""
  }`;

  return (
    <UiTooltip content={<LegendHover label={fullLabel} variable={variable} />}>
      <div className="flex font-light justify-between">
        <div className="flex items-center">
          <div
            className="h-5 w-5 border border-solid border-gray-700 mr-1 rounded"
            style={{ background }}
          />{" "}
          <LegendText
            label={label}
            strongLabel={strongLabel}
            value={value ?? "?"}
          />
        </div>
      </div>
    </UiTooltip>
  );
};
