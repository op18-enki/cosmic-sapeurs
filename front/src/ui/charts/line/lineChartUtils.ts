import { DateString, mutateLast, sortByTimestamp } from "utils";
import { ChartDataSets } from "chart.js";
import * as R from "ramda";
import { Dispatch, SetStateAction, useEffect, useState } from "react";

export type ChartJSDataset<V extends string> = ChartDataSets & {
  variable: V;
  data: (number | null)[];
};

export type ChartJSData<V extends string> = {
  labels: string[];
  datasets: ChartJSDataset<V>[];
};

export type LineInput<V extends string> = {
  timestamp: DateString;
  value: number | null;
  variable: V;
};

export type AxisPosition = "left" | "right";

export type LineStyleByVariable<V extends string> = Record<
  V,
  {
    color: string;
    // label?: string;
    axis: AxisPosition;
  }
>;

type DataByVariable<V extends string> = Record<V, (number | null)[]>;

type DatasAndLabels<V extends string> = {
  datas: DataByVariable<V>;
  labels: string[];
};

const getDataAndLabels = <V extends string>(
  inputs: LineInput<V>[],
  styleByVariable: LineStyleByVariable<V>,
): DatasAndLabels<V> => {
  const result: DatasAndLabels<V> = {
    datas: R.mapObjIndexed(() => [], styleByVariable),
    labels: [],
  };

  const variables = R.keys(styleByVariable);
  const filtered_inputs = inputs.filter((input) => {
    return variables.includes(input.variable);
  });

  filtered_inputs.forEach((input) => {
    if (result.labels[result.labels.length - 1] === input.timestamp) {
      mutateLast(result.datas[input.variable], input.value);
      return result;
    }

    variables.forEach((variable) => {
      const data = result.datas[variable];
      data.push(data[data.length - 1] ?? null);
    });
    mutateLast(result.datas[input.variable], input.value);
    result.labels.push(input.timestamp);
  });

  return result;
};

export const inputDataToChartJSData = <V extends string>(
  inputs: LineInput<V>[],
  styleByVariable: LineStyleByVariable<V>,
): ChartJSData<V> => {
  if (inputs.length === 0) return { datasets: [], labels: [] };

  const sortedInputs = sortByTimestamp(inputs, "asc");

  const { datas, labels } = getDataAndLabels(sortedInputs, styleByVariable);

  const datasets = R.keys(datas).map((variable) => {
    const style = styleByVariable[variable];

    return {
      data: datas[variable],
      variable,
      borderColor: style?.color,
      fill: false,
      yAxisID: styleByVariable[variable].axis,
    };
  });

  return {
    datasets,
    labels,
  };
};

export const useDebounce = <T>(
  value: T,
  callback: (...params: any[]) => void,
  delay: number,
) => {
  useEffect(
    () => {
      const timeoutRef = setTimeout(() => {
        callback(value);
      }, delay);

      return () => {
        clearTimeout(timeoutRef);
      };
    },
    [value, delay], // Only re-call effect if value or delay changes
  );
};

export const useStateWatching = <T>(v: T): [T, Dispatch<SetStateAction<T>>] => {
  const [state, setState] = useState(v);
  useEffect(() => setState(v), [v]);
  return [state, setState];
};
