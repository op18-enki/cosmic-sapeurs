import Switch from "@material-ui/core/Switch";
import { green } from "@material-ui/core/colors";
import { withStyles } from "@material-ui/core/styles";
import { omnibusBothAvailableColor } from "app/theme";

const GreenSwitch = withStyles({
  switchBase: {
    color: green[300],
    "&$checked": {
      color: omnibusBothAvailableColor,
    },
    "&$checked + $track": {
      backgroundColor: omnibusBothAvailableColor,
    },
  },
  checked: {},
  track: {},
})(Switch);

type UiSwitchProps = {
  checked: boolean;
  onChange: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean,
  ) => void;
};

type DifferentLabels = {
  checkedLabel?: string;
  notCheckedLabel?: string;
};

export const UiSwitchWithCustomLabels = ({
  checked,
  onChange,
  checkedLabel,
  notCheckedLabel,
}: UiSwitchProps & DifferentLabels) => (
  <div className="flex items-center">
    <div className={`text-sm font-light ${checked ? "invisible" : ""}`}>
      {notCheckedLabel}
    </div>
    <GreenSwitch checked={checked} onChange={onChange} color="primary" />
    {checked && (
      <div
        className={`text-sm font-light ${!checked ? "invisible" : ""}`}
        style={{ color: omnibusBothAvailableColor }}
      >
        {checkedLabel}
      </div>
    )}
  </div>
);

export const UiSwitch = ({
  checked,
  onChange,
  label,
}: UiSwitchProps & { label: string }) => (
  <div className="flex items-center">
    <Switch checked={checked} onChange={onChange} color="primary" />
    <div
      className={`text-sm ${
        checked ? "font-bold text-blue-900" : "font-light"
      }`}
    >
      {label}
    </div>
  </div>
);
