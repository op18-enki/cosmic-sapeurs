import SearchIcon from "@material-ui/icons/Search";
import { UiFab } from "ui/UiFab";

export const UiSearchButton = () => (
  <UiFab>
    <SearchIcon />
  </UiFab>
);
