import type { VehicleEvent } from "interfaces";
import type { WithTimestamp } from "utils";

type Extend<T extends string, K extends string> = K extends T ? K : never;
type VehicleInfos =
  | Extend<keyof VehicleEvent, "home_area" | "vehicle_name" | "raw_status">
  | "since";

export const vehicleTableHeaders: Record<VehicleInfos, string> & WithTimestamp =
  {
    vehicle_name: "Immatriculation",
    home_area: "Affectation",
    raw_status: "Statut",
    timestamp: "Heure",
    since: "Dernière maj",
  };
