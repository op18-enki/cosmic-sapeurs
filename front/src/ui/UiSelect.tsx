import { makeStyles, TextField } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import { Autocomplete } from "@material-ui/lab";

export type Option<T> = {
  value: T;
  label: string;
};

type UiSelectProps<T> = {
  onChange: (selected: string | Option<T> | null) => void;
  options: Option<T>[];
  label: string;
  initialValue?: T;
};

const useStyle = makeStyles({
  formControl: {
    margin: 5,
    minWidth: 200,
  },
});

export const UiSelect = <T extends unknown>({
  onChange,
  options,
  label,
  initialValue,
}: UiSelectProps<T>) => {
  const classes = useStyle();

  return (
    <FormControl className={classes.formControl}>
      <Autocomplete
        freeSolo
        options={options}
        defaultValue={options.find((option) => option.value === initialValue)}
        getOptionLabel={(option) => option.label ?? ""}
        onChange={(event, value) => {
          onChange(value);
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            margin="normal"
            variant="outlined"
            autoFocus
          />
        )}
      />
    </FormControl>
  );
};
