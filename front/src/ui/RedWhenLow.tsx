import * as R from "ramda";

type RedWhenLowProps = {
  children: React.ReactNode;
  value: number | undefined;
  fullWidth?: boolean;
};

export const isLow = (percentage?: number) =>
  !R.isNil(percentage) && percentage <= 33;

export const RedWhenLow = ({ children, value, fullWidth }: RedWhenLowProps) => {
  const redIfLow = isLow(value) ? "text-red-500 font-bold" : "";
  const fullWidthIfNeeded = fullWidth ? "w-full" : "";
  return <span className={`${redIfLow} ${fullWidthIfNeeded}`}>{children}</span>;
};
