import TextField from "@material-ui/core/TextField";

type UiDatepickerProps = {
  value: string;
  onChange: (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
};

export const UiDatepicker = ({ value, onChange }: UiDatepickerProps) => {
  return (
    <TextField
      id="date"
      variant="outlined"
      label="Début"
      type="date"
      value={value}
      onChange={onChange}
    />
  );
};
