import {
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { isBefore } from "date-fns";
import { AvailabilityKind } from "interfaces";
import * as R from "ramda";
import React from "react";
import { sortByTimestamp, WithTimestamp } from "utils";

const isNotUnavailable = <T extends WithAvailabilityKind>(element: T) =>
  element.availability_kind !== "unavailable";

const navBarHeight = "48px";

const useStyles = makeStyles((theme) => ({
  drawer: {
    top: navBarHeight,
    height: `calc(100% - ${navBarHeight} - 28px)`,
  },
  unavailable: {
    color: theme.palette.grey[400],
  },
}));

type WithAvailabilityKind = {
  availability_kind?: AvailabilityKind;
};

type VehicleDrawerInput<K extends string> = { [key in K]?: any } & // TODO : remove this any
  WithTimestamp &
  WithAvailabilityKind;

type UiDetailedVehicleTableProps<K extends string> = {
  data: VehicleDrawerInput<K>[];
  headers: VehicleDrawerInput<K>;
  title: React.ReactNode;
};

export const UiDetailedVehicleTable = <K extends string>({
  data,
  headers,
  title,
}: UiDetailedVehicleTableProps<K>) => {
  const classes = useStyles();
  const headersValues = R.values(headers);

  return (
    <>
      <div className="text-center p-2 bg-gray-50">{title}</div>
      <TableContainer component={Paper}>
        <Table stickyHeader size="small">
          <TableHead>
            <TableRow>
              {headersValues.map((value, index) => (
                <TableCell key={index} align="right">
                  {value}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {R.concat(
              ...R.partition(isNotUnavailable, sortByTimestamp(data, "desc")),
            ).map((row, index) => {
              const columnToDisplayForThisRow = isBefore(
                new Date(row.timestamp),
                new Date("2020"),
              )
                ? { ...row, timestamp: "", since: "" }
                : row;
              return (
                <TableRow key={index}>
                  {R.keys(headers).map((columnName) => {
                    return (
                      <TableCell
                        align="right"
                        key={columnName}
                        className={
                          row.availability_kind === "unavailable"
                            ? classes.unavailable
                            : ""
                        }
                      >
                        {columnToDisplayForThisRow[columnName]}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
