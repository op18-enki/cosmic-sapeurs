import { yellow } from "@material-ui/core/colors";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/core/styles";
import { coverStyles } from "app/theme";

const CoverColorsSwitch = withStyles({
  switchBase: {
    color: coverStyles.ep.mainColor,
    "&$checked": {
      color: coverStyles.sap.mainColor,
    },
    "&$checked + $track": {
      backgroundColor: coverStyles.sap.mainColor,
    },
  },
  checked: {},
  track: {
    backgroundColor: yellow["400"],
    border: "1px solid lightgrey",
  },
})(Switch);

type UiSwitchProps = {
  checked: boolean;
  onChange: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean,
  ) => void;
  upLabel?: string;
  downLabel?: string;
};

export const UiVerticalSwitch = ({
  checked,
  onChange,
  upLabel,
  downLabel,
}: UiSwitchProps) => (
  <div className="flex items-center">
    <div style={{ transform: "rotate(-90deg)" }}>
      <CoverColorsSwitch
        checked={checked}
        onChange={onChange}
        color="primary"
      />
    </div>
    <div
      style={{
        color: checked ? coverStyles.sap.mainColor : coverStyles.ep.mainColor,
      }}
    >
      {checked ? upLabel : downLabel}
    </div>
  </div>
);
