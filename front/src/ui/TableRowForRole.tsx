import { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { vehiclesLabels } from "core-logic";
import { VehicleRole } from "interfaces";
import { UiTooltip } from "ui/UiTooltip";

import * as R from "ramda";
import type { AvailabilityCellValue } from "core-logic";
import { RedWhenLow } from "./RedWhenLow";
import { DrawerForRole } from "./DrawerForRole";

type TableRowForRoleProps = {
  valuesByColumn: Partial<Record<VehicleRole, AvailabilityCellValue>>;
};

const ValueDisplay = ({
  value,
  inService,
  theoretical,
}: {
  value?: number | string;
  inService?: number | string;
  theoretical?: number | string;
}) => (
  <>
    {value} / {inService}
    {!R.isNil(theoretical) && (
      <sup className="font-light">{` (${theoretical})`}</sup>
    )}
  </>
);

export const TableRowForRole = ({ valuesByColumn }: TableRowForRoleProps) => {
  const valuesByColumnNotPartial = valuesByColumn as Record<
    VehicleRole,
    AvailabilityCellValue
  >;

  const roles = R.keys(valuesByColumnNotPartial);
  const fullWidthIfUniq = (roles: unknown[]) =>
    roles.length === 1 ? " w-full" : "";

  return (
    <TableContainer component={Paper}>
      <div className="flex">
        {roles.map((role) => {
          const cellValue = valuesByColumnNotPartial[role];
          const { percentage } = cellValue;
          const fullWidthIfNeeded = fullWidthIfUniq(roles);

          return (
            <DrawerForRole key={role} role={role} className={fullWidthIfNeeded}>
              <div
                key={role}
                className={"hover:opacity-60" + fullWidthIfNeeded}
              >
                <Table
                  className="w-auto"
                  size="small"
                  aria-label="a dense table"
                >
                  <TableHead className="bg-gray-300 w-full">
                    <TableHeadCell cellValue={cellValue} key={role} role={role}>
                      {vehiclesLabels.roles[role]}
                    </TableHeadCell>
                  </TableHead>
                  <TableBody className="w-full">
                    <TableRow>
                      <TableCell align="center">
                        <UiTooltip
                          placement="bottom"
                          delay={800}
                          content={
                            <ValueDisplay
                              value="disponible"
                              inService="en service"
                              theoretical="théorique"
                            />
                          }
                        >
                          <RedWhenLow value={percentage}>
                            <ValueDisplay {...cellValue} />
                          </RedWhenLow>
                        </UiTooltip>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center">
                        <UiTooltip
                          delay={500}
                          placement="bottom"
                          content="pourcentage par rapport au théorique"
                        >
                          <RedWhenLow value={percentage}>
                            {R.isNil(percentage) ? "-" : `${percentage}%`}
                          </RedWhenLow>
                        </UiTooltip>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </div>
            </DrawerForRole>
          );
        })}
      </div>
    </TableContainer>
  );
};

type ArrowProps = {
  visible?: boolean;
};

const Increased = ({ visible = false }: ArrowProps) => (
  <div
    className="w-0 h-0 bg-fixed border-l-8 border-r-8 border-transparent"
    style={{
      visibility: visible ? "visible" : "hidden",
      borderBottom: "8px solid green",
    }}
  />
);

const Decreased = ({ visible = false }: ArrowProps) => (
  <div
    className="w-0 h-0 bg-fixed border-l-8 border-r-8 border-transparent"
    style={{
      visibility: visible ? "visible" : "hidden",
      borderTop: "8px solid red",
    }}
  />
);

type TableHeadCellProps = {
  children: React.ReactNode;
  cellValue: AvailabilityCellValue;
  role: VehicleRole;
  className?: string;
};

const TableHeadCell = ({
  children,
  cellValue,
  role,
  className,
}: TableHeadCellProps) => {
  const [moving, setMoving] = useState<"up" | "down" | null>(null);
  const [lastCellValue, setLastCellValue] =
    useState<AvailabilityCellValue>(cellValue);

  useEffect(() => {
    if (!cellValue.value || cellValue.value === lastCellValue.value) {
    } else if (!lastCellValue.value) {
      setMoving("up");
    } else {
      setMoving(cellValue.value! > lastCellValue.value! ? "up" : "down");
    }
    setLastCellValue(cellValue);

    const timer = setTimeout(() => setMoving(null), 8000);

    return () => clearTimeout(timer);
  }, [cellValue.percentage, cellValue.inService, cellValue.value]);

  return (
    <TableCell
      align="center"
      style={{ padding: "2px 24px" }}
      className={className}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Increased visible={moving === "up"} />
        <RedWhenLow value={cellValue.percentage}>{children}</RedWhenLow>
        <Decreased visible={moving === "down"} />
      </div>
    </TableCell>
  );
};
