import { AvailabilityKind } from "interfaces";
import { UiDrawer } from "ui/UiDrawer";
import { WithTimestamp } from "utils";
import { UiDetailedVehicleTable } from "ui/UiDetailedVehicleTable";

type WithAvailabilityKind = {
  availability_kind?: AvailabilityKind;
};

type VehicleDrawerInput<K extends string> = { [key in K]?: any } & // TODO : remove this any
  WithTimestamp &
  WithAvailabilityKind;

type UiDrawerProps<K extends string> = {
  children: React.ReactNode;
  data: VehicleDrawerInput<K>[];
  headers: VehicleDrawerInput<K>;
  title: React.ReactNode;
  className?: string;
};

export const UiDrawerForRole = <K extends string>({
  children,
  className,
  ...contentProps
}: UiDrawerProps<K>) => {
  return (
    <>
      <UiDrawer
        className={className}
        content={<UiDetailedVehicleTable {...contentProps} />}
      >
        {children}
      </UiDrawer>
    </>
  );
};
