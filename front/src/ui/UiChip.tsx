import Chip from "@material-ui/core/Chip";

type UiChipProps = {
  label: string;
  onDelete: () => void;
  color?: string;
};

const getExtraProps = (color?: string) => {
  switch (color) {
    case undefined:
      return { color: "primary" as const };
    case "white":
      return { variant: "outlined" as const };
    default:
      return { style: { backgroundColor: color } };
  }
};

export const UiChip = ({ label, onDelete, color }: UiChipProps) => (
  <Chip label={label} onDelete={onDelete} {...getExtraProps(color)} />
);
