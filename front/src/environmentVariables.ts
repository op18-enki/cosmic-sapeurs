import { throwIfNotInArray, throwIfVariableUndefined } from "utils";

const processEnv = process.env;

const httpClientKind = throwIfNotInArray({
  processEnv,
  authorizedValues: ["IN_MEMORY", "HTTP"],
  variableName: "REACT_APP_HTTP_CLIENT",
});

const pubSubClientKind = throwIfNotInArray({
  processEnv,
  authorizedValues: ["IN_MEMORY", "WEB_SOCKET"],
  variableName: "REACT_APP_PUBSUB_CLIENT",
});

const mapDataOrigin = throwIfNotInArray({
  processEnv,
  authorizedValues: ["BSPP", "ARRONDISSEMENTS_PARIS"],
  variableName: "REACT_APP_MAP_DATA_ORIGIN",
});

const backendHost =
  processEnv.NODE_ENV === "production" || processEnv.NODE_ENV === "test"
    ? window.location.host
    : "localhost:8080";

const mapTilesUrl = throwIfVariableUndefined({
  processEnv,
  variableName: "REACT_APP_MAP_TILES_URL",
});

export const ENV = {
  node_env: process.env.NODE_ENV,
  logActionsDuration: process.env.REACT_APP_LOG_ACTION_DURATION === "TRUE",
  backendHost,
  httpClientKind,
  pubSubClientKind,
  mapDataOrigin,
  mapTilesUrl,
};

console.log("ENV :", JSON.stringify(ENV, null, 2));
