import type { VehicleSubCategory } from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";

export type CoverOpsVehicleSubCategory =
  | Extract<VehicleSubCategory, "ep" | "sap">
  | "epWithOmni"
  | "sapWithOmni";
