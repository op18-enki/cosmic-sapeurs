import { createSelector } from "@reduxjs/toolkit";
import { AvailabilityKind, VehicleRole } from "interfaces";
import * as R from "ramda";
import {
  rolesPerCoverOpsSubCategory,
  vehiclesLabels,
} from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
import { availabilityKindOptionsWithoutOmnibus } from "interfaces";
import { availabilityChangedEventsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { AvailabilityChangedEventData } from "interfaces/generated/AvailabilityChangedEventData";
import { VehicleEventData } from "interfaces/generated/VehicleEventData";

const getLastByRawVehicleId = R.pipe<
  AvailabilityChangedEventData[],
  Record<string, AvailabilityChangedEventData[]>,
  Record<string, AvailabilityChangedEventData>
>(
  R.groupBy(R.prop("raw_vehicle_id")),
  R.mapObjIndexed((events) => R.last(events)!),
);

export const availabilityChangedEventsByIdSelector = createSelector(
  availabilityChangedEventsSelector,
  getLastByRawVehicleId,
);

const addLastVehicleEventForRole = (role: VehicleRole) =>
  R.pipe<
    Record<string, AvailabilityChangedEventData>,
    Record<string, AvailabilityChangedEventData>,
    Record<string, VehicleEventData>
  >(
    R.filter<AvailabilityChangedEventData>(
      (availabilityChangedEvent) => availabilityChangedEvent.role === role,
    ),
    R.mapObjIndexed((availabilityEvent) => ({
      ...availabilityEvent.latest_event_data,
    })),
  );

const makeVehicleEventByRawVehicleIdForRoleSelector = (role: VehicleRole) => {
  return createSelector(
    availabilityChangedEventsByIdSelector,
    addLastVehicleEventForRole(role),
  );
};

const makeLatestVehicleEventByRawVehicleIdForSubCategorySelector = (
  subCategory: CoverOpsVehicleSubCategory,
) => {
  return createSelector(
    availabilityChangedEventsByIdSelector,
    (latestAvailabilityChangedEventsById) => {
      return rolesPerCoverOpsSubCategory[subCategory].flatMap((role) => {
        return R.pipe<
          Record<string, AvailabilityChangedEventData>,
          Record<string, AvailabilityChangedEventData>,
          AvailabilityChangedEventData[]
        >(
          R.filter<AvailabilityChangedEventData>(
            (availabilityChangedEvent) =>
              availabilityChangedEvent.role === role,
          ),
          R.values,
        )(latestAvailabilityChangedEventsById);
      });
    },
  );
};

export const latestVehicleEventByRawVehicleIdForRoleSelectors = R.keys(
  vehiclesLabels.roles,
).reduce((acc, role) => {
  return {
    ...acc,
    [role]: makeVehicleEventByRawVehicleIdForRoleSelector(role),
  };
}, {} as Record<VehicleRole, ReturnType<typeof makeVehicleEventByRawVehicleIdForRoleSelector>>);

export const latestVehicleEventByRawVehicleIdForCoverOpsSubCategorySelectors =
  R.keys(rolesPerCoverOpsSubCategory).reduce((acc, subCategory) => {
    return {
      ...acc,
      [subCategory]:
        makeLatestVehicleEventByRawVehicleIdForSubCategorySelector(subCategory),
    };
  }, {} as Record<CoverOpsVehicleSubCategory, ReturnType<typeof makeLatestVehicleEventByRawVehicleIdForSubCategorySelector>>);

const groupByIdForAvailabilityKind = (availabilityKind: AvailabilityKind) =>
  R.pipe<
    AvailabilityChangedEventData[],
    AvailabilityChangedEventData[],
    Record<string, AvailabilityChangedEventData[]>
  >(
    R.filter<AvailabilityChangedEventData>(
      ({ latest_event_data }) =>
        latest_event_data.availability_kind === availabilityKind,
    ),
    R.groupBy(({ latest_event_data }) => latest_event_data.raw_status),
  );

export const makeCountByStatusForSubCategoryForAvailabilityKind = (
  subCategory: CoverOpsVehicleSubCategory,
  availabilityKind: AvailabilityKind,
) => {
  return createSelector(
    latestVehicleEventByRawVehicleIdForCoverOpsSubCategorySelectors[
      subCategory
    ],
    groupByIdForAvailabilityKind(availabilityKind),
  );
};

const makeSubCategoryToCountByStatusByAvailabilityKind = (
  subCategory: CoverOpsVehicleSubCategory,
) =>
  availabilityKindOptionsWithoutOmnibus.reduce((acc, availabilityKind) => {
    return {
      ...acc,
      [availabilityKind]: makeCountByStatusForSubCategoryForAvailabilityKind(
        subCategory,
        availabilityKind,
      ),
    };
  }, {} as Record<AvailabilityKind, ReturnType<typeof makeCountByStatusForSubCategoryForAvailabilityKind>>);

export const availabilityEventByIdByAvailabilityKindByBySubCategory: Record<
  CoverOpsVehicleSubCategory,
  ReturnType<typeof makeSubCategoryToCountByStatusByAvailabilityKind>
> = {
  ep: makeSubCategoryToCountByStatusByAvailabilityKind("ep"),
  epWithOmni: makeSubCategoryToCountByStatusByAvailabilityKind("epWithOmni"),
  sap: makeSubCategoryToCountByStatusByAvailabilityKind("sap"),
  sapWithOmni: makeSubCategoryToCountByStatusByAvailabilityKind("sapWithOmni"),
};
