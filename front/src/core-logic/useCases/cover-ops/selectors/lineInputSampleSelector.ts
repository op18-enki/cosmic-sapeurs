import { createSelector } from "@reduxjs/toolkit";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import { toLastEventByOperationId } from "core-logic/useCases/cover-ops/selectors/lastOnGoingOpEventByOperationIdSelector";
import type { AvailabilityChangedEvent } from "interfaces";
import { OperationCause, OperationRelevantStatus } from "interfaces";
import * as R from "ramda";
import { LineInput } from "ui";
import { sortByTimestamp } from "utils";
import {
  fireLineInputsFromHistorySelector,
  ongoingOpChangedEventsSelector,
  takeOmnibusIntoAccountSelector,
  victimLineInputsFromHistorySelector,
} from "./intermediate/rootSelectors";

type EpAndSapLineInputSample = LineInput<"ep" | "sap">;

type ToEPAndSAPLineInput = (
  takeOmnibusIntoAccount: boolean,
  availabilityChangedEventBySubCategory: Record<
    CoverOpsVehicleSubCategory,
    AvailabilityChangedEvent[]
  >,
) => {
  ep: EpAndSapLineInputSample[];
  sap: EpAndSapLineInputSample[];
};

const toEpAndSapLineInput: ToEPAndSAPLineInput = (
  takeOmnibusIntoAccount,
  availabilityChangedEventBySubCategory,
) =>
  R.pipe<
    Record<CoverOpsVehicleSubCategory, AvailabilityChangedEvent[]>,
    CoverOpsVehicleSubCategory[],
    CoverOpsVehicleSubCategory[],
    Record<"ep" | "sap", EpAndSapLineInputSample[]>
  >(
    (obj) => R.keys(obj),
    R.filter<CoverOpsVehicleSubCategory>((subCategory) =>
      takeOmnibusIntoAccount
        ? ["epWithOmni", "sapWithOmni"].includes(subCategory)
        : ["ep", "sap"].includes(subCategory),
    ),
    R.reduce((acc, subCategory) => {
      const subCategoryRenamed = subCategory.includes("ep") ? "ep" : "sap";

      const events = availabilityChangedEventBySubCategory[subCategory];

      return {
        ...acc,
        [subCategoryRenamed]: events.map((event) => ({
          timestamp: event.timestamp,
          value: event.bspp_availability.available ?? null,
          variable: subCategoryRenamed,
        })),
      };
    }, {} as Record<"ep" | "sap", EpAndSapLineInputSample[]>),
  )(availabilityChangedEventBySubCategory);

export const availabilityToLineInputSelector = createSelector(
  takeOmnibusIntoAccountSelector,
  availabilityChangedEventsByCoverOpsSubCategorySelector,
  toEpAndSapLineInput,
);

const operationOngoingStatus: OperationRelevantStatus[] = [
  "first_vehicle_affected",
  "some_affected_vehicle_changed",
];

type ExtractFromExisting<T extends string, K extends T> = Extract<T, K>;
type FireOrVictim = ExtractFromExisting<OperationCause, "fire" | "victim">;

export const makeOngoingOpToLineInputSelector = <T extends FireOrVictim>(
  cause: T,
) =>
  createSelector(ongoingOpChangedEventsSelector, (ongoingOpChangedEvents) => {
    const causeEvents = ongoingOpChangedEvents.filter(
      (event) => event.opening_infos.cause === cause,
    );

    const causeLineInputs = causeEvents.map<LineInput<T>>(({ timestamp }) => {
      const lastEventsByOperationIdBeforeCurrent = toLastEventByOperationId(
        causeEvents.filter((event) => event.timestamp <= timestamp),
      );

      return {
        timestamp,
        variable: cause,
        value: lastEventsByOperationIdBeforeCurrent.filter((event) =>
          operationOngoingStatus.includes(
            event.status as OperationRelevantStatus,
          ),
        ).length,
      };
    });

    return sortByTimestamp(causeLineInputs, "asc");
  });

const victimLineInputFromEventsSelector = createSelector(
  availabilityToLineInputSelector,
  makeOngoingOpToLineInputSelector("victim"),
  ({ sap }, victimOngoingOpLineInput) => [
    ...(sap as LineInput<"sap">[]),
    ...victimOngoingOpLineInput,
  ],
);

export const victimLineInputSelector = createSelector(
  victimLineInputsFromHistorySelector,
  victimLineInputFromEventsSelector,
  (fromHistory, fromEvents) => [...fromHistory, ...fromEvents],
);

const fireLineInputsFromEventsSelector = createSelector(
  availabilityToLineInputSelector,
  makeOngoingOpToLineInputSelector("fire"),
  ({ ep }, fireOngoingOpLineInput) => [
    ...(ep as LineInput<"ep">[]),
    ...fireOngoingOpLineInput,
  ],
);

export const fireLineInputSelector = createSelector(
  fireLineInputsFromHistorySelector,
  fireLineInputsFromEventsSelector,
  (fromHistory, fromEvents) => [...fromHistory, ...fromEvents],
);
