import { createSelector } from "@reduxjs/toolkit";
import type { AvailabilityChangedEvent } from "interfaces";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import * as R from "ramda";

export const lastAvailabilityChangedEventsByCoverOpsSubCategorySelector =
  createSelector(
    availabilityChangedEventsByCoverOpsSubCategorySelector,
    (
      availabilityChangedEventsByCoverOpsSubCategory,
    ): Partial<
      Record<CoverOpsVehicleSubCategory, AvailabilityChangedEvent>
    > => {
      return R.mapObjIndexed(
        // availabilityChangedEvents has already been sorted by previous selector (in asc order)
        (availabilityChangedEvents) => availabilityChangedEvents.pop()!,
        availabilityChangedEventsByCoverOpsSubCategory,
      );
    },
  );
