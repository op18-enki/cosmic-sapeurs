import type { RootState } from "core-logic/setup/root.reducer";

export const availabilityChangedEventsSelector = (state: RootState) =>
  state.cover.availabilityChangedEvents;

export const ongoingOpChangedEventsSelector = (state: RootState) =>
  state.ops.ongoingOpChangedEvents;

export const activeOpsFiltersSelector = (state: RootState) =>
  state.ops.activeFilters;

export const withVehicleOnlySelector = (state: RootState) =>
  state.ops.withVehicleOnly;

export const inuBlinkStartTimeSelector = (state: RootState) =>
  state.ops.inuBlinkStartTime;

export const omnibusDetailsSelector = (state: RootState) =>
  state.omnibus.details;

export const numberOfHoursToShowSelector = (state: RootState) =>
  state.interactions.numberOfHoursToShow;

export const takeOmnibusIntoAccountSelector = (state: RootState) =>
  state.interactions.takeOmnibusIntoAccount;

export const valorizationSelector = (state: RootState) =>
  state.interactions.valorization;

export const selectedAreaSelector = (state: RootState) =>
  state.cover.selectedArea;

export const selectedVehicleRolesSelector = (state: RootState) =>
  state.interactions.selectedVehicleRoles;

export const areVehicleLinkedSelector = (state: RootState) =>
  state.interactions.areVehicleLinked;

export const isLoadingDataSelector = (state: RootState) =>
  state.cover.isFetchingCover ||
  state.ops.isFetchingOps ||
  state.resetCoverOps.isResetting ||
  state.interactions.pubSubStatus === "tryingToReconnect";

export const isResetingCoverOpsSelector = (state: RootState) =>
  state.resetCoverOps.isResetting;

export const victimLineInputsFromHistorySelector = (state: RootState) =>
  state.linesHistory.victim;

export const fireLineInputsFromHistorySelector = (state: RootState) =>
  state.linesHistory.fire;
