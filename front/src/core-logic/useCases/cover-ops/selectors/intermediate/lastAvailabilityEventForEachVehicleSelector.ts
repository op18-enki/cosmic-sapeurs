import { createSelector } from "@reduxjs/toolkit";
import { availabilityChangedEventsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { AvailabilityChangedEvent } from "interfaces";
import { replaceAt } from "utils";

export const lastAvailabilityEventForEachVehicleSelector = createSelector(
  availabilityChangedEventsSelector,
  (availabilityEvents): AvailabilityChangedEvent[] =>
    availabilityEvents.reduce<AvailabilityChangedEvent[]>((acc, event) => {
      const existingEventIndex = acc.findIndex(
        (existingEvent) =>
          existingEvent.raw_vehicle_id === event.raw_vehicle_id,
      );

      if (existingEventIndex === -1) return [...acc, event];

      const existingEvent = acc[existingEventIndex];
      if (existingEvent.timestamp < event.timestamp) {
        return replaceAt(acc, existingEventIndex, event);
      }
      return acc;
    }, []),
);
