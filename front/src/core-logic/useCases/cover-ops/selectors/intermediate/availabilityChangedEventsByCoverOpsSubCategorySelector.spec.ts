import { createTestStore } from "core-logic/useCases/test.utils";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { AvailabilityChangedEvent } from "interfaces";

describe("Availability changed events summed by sub-category selector", () => {
  it("Sums the availability counts for each sub-category", () => {
    const sap1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "vsav_solo",
      timestamp: "2020-01-01T12:00:00.000Z",
      home_area: "STOU",
      area_availability: {
        available: 1,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: 1,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    });
    const ep1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
      home_area: "STOU",
      timestamp: "2020-01-01T13:00:00.000Z",
      area_availability: {
        available: 1,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: 3,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    });
    const ep2AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "fptl",
      home_area: "STOU",
      timestamp: "2020-01-01T14:00:00.000Z",
      area_availability: {
        available: 2,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: 8,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    });

    const ep3AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "fpt",
      home_area: "CHPT",
      timestamp: "2020-01-01T15:00:00.000Z",
      area_availability: {
        available: 1,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: 4,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    });

    const sap2AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "vsav_solo",
      timestamp: "2020-01-01T16:00:00.000Z",
      home_area: "CHPT",
      area_availability: {
        available: 8,
        unavailable: 1,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: 2,
        unavailable: 1,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    });

    const { store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          sap1AvailabilityEvent,
          ep1AvailabilityEvent,
          ep2AvailabilityEvent,
          ep3AvailabilityEvent,
          sap2AvailabilityEvent,
        ],
      },
    });

    const actual = availabilityChangedEventsByCoverOpsSubCategorySelector(
      store.getState(),
    );

    const sap = [
      sap1AvailabilityEvent,
      {
        ...sap2AvailabilityEvent,
        area_availability: {
          available: 8,
          unavailable: 1,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
        bspp_availability: {
          available: 2,
          unavailable: 1,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
      },
    ];

    const ep = [
      ep1AvailabilityEvent,
      {
        ...ep2AvailabilityEvent,
        area_availability: {
          available: 3,
          unavailable: 0,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
        bspp_availability: {
          available: 11,
          unavailable: 0,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
      },
      {
        ...ep3AvailabilityEvent,
        area_availability: {
          available: 1,
          unavailable: 0,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
        bspp_availability: {
          available: 15,
          unavailable: 0,
          on_operation: 0,
          recoverable: 0,
          unavailable_omnibus: 0,
        },
      },
    ];

    const expected: {
      ep: AvailabilityChangedEvent[];
      epWithOmni: AvailabilityChangedEvent[];
      sap: AvailabilityChangedEvent[];
      sapWithOmni: AvailabilityChangedEvent[];
    } = {
      ep,
      epWithOmni: ep,
      sap,
      sapWithOmni: sap,
    };
    expect(actual).toEqual(expected);
  });
});
