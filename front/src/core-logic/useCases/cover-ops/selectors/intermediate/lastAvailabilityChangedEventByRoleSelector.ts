import { createSelector } from "@reduxjs/toolkit";
import { availabilityChangedEventsByRoleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByRoleSelector";
import type { VehicleRole, AvailabilityChangedEvent } from "interfaces";
import * as R from "ramda";

export const lastAvailabilityChangedEventByRoleSelector = createSelector(
  availabilityChangedEventsByRoleSelector,
  (
    availabilityChangedEventsByRole,
  ): Partial<Record<VehicleRole, AvailabilityChangedEvent>> => {
    return R.mapObjIndexed(
      (availabilityChangedEvents) => R.last(availabilityChangedEvents),
      availabilityChangedEventsByRole,
    );
  },
);
