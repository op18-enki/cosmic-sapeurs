import { createSelector } from "@reduxjs/toolkit";
import { availabilityChangedEventsSelector } from "./rootSelectors";
import { groupBy } from "utils";
import type { VehicleRole, AvailabilityChangedEvent } from "interfaces";

const groupByRole = groupBy<AvailabilityChangedEvent, VehicleRole>(
  (count) => count.role,
);

export const availabilityChangedEventsByRoleSelector = createSelector(
  availabilityChangedEventsSelector,
  groupByRole,
);
