import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { lastAvailabilityEventForEachVehicleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityEventForEachVehicleSelector";
import { createTestStore, expectToEqual } from "core-logic/useCases/test.utils";

describe("lastAvailablityEventByVehicleSelector", () => {
  it("can be created", () => {
    const vsavAvailabilityEvent = makeAvailabilityChangedEvent({
      raw_vehicle_id: "VSAV_1",
      role: "vsav_solo",
    });
    const pse1AvailabilityEvent = makeAvailabilityChangedEvent({
      raw_vehicle_id: "PSE_1",
      role: "pse_solo",
    });
    const cesdAvailabilityEventOld = makeAvailabilityChangedEvent({
      timestamp: "2020-01-01T00:00:00.000Z",
      raw_vehicle_id: "CESD_1",
      role: "cesd",
      home_area: "CHPT",
    });
    const cesdAvailabilityEvent = makeAvailabilityChangedEvent({
      timestamp: "2020-01-01T00:10:00.000Z",
      raw_vehicle_id: "CESD_1",
      role: "cesd",
      home_area: "CHPT",
    });
    const pse2AvailabilityEvent = makeAvailabilityChangedEvent({
      raw_vehicle_id: "PSE_2",
      role: "fa",
      home_area: "STOU",
    });

    const { store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          vsavAvailabilityEvent,
          pse1AvailabilityEvent,
          pse2AvailabilityEvent,
          cesdAvailabilityEventOld,
          cesdAvailabilityEvent,
        ],
      },
    });

    // act
    const lastAvailabilityEventsForEachVehicle =
      lastAvailabilityEventForEachVehicleSelector(store.getState());

    expect(lastAvailabilityEventsForEachVehicle).toHaveLength(4);

    expectToEqual(lastAvailabilityEventsForEachVehicle, [
      vsavAvailabilityEvent,
      pse1AvailabilityEvent,
      pse2AvailabilityEvent,
      cesdAvailabilityEvent,
    ]);
  });
});
