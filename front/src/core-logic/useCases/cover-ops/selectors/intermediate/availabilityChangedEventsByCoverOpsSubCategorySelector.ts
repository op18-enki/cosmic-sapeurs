import { Availability, VehicleRole } from "interfaces";
import { createSelector } from "@reduxjs/toolkit";
import * as R from "ramda";
import type { AvailabilityChangedEvent } from "interfaces";
import { rolesPerCoverOpsSubCategory } from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
import { availabilityChangedEventsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";

const sumAvailabilityFor =
  (availabilityToAccess: "area_availability" | "bspp_availability") =>
  (eventsByRole: AvailabilityChangedEvent[]): Availability =>
    eventsByRole.reduce<Availability>(
      (acc, evt) => {
        if (!evt) return acc;
        return sumAvailabilities(acc, R.prop(availabilityToAccess, evt));
      },
      {
        available: 0,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
    );

const getLastByRole = R.pipe<
  AvailabilityChangedEvent[],
  Record<string, AvailabilityChangedEvent[]>,
  AvailabilityChangedEvent[][],
  AvailabilityChangedEvent[]
>(
  R.groupBy((event) => event.role),
  R.values,
  R.map((events) => R.last(events)!),
);

const combineAvailabilityChangedEventsByRole = (
  events: AvailabilityChangedEvent[],
): AvailabilityChangedEvent[] =>
  events.map((currentEvent, currentIndex) => {
    const eventsBefore = events.slice(0, currentIndex + 1);
    const eventsBeforeForCurrentArea = eventsBefore.filter(
      ({ home_area }) => home_area === currentEvent.home_area,
    );

    const cumulatedAreaAvailability = R.pipe(
      getLastByRole,
      sumAvailabilityFor("area_availability"),
    )(eventsBeforeForCurrentArea);

    const cumulatedBsppAvailability = R.pipe(
      getLastByRole,
      sumAvailabilityFor("bspp_availability"),
    )(eventsBefore);

    return {
      ...currentEvent,
      area_availability: cumulatedAreaAvailability,
      bspp_availability: cumulatedBsppAvailability,
    };
  });

const makeAvailabilityChangedEventsCombinedBySubCategorySelector = (
  roles: VehicleRole[],
) => {
  const combineAvailabilities = R.pipe<
    AvailabilityChangedEvent[],
    AvailabilityChangedEvent[],
    AvailabilityChangedEvent[]
  >(
    R.filter<AvailabilityChangedEvent>((evt) => roles.includes(evt.role)),
    combineAvailabilityChangedEventsByRole,
  );

  return createSelector(
    availabilityChangedEventsSelector,
    combineAvailabilities,
  );
};

const availabilityChangedEventsBySelectors: Record<
  CoverOpsVehicleSubCategory,
  ReturnType<typeof makeAvailabilityChangedEventsCombinedBySubCategorySelector>
> = {
  ep: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.ep ?? [],
  ),
  epWithOmni: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.epWithOmni ?? [],
  ),
  sap: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.sap ?? [],
  ),
  sapWithOmni: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.sapWithOmni ?? [],
  ),
};

const optionalSum = (x?: number, y?: number) => (x ?? 0) + (y ?? 0);

const sumAvailabilities = (a: Availability, b: Availability): Availability => ({
  available: optionalSum(a.available, b.available),
  unavailable: optionalSum(a.unavailable, b.unavailable),
  on_operation: optionalSum(a.on_operation, b.on_operation),
  recoverable: optionalSum(a.recoverable, b.recoverable),
  unavailable_omnibus: optionalSum(
    a.unavailable_omnibus,
    b.unavailable_omnibus,
  ),
});

export const availabilityChangedEventsByCoverOpsSubCategorySelector =
  createSelector(
    availabilityChangedEventsBySelectors.ep,
    availabilityChangedEventsBySelectors.epWithOmni,
    availabilityChangedEventsBySelectors.sap,
    availabilityChangedEventsBySelectors.sapWithOmni,
    (
      ep,
      epWithOmni,
      sap,
      sapWithOmni,
    ): Record<CoverOpsVehicleSubCategory, AvailabilityChangedEvent[]> => ({
      ep,
      epWithOmni,
      sap,
      sapWithOmni,
    }),
  );
