import type { Availability, AvailabilityKind } from "interfaces";
import { createSelector } from "@reduxjs/toolkit";
import { lastAvailabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityChangedEventsByCoverOpsSubCategorySelector";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";

const createBarInputSelectorForSubCategory = <
  S extends CoverOpsVehicleSubCategory,
>(
  subCategory: S,
) =>
  createSelector(
    lastAvailabilityChangedEventsByCoverOpsSubCategorySelector,
    (
      lastAvailabilityChangedEventsByCoverOpsSubCategory,
    ): Record<AvailabilityKind, number> => {
      const bspp_availability: Availability =
        lastAvailabilityChangedEventsByCoverOpsSubCategory[subCategory]
          ?.bspp_availability ?? {
          available: 0,
          on_operation: 0,
          recoverable: 0,
          unavailable: 0,
          unavailable_omnibus: 0,
        };

      return bspp_availability;
    },
  );

export const sapBarInputSelector = createBarInputSelectorForSubCategory("sap");
export const epBarInputSelector = createBarInputSelectorForSubCategory("ep");
