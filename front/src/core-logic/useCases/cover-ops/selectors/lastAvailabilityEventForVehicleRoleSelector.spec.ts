import { AvailabilityChangedEvent } from "../../../../interfaces";
import { createTestStore, expectToEqual } from "../../test.utils";
import { makeAvailability } from "../factories/makeAvailability";
import { makeAvailabilityChangedEvent } from "../factories/makeAvailabilityChangedEvent";
import { mapByVehicleRoleDataSelector } from "./lastAvailabilityEventForVehicleRoleSelector";

describe("mapByVehicleRoleDataSelector", () => {
  it("gets correct data on map when vehicle not linked", () => {
    const availabilityChangedEvents: AvailabilityChangedEvent[] = [
      makeAvailabilityChangedEvent({
        home_area: "ANTO",
        role: "fa",
        availabilityKind: "available",
      }),
      makeAvailabilityChangedEvent({
        home_area: "ANTO",
        role: "ca_ba",
        availabilityKind: "unavailable",
      }),
    ];

    const { store } = createTestStore({
      cover: { availabilityChangedEvents },
      interactions: {
        selectedVehicleRoles: [[], ["fa", "ca_ba"], [], []],
        areVehicleLinked: [false, false, false, false],
      },
    });

    expectToEqual(mapByVehicleRoleDataSelector(store.getState()), [
      {},
      {
        ANTO: {
          available: 1,
        },
      },
      {},
      {},
    ]);
  });

  it("gets correct data on map when vehicle are linked", () => {
    const availabilityChangedEvents: AvailabilityChangedEvent[] = [
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "fa-anto",
        home_area: "ANTO",
        role: "fa",
        availabilityKind: "available",
      }),
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "ca-anto-1",
        home_area: "ANTO",
        role: "ca_ba",
        availabilityKind: "recoverable",
      }),
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "ca-anto-2",
        home_area: "ANTO",
        role: "ca_ba",
        availabilityKind: "unavailable",
      }),
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "fa-stou",
        home_area: "STOU",
        role: "fa",
        availabilityKind: "available",
      }),
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "ca-stou",
        home_area: "STOU",
        role: "ca_ba",
        availabilityKind: "available",
      }),

      makeAvailabilityChangedEvent({
        raw_vehicle_id: "fa-chpt",
        home_area: "CHPT",
        role: "fa",
        availabilityKind: "available",
      }),
      makeAvailabilityChangedEvent({
        raw_vehicle_id: "ca-chpt",
        home_area: "CHPT",
        role: "ca_ba",
        availabilityKind: "unavailable",
      }),
    ];

    const { store } = createTestStore({
      cover: { availabilityChangedEvents },
      interactions: {
        selectedVehicleRoles: [[], ["fa", "ca_ba"], [], []],
        areVehicleLinked: [false, true, false, false],
      },
    });

    expectToEqual(mapByVehicleRoleDataSelector(store.getState()), [
      {},
      {
        ANTO: makeAvailability({
          recoverable: 1,
        }),
        STOU: makeAvailability({
          available: 1,
        }),
        CHPT: makeAvailability({
          unavailable: 1,
        }),
      },
      {},
      {},
    ]);
  });

  it("gets correct data on map when vehicle linked with only one role", () => {
    const availabilityChangedEvents: AvailabilityChangedEvent[] = [
      makeAvailabilityChangedEvent({
        home_area: "ANTO",
        role: "fa",
        availabilityKind: "available",
      }),
    ];

    const { store } = createTestStore({
      cover: { availabilityChangedEvents },
      interactions: {
        selectedVehicleRoles: [[], ["fa"], [], []],
        areVehicleLinked: [false, true, false, false],
      },
    });

    expectToEqual(mapByVehicleRoleDataSelector(store.getState()), [
      {},
      {
        ANTO: makeAvailability({
          available: 1,
        }),
      },
      {},
      {},
    ]);
  });
});

// indispo: 0, operation: 1, recupérable: 2, disponible: 3
