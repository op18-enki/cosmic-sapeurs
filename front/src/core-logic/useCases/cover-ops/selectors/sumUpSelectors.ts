import { createSelector } from "@reduxjs/toolkit";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import {
  epBarInputSelector,
  sapBarInputSelector,
} from "core-logic/useCases/cover-ops/selectors/barInputSampleSelector";

export const socleAvailableSelector = createSelector(
  epBarInputSelector,
  sapBarInputSelector,
  (epBarInput, sapBarInput) => {
    const socleAvailable: Record<CoverOpsVehicleSubCategory, number> = {
      ep: epBarInput.available,
      epWithOmni: epBarInput.available,
      sap: sapBarInput.available,
      sapWithOmni: sapBarInput.available,
    };

    return socleAvailable;
  },
);
