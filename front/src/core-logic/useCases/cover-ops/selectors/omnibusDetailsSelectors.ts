import { createSelector } from "@reduxjs/toolkit";
import { omnibusDetailsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import type { Area, OmnibusBalanceKind, OmnibusDetail } from "interfaces";
import { omnibusBalanceKindOptions } from "interfaces";
import * as R from "ramda";
import { groupBy } from "utils";

export const omnibusBalanceSelector = createSelector(
  omnibusDetailsSelector,
  (details) =>
    omnibusBalanceKindOptions.reduce(
      (acc, balanceKind) => ({
        ...acc,
        [balanceKind]: details.filter(
          (detail) => detail.balance_kind === balanceKind,
        ).length,
      }),
      {} as Record<OmnibusBalanceKind, number>,
    ),
);

const makeOmnibusDetailsSelectorForBalanceKind = (
  omnibusBalanceKind: OmnibusBalanceKind,
) =>
  createSelector(omnibusDetailsSelector, (omnibusDetails) => {
    return omnibusDetails.filter(
      ({ balance_kind }) => balance_kind === omnibusBalanceKind,
    );
  });

export const omnibusDetailsSelectorByBalanceKind =
  omnibusBalanceKindOptions.reduce((acc, omnibusBalanceKind) => {
    return {
      ...acc,
      [omnibusBalanceKind]:
        makeOmnibusDetailsSelectorForBalanceKind(omnibusBalanceKind),
    };
  }, {} as Record<OmnibusBalanceKind, ReturnType<typeof makeOmnibusDetailsSelectorForBalanceKind>>);

export const omnibusDetailsByArea = createSelector(
  omnibusDetailsSelector,
  (details): Partial<Record<Area, OmnibusDetail[]>> =>
    groupBy<OmnibusDetail, Area>(R.prop("area"))(details),
);
