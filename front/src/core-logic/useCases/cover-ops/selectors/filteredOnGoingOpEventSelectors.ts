import { createSelector } from "@reduxjs/toolkit";
import { procedureToColorAndLabel } from "app/labels";
import type { FiltrableField } from "core-logic";
import type { OngoingOpChangedEvent } from "interfaces";
import * as R from "ramda";
import { groupBy } from "utils";
import {
  activeOpsFiltersSelector,
  withVehicleOnlySelector,
} from "./intermediate/rootSelectors";
import { lastOnGoingOpEventByOperationIdSelector } from "./lastOnGoingOpEventByOperationIdSelector";

export const filteredOnGoingOpEventSelector = createSelector(
  lastOnGoingOpEventByOperationIdSelector,
  activeOpsFiltersSelector,
  withVehicleOnlySelector,
  (ongoingOpChangedEvents, activeFilters, withVehicleOnly) => {
    const filtrableFields = R.keys(activeFilters);

    return ongoingOpChangedEvents.filter((event) => {
      if (withVehicleOnly && event.affected_vehicles.length === 0) return false;

      return filtrableFields.every((filterableField) => {
        const acceptedValues = activeFilters[filterableField]!;
        const eventFieldValue = event.opening_infos[filterableField]!;
        return acceptedValues.includes(eventFieldValue);
      });
    });
  },
);

type LabelAndValue = {
  value: string;
  label: string;
};

type GetFieldOptions = (
  events: OngoingOpChangedEvent[],
  field: FiltrableField,
) => LabelAndValue[];

const optionsFromAllOnGoingOpEventForField =
  (
    buildLabel: (
      eventsForField: OngoingOpChangedEvent[],
      fieldValue: string,
    ) => string,
  ): GetFieldOptions =>
  (events: OngoingOpChangedEvent[], field: FiltrableField) =>
    R.pipe<
      OngoingOpChangedEvent[],
      Record<string, OngoingOpChangedEvent[]>,
      Record<string, LabelAndValue>,
      LabelAndValue[]
    >(
      groupBy((event) => event.opening_infos[field]!),
      R.mapObjIndexed((eventsForField, fieldValue) => ({
        value: fieldValue,
        label: buildLabel(eventsForField, fieldValue),
      })),
      R.values,
    )(events);

const makeOptionsSelectorFor = (
  field: FiltrableField,
  getFieldOption: GetFieldOptions,
) =>
  createSelector(
    lastOnGoingOpEventByOperationIdSelector,
    activeOpsFiltersSelector,
    (ongoingOpChangedEvents, activeOpsFilters) => {
      const fieldOptionsFromAllOnGoingOpEvent = getFieldOption(
        ongoingOpChangedEvents,
        field,
      );

      if (activeOpsFilters[field]?.length)
        return R.reject(
          ({ value }) => activeOpsFilters[field]!.includes(value),
          fieldOptionsFromAllOnGoingOpEvent,
        );
      return fieldOptionsFromAllOnGoingOpEvent;
    },
  );

export const procedureOptionsSelector = makeOptionsSelectorFor(
  "raw_procedure",
  optionsFromAllOnGoingOpEventForField(
    (eventsForField, fieldValue) =>
      procedureToColorAndLabel[fieldValue]?.label ?? fieldValue,
  ),
);

export const departureCriteriaOptionsSelector = makeOptionsSelectorFor(
  "departure_criteria",
  optionsFromAllOnGoingOpEventForField((eventsForField, fieldValue) => {
    const onGoingOpEventForFieldValue = eventsForField[0];

    return `${fieldValue} - ${
      onGoingOpEventForFieldValue.opening_infos.raw_cause ?? ""
    }`;
  }),
);
