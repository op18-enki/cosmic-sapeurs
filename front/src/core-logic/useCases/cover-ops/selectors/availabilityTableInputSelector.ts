import { createSelector } from "@reduxjs/toolkit";

import { AvailabilityTableData } from "ui";
import { lastAvailabilityChangedEventByRoleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityChangedEventByRoleSelector";
import {
  getTheoreticalByRole,
  vehicleRoleClassification,
} from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
import * as R from "ramda";
import type {
  Availability,
  VehicleRole,
  AvailabilityChangedEvent,
} from "interfaces";

export type AvailabilityCellValue = {
  value?: number;
  inService?: number;
  theoretical?: number;
  percentage?: number;
};

const calculatePercentage = (value?: number, theoretical?: number) => {
  if (!theoretical || R.isNil(value)) return undefined;
  return Math.round((value / theoretical) * 100);
};

const theoreticalByRole = getTheoreticalByRole();

const getCellValue = (
  role: VehicleRole,
  availabilityForRole?: Availability,
): AvailabilityCellValue => {
  const value = availabilityForRole?.available;
  const theoretical = theoreticalByRole[role];

  const inService = availabilityForRole
    ? R.sum(Object.values(availabilityForRole)) -
      (availabilityForRole.unavailable ?? 0)
    : undefined;

  return {
    value: value,
    theoretical,
    inService,
    percentage: calculatePercentage(value, theoretical),
  };
};

const getAvailabilityCellByVehicleRole =
  (
    lastAvailabilityChangedEventByRole: Partial<
      Record<VehicleRole, AvailabilityChangedEvent>
    >,
  ) =>
  (roles: VehicleRole[]) =>
    roles.reduce(
      (acc, role) => ({
        ...acc,
        [role]: getCellValue(
          role,
          lastAvailabilityChangedEventByRole[role]?.bspp_availability,
        ),
      }),
      {} as Partial<Record<VehicleRole, AvailabilityCellValue>>,
    );

export const availabilityTableInputSelector = createSelector(
  lastAvailabilityChangedEventByRoleSelector,
  (lastAvailabilityChangedEventByRole): AvailabilityTableData => {
    const availabilityTableData = R.mapObjIndexed(
      (rolesBySubCategory) =>
        R.mapObjIndexed(
          getAvailabilityCellByVehicleRole(lastAvailabilityChangedEventByRole),
          rolesBySubCategory,
        ),
      vehicleRoleClassification,
    );

    return availabilityTableData;
  },
);
