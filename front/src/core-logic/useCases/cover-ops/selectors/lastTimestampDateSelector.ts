import { createSelector } from "@reduxjs/toolkit";
import { fireLineInputSelector, victimLineInputSelector } from "core-logic";

const timestampToDate = (timestamp: string): Date => new Date(timestamp);

export const lastTimestampDateSelector = createSelector(
  victimLineInputSelector,
  fireLineInputSelector,
  (victimInputs, fireInputs) => {
    if (fireInputs.length === 0 && victimInputs.length === 0) return new Date();

    const fireLastTime =
      fireInputs.length > 0
        ? Math.max(
            ...fireInputs.map((input) =>
              timestampToDate(input.timestamp).getTime(),
            ),
          )
        : 0;

    const victimLastTime =
      victimInputs.length > 0
        ? Math.max(
            ...victimInputs.map((input) =>
              timestampToDate(input.timestamp).getTime(),
            ),
          )
        : 0;

    const lastDate = new Date(Math.max(fireLastTime, victimLastTime));
    return lastDate;
  },
);
