import * as R from "ramda";
import { ongoingOpChangedEventsSelector } from "./intermediate/rootSelectors";
import type {
  OngoingOpChangedEvent,
  OperationRelevantStatus,
} from "interfaces";
import { createSelector } from "@reduxjs/toolkit";

export const toLastEventByOperationIdBeforeFilter = R.pipe<
  OngoingOpChangedEvent[],
  Record<string, OngoingOpChangedEvent[]>,
  Record<string, OngoingOpChangedEvent>,
  OngoingOpChangedEvent[]
>(
  R.groupBy((event) => event.raw_operation_id),
  R.mapObjIndexed((event) => R.last(event)!),
  R.values,
);

export const lastEventByOperationIdSelector = createSelector(
  ongoingOpChangedEventsSelector,
  toLastEventByOperationIdBeforeFilter,
);

const statusToDisplay: OperationRelevantStatus[] = [
  "opened",
  "first_vehicle_affected",
  "some_affected_vehicle_changed",
  "opened_by_snapshot",
];

export const keepOnlyEventsWithStatusToDisplay =
  (statusToKeep: OperationRelevantStatus[]) => (event: OngoingOpChangedEvent) =>
    statusToKeep.includes(event.status as OperationRelevantStatus);

const toEventsFilterByStatusAndOrdered = (
  statusToKeep: OperationRelevantStatus[],
) =>
  R.pipe<
    OngoingOpChangedEvent[],
    OngoingOpChangedEvent[],
    OngoingOpChangedEvent[]
  >(R.filter(keepOnlyEventsWithStatusToDisplay(statusToKeep)), R.reverse);

export const toLastEventByOperationId = R.pipe(
  toLastEventByOperationIdBeforeFilter,
  toEventsFilterByStatusAndOrdered(statusToDisplay),
);

export const lastOnGoingOpEventByOperationIdSelector = createSelector(
  lastEventByOperationIdSelector,
  toEventsFilterByStatusAndOrdered(statusToDisplay),
);

const statusToShowOnOngoingOpMap: OperationRelevantStatus[] = [
  "first_vehicle_affected",
  "some_affected_vehicle_changed",
];

export const lastOnGoingOpEventByOperationIdWithoutOpenStatusSelector =
  createSelector(
    lastEventByOperationIdSelector,
    toEventsFilterByStatusAndOrdered(statusToShowOnOngoingOpMap),
  );
