import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import { lastOnGoingOpEventByOperationIdSelector } from "core-logic/useCases/cover-ops/selectors/lastOnGoingOpEventByOperationIdSelector";
import { createTestStore } from "core-logic/useCases/test.utils";
import type { OngoingOpChangedEvent } from "interfaces";

describe("lastOnGoingOpEventByOperationIdSelector", () => {
  it("Get last operation event", () => {
    const ope1Event = makeOngoingOpChangedEvent({
      timestamp: "2020-02-01",
      raw_operation_id: "ope1",
      status: "opened",
      opening_infos: {
        cause: "victim",
        address_area: "ASNI",
        raw_procedure: "red",
        departure_criteria: "301 (default)",
        latitude: 60000.25,
        longitude: 60000.25,
      },
    });

    const ope2Event = makeOngoingOpChangedEvent({
      timestamp: "2020-02-02",
      status: "opened",
      affected_vehicles: ["vehicle_c"],
      raw_operation_id: "ope2",
      opening_infos: {
        cause: "victim",
        address_area: "ASNI",
        raw_procedure: "green",
        departure_criteria: "301 (default)",
        latitude: 60000.25,
        longitude: 60000.25,
        opening_timestamp: "2020-01-03T12:00",
      },
    });

    const ope3Event = makeOngoingOpChangedEvent({
      timestamp: "2020-02-05",
      status: "opened",
      affected_vehicles: ["vehicle_d"],
      raw_operation_id: "ope3",
      opening_infos: {
        cause: "victim",
        address_area: "ASNI",
        raw_procedure: "green",
        departure_criteria: "301 (default)",
        latitude: 60000.25,
        longitude: 60000.25,
        opening_timestamp: "2020-01-03T12:00",
      },
    });

    const ope1LastEvent = makeOngoingOpChangedEvent({
      timestamp: "2020-02-03",
      raw_operation_id: "ope1",
      affected_vehicles: ["vehicle_a", "vehicle_b"],
      status: "first_vehicle_affected",
      opening_infos: {
        address_area: "ASNI",
        cause: "victim",
        raw_procedure: "red",
        departure_criteria: "301 (default)",
        latitude: 60000.25,
        longitude: 60000.25,
        opening_timestamp: "2020-01-01T12:00",
      },
    });

    const ope2EventAllVehicleReleased = makeOngoingOpChangedEvent({
      timestamp: "2020-02-04",
      status: "all_vehicles_released",
      affected_vehicles: ["vehicle_c"],
      raw_operation_id: "ope2",
      opening_infos: {
        cause: "victim",
        address_area: "ASNI",
        raw_procedure: "green",
        departure_criteria: "301 (default)",
        latitude: 60000.25,
        longitude: 60000.25,
        opening_timestamp: "2020-01-03T12:00",
      },
    });

    const { store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [
          ope1Event,
          ope3Event,
          ope2Event,
          ope1LastEvent,
          ope2EventAllVehicleReleased,
        ],
      },
    });

    const actual = lastOnGoingOpEventByOperationIdSelector(store.getState());

    const expected: OngoingOpChangedEvent[] = [ope3Event, ope1LastEvent];

    expect(actual).toEqual(expected);
  });
});
