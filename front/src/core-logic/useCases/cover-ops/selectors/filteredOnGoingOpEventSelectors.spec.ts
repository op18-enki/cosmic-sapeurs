import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import { filteredOnGoingOpEventSelector } from "core-logic/useCases/cover-ops/selectors/filteredOnGoingOpEventSelectors";
import { createTestStore } from "core-logic/useCases/test.utils";
import type { OngoingOpChangedEvent } from "interfaces";

describe("filterOnGoingOpEventSelector", () => {
  it("Get only onGoingOp matching filters", () => {
    const opeClosedProcedure = makeOngoingOpChangedEvent({
      raw_operation_id: "OpeClosed",
      timestamp: "2020-02-01",
      status: "closed",
      opening_infos: {
        raw_procedure: "O",
        departure_criteria: "302",
      },
    });
    const opeRedProcedure = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope1",
      timestamp: "2020-02-01",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "O",
        departure_criteria: "302",
      },
    });
    const opeGreenProcedure = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope2",
      timestamp: "2020-02-02",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "G",
        departure_criteria: "220",
      },
    });
    const opeTwoFilterFieldCorrect = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope3",
      timestamp: "2020-02-03",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "R",
        departure_criteria: "301",
      },
    });
    const opeOneFilterFieldCorrect = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope4",
      timestamp: "2020-02-04",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "R",
        departure_criteria: "101",
      },
    });
    const { store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [
          opeClosedProcedure,
          opeRedProcedure,
          opeGreenProcedure,
          opeTwoFilterFieldCorrect,
          opeOneFilterFieldCorrect,
        ],
        activeFilters: {
          raw_procedure: ["R", "O"],
          departure_criteria: ["301", "302"],
        },
      },
    });

    const actualOpsFilter = filteredOnGoingOpEventSelector(store.getState());

    const expectedOpsFilter: OngoingOpChangedEvent[] = [
      opeTwoFilterFieldCorrect,
      opeRedProcedure,
    ];

    expect(actualOpsFilter).toEqual(expectedOpsFilter);
  });

  it("Get only onGoingOp with vehicle on it", () => {
    const opeRedProcedure = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope1",
      timestamp: "2020-02-01",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "O",
        departure_criteria: "302",
      },
      affected_vehicles: ["123"],
    });

    const opeTwoFilterFieldCorrect = makeOngoingOpChangedEvent({
      raw_operation_id: "Ope3",
      timestamp: "2020-02-03",
      status: "first_vehicle_affected",
      opening_infos: {
        raw_procedure: "R",
        departure_criteria: "301",
      },
      affected_vehicles: [],
    });

    const { store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [opeRedProcedure, opeTwoFilterFieldCorrect],
        withVehicleOnly: true,
      },
    });

    const actualOpsFilter = filteredOnGoingOpEventSelector(store.getState());

    const expectedOpsFilter: OngoingOpChangedEvent[] = [opeRedProcedure];

    expect(actualOpsFilter).toEqual(expectedOpsFilter);
  });
});
