import { createSelector } from "@reduxjs/toolkit";
import { lastAvailabilityEventForEachVehicleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityEventForEachVehicleSelector";
import {
  areVehicleLinkedSelector,
  selectedVehicleRolesSelector,
} from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import {
  Area,
  Availability,
  AvailabilityChangedEvent,
  AvailabilityKind,
  VehicleRole,
} from "interfaces";
import { countBy, map, mapObjIndexed, pipe } from "ramda";
import { groupBy } from "utils";

const minOfArray = (array: number[]) => (array.length ? Math.min(...array) : 0);

const countMinFilteredEventsAgainstNbOfRoles = (
  filteredEvents: AvailabilityChangedEvent[],
  numberOfRoles: number,
) => {
  const countFilteredEventsByRole = countBy(
    (event) => event.role,
    filteredEvents,
  );
  const numbersOfFilteredEvents = Object.values(countFilteredEventsByRole);
  const minCountFilteredEvents =
    numbersOfFilteredEvents.length === numberOfRoles
      ? minOfArray(numbersOfFilteredEvents)
      : 0;
  return minCountFilteredEvents;
};
const linkedVehicleCount = (
  events: AvailabilityChangedEvent[],
  linkedRoles: VehicleRole[],
): Availability => {
  const nbOfRoles = linkedRoles.length;
  // Available linked vehicles
  const availableEvents = events.filter(
    (event) => event.latest_event_data.availability_kind === "available",
  );
  const minAvailable = countMinFilteredEventsAgainstNbOfRoles(
    availableEvents,
    nbOfRoles,
  );

  // Recoverable linked vehicles
  const atLeastRecoverableEvents = events.filter(
    (event) =>
      event.latest_event_data.availability_kind === "available" ||
      event.latest_event_data.availability_kind === "recoverable",
  );

  const minAtLeastRecoverable = countMinFilteredEventsAgainstNbOfRoles(
    atLeastRecoverableEvents,
    nbOfRoles,
  );
  const minRecoverable = minAtLeastRecoverable - minAvailable;

  // On operation linked vehicles
  const atLeastOnOperationEvents = events.filter(
    (event) =>
      event.latest_event_data.availability_kind === "available" ||
      event.latest_event_data.availability_kind === "recoverable" ||
      event.latest_event_data.availability_kind === "on_operation",
  );
  const minAtLeastOnOperation = countMinFilteredEventsAgainstNbOfRoles(
    atLeastOnOperationEvents,
    nbOfRoles,
  );
  const minOnOperation = minAtLeastOnOperation - minAvailable - minRecoverable;

  // Rest linked vehicles (at least unavailable)
  const minAtLeastUnavailable = countMinFilteredEventsAgainstNbOfRoles(
    events,
    nbOfRoles,
  );
  const minUnavailable =
    minAtLeastUnavailable - minAvailable - minRecoverable - minOnOperation;

  return {
    available: minAvailable,
    recoverable: minRecoverable,
    on_operation: minOnOperation,
    unavailable: minUnavailable,
  } as Availability;
};

export const lastAvailabilityEventForVehicleRoleSelector = createSelector(
  lastAvailabilityEventForEachVehicleSelector,
  selectedVehicleRolesSelector,
  (availabilityEvents, selectedVehicleRoles4Times) =>
    selectedVehicleRoles4Times.map((selectedVehicleRoles) =>
      availabilityEvents.filter((event) =>
        selectedVehicleRoles.includes(event.role),
      ),
    ),
);

export const mapByVehicleRoleDataSelector = createSelector(
  lastAvailabilityEventForVehicleRoleSelector,
  areVehicleLinkedSelector,
  selectedVehicleRolesSelector,
  (
    availabilityEvents4times,
    areVehicleLinked4Times,
    selectedVehicleRoles4Times,
  ): Partial<Record<Area, Availability>>[] =>
    availabilityEvents4times.map((availabilityEvents, index) => {
      const areVehicleLinked = areVehicleLinked4Times[index];
      const selectedVehicleRoles = selectedVehicleRoles4Times[index];

      return pipe<
        AvailabilityChangedEvent[],
        Record<Area, AvailabilityChangedEvent[]>,
        Partial<Record<Area, Availability>>
      >(
        groupBy<AvailabilityChangedEvent, Area>((event) => event.home_area),
        mapObjIndexed<AvailabilityChangedEvent[], Availability, Area>(
          (events) =>
            areVehicleLinked
              ? linkedVehicleCount(events, selectedVehicleRoles)
              : (countBy(
                  (event) => event.latest_event_data.availability_kind!,
                  events,
                ) as unknown as Availability),
        ),
      )(availabilityEvents);
    }),
);

const lastAvailabilityEventForVehicleRoleByAvailabilityKindSelector =
  createSelector(
    lastAvailabilityEventForVehicleRoleSelector,
    map(
      groupBy<AvailabilityChangedEvent, AvailabilityKind>(
        (event) => event.latest_event_data.availability_kind!,
      ),
    ),
  );

export const lastAvailabilityEventsForVehicleRoleByRawStatusSelector =
  createSelector(
    lastAvailabilityEventForVehicleRoleByAvailabilityKindSelector,
    (
      eventsByVehicleKind4times,
    ): Partial<
      Record<AvailabilityKind, Record<string, AvailabilityChangedEvent[]>>
    >[] =>
      map(
        mapObjIndexed(
          groupBy<AvailabilityChangedEvent, string>(
            (event) => event.latest_event_data.raw_status,
          ),
        ),
        eventsByVehicleKind4times,
      ),
  );
