import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { theoretical2021 } from "core-logic/useCases/cover-ops/selectors/configurations/theoretical2021";
import { VehicleRole } from "interfaces";

export type VehicleCategory =
  | "omnibus"
  | "sanitary"
  | "fire"
  | "special"
  | "command";

export type VehicleSubCategory =
  | "san"
  | "pump"
  | "mea"
  | "strongPower" // grande puissance
  | "ep"
  | "sap"
  | "medical"
  | "nautical"
  | "rsmu"
  | "nrbc"
  | "eld"
  | "cynoTech"
  | "imp"
  | "exhaustion" // Épuisement
  | "support" // Soutien
  | "cie"
  | "group" // Groupement
  | "cmo";

type VehicleRoleClassification = Record<
  VehicleCategory,
  Partial<Record<VehicleSubCategory, VehicleRole[]>>
>;

export const vehicleRoleClassification: VehicleRoleClassification = {
  omnibus: {
    san: ["vsav_omni", "pse_omni_san"],
    pump: ["pse_omni_pump"],
  },
  sanitary: {
    sap: ["vsav_solo", "vpsp"],
    medical: ["ar_bspp"],
  },
  command: {
    // cie: [],
    group: ["vpc_tac"],
    cmo: ["cmo_appui", "cmo_san"],
  },
  fire: {
    mea: ["bea", "epa_epsa"],
    strongPower: ["fa", "ca_ba", "fmogp", "bem"],
    ep: [
      "fpt",
      "fa",
      "fptl",
      "pse_solo",
      "ccr",
      "pst",
      "fptlhp",
      "vivp_g1",
      "vivp_g2",
      "vivp_g3",
    ],
  },
  special: {
    nautical: ["vsis", "esav", "esavi"],
    rsmu: ["vrsd", "cesd", "bars"],
    nrbc: ["vi_nrbc", "bap", "bumd", "lot_nrbc"],
    eld: ["celd"], // "vrex" (-> Until we do not handle the Resources, Cpt D. prefers not to see VREX at all. )
    cynoTech: ["vec", "vra"],
    imp: ["vimp"],
    exhaustion: ["vid", "vigi", "vps", "bpe"],
    support: ["crac", "pev", "cd", "vrcp", "dap", "vpma"],
  },
};

export const rolesPerCoverOpsSubCategory: Record<
  CoverOpsVehicleSubCategory,
  VehicleRole[]
> = {
  ep: vehicleRoleClassification.fire.ep ?? [],
  epWithOmni: [
    ...(vehicleRoleClassification.fire.ep ?? []),
    ...(vehicleRoleClassification.omnibus.pump ?? []),
  ],
  sap: vehicleRoleClassification.sanitary.sap ?? [],
  sapWithOmni: [
    ...(vehicleRoleClassification.sanitary.sap ?? []),
    ...(vehicleRoleClassification.omnibus.san ?? []),
  ],
};

export type VehicleLabels = {
  categories: Record<VehicleCategory, string>;
  subCategories: Record<VehicleSubCategory, string>;
  roles: Record<VehicleRole, string>;
};

export const vehiclesLabels: VehicleLabels = {
  categories: {
    omnibus: "MODULAIRE",
    fire: "INCENDIE",
    sanitary: "SANITAIRE",
    command: "COMMANDEMENT",
    special: "SPÉCIAUX",
  },
  subCategories: {
    san: "SAN (modulaire)",
    pump: "PSE pompe (modulaire)",
    mea: "MEA",
    strongPower: "Grande puissance",
    ep: "EP",
    sap: "C. San",
    medical: "Médicalisé",
    group: "GRPT",
    cie: "Cie",
    cmo: "CMO",
    nautical: "Nautique",
    rsmu: "RSMU",
    nrbc: "NRBC",
    eld: "ELD",
    cynoTech: "CYNO - TECH",
    imp: "IMP",
    exhaustion: "Épuisement",
    support: "Soutien",
  },
  roles: {
    ar_bspp: "AR BSPP",
    bamr: "BAMR",
    ban: "BAN",
    bap: "BAP",
    bars: "BARS",
    bea: "BEA",
    bec: "BEC",
    bel: "BEL",
    bem: "BEM",
    besi: "BESI",
    boitelec: "BOITELEC",
    bpe: "BPE",
    bpm: "BPM",
    bsn: "BSN",
    bspc: "BSPC",
    btrt: "BTRT",
    bumd: "BUMD",
    ca_ba: "CA",
    cahp: "CAHP",
    ccr: "CCR",
    cd: "CD",
    celd: "CELD",
    cesd: "CESD",
    cmo_appui: "CMO Appui",
    cmo_san: "CMO SAN",
    crac: "CRAC",
    dap: "DAP",
    emf: "EMF",
    epa_epsa: "EPA / EPAN",
    esav: "ESAV",
    esavi: "ESAVI",
    esi: "ESI",
    fa: "FA",
    fmogp: "FMOGP",
    fpt: "FPT",
    fptl: "FPTL",
    fptlhp: "FPTL HP",
    gvc: "GVC",
    linc: "LINC",
    lot_nrbc: "LOT_NRBC",
    lot_san: "LOT_SAN",
    mpe: "MPE",
    mpr: "MPR",
    o2: "O2",
    osec: "OSEC",
    other: "Autre",
    pesi: "PESI",
    pev: "PEV",
    pse_omni_pump: "PSE modulaire",
    pse_omni_san: "EP3",
    pse_solo: "PSE pompe",
    pst: "PST",
    rac: "RAC",
    rp: "RP",
    rtra: "RTRA",
    smg: "SMG",
    spcr: "SPCR",
    umh: "UMH",
    vec: "VEC",
    vi_nrbc: "VI NRBC",
    vid: "VID",
    vigi: "VIGI",
    vimp: "VIMP",
    vivp_g1: "VIVP G1",
    vivp_g2: "VIVP G2",
    vivp_g3: "VIVP G3",
    vpc_tac: "VPC TAC",
    vpma: "VPMA",
    vps: "VPS",
    vpsp: "VPSP (associatif)",
    vra: "VRA",
    vrcp: "VRCP",
    vrex: "VREX",
    vrsd: "VRSD",
    vsav_omni: "VSAV modulaire",
    vsav_solo: "VSAV soclé",
    vsis: "VSIS",
  },
};

export type TheoreticalByRole = Partial<Record<VehicleRole, number>>;

export const getTheoreticalByRole = () => {
  return theoretical2021; // change here for next year values
};
