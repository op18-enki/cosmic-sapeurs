import type { TheoreticalByRole } from "./vehicleRoleClassification";

export const theoretical2020: TheoreticalByRole = {
  ar_bspp: 9,
  bea: 6,
  ca_ba: 10,
  ccr: 0,
  cd: 4,
  celd: 2,
  cesd: 1,
  cmo_appui: 1,
  cmo_san: 1,
  crac: 3,
  epa_epsa: 13 + 23 + 4, // = epa + epan + epsa
  esav: 3,
  esavi: 3,
  fa: 10,
  fmogp: 3,
  fpt: 9,
  fptl: 29,
  fptlhp: 1,
  pse_omni_pump: 44,
  pse_solo: 15,
  vec: 1,
  vid: 15,
  vigi: 12,
  vimp: 1,
  vpc_tac: 4,
  vra: 1,
  vi_nrbc: 5,
  vrcp: 1,
  vrex: 1,
  vrsd: 1,
  vsav_omni: 44,
  vsav_solo: 105,
  vsis: 2,
};
