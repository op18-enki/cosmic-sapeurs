import { createSelector } from "@reduxjs/toolkit";
import { selectedAreaSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { availabilityChangedEventsByIdSelector } from "core-logic/useCases/cover-ops/selectors/vehiclesByIdForRoleSelector";
import { AvailabilityChangedEvent, VehicleEvent } from "interfaces";

import * as R from "ramda";

export const latestVehicleChangedEventsByIdForAreaSelector = createSelector(
  availabilityChangedEventsByIdSelector,
  selectedAreaSelector,
  (availabilityChangedEventsById, area) =>
    R.pipe<
      Record<string, AvailabilityChangedEvent>,
      AvailabilityChangedEvent[],
      AvailabilityChangedEvent[],
      VehicleEvent[]
    >(
      R.values,
      R.filter<AvailabilityChangedEvent>(
        (availabilityChangedEvent) =>
          availabilityChangedEvent.home_area === area,
      ),
      R.map((availabilityEvent) => availabilityEvent.latest_event_data),
    )(availabilityChangedEventsById),
);
