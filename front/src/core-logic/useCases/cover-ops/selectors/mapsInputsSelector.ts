import { createSelector } from "@reduxjs/toolkit";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import { lastOnGoingOpEventByOperationIdWithoutOpenStatusSelector } from "core-logic/useCases/cover-ops/selectors/lastOnGoingOpEventByOperationIdSelector";
import { omnibusDetailsByArea } from "core-logic/useCases/cover-ops/selectors/omnibusDetailsSelectors";
import type { AvailabilityChangedEvent } from "interfaces";
import { bsppAreaOptions } from "interfaces";
import { OmnibusBalance } from "interfaces/generated/OmnibusBalance";
import * as R from "ramda";
import type { AreaCounts, LeafletMapInputs } from "ui";

export const mapsInputsSelector = createSelector(
  availabilityChangedEventsByCoverOpsSubCategorySelector,
  lastOnGoingOpEventByOperationIdWithoutOpenStatusSelector,
  omnibusDetailsByArea,
  (
    availabilitiesByAreaByRole,
    ongoingOpChangedEventsByOperationId,
    omnibusDetailsByArea,
  ) =>
    bsppAreaOptions.reduce((acc, area) => {
      const availabilities: Record<
        CoverOpsVehicleSubCategory,
        number | undefined
      > = R.mapObjIndexed(
        R.pipe<
          AvailabilityChangedEvent[],
          AvailabilityChangedEvent[],
          AvailabilityChangedEvent,
          number | undefined
        >(
          R.filter<AvailabilityChangedEvent>(
            ({ home_area }) => area === home_area,
          ),
          R.last,
          (event) => event?.area_availability.available,
        ),
        availabilitiesByAreaByRole,
      );

      const numberOfFireOngoingOps = ongoingOpChangedEventsByOperationId.filter(
        (e) =>
          e.opening_infos.cause === "fire" &&
          e.opening_infos.address_area === area,
      ).length;

      const numberOfVictimOngoingOps =
        ongoingOpChangedEventsByOperationId.filter(
          (e) =>
            e.opening_infos.cause === "victim" &&
            e.opening_infos.address_area === area,
        ).length;

      const operations = {
        fire: numberOfFireOngoingOps,
        victim: numberOfVictimOngoingOps,
      };

      const omnibus: OmnibusBalance = R.countBy(
        R.prop("balance_kind"),
        omnibusDetailsByArea[area] ?? [],
      );

      const areaCounts: AreaCounts = { availabilities, operations, omnibus };

      return { ...acc, [area]: areaCounts };
    }, {} as LeafletMapInputs),
);
