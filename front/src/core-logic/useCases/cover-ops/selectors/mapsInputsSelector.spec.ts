import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { makeOmnibusDetail } from "core-logic/useCases/cover-ops/factories/makeOmnibusDetail";
import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import { mapsInputsSelector } from "core-logic/useCases/cover-ops/selectors/mapsInputsSelector";
import { createTestStore, expectToEqual } from "core-logic/useCases/test.utils";
import { Area } from "interfaces";
import * as R from "ramda";
import { DeepPartial } from "redux";
import type { LeafletMapInputs, AreaCounts } from "ui";

describe("map input selectors", () => {
  it("Get last operation event", () => {
    const vsavAvailabilityEvent = makeAvailabilityChangedEvent({
      role: "vsav_solo",
      home_area: "ANTO",
      area_availability: { available: 10, on_operation: 3, unavailable: 0 },
    });
    const pse1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
      home_area: "ANTO",
      area_availability: { available: 13, on_operation: 9, unavailable: 0 },
    });
    const pse2AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
      home_area: "CHPT",
      area_availability: { available: 3 },
    });

    const ope1Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope1",
      status: "first_vehicle_affected",
      opening_infos: { cause: "victim", address_area: "ASNI" },
    });
    const ope2Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope2",
      status: "first_vehicle_affected",
      opening_infos: { cause: "victim", address_area: "STOU" },
    });
    const opeStouBisEvent = makeOngoingOpChangedEvent({
      raw_operation_id: "ope-stou",
      status: "some_affected_vehicle_changed",
      opening_infos: { cause: "victim", address_area: "STOU" },
    });
    const ope3Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope3",
      status: "first_vehicle_affected",
      opening_infos: { cause: "fire", address_area: "CHPT" },
    });
    const ope4Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope4",
      opening_infos: { cause: "fire", address_area: "CHPT" },
      status: "all_vehicles_released",
    });
    const ope5Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope5",
      opening_infos: { cause: "fire", address_area: "CHPT" },
      status: "opened",
    });
    const ope6Event = makeOngoingOpChangedEvent({
      raw_operation_id: "ope6",
      opening_infos: { cause: "fire", address_area: "CHPT" },
      status: "opened_by_snapshot",
    });

    const omniDetail1 = makeOmnibusDetail({
      area: "STOU",
      balance_kind: "both_available",
    });
    const omniDetail2 = makeOmnibusDetail({
      area: "STOU",
      balance_kind: "both_available",
    });

    const { store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [
          ope1Event,
          ope2Event,
          ope3Event,
          ope4Event,
          ope5Event,
          ope6Event,
          opeStouBisEvent,
        ],
      },
      cover: {
        availabilityChangedEvents: [
          vsavAvailabilityEvent,
          pse1AvailabilityEvent,
          pse2AvailabilityEvent,
        ],
      },
      omnibus: {
        details: [omniDetail1, omniDetail2],
      },
    });
    const actual = mapsInputsSelector(store.getState());

    const expected: LeafletMapInputs = {
      ...makeLeafletMapInputForArea("ANTO", {
        availabilities: { ep: 13, sap: 10, epWithOmni: 13, sapWithOmni: 10 },
        operations: { fire: 0, victim: 0 },
      }),
      ...makeLeafletMapInputForArea("CHPT", {
        availabilities: { ep: 3, epWithOmni: 3 },
        operations: { fire: 1, victim: 0 },
      }),
      ...makeLeafletMapInputForArea("ASNI", {
        operations: { fire: 0, victim: 1 },
      }),
      ...makeLeafletMapInputForArea("STOU", {
        operations: { fire: 0, victim: 2 },
        omnibus: { both_available: 2 },
      }),
    };

    expectMapInputToEqual(actual, expected);
  });

  describe("mapsInputsSelector for ongoingOps", () => {
    it("gets the number of operation by area", () => {
      const event1 = makeOngoingOpChangedEvent({
        raw_operation_id: "1",
        timestamp: "2022-01-01T12:00:00",
        status: "first_vehicle_affected",
        opening_infos: { cause: "victim", address_area: "STOU" },
      });

      const event2 = makeOngoingOpChangedEvent({
        raw_operation_id: "2",
        timestamp: "2022-01-01T12:05:00",
        status: "first_vehicle_affected",
        opening_infos: { cause: "victim", address_area: "STOU" },
      });

      const event1vehicleChanged = makeOngoingOpChangedEvent({
        raw_operation_id: "1",
        timestamp: "2022-01-01T12:10:00",
        status: "some_affected_vehicle_changed",
        opening_infos: { cause: "victim", address_area: "STOU" },
      });

      const event3released = makeOngoingOpChangedEvent({
        raw_operation_id: "3",
        timestamp: "2022-01-01T12:10:00",
        status: "all_vehicles_released",
        opening_infos: { cause: "victim", address_area: "STOU" },
      });

      const { store } = createTestStore({
        ops: {
          ongoingOpChangedEvents: [
            event1,
            event2,
            event1vehicleChanged,
            event3released,
          ],
        },
      });

      const matched = mapsInputsSelector(store.getState());
      expectToEqual(matched.STOU!.operations, {
        victim: 2,
        fire: 0,
      });
    });
  });
});

const defaultAreaCounts: AreaCounts = {
  availabilities: {
    ep: undefined,
    sap: undefined,
    epWithOmni: undefined,
    sapWithOmni: undefined,
  },
  operations: { fire: undefined, victim: undefined },
  omnibus: {},
};

const makeLeafletMapInputForArea = (
  area: Area,
  areaCounts: DeepPartial<AreaCounts>,
) => {
  return {
    [area]: R.mergeDeepRight(defaultAreaCounts, areaCounts),
  } as LeafletMapInputs;
};

const expectMapInputToEqual = (
  actual: LeafletMapInputs,
  expected: LeafletMapInputs,
) => expect(actual).toMatchObject(expected);
