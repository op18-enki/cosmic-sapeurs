import type { AppEvent } from "core-logic/ports/PubSubClient";
import { InstantOnceRefresher } from "core-logic/ports/Refresher";
import { InMemoryPubSubClient } from "core-logic/secondaryAdapters/InMemoryPubSubClient";
import type { ReduxStore } from "core-logic/setup/store.config";
import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";
import type { OngoingOpChangedEvent } from "interfaces";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import { opsActions } from "./ops.slice";
import {
  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,
} from "./thunks/subscribeToOngoingOpChangedEventsThunk";

describe("Update Ops", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to ongoing op changed event, then unsubscribes", async () => {
    const timestamp_0 = "2021-03-03T16:01:00.0";
    const payload: OngoingOpChangedEvent = {
      timestamp: timestamp_0,
      raw_operation_id: "1",
      status: "opened",
      affected_vehicles: ["vehicle_a"],
      opening_infos: {
        cause: "fire" as const,
        address_area: "CHPT" as const,
        raw_procedure: "red",
        departure_criteria: "301",
        latitude: 68258,
        longitude: 68258,
        opening_timestamp: "2021-03-03T16:01:00.0",
      },
    };

    const incomingEvent: AppEvent = {
      topic: "ongoingOpChanged",
      payload,
    };

    store.dispatch(loopOverDispatchBatchThunk());

    store.dispatch(subscribeToOngoingOpChangedEventsThunk());

    publishEvent(incomingEvent);

    expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [payload],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    const payload2: OngoingOpChangedEvent = {
      ...payload,
      timestamp: "2021-03-03T16:01:30.0",
      status: "first_vehicle_affected",
      opening_infos: {
        ...payload.opening_infos,
        opening_timestamp: "2021-03-03T16:01:00.0",
      },
    };

    const incomingEvent2: AppEvent = {
      topic: "ongoingOpChanged",
      payload: payload2,
    };

    publishEvent(incomingEvent2);

    expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [payload, payload2],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    const incomingEvent3: AppEvent = {
      topic: "ongoingOpChanged",
      payload: { ...payload2 },
    };

    store.dispatch(unsubscribeFromOngoingOpChangedEventsThunk());

    publishEvent(incomingEvent3);

    expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [payload, payload2],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });

  it("Purges old ongoingOp changed events", async () => {
    const ongoingOp1 = makeOngoingOpChangedEvent({
      raw_operation_id: "1",
      timestamp: "2021-03-03T15:59:00.000Z",
    });
    const ongoingOp2 = makeOngoingOpChangedEvent({
      raw_operation_id: "2",
      timestamp: "2021-03-03T16:01:00.000Z",
    });
    const ongoingOp3 = makeOngoingOpChangedEvent({
      raw_operation_id: "3",
      timestamp: "2021-03-03T20:00:00.000Z",
    });

    const { store: _store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [ongoingOp1, ongoingOp2, ongoingOp3],
      },
    });
    const _expectStateToEqual = buildExpectStateToEqual(
      _store,
      _store.getState(),
    );

    _store.dispatch(opsActions.purgeOldOngoingOps());

    _expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [ongoingOp2, ongoingOp3],
      },
    });
  });

  it("Add filter on Ops", async () => {
    store.dispatch(
      opsActions.addFilter({ field: "raw_procedure", valueToMatch: "R" }),
    );
    expectStateToEqual({ ops: { activeFilters: { raw_procedure: ["R"] } } });
  });

  it("Remove filter on Ops", async () => {
    const { store: _store } = createTestStore({
      ops: { activeFilters: { raw_procedure: ["R", "O"] } },
    });

    _store.dispatch(
      opsActions.removeFilter({ field: "raw_procedure", valueToMatch: "R" }),
    );
    const _expectStateToEqual = buildExpectStateToEqual(
      _store,
      _store.getState(),
    );
    _expectStateToEqual({ ops: { activeFilters: { raw_procedure: ["O"] } } });
  });

  it("Add filters then remove them", async () => {
    store.dispatch(
      opsActions.addFilter({ field: "raw_procedure", valueToMatch: "R" }),
    );
    store.dispatch(
      opsActions.removeFilter({ field: "raw_procedure", valueToMatch: "R" }),
    );
    expectStateToEqual({ ops: { activeFilters: {} } });
  });

  it("Toggles with vehicle only mode", () => {
    store.dispatch(opsActions.withVehicleOnlyToggled());
    expectStateToEqual({ ops: { withVehicleOnly: true } });
    store.dispatch(opsActions.withVehicleOnlyToggled());
    expectStateToEqual({ ops: { withVehicleOnly: false } });
  });

  it("Change the value of the delay before INU start blinking", async () => {
    store.dispatch(opsActions.setInuBlinkTime({ inuBlinkStartTime: 314 }));
    expectStateToEqual({ ops: { inuBlinkStartTime: 314 } });
  });

  const publishEvent = (event: AppEvent) => {
    pubSubClient.publish(event);
    renderingRefresher.runLoop();
  };
});
