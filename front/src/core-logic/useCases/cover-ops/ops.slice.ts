import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  keepOnlyRecentEvents,
  pushAfterLatestTimestamp,
} from "core-logic/useCases/cover-ops/coverOps.utils";
import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";
import type { OngoingOpChangedEvent, OperationOpeningInfos } from "interfaces";
import { findBackwards, id, removeAtIndex } from "utils";

export type FiltrableField = Extract<
  keyof OperationOpeningInfos,
  "cause" | "raw_cause" | "raw_procedure" | "departure_criteria"
>;

type OpsState = {
  ongoingOpChangedEvents: OngoingOpChangedEvent[];
  isFetchingOps: boolean;
  fetchOpsError?: string;
  activeFilters: Partial<Record<FiltrableField, string[]>>;
  withVehicleOnly: boolean;
  inuBlinkStartTime: number;
};

export const opsSliceName = "ops";

const opsSlice = createSlice({
  name: opsSliceName,
  initialState: id<OpsState>({
    ongoingOpChangedEvents: [],
    isFetchingOps: false,
    activeFilters: {},
    inuBlinkStartTime: 7,
    withVehicleOnly: false,
  }),
  reducers: {
    purgeOldOngoingOps: ({ ongoingOpChangedEvents, ...rest }) => {
      const events = keepOnlyRecentEvents(ongoingOpChangedEvents);
      return {
        ...rest,
        ongoingOpChangedEvents: events,
      };
    },
    ongoingOpChanged: (
      state,
      { payload }: PayloadAction<OngoingOpChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.ongoingOpChangedEvents,
        (evt) => evt.raw_operation_id === payload.raw_operation_id,
      );

      const previousEventHasSameStatus =
        previousEvent?.status === payload.status;
      const removePreviousEvent = () =>
        state.ongoingOpChangedEvents.splice(previousEventIndex!, 1);

      if (previousEventHasSameStatus) removePreviousEvent();
      pushAfterLatestTimestamp(state.ongoingOpChangedEvents, payload);
    },

    opsRequested: (state) => {
      state.isFetchingOps = true;
    },
    opsFetched: (state, action: PayloadAction<OngoingOpChangedEvent[]>) => {
      state.ongoingOpChangedEvents = action.payload;
      state.isFetchingOps = false;
    },
    opsCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingOps = false;
      state.fetchOpsError = action.payload;
    },
    addFilter: (
      state,
      action: PayloadAction<{
        field: FiltrableField;
        valueToMatch: string;
      }>,
    ) => {
      const activeFiltersForField = state.activeFilters[action.payload.field];
      if (!activeFiltersForField) {
        state.activeFilters[action.payload.field] = [];
      }

      state.activeFilters[action.payload.field]!.push(
        action.payload.valueToMatch,
      );
    },
    withVehicleOnlyToggled: (state) => ({
      ...state,
      withVehicleOnly: !state.withVehicleOnly,
    }),
    removeFilter: (
      state,
      action: PayloadAction<{
        field: FiltrableField;
        valueToMatch: string;
      }>,
    ) => {
      const arrayFilters = state.activeFilters[action.payload.field];
      const index = arrayFilters?.findIndex(
        (valueToMatch) => valueToMatch === action.payload.valueToMatch,
      );
      if (index !== undefined && index > -1)
        removeAtIndex(arrayFilters!, index);

      if (!arrayFilters?.length)
        delete state.activeFilters[action.payload.field];
    },
    setInuBlinkTime: (
      state,
      action: PayloadAction<{
        inuBlinkStartTime: number;
      }>,
    ) => {
      return {
        ...state,
        inuBlinkStartTime: action.payload.inuBlinkStartTime,
      };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(resetCoverOpsActions.resetFinished, (state, action) => {
      state.ongoingOpChangedEvents = action.payload.ongoing_ops;
    });
  },
});

export const { reducer: opsReducer, actions: opsActions } = opsSlice;
