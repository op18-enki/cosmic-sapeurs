import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { ReduxStore } from "core-logic/setup/store.config";
import { fetchOmnibusThunk } from "core-logic/useCases/cover-ops/thunks/fetchOmnibusThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";
import { OmnibusDetail } from "interfaces";

describe("Fetch Omnibus", () => {
  let store: ReduxStore;
  let httpClient: InMemoryHttpClient;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    httpClient = testStore.httpClient;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("reflects availabilities given received data", async () => {
    const omnibusDetails: OmnibusDetail[] = [
      {
        area: "STOU",
        balance_kind: "only_pse_available",
        pse_id: "my_pse_id",
        vsav_id: "my_vsav_id",
      },
    ];

    httpClient.setOmnibusResponse({
      timestamp: "2020:01:01T12",
      details: omnibusDetails,
    });

    await store.dispatch(fetchOmnibusThunk());
    expectStateToEqual({
      omnibus: {
        details: omnibusDetails,
      },
    });
  });
});
