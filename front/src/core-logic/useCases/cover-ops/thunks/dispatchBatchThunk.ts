import { AppThunk } from "core-logic/setup/store.config";

const dispatchBatchId = "dispatchBatch";

export const loopOverDispatchBatchThunk =
  (): AppThunk =>
  async (dispatch, _, { renderingRefresher, batchHandler }) =>
    renderingRefresher.addToLoop(() => {
      batchHandler.dispatchBatch(dispatch);
    }, dispatchBatchId);

export const stopLoopOverDispatchBatchThunk =
  (): AppThunk =>
  async (dispatch, _, { renderingRefresher }) =>
    renderingRefresher.removeFromLoop(dispatchBatchId);
