import { AppThunk } from "core-logic/setup/store.config";
import { linesHistoryActions } from "core-logic/useCases/cover-ops/linesHistory.slice";

export const fetchFrontLinesHistory =
  (): AppThunk =>
  async (dispatch, _, { httpClient }) => {
    dispatch(linesHistoryActions.historyRequested());
    const history = await httpClient.fetchHistory();
    dispatch(linesHistoryActions.historyFetched(history));
  };
