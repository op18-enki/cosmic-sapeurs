import {
  fireLineInputSelector,
  victimLineInputSelector,
} from "core-logic/useCases/cover-ops/selectors/lineInputSampleSelector";
import { AppThunk } from "core-logic/setup/store.config";
import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";

export const triggerResetCoverOpsThunk =
  (): AppThunk =>
  async (dispatch, getState, { httpClient }) => {
    dispatch(resetCoverOpsActions.resetRequested());
    const state = getState();

    try {
      await httpClient.resetCoverOps({
        victim: victimLineInputSelector(state),
        fire: fireLineInputSelector(state),
      });
    } catch (e: any) {
      dispatch(resetCoverOpsActions.resetFailed(e.message));
    }
  };
