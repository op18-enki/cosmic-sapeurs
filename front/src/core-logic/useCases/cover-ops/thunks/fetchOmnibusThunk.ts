import { AppThunk } from "core-logic/setup/store.config";
import { omnibusActions } from "core-logic/useCases/cover-ops/omnibus.slice";

export const fetchOmnibusThunk =
  (): AppThunk =>
  async (dispatch, state, { httpClient }) => {
    try {
      const omnibusBalance = await httpClient.fetchOmnibus();
      dispatch(omnibusActions.omnibusBalanceChanged(omnibusBalance));
    } catch (e: any) {
      console.log("Fetch omnibus failed :", e.message);
    }
  };
