import { AppThunk } from "core-logic/setup/store.config";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import type { Hours } from "core-logic/useCases/cover-ops/interactions.slice";

export const fetchCoverThunk =
  (hours: Hours): AppThunk =>
  async (dispatch, _, { httpClient }) => {
    dispatch(coverActions.coverRequested());
    try {
      const availability_events = await httpClient.fetchCover(hours);
      dispatch(coverActions.coverFetched(availability_events));
    } catch (e: any) {
      dispatch(coverActions.coverCouldNotFetch(e.message));
    }
  };
