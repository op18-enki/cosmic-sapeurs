import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { ReduxStore } from "core-logic/setup/store.config";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { fetchCoverThunk } from "core-logic/useCases/cover-ops/thunks/fetchCoverThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";
import type {
  Availability,
  AvailabilityChangedEvent,
  VehicleEvent,
} from "interfaces";

const allAvailabilitiesAt0: Availability = {
  available: 0,
  on_operation: 0,
  recoverable: 0,
  unavailable: 0,
  unavailable_omnibus: 0,
};

describe("Fetch Cover", () => {
  let store: ReduxStore;
  let httpClient: InMemoryHttpClient;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    httpClient = testStore.httpClient;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("shows fetch is ongoing", async () => {
    await store.dispatch(coverActions.coverRequested());
    expectStateToEqual({ cover: { isFetchingCover: true } });
  });

  it("reflects availabilities given received data", async () => {
    const latest_event_data: VehicleEvent = {
      home_area: "STOU",
      raw_status: "Yeah",
      raw_vehicle_id: "vehicle_id",
      role: "vsav_solo",
      status: "arrived_on_intervention",
      timestamp: "2021-03-03T12:00:00.000Z",
      vehicle_name: "VSAV 123",
      availability_kind: "on_operation",
    };

    const availabilityChangedEvent: AvailabilityChangedEvent = {
      area_availability: {
        ...allAvailabilitiesAt0,
        on_operation: 1,
        unavailable: 3,
      },
      bspp_availability: { ...allAvailabilitiesAt0, unavailable: 2 },
      home_area: "STOU",
      raw_vehicle_id: "1",
      role: "epa_epsa",
      timestamp: "2020-01-01T12:00:00.1",
      latest_event_data,
      availability_changed: true,
    };

    httpClient.setCoverResponse([availabilityChangedEvent]);

    await store.dispatch(fetchCoverThunk(3));
    expectStateToEqual({
      cover: {
        isFetchingCover: false,
        availabilityChangedEvents: [availabilityChangedEvent],
      },
    });
  });

  it("warns when fetch fails", async () => {
    httpClient.setError("Could not fetch!");
    await store.dispatch(fetchCoverThunk(3));
    expectStateToEqual({
      cover: { fetchCoverError: "Could not fetch!", isFetchingCover: false },
    });
  });
});
