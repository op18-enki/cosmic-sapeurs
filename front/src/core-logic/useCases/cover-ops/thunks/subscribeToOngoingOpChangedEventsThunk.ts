import { AppThunk } from "../../../setup/store.config";
import { opsActions } from "../ops.slice";

const purgeOldOngoingOpEvents = "purgeOldOngoingOpEvents";

export const subscribeToOngoingOpChangedEventsThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient, purgeRefresher, batchHandler }) => {
    pubSubClient.subscribe("ongoingOpChanged", (event) => {
      batchHandler.addForDispatch(opsActions.ongoingOpChanged(event.payload));
    });

    purgeRefresher.addToLoop(() => {
      dispatch(opsActions.purgeOldOngoingOps());
    }, purgeOldOngoingOpEvents);
  };

export const unsubscribeFromOngoingOpChangedEventsThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient, purgeRefresher }) => {
    pubSubClient.unsubscribe("ongoingOpChanged");
    purgeRefresher.removeFromLoop(purgeOldOngoingOpEvents);
  };
