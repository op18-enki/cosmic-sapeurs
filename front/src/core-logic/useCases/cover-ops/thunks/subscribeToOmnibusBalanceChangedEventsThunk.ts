import { AppThunk } from "../../../setup/store.config";
import { omnibusActions } from "../omnibus.slice";

export const subscribeToOmnibusBalanceChangedEventsThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient, batchHandler }) => {
    pubSubClient.subscribe("omnibusBalanceChanged", (event) => {
      batchHandler.addForDispatch(
        omnibusActions.omnibusBalanceChanged(event.payload),
      );
    });
  };

export const unsubscribeFromOmnibusBalanceChangedEventsThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient }) => {
    pubSubClient.unsubscribe("omnibusBalanceChanged");
  };
