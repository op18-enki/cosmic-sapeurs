import { AppThunk } from "core-logic/setup/store.config";
import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";

export const subscribeToResetCoverOpsRequestedThunk =
  (): AppThunk =>
  async (dispatch, state, { pubSubClient }) => {
    pubSubClient.subscribe("resetCoverOpsRequested", (e) => {
      dispatch(resetCoverOpsActions.resetRequestedFromExternal());
    });
  };

export const unsubscribeFromResetCoverOpsRequestedThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient }) => {
    pubSubClient.unsubscribe("resetCoverOpsRequested");
  };

export const subscribeToResetCoverOpsFinishedThunk =
  (): AppThunk =>
  async (dispatch, state, { pubSubClient }) => {
    pubSubClient.subscribe("resetCoverOpsFinished", (event) =>
      dispatch(resetCoverOpsActions.resetFinished(event.payload)),
    );
  };

export const unsubscribeFromResetCoverOpsFinishedThunk =
  (): AppThunk =>
  async (dispatch, _, { pubSubClient }) => {
    pubSubClient.unsubscribe("resetCoverOpsFinished");
  };
