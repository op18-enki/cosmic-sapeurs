import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { VehicleRole } from "interfaces";
import { id, replaceAt } from "utils";
import type { Flavor } from "utils";
import * as R from "ramda";

export type PubSubStatus =
  | "notConnected"
  | "open"
  | "closed"
  | "error"
  | "tryingToReconnect";

export type Hours = Flavor<number, "Hours">;

export type Valorization = typeof valorizationOptions[number];
export const valorizationOptions = ["sap", "both", "ep"] as const;

type SelectedVehicleRoles = [
  VehicleRole[],
  VehicleRole[],
  VehicleRole[],
  VehicleRole[],
];

type AreVehicleLinkedTuple = [boolean, boolean, boolean, boolean];

type InteractionState = {
  pubSubStatus: PubSubStatus;
  numberOfHoursToShow: Hours;
  takeOmnibusIntoAccount: boolean;
  valorization: Valorization;
  selectedVehicleRoles: SelectedVehicleRoles;
  areVehicleLinked: AreVehicleLinkedTuple;
};

export const interactionSliceName = "interactions";

const interactionsSlice = createSlice({
  name: interactionSliceName,
  initialState: id<InteractionState>({
    pubSubStatus: "notConnected",
    numberOfHoursToShow: 2,
    takeOmnibusIntoAccount: true,
    valorization: "both",
    selectedVehicleRoles: [[], [], [], []],
    areVehicleLinked: [false, false, false, false],
  }),
  reducers: {
    pubSubStatusUpdated: (state, action: PayloadAction<PubSubStatus>) => {
      state.pubSubStatus = action.payload;
    },
    numberOfHoursToShowChanged: (state, action: PayloadAction<Hours>) => {
      state.numberOfHoursToShow = action.payload;
    },
    toggleShowOmnibusOnMap: (state) => {
      state.takeOmnibusIntoAccount = !state.takeOmnibusIntoAccount;
    },
    setValorization: (state, action: PayloadAction<Valorization>) => {
      state.valorization = action.payload;
    },
    setSelectedVehicleRoles: (
      state,
      action: PayloadAction<{
        roleToAdd: VehicleRole | null;
        index: number;
      }>,
    ) => ({
      ...state,
      selectedVehicleRoles: replaceAt(
        state.selectedVehicleRoles,
        action.payload.index,
        [action.payload.roleToAdd],
      ) as SelectedVehicleRoles,
    }),
    addSelectedVehicleRole: (
      state,
      action: PayloadAction<{
        rolesToAdd: VehicleRole[];
        index: number;
      }>,
    ): InteractionState => ({
      ...state,
      selectedVehicleRoles: replaceAt(
        state.selectedVehicleRoles,
        action.payload.index,
        R.uniq([
          ...state.selectedVehicleRoles[action.payload.index],
          ...action.payload.rolesToAdd,
        ]),
      ) as SelectedVehicleRoles,
    }),
    removeSelectedVehicleRole: (
      state,
      action: PayloadAction<{
        roleToDelete: VehicleRole | null;
        index: number;
      }>,
    ): InteractionState => ({
      ...state,
      selectedVehicleRoles: replaceAt(
        state.selectedVehicleRoles,
        action.payload.index,
        state.selectedVehicleRoles[action.payload.index].filter(
          (vehicleRole) => vehicleRole !== action.payload.roleToDelete,
        ),
      ) as SelectedVehicleRoles,
    }),
    clearSelectedVehicleRoles: (state, action: PayloadAction<number>) => ({
      ...state,
      selectedVehicleRoles: replaceAt(
        state.selectedVehicleRoles,
        action.payload,
        [],
      ) as SelectedVehicleRoles,
    }),
    areVehicleLinkedToggled: (
      state,
      action: PayloadAction<number>,
    ): InteractionState => ({
      ...state,
      areVehicleLinked: replaceAt(
        state.areVehicleLinked,
        action.payload,
        !state.areVehicleLinked[action.payload],
      ) as AreVehicleLinkedTuple,
    }),
  },
});

export const { reducer: interactionsReducer, actions: interactionsActions } =
  interactionsSlice;
