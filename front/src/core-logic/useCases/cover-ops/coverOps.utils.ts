import { subHours } from "date-fns";
import type {
  AvailabilityChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";
import * as R from "ramda";
import { mutateLast, WithTimestamp } from "utils";

const maxNumberOfHoursToShow = 4;

export const keepOnlyRecentEvents = <
  E extends AvailabilityChangedEvent | OngoingOpChangedEvent,
>(
  events: E[],
): E[] => {
  const mostRecentEvent = R.last(events);
  if (!mostRecentEvent) return events;
  const fromDate = subHours(
    new Date(mostRecentEvent.timestamp),
    maxNumberOfHoursToShow,
  );
  return events.filter((e) => fromDate < new Date(e.timestamp));
};

const isTimestampAfterLast = <T extends WithTimestamp>(
  events: T[],
  newEvent: T,
): boolean => {
  const lastEvent = R.last(events);
  if (!lastEvent) return true;
  return lastEvent.timestamp < newEvent.timestamp;
};

export const mutateLastWithMostRecentTimestamp = <T extends WithTimestamp>(
  array: T[],
  newElement: T,
) => {
  const newLast = isTimestampAfterLast(array, newElement)
    ? newElement
    : { ...newElement, timestamp: R.last(array)!.timestamp };

  mutateLast(array, newLast);
};

export const pushAfterLatestTimestamp = <T extends WithTimestamp>(
  array: T[],
  newElement: T,
) => {
  const newElementToPush = isTimestampAfterLast(array, newElement)
    ? newElement
    : { ...newElement, timestamp: R.last(array)!.timestamp };
  array.push(newElementToPush);
};

export const getLatestForEachRawId = (
  events: AvailabilityChangedEvent[],
): AvailabilityChangedEvent[] => {
  // TODO : refactor with pipe
  const eventsByRawVehicleId = R.groupBy((e) => e.raw_vehicle_id, events);
  return R.values(
    R.mapObjIndexed((events) => R.last(events)!, eventsByRawVehicleId),
  );
};
