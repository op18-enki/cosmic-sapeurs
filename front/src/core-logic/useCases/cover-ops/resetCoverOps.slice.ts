import { ResetCoverOpsFinishedEvent } from "interfaces";
import { id } from "utils";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type ResetState = {
  isResetting: boolean;
  error?: string;
};

export const resetCoverOpsName = "resetCoverOps";

const resetCoverOpsSlice = createSlice({
  name: resetCoverOpsName,
  initialState: id<ResetState>({
    isResetting: false,
  }),
  reducers: {
    resetRequestedFromExternal: (state): ResetState => ({
      ...state,
      isResetting: true,
    }),
    resetRequested: (state): ResetState => ({ ...state, isResetting: true }),
    resetFinished: (
      state,
      action: PayloadAction<ResetCoverOpsFinishedEvent>,
    ): ResetState => ({
      ...state,
      isResetting: false,
    }),
    resetFailed: (state, action: PayloadAction<string>): ResetState => ({
      ...state,
      isResetting: false,
      error: action.payload,
    }),
  },
});

export const { reducer: resetCoverOpsReducer, actions: resetCoverOpsActions } =
  resetCoverOpsSlice;
