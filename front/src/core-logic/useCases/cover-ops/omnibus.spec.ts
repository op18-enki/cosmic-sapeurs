import { InMemoryPubSubClient } from "../../secondaryAdapters/InMemoryPubSubClient";
import { ReduxStore } from "../../setup/store.config";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import {
  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,
} from "./thunks/subscribeToOmnibusBalanceChangedEventsThunk";
import { InstantOnceRefresher } from "../../ports/Refresher";
import { OmnibusDetail } from "interfaces";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";

describe("Update Cover", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to : omnibus balance changed events, then unsubscribes", async () => {
    await store.dispatch(loopOverDispatchBatchThunk());
    await store.dispatch(subscribeToOmnibusBalanceChangedEventsThunk());
    const omnibusDetails: OmnibusDetail[] = [
      {
        area: "ANTO",
        balance_kind: "both_available",
        pse_id: "my_pse_id",
        vsav_id: "my_vsav_id",
      },
    ];

    pubSubClient.publish({
      topic: "omnibusBalanceChanged",
      payload: { timestamp: "", details: omnibusDetails },
    });
    renderingRefresher.runLoop();

    expectStateToEqual({
      omnibus: {
        details: omnibusDetails,
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    await store.dispatch(unsubscribeFromOmnibusBalanceChangedEventsThunk());

    pubSubClient.publish({
      topic: "omnibusBalanceChanged",
      payload: { timestamp: "", details: [] },
    });
    renderingRefresher.runLoop();

    expectStateToEqual({
      omnibus: {
        details: omnibusDetails,
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });
});
