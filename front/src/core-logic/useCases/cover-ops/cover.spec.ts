import { InMemoryPubSubClient } from "../../secondaryAdapters/InMemoryPubSubClient";
import { ReduxStore } from "../../setup/store.config";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import {
  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,
} from "./thunks/subscribeToAvailabilityChangedEventsThunk";
import { InstantOnceRefresher } from "../../ports/Refresher";
import { AppEvent } from "../../ports/PubSubClient";
import { AvailabilityChangedEvent, VehicleEvent } from "interfaces";
import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";

const latest_event_data: VehicleEvent = {
  home_area: "STOU",
  raw_status: "Yeah",
  raw_vehicle_id: "vehicle_id",
  role: "vsav_solo",
  status: "arrived_on_intervention",
  timestamp: "2021-03-03T12:00:00.000Z",
  vehicle_name: "VSAV 123",
  availability_kind: "on_operation",
};

describe("Update Cover", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to : availability changed events, then unsubscribes", async () => {
    const timestamp_0 = new Date().toISOString();
    const count_0 = 2;

    store.dispatch(loopOverDispatchBatchThunk());
    store.dispatch(subscribeToAvailabilityChangedEventsThunk());

    const payload: AvailabilityChangedEvent = {
      timestamp: timestamp_0,
      area_availability: {
        available: count_0,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      bspp_availability: {
        available: count_0,
        unavailable: 0,
        on_operation: 0,
        recoverable: 0,
        unavailable_omnibus: 0,
      },
      role: "vsav_solo",
      home_area: "STOU",
      raw_vehicle_id: "1",
      latest_event_data,
      availability_changed: true,
    };

    const incomingEvent: AppEvent = {
      topic: "availabilityChanged",
      payload,
    };

    publishEvent(incomingEvent);

    expectStateToEqual({
      cover: {
        availabilityChangedEvents: [payload],
      },
      interactions: { pubSubStatus: "open" },
    });

    const incomingEvent2: AppEvent = {
      topic: "availabilityChanged",
      payload: {
        ...payload,
        latest_event_data: {
          ...payload.latest_event_data,
          availability_kind: "recoverable",
        },
      },
    };

    publishEvent(incomingEvent2);

    expectStateToEqual({
      cover: {
        availabilityChangedEvents: [payload, incomingEvent2.payload],
      },
      interactions: { pubSubStatus: "open" },
    });

    const incomingEvent3: AppEvent = {
      topic: "availabilityChanged",
      payload: {
        ...payload,
        latest_event_data: {
          ...payload.latest_event_data,
          availability_kind: "available",
        },
      },
    };

    store.dispatch(unsubscribeFromAvailabilityChangedEventsThunk());

    pubSubClient.publish(incomingEvent3);
    renderingRefresher.runLoop();

    expectStateToEqual({
      cover: {
        availabilityChangedEvents: [payload, incomingEvent2.payload],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });

  it("Purges old availability changed events", async () => {
    const availabilityVehicle1beforeDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "1",
      timestamp: "2021-03-03T12:00:00.000Z",
    });
    const availabilityVehicle2beforeDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "2",
      timestamp: "2021-03-03T11:00:00.000Z",
    });
    const availabilityVehicle1afterDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "1",
      timestamp: "2021-03-03T20:00:00.000Z",
    });

    const { store: _store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          availabilityVehicle1beforeDate,
          availabilityVehicle2beforeDate,
          availabilityVehicle1afterDate,
        ],
      },
    });
    const _expectStateToEqual = buildExpectStateToEqual(
      _store,
      _store.getState(),
    );

    _store.dispatch(coverActions.purgeOldAvailabilityEvents());

    _expectStateToEqual({
      cover: {
        availabilityChangedEvents: [
          availabilityVehicle2beforeDate,
          availabilityVehicle1afterDate,
        ],
      },
    });
  });

  const publishEvent = (event: AppEvent) => {
    pubSubClient.publish(event);
    renderingRefresher.runLoop();
  };
});
