import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { LineInput } from "ui";
import { id } from "utils";
import { FrontLinesHistory as FrontLinesHistoryFromBack } from "interfaces";
import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";

type VictimLinesInput = LineInput<"sap"> | LineInput<"victim">;
type FireLinesInput = LineInput<"ep"> | LineInput<"fire">;

export type FrontLinesHistory = {
  victim: VictimLinesInput[];
  fire: FireLinesInput[];
};

// for type check only :
// prettier-ignore
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const _isAssignable = (history: FrontLinesHistory): FrontLinesHistoryFromBack => history;

type LinesHistoryState = {
  isFetchingHistory: boolean;
  victim: VictimLinesInput[];
  fire: FireLinesInput[];
};

const linesHistorySlice = createSlice({
  name: "linesHistory",
  initialState: id<LinesHistoryState>({
    isFetchingHistory: false,
    victim: [],
    fire: [],
  }),
  reducers: {
    historyRequested: (state) => {
      state.isFetchingHistory = true;
    },
    historyFetched: (state, action: PayloadAction<FrontLinesHistory>) => ({
      isFetchingHistory: false,
      ...action.payload,
    }),
  },
  extraReducers: (builder) => {
    builder.addCase(resetCoverOpsActions.resetFinished, (state, action) => ({
      isFetchingHistory: false,
      ...(action.payload.front_lines_history as FrontLinesHistory),
    }));
  },
});

export const {
  reducer: linesHistoryReducer,
  actions: linesHistoryActions,
  name: linesHistorySliceName,
} = linesHistorySlice;
