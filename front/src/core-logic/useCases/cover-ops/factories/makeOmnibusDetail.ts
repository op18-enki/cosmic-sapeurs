import { DeepPartial } from "@reduxjs/toolkit";
import type { OmnibusDetail } from "interfaces";
import * as R from "ramda";

const defaultOngoingOpChangedEvent: OmnibusDetail = {
  area: "STOU",
  balance_kind: "both_available",
  pse_id: "default_pse_id",
  vsav_id: "default_vsav_id",
};

export const makeOmnibusDetail = (
  props: DeepPartial<OmnibusDetail>,
): OmnibusDetail => {
  return R.mergeDeepRight(defaultOngoingOpChangedEvent, props);
};
