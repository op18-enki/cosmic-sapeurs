import type { OngoingOpChangedEvent } from "interfaces";

const defaultOngoingOpChangedEvent: OngoingOpChangedEvent = {
  timestamp: "2021-03-10T20:00:00.000Z",
  raw_operation_id: "operation_id",
  affected_vehicles: [],
  status: "opened",
  opening_infos: {
    address_area: "STOU",
    cause: "victim",
    departure_criteria: "301",
    raw_procedure: "red",
    latitude: 48.8,
    longitude: 2.3,
    opening_timestamp: "2021-03-10T20:00:00.0",
  },
};

type MakeOngoingOpChangedEventProps = Partial<
  Omit<OngoingOpChangedEvent, "opening_infos">
> & {
  opening_infos?: Partial<OngoingOpChangedEvent["opening_infos"]>;
};

export const makeOngoingOpChangedEvent = (
  props: MakeOngoingOpChangedEventProps = {},
): OngoingOpChangedEvent => {
  return {
    ...defaultOngoingOpChangedEvent,
    ...(props.opening_infos
      ? {
          opening_infos: {
            ...defaultOngoingOpChangedEvent.opening_infos,
            ...props.opening_infos,
          },
        }
      : {}),
    ...(props as Partial<OngoingOpChangedEvent>),
  };
};
