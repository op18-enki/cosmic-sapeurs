import { Availability } from "interfaces";

const defaultAvailability: Partial<Availability> = {
  available: 0,
  on_operation: 0,
  unavailable: 0,
  recoverable: 0,
};

export const makeAvailability = (
  availability: Partial<Availability>,
): Partial<Availability> => ({ ...defaultAvailability, ...availability });
