import { DeepPartial } from "@reduxjs/toolkit";
import type { AvailabilityChangedEvent, AvailabilityKind } from "interfaces";
import * as R from "ramda";

type AvailabilityChangedEventWithoutLatestEventData = Omit<
  AvailabilityChangedEvent,
  "latest_event_data"
>;

const defaultAvailabilityChangedEventWithoutLatestEventData: AvailabilityChangedEventWithoutLatestEventData =
  {
    timestamp: "2021-03-10T20:00:00.000Z",
    bspp_availability: {
      available: 10,
      unavailable: 20,
      on_operation: 30,
      recoverable: 40,
      unavailable_omnibus: 12,
    },
    area_availability: {
      available: 3,
      unavailable: 4,
      on_operation: 2,
      recoverable: 0,
      unavailable_omnibus: 1,
    },
    role: "vsav_solo",
    home_area: "STOU",
    raw_vehicle_id: "vehicleA",
    previous_raw_operation_id: "operationY",
    availability_changed: true,
  };

export const makeAvailabilityChangedEvent = ({
  availabilityKind,
  ...props
}: DeepPartial<
  AvailabilityChangedEventWithoutLatestEventData & {
    availabilityKind: AvailabilityKind;
  }
> = {}): AvailabilityChangedEvent => {
  const availabilityChangedEventWithoutLatestEventData = R.mergeDeepRight(
    defaultAvailabilityChangedEventWithoutLatestEventData,
    props,
  );

  return {
    ...availabilityChangedEventWithoutLatestEventData,
    latest_event_data: {
      timestamp: availabilityChangedEventWithoutLatestEventData.timestamp,
      raw_vehicle_id:
        availabilityChangedEventWithoutLatestEventData.raw_vehicle_id,
      raw_operation_id: "operationX",
      status: "arrived_at_home",
      raw_status: "arrivé",
      home_area: availabilityChangedEventWithoutLatestEventData.home_area,
      role: availabilityChangedEventWithoutLatestEventData.role,
      vehicle_name: "VSAV 103",
      availability_kind: availabilityKind,
    },
  };
};
