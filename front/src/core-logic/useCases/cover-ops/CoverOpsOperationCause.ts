import type { OperationCause } from "interfaces";

export type CoverOpsOperationCause = Extract<OperationCause, "victim" | "fire">;
