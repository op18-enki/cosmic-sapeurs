import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";
import type { Area, AvailabilityChangedEvent } from "interfaces";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { findBackwards, id, removeAtIndex, sortByTimestamp } from "utils";
import * as R from "ramda";
import {
  getLatestForEachRawId,
  keepOnlyRecentEvents,
  pushAfterLatestTimestamp,
} from "core-logic/useCases/cover-ops/coverOps.utils";

type CoverState = {
  availabilityChangedEvents: AvailabilityChangedEvent[];
  isFetchingCover: boolean;
  fetchCoverError?: string;
  selectedArea: Area | null;
};

export const coverSliceName = "cover";

const coverSlice = createSlice({
  name: coverSliceName,
  initialState: id<CoverState>({
    availabilityChangedEvents: [],
    isFetchingCover: false,
    selectedArea: null,
  }),
  reducers: {
    purgeOldAvailabilityEvents: ({ availabilityChangedEvents, ...rest }) => {
      const events = R.concat(
        getLatestForEachRawId(availabilityChangedEvents),
        keepOnlyRecentEvents(availabilityChangedEvents),
      );
      return {
        ...rest,
        availabilityChangedEvents: sortByTimestamp(
          R.uniqBy((e) => `${e.raw_vehicle_id}_${e.timestamp}`, events),
          "asc",
        ),
      };
    },
    availabilityChanged: (
      state,
      { payload }: PayloadAction<AvailabilityChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.availabilityChangedEvents,
        (evt) => evt.raw_vehicle_id === payload.raw_vehicle_id,
      );

      const perviousEventHasSameAvailability =
        previousEvent?.latest_event_data.availability_kind ===
        payload.latest_event_data.availability_kind;

      if (perviousEventHasSameAvailability)
        removeAtIndex(state.availabilityChangedEvents, previousEventIndex ?? 0);

      pushAfterLatestTimestamp(state.availabilityChangedEvents, payload);
    },

    coverRequested: (state) => {
      state.isFetchingCover = true;
    },
    coverFetched: (
      state,
      action: PayloadAction<AvailabilityChangedEvent[]>,
    ) => {
      state.availabilityChangedEvents = action.payload;
      state.isFetchingCover = false;
    },
    coverCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingCover = false;
      state.fetchCoverError = action.payload;
    },
    setSelectedArea: (state, action: PayloadAction<Area | null>) => {
      state.selectedArea = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(resetCoverOpsActions.resetFinished, (state, action) => {
      state.availabilityChangedEvents = action.payload.availabilities;
    });
  },
});

export const { reducer: coverReducer, actions: coverActions } = coverSlice;
