import {
  AvailabilityChangedEvent,
  OmnibusDetail,
  OngoingOpChangedEvent,
  ResetCoverOpsFinishedEvent,
} from "interfaces";
import { resetCoverOpsActions } from "./resetCoverOps.slice";
import { triggerResetCoverOpsThunk } from "core-logic/useCases/cover-ops/thunks/triggerResetCoverOpsThunk";
import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import type { ReduxStore } from "core-logic/setup/store.config";
import { FrontLinesHistory } from "core-logic/useCases/cover-ops/linesHistory.slice";

describe("Reset cover-Ops", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let httpClient: InMemoryHttpClient;

  beforeEach(() => {
    const testStore = createTestStore();
    store = testStore.store;
    httpClient = testStore.httpClient;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  describe("Fetching reset", () => {
    it("Indicates when resetting is ongoing", () => {
      store.dispatch(resetCoverOpsActions.resetRequested());
      expectStateToEqual({ resetCoverOps: { isResetting: true } });
    });
    describe("When all is good", () => {
      it("Resting stays to true until resetFinishedEvent happens", async () => {
        await store.dispatch(triggerResetCoverOpsThunk());

        expectStateToEqual({
          resetCoverOps: { isResetting: true },
        });
      });
    });
    describe("When something wrong happens", () => {
      it("gets the error message", async () => {
        const errorMessage = "My expected error !";
        httpClient.setError(errorMessage);

        await store.dispatch(triggerResetCoverOpsThunk());

        expectStateToEqual({
          resetCoverOps: {
            isResetting: false,
            error: errorMessage,
          },
        });
      });
    });

    describe("When event reset cover ops finished gets triggered", () => {
      it("stops isResetting and updates with received data", async () => {
        const availabilities = [{}] as AvailabilityChangedEvent[];
        const ongoing_ops = [{}, {}] as OngoingOpChangedEvent[];
        const omnibus_details = [{}] as OmnibusDetail[];
        const front_lines_history = {
          fire: [{}],
          victim: [{}, {}, {}],
        } as FrontLinesHistory;

        const finishedEvent: ResetCoverOpsFinishedEvent = {
          omnibus_details,
          availabilities,
          ongoing_ops,
          front_lines_history,
        };

        await store.dispatch(resetCoverOpsActions.resetFinished(finishedEvent));

        expectStateToEqual({
          resetCoverOps: { isResetting: false },
          cover: {
            isFetchingCover: false,
            availabilityChangedEvents: availabilities,
          },
          ops: {
            isFetchingOps: false,
            ongoingOpChangedEvents: ongoing_ops,
          },
          linesHistory: {
            isFetchingHistory: false,
            ...front_lines_history,
          },
          omnibus: {
            details: omnibus_details,
          },
        });
      });
    });
  });
});
