import { createAction } from "@reduxjs/toolkit";

export const historizeEvents = createAction("historizeEvents");
