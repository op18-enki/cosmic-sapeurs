import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { resetCoverOpsActions } from "core-logic/useCases/cover-ops/resetCoverOps.slice";
import { OmnibusDetail } from "interfaces";
import { OmnibusBalanceChangedEventData } from "interfaces/generated/OmnibusBalanceChangedEventData";

type OmnibusState = {
  details: OmnibusDetail[];
};

export const omnibusSliceName = "omnibus";

const initialState: OmnibusState = {
  details: [],
};

const onOmnibusBalanceChanged = (
  omnibusDetails: OmnibusDetail[],
): OmnibusState =>
  omnibusDetails ? { details: omnibusDetails } : initialState;

const omnibusSlice = createSlice({
  name: omnibusSliceName,
  initialState,
  reducers: {
    omnibusBalanceChanged: (
      state,
      { payload }: PayloadAction<OmnibusBalanceChangedEventData>,
    ) => onOmnibusBalanceChanged(payload.details),
  },
  extraReducers: (builder) => {
    builder.addCase(resetCoverOpsActions.resetFinished, (state, action) => {
      return onOmnibusBalanceChanged(action.payload.omnibus_details);
    });
  },
});

export const { reducer: omnibusReducer, actions: omnibusActions } =
  omnibusSlice;
