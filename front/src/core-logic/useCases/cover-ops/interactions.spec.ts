import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import type { ReduxStore } from "core-logic/setup/store.config";
import { interactionsActions } from "core-logic/useCases/cover-ops/interactions.slice";

describe("Ui interactions", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  describe("Hours to show", () => {
    it("updates the number of hours to display", async () => {
      const payload = 7;
      store.dispatch(interactionsActions.numberOfHoursToShowChanged(payload));

      expectStateToEqual({
        interactions: {
          numberOfHoursToShow: payload,
        },
      });
    });
  });

  describe("Toggle omnibus on map", () => {
    it("Toggles if we should or not see omnibus on map", async () => {
      await store.dispatch(interactionsActions.toggleShowOmnibusOnMap());

      expectStateToEqual({
        interactions: {
          takeOmnibusIntoAccount: false,
        },
      });
    });
  });

  describe("vehicle role selection", () => {
    it("adds a vehicle roles to selection than removes one, than clears", async () => {
      await store.dispatch(
        interactionsActions.addSelectedVehicleRole({
          index: 0,
          rolesToAdd: ["ccr"],
        }),
      );

      expectStateToEqual({
        interactions: {
          selectedVehicleRoles: [["ccr"], [], [], []],
        },
      });

      await store.dispatch(
        interactionsActions.addSelectedVehicleRole({
          index: 0,
          rolesToAdd: ["crac"],
        }),
      );

      expectStateToEqual({
        interactions: {
          selectedVehicleRoles: [["ccr", "crac"], [], [], []],
        },
      });

      await store.dispatch(
        interactionsActions.removeSelectedVehicleRole({
          index: 0,
          roleToDelete: "ccr",
        }),
      );

      await store.dispatch(interactionsActions.clearSelectedVehicleRoles(0));

      expectStateToEqual({
        interactions: {
          selectedVehicleRoles: [[], [], [], []],
        },
      });
    });

    it("cannot add same role twice in one of the 4 indexes", async () => {
      await store.dispatch(
        interactionsActions.addSelectedVehicleRole({
          index: 2,
          rolesToAdd: ["ccr"],
        }),
      );

      expectStateToEqual({
        interactions: {
          selectedVehicleRoles: [[], [], ["ccr"], []],
        },
      });

      await store.dispatch(
        interactionsActions.addSelectedVehicleRole({
          index: 2,
          rolesToAdd: ["ccr", "ban", "vsav_solo"],
        }),
      );

      expectStateToEqual({
        interactions: {
          selectedVehicleRoles: [[], [], ["ccr", "ban", "vsav_solo"], []],
        },
      });
    });

    it("toggle vehicle linked", async () => {
      await store.dispatch(interactionsActions.areVehicleLinkedToggled(2));
      expectStateToEqual({
        interactions: {
          areVehicleLinked: [false, false, true, false],
        },
      });

      await store.dispatch(interactionsActions.areVehicleLinkedToggled(2));
      expectStateToEqual({
        interactions: {
          areVehicleLinked: [false, false, false, false],
        },
      });
    });
  });
});
