import { ReduxStore } from "core-logic";
import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import {
  FrontLinesHistory,
  linesHistoryActions,
} from "core-logic/useCases/cover-ops/linesHistory.slice";
import { fetchFrontLinesHistory } from "core-logic/useCases/cover-ops/thunks/fetchFrontLinesHistoryThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";

describe("Reset cover-Ops", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let httpClient: InMemoryHttpClient;

  beforeEach(() => {
    const testStore = createTestStore();
    store = testStore.store;
    httpClient = testStore.httpClient;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  describe("Fetching history", () => {
    it("Indicates when fetch is ongoing", () => {
      store.dispatch(linesHistoryActions.historyRequested());
      expectStateToEqual({ linesHistory: { isFetchingHistory: true } });
    });

    it("gets the front history data", async () => {
      const history: FrontLinesHistory = {
        victim: [
          { timestamp: new Date().toISOString(), value: 8, variable: "sap" },
        ],
        fire: [
          { timestamp: new Date().toISOString(), value: 10, variable: "ep" },
        ],
      };

      httpClient.setFrontHistory(history);

      await store.dispatch(fetchFrontLinesHistory());
      expectStateToEqual({
        linesHistory: { isFetchingHistory: false, ...history },
      });
    });
  });
});
