import { helloSapeurActions } from "./slice";
import { helloSapeurThunk } from "./helloSapeur.thunk";
import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import type { ReduxStore } from "core-logic/setup/store.config";

describe("Hello sapeur", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let httpClient: InMemoryHttpClient;

  beforeEach(() => {
    const testStore = createTestStore();
    store = testStore.store;
    httpClient = testStore.httpClient;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  describe("Fetching hello sapeur", () => {
    it("Indicates when fetching is ongoing", () => {
      store.dispatch(helloSapeurActions.helloSapeurRequested());
      expectStateToEqual({ helloSapeur: { isFetching: true } });
    });
    describe("When all is good", () => {
      it("Retrieves the hello sapeur message", async () => {
        const expectedValue = "Hello Sapeur expected !";
        httpClient.setHelloWorldMessage(expectedValue);

        await store.dispatch(helloSapeurThunk());

        expectStateToEqual({
          helloSapeur: { message: expectedValue, isFetching: false },
        });
      });
    });
    describe("When something wrong happens", () => {
      it("gets the error message", async () => {
        const errorMessage = "My expected error !";
        httpClient.setError(errorMessage);

        await store.dispatch(helloSapeurThunk());

        expectStateToEqual({
          helloSapeur: {
            isFetching: false,
            error: errorMessage,
          },
        });
      });
    });
  });
});
