import { DeepPartial, Store } from "@reduxjs/toolkit";
import { ActualBatchHandler } from "core-logic/ports/BatchHandler";
import { InstantOnceRefresher } from "core-logic/ports/Refresher";
import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { InMemoryPubSubClient } from "core-logic/secondaryAdapters/InMemoryPubSubClient";
import { RootState } from "core-logic/setup/root.reducer";
import {
  configureReduxStore,
  Dependencies,
  ReduxStore,
} from "core-logic/setup/store.config";
import * as R from "ramda";

type Partial2Levels<T> = {
  [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<Partial<U>>
    : T[P] extends ReadonlyArray<infer U>
    ? ReadonlyArray<Partial<U>>
    : Partial<T[P]>;
};

export const buildExpectStateToEqual =
  (store: Store<RootState>, initialState: RootState) =>
  (expectedState: Partial2Levels<RootState>) => {
    const expectedStateMergedWithInitial: RootState = {
      interactions: {
        ...initialState.interactions,
        ...expectedState.interactions,
      },
      cover: { ...initialState.cover, ...expectedState.cover },
      ops: { ...initialState.ops, ...expectedState.ops },
      omnibus: { ...initialState.omnibus, ...expectedState.omnibus },
      helloSapeur: {
        ...initialState.helloSapeur,
        ...expectedState.helloSapeur,
      },
      resetCoverOps: {
        ...initialState.resetCoverOps,
        ...expectedState.resetCoverOps,
      },
      linesHistory: {
        ...initialState.linesHistory,
        ...expectedState.linesHistory,
      },
    };
    expect(store.getState()).toEqual(expectedStateMergedWithInitial);
  };

export type ExpectStateToEqual = ReturnType<typeof buildExpectStateToEqual>;

type TestDependencies = {
  httpClient: InMemoryHttpClient;
  pubSubClient: InMemoryPubSubClient;
  renderingRefresher: InstantOnceRefresher;
  purgeRefresher: InstantOnceRefresher;
  batchHandler: ActualBatchHandler;
};

// <<<<---- this function is not used but it allows to typecheck that TestDependencies are assignable to Dependencies
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const checkDependencies = (dependencies: TestDependencies): Dependencies =>
  dependencies;
// ---->>>>

export const createTestStore = (
  preloadedState?: DeepPartial<RootState>,
): TestDependencies & { store: ReduxStore } => {
  const httpClient = new InMemoryHttpClient();
  const pubSubClient = new InMemoryPubSubClient();
  const renderingRefresher = new InstantOnceRefresher();
  const purgeRefresher = new InstantOnceRefresher();
  const batchHandler = new ActualBatchHandler();

  const dependencies: TestDependencies = {
    httpClient,
    pubSubClient,
    renderingRefresher,
    purgeRefresher,
    batchHandler,
  };

  const initialStore = configureReduxStore(dependencies);

  if (!preloadedState) return { store: initialStore, ...dependencies };

  const initialState = initialStore.getState();
  const fullPreloadedState = R.mergeDeepRight(initialState, preloadedState);

  const store = configureReduxStore(dependencies, fullPreloadedState as any);

  return { store, ...dependencies };
};

export const expectToEqual = <T>(actual: T, expected: T) => {
  expect(actual).toEqual(expected);
};

export const expectToMatchObject = <T>(actual: T, expected: Partial<T>) => {
  expect(actual).toMatchObject(expected);
};
