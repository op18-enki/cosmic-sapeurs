export type { RootState } from "core-logic/setup/root.reducer";
export type {
  PubSubStatus,
  Hours,
  Valorization,
} from "core-logic/useCases/cover-ops/interactions.slice";
export type { FiltrableField } from "core-logic/useCases/cover-ops/ops.slice";
export type { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
export type { CoverOpsOperationCause } from "core-logic/useCases/cover-ops/CoverOpsOperationCause";
export * from "core-logic/setup/store.config";
export * from "core-logic/setup/initializeStore";
export * from "core-logic/setup/root.actions";
export * from "core-logic/useCases/cover-ops/selectors/barInputSampleSelector";
export * from "core-logic/useCases/cover-ops/selectors/lineInputSampleSelector";
export * from "core-logic/useCases/cover-ops/selectors/mapsInputsSelector";
export * from "core-logic/useCases/cover-ops/selectors/lastTimestampDateSelector";
export { availabilityTableInputSelector } from "core-logic/useCases/cover-ops/selectors/availabilityTableInputSelector";
export { latestVehicleChangedEventsByIdForAreaSelector } from "core-logic/useCases/cover-ops/selectors/latestVehicleChangedEventsByIdForAreaSelectors";

export {
  latestVehicleEventByRawVehicleIdForRoleSelectors,
  availabilityEventByIdByAvailabilityKindByBySubCategory,
} from "core-logic/useCases/cover-ops/selectors/vehiclesByIdForRoleSelector";
export type { AvailabilityCellValue } from "core-logic/useCases/cover-ops/selectors/availabilityTableInputSelector";
export { vehiclesLabels } from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
export type {
  VehicleCategory,
  VehicleSubCategory,
  VehicleLabels,
} from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
export { lastOnGoingOpEventByOperationIdSelector } from "core-logic/useCases/cover-ops/selectors/lastOnGoingOpEventByOperationIdSelector";
export {
  filteredOnGoingOpEventSelector,
  procedureOptionsSelector,
  departureCriteriaOptionsSelector,
} from "core-logic/useCases/cover-ops/selectors/filteredOnGoingOpEventSelectors";
export { socleAvailableSelector } from "core-logic/useCases/cover-ops/selectors/sumUpSelectors";
export { omnibusBalanceSelector } from "core-logic/useCases/cover-ops/selectors/omnibusDetailsSelectors";
export {
  takeOmnibusIntoAccountSelector,
  valorizationSelector,
  selectedAreaSelector,
  activeOpsFiltersSelector,
  inuBlinkStartTimeSelector,
  isLoadingDataSelector,
} from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
export { vehicleRoleClassification } from "core-logic/useCases/cover-ops/selectors/configurations/vehicleRoleClassification";
export { mapByVehicleRoleDataSelector } from "core-logic/useCases/cover-ops/selectors/lastAvailabilityEventForVehicleRoleSelector";
