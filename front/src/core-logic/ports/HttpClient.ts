import type { Hours } from "core-logic/useCases/cover-ops/interactions.slice";
import type {
  AvailabilityChangedEvent,
  OmnibusBalanceChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";
import type { FrontLinesHistory } from "core-logic/useCases/cover-ops/linesHistory.slice";

export interface HttpClient {
  helloSapeur: () => Promise<string>;
  resetCoverOps: (curveData: FrontLinesHistory) => Promise<void>;
  fetchCover: (hours: Hours) => Promise<AvailabilityChangedEvent[]>;
  fetchOps: (hours: Hours) => Promise<OngoingOpChangedEvent[]>;
  fetchOmnibus: () => Promise<OmnibusBalanceChangedEvent>;
  fetchHistory: () => Promise<FrontLinesHistory>;
}
