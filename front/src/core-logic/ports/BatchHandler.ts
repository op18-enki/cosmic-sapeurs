import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { omnibusActions } from "core-logic/useCases/cover-ops/omnibus.slice";
import { opsActions } from "core-logic/useCases/cover-ops/ops.slice";
import { Dispatch } from "@reduxjs/toolkit";
import { batchActions } from "redux-batched-actions";

type BatchedAction =
  | ReturnType<typeof coverActions.availabilityChanged>
  | ReturnType<typeof opsActions.ongoingOpChanged>
  | ReturnType<typeof omnibusActions.omnibusBalanceChanged>;

export interface BatchHandler {
  addForDispatch: (action: BatchedAction) => void;
  dispatchBatch: (dispatch: Dispatch) => void;
}

export class ActualBatchHandler implements BatchHandler {
  private actionsToDispatch: BatchedAction[] = [];

  public addForDispatch(action: BatchedAction) {
    this.actionsToDispatch.push(action);
  }

  public dispatchBatch(dispatch: Dispatch) {
    if (this.actionsToDispatch.length) {
      dispatch(batchActions(this.actionsToDispatch));
      this.actionsToDispatch = [];
    }
  }
}
