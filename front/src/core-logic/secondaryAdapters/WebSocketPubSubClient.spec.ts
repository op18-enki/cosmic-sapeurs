import { AppEvent } from "core-logic/ports/PubSubClient";
import { WebSocketPubSubClient } from "./WebSocketPubSubClient";

const delay = (ms: number) => new Promise((r) => setTimeout(r, ms));

const makeSpy = <T>() => {
  const eventsAcc: T[] = [];
  return {
    spy: (event: T) => eventsAcc.push(event),
    eventsAcc,
  };
};

// This specific test suite must be run all together, the order is important
// This is only for this test suite which uses external service, tests should normally be able to run independently

describe.skip("Secondary adapter | WebSocketPubSubClient", () => {
  let socketPubSub: WebSocketPubSubClient;

  const { eventsAcc: availabilityChangedAcc, spy: availabilityChangedSpy } =
    makeSpy();

  const { eventsAcc: ongoingOpChangedAcc, spy: ongoingOpChangedSpy } =
    makeSpy();

  beforeAll(async () => {
    socketPubSub = new WebSocketPubSubClient("wss://echo.websocket.org");
  });

  describe("subscribe", () => {
    it("subscribes to echo websocket", async () => {
      socketPubSub.subscribe("availabilityChanged", availabilityChangedSpy);
      // this delay is to give time to connect to echo.websocket.org
      await delay(2000);

      const expectedReceivedEvent = {
        topic: "availabilityChanged" as const,
      };

      await simulateMessageFromBackend(expectedReceivedEvent);

      expect(availabilityChangedAcc).toEqual([expectedReceivedEvent]);
    });

    it("When subscribe twice to the same topic, only first one is taken into account", async () => {
      const { eventsAcc, spy } = makeSpy();
      socketPubSub.subscribe("availabilityChanged", spy);
      await simulateMessageFromBackend({
        topic: "availabilityChanged",
      });
      expect(eventsAcc).toEqual([]);
      expect(availabilityChangedAcc.length).toBe(2);
    });

    it("Can subscribe to a different topic", async () => {
      socketPubSub.subscribe("ongoingOpChanged", ongoingOpChangedSpy);
      const expectedOngoingOpChangedEvent = {
        topic: "ongoingOpChanged",
        payload: undefined,
      };
      await simulateMessageFromBackend({
        topic: "ongoingOpChanged",
        payload: undefined,
      });
      expect(ongoingOpChangedAcc).toEqual([expectedOngoingOpChangedEvent]);
      expect(availabilityChangedAcc.length).toBe(2);
    });
  });

  describe("unsubscribe", () => {
    it("can unsubscribe to a topic", async () => {
      socketPubSub.unsubscribe("availabilityChanged");
      await simulateMessageFromBackend({
        topic: "availabilityChanged",
        payload: undefined,
      });
      await simulateMessageFromBackend({
        topic: "ongoingOpChanged",
        payload: undefined,
      });
      expect(availabilityChangedAcc.length).toBe(2);
      expect(ongoingOpChangedAcc.length).toBe(2);
    });
    it("does nothing but warn when topic was not subscribe", async () => {
      socketPubSub.unsubscribe("availabilityChanged");
      await simulateMessageFromBackend({
        topic: "availabilityChanged",
        payload: undefined,
      });
      expect(availabilityChangedAcc.length).toBe(2);
      expect(ongoingOpChangedAcc.length).toBe(2);
    });
  });

  const simulateMessageFromBackend = async (
    expectedReceivedEvent: Partial<AppEvent>,
  ) => {
    // following is working because we use "wss://echo.websocket.org" which sends back the same message
    socketPubSub.send(expectedReceivedEvent as any);
    await delay(300);
  };
});
