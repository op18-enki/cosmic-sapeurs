import { HttpClient } from "core-logic/ports/HttpClient";
import { FrontLinesHistory } from "core-logic/useCases/cover-ops/linesHistory.slice";
import type {
  AvailabilityChangedEvent,
  OngoingOpChangedEvent,
  OmnibusBalanceChangedEvent,
} from "interfaces";

const wait = (delay: number) =>
  new Promise((resolve) => setTimeout(resolve, delay));

export class InMemoryHttpClient implements HttpClient {
  public async helloSapeur(): Promise<string> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
    return this.message;
  }
  public async resetCoverOps(): Promise<void> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
  }
  public async fetchCover(): Promise<AvailabilityChangedEvent[]> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
    return this.coverResponse;
  }

  public async fetchOps(): Promise<OngoingOpChangedEvent[]> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
    return this.opsResponse;
  }

  public async fetchOmnibus(): Promise<OmnibusBalanceChangedEvent> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
    return this.omnibusResponse;
  }

  public async fetchHistory(): Promise<FrontLinesHistory> {
    if (this.requestsDelay) await wait(this.requestsDelay);
    if (this.error) throw new Error(this.error);
    return this.frontHistory;
  }

  private message: string = "Initial Hello Sapeur msg from InMemory Adapter !";
  private error?: string;
  private coverResponse: AvailabilityChangedEvent[] = [];
  private opsResponse: OngoingOpChangedEvent[] = [];
  private frontHistory: FrontLinesHistory = {
    fire: [],
    victim: [],
  };
  private omnibusResponse: OmnibusBalanceChangedEvent = {
    timestamp: "2000-01-01T12",
    details: [],
  };

  constructor(private requestsDelay?: number) {}

  public setHelloWorldMessage(message: string) {
    this.message = message;
  }

  public setCoverResponse(response: AvailabilityChangedEvent[]) {
    this.coverResponse = response;
  }

  public setOpsResponse(response: OngoingOpChangedEvent[]) {
    this.opsResponse = response;
  }

  public setOmnibusResponse(response: OmnibusBalanceChangedEvent) {
    this.omnibusResponse = response;
  }

  public setFrontHistory(response: FrontLinesHistory) {
    this.frontHistory = response;
  }

  public setError(errorMessage: string) {
    this.error = errorMessage;
  }
}
