import {
  AppEvent,
  NarrowEvent,
  PubSubClient,
} from "core-logic/ports/PubSubClient";

type Subscriptions = {
  [key in AppEvent["topic"]]: Array<(event: AppEvent) => void>;
};

export class InMemoryPubSubClient implements PubSubClient {
  private _internalSocketEventsHandler?: (e: any) => void;
  public setInternalSocketEventsHandler(cb: (e: any) => void) {
    this._internalSocketEventsHandler = cb;
  }

  private subscriptions: Subscriptions = {
    availabilityChanged: [],
    ongoingOpChanged: [],
    omnibusBalanceChanged: [],
    resetCoverOpsFinished: [],
    resetCoverOpsRequested: [],
  };

  subscribe<T extends AppEvent["topic"]>(
    topic: T,
    callback: (narrowEvent: NarrowEvent<T>) => void,
  ): void {
    this._internalSocketEventsHandler?.({ type: "open" });
    this.subscriptions[topic].push(callback as (event: AppEvent) => void);
  }

  unsubscribe(topic: AppEvent["topic"]) {
    this.subscriptions[topic] = [];
  }

  publish(event: AppEvent) {
    this.subscriptions[event.topic].forEach((callback) => {
      callback(event);
    });
  }
}
