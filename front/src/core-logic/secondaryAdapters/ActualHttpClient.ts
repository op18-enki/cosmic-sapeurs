import Axios from "axios";
import type { HttpClient } from "core-logic/ports/HttpClient";
import type { Hours } from "core-logic/useCases/cover-ops/interactions.slice";
import { FrontLinesHistory } from "core-logic/useCases/cover-ops/linesHistory.slice";
import type {
  AvailabilityChangedEvent,
  OmnibusBalanceChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";
import { SapeursRoutes } from "interfaces/generated/SapeursRoutes";

const prefix: SapeursRoutes["prefix"] = "api";

export class ActualHttpClient implements HttpClient {
  public async helloSapeur(): Promise<string> {
    const hello: SapeursRoutes["hello"] = "hello";
    const response = await Axios.get(`/${prefix}/${hello}`);
    return response.data.message;
  }
  public async resetCoverOps(history: FrontLinesHistory): Promise<void> {
    const reset: SapeursRoutes["reset"] = "reset";
    await Axios.post(`/${prefix}/${reset}`, history);
  }
  public async fetchCover(hours: Hours): Promise<AvailabilityChangedEvent[]> {
    const cover: SapeursRoutes["get_cover"] = "cover";
    const response = await Axios.get(`/${prefix}/${cover}`, {
      params: { hours },
    });
    return response.data;
  }

  public async fetchOps(hours: Hours): Promise<OngoingOpChangedEvent[]> {
    const ops: SapeursRoutes["get_ops"] = "ops";
    const response = await Axios.get(`/${prefix}/${ops}`, {
      params: { hours },
    });
    return response.data;
  }

  public async fetchHistory(): Promise<FrontLinesHistory> {
    const frontHistory: SapeursRoutes["get_front_history"] = "front_history";
    const response = await Axios.get(`/${prefix}/${frontHistory}`);
    return response.data;
  }

  public async fetchOmnibus(): Promise<OmnibusBalanceChangedEvent> {
    const omnibus: SapeursRoutes["get_omnibus"] = "omnibus";
    const response = await Axios.get(`/${prefix}/${omnibus}`);
    return response.data ?? null;
  }
}
