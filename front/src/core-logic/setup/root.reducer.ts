import { combineReducers } from "@reduxjs/toolkit";
import {
  interactionSliceName,
  interactionsReducer,
} from "core-logic/useCases/cover-ops/interactions.slice";
import {
  coverSliceName,
  coverReducer,
} from "core-logic/useCases/cover-ops/cover.slice";
import {
  opsSliceName,
  opsReducer,
} from "core-logic/useCases/cover-ops/ops.slice";
import {
  helloSapeurReducer,
  sliceName as helloSapeurName,
} from "core-logic/useCases/helloSapeur/slice";
import {
  omnibusSliceName,
  omnibusReducer,
} from "core-logic/useCases/cover-ops/omnibus.slice";
import {
  resetCoverOpsName,
  resetCoverOpsReducer,
} from "core-logic/useCases/cover-ops/resetCoverOps.slice";
import {
  linesHistoryReducer,
  linesHistorySliceName,
} from "core-logic/useCases/cover-ops/linesHistory.slice";

export const rootReducer = combineReducers({
  [helloSapeurName]: helloSapeurReducer,
  [interactionSliceName]: interactionsReducer,
  [opsSliceName]: opsReducer,
  [coverSliceName]: coverReducer,
  [omnibusSliceName]: omnibusReducer,
  [resetCoverOpsName]: resetCoverOpsReducer,
  [linesHistorySliceName]: linesHistoryReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
