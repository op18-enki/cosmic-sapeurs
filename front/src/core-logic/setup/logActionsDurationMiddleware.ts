export const logActionsDurationMiddleware =
  () => (next: any) => (action: any) => {
    const actionName = `  -> ACTION ${action.type} DURATION`;

    if (action.type === "BATCHING_REDUCER.BATCH") {
      console.log(
        `     - Number of '${action.payload[0].type}' actions in batch : `,
        action.payload.length,
      );
    }

    console.time(actionName);
    const result = next(action);
    console.timeEnd(actionName);
    return result;
  };
