import { helloSapeurThunk } from "core-logic/useCases/helloSapeur/helloSapeur.thunk";
import {
  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToAvailabilityChangedEventsThunk";
import {
  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToOngoingOpChangedEventsThunk";
import {
  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToOmnibusBalanceChangedEventsThunk";
import {
  loopOverDispatchBatchThunk,
  stopLoopOverDispatchBatchThunk,
} from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";
import { fetchCoverThunk } from "core-logic/useCases/cover-ops/thunks/fetchCoverThunk";
import { fetchOpsThunk } from "core-logic/useCases/cover-ops/thunks/fetchOpsThunk";
import { fetchOmnibusThunk } from "core-logic/useCases/cover-ops/thunks/fetchOmnibusThunk";
import {
  subscribeToResetCoverOpsFinishedThunk,
  subscribeToResetCoverOpsRequestedThunk,
  unsubscribeFromResetCoverOpsFinishedThunk,
  unsubscribeFromResetCoverOpsRequestedThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToResetCoverOpsThunk";

import { interactionsActions } from "core-logic/useCases/cover-ops/interactions.slice";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { opsActions } from "core-logic/useCases/cover-ops/ops.slice";
import { triggerResetCoverOpsThunk } from "core-logic/useCases/cover-ops/thunks/triggerResetCoverOpsThunk";
import { fetchFrontLinesHistory } from "core-logic/useCases/cover-ops/thunks/fetchFrontLinesHistoryThunk";

export const actions = {
  helloSapeurThunk,

  ...interactionsActions,

  setSelectedArea: coverActions.setSelectedArea,

  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,

  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,

  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,

  subscribeToResetCoverOpsRequestedThunk,
  unsubscribeFromResetCoverOpsRequestedThunk,

  subscribeToResetCoverOpsFinishedThunk,
  unsubscribeFromResetCoverOpsFinishedThunk,

  loopOverDispatchBatchThunk,
  stopLoopOverDispatchBatchThunk,

  fetchCoverThunk,
  fetchOpsThunk,
  fetchOmnibusThunk,

  triggerResetCoverOpsThunk,

  fetchFrontLinesHistory,

  addOpsFilter: opsActions.addFilter,
  removeOpsFilter: opsActions.removeFilter,

  setInuBlinkTime: opsActions.setInuBlinkTime,
};
