import {
  sapAllOmniMapCountGetter,
  sapMapCountGetter,
  sapOmniVsavOnlyMapCountGetter,
} from "app/countGetters";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { useSelector } from "app/redux-hooks";
import { contrastedMapColors, coverStyles } from "app/theme";
import { takeOmnibusIntoAccountSelector } from "core-logic";
import { ContrastedLeafletMap, LeafletMapInputs } from "ui";
import { OmnibusDisplayOption } from "../ui/map/LeafletMap";

const getSapColors = (omnibusDisplay: OmnibusDisplayOption) => {
  const colors = [
    contrastedMapColors.red,
    contrastedMapColors.orange,
    contrastedMapColors.green,
  ];

  switch (omnibusDisplay) {
    case "greenLines":
      return makeMapColorAndScale({
        colors,
        countGetter: sapOmniVsavOnlyMapCountGetter,
      });
    case "includeWithOthers":
      return makeMapColorAndScale({
        colors,
        countGetter: sapAllOmniMapCountGetter,
      });
    case "notShown":
      return makeMapColorAndScale({
        colors,
        countGetter: sapMapCountGetter,
      });
    default:
      const exhaustiveCheck: never = omnibusDisplay;
      return exhaustiveCheck;
  }
};

export const CoverMapVsav = (props: {
  data: LeafletMapInputs;
  mapContainerStyle?: React.CSSProperties;
  mapZoom?: number;
}) => {
  const takeOmnibusIntoAccount = useSelector(takeOmnibusIntoAccountSelector);
  const omnibusDisplay: OmnibusDisplayOption = takeOmnibusIntoAccount
    ? "includeWithOthers"
    : "notShown";

  return (
    <ContrastedLeafletMap
      colors={getSapColors(omnibusDisplay)}
      data={props.data}
      omnibusDisplay={omnibusDisplay}
      subCategory="sap"
      titleColor={coverStyles.sap.mainColor}
      legendPosition="top"
      mapContainerStyle={props.mapContainerStyle}
      mapZoom={props.mapZoom}
    />
  );
};
