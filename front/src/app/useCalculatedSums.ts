import {
  omnibusBalanceSelector,
  socleAvailableSelector,
  takeOmnibusIntoAccountSelector,
} from "core-logic";
import { useSelector } from "app/redux-hooks";

const or0 = (n?: number) => n ?? 0;

export const useCalculatedSums = () => {
  const omniTakenIntoAccount = useSelector(takeOmnibusIntoAccountSelector);
  const omnibusBalance = useSelector(omnibusBalanceSelector);
  const numSocleByVehicleSubCategory = useSelector(socleAvailableSelector);

  const {
    both_available,
    only_pse_available,
    pse_on_inter_san,
    vsav_on_inter_san,
  } = omnibusBalance;

  const ep = or0(both_available) + or0(only_pse_available);
  const sap =
    or0(both_available) * 2 + or0(pse_on_inter_san) + or0(vsav_on_inter_san);

  return {
    omniTakenIntoAccount,
    socle: numSocleByVehicleSubCategory,
    omni: {
      ep,
      epWithOmni: ep,
      sap,
      sapWithOmni: sap,
    },
  };
};
