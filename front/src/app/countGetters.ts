import * as R from "ramda";
import { AreaCounts } from "ui";

export type MapCountGetter = (counts?: AreaCounts) => number | undefined;

export const epMapCountGetter: MapCountGetter = (areaCounts) =>
  areaCounts?.availabilities.ep;

export const epOmniEpOnlyMapCountGetter: MapCountGetter = (areaCounts) => {
  const epCount = areaCounts?.availabilities.ep;
  const omniSocleEp = areaCounts?.omnibus.only_pse_available;
  if (R.isNil(epCount) && R.isNil(omniSocleEp)) return;
  return (epCount ?? 0) + (omniSocleEp ?? 0);
};

export const epAllOmniMapCountGetter: MapCountGetter = (areaCounts) => {
  const epCount = areaCounts?.availabilities.ep;
  const omniSocleEp = areaCounts?.omnibus.only_pse_available;
  const bothAvailable = areaCounts?.omnibus.both_available;
  if (R.isNil(epCount) && R.isNil(omniSocleEp) && R.isNil(bothAvailable))
    return;
  return (epCount ?? 0) + (omniSocleEp ?? 0) + (bothAvailable ?? 0);
};

export const sapMapCountGetter: MapCountGetter = (areaCounts) =>
  areaCounts?.availabilities.sap;

export const sapOmniVsavOnlyMapCountGetter: MapCountGetter = (areaCounts) => {
  const sapCount = areaCounts?.availabilities.sap;

  const { vsav_on_inter_san, pse_on_inter_san } = areaCounts?.omnibus ?? {};

  const allNil = [sapCount, vsav_on_inter_san, pse_on_inter_san].every(R.isNil);

  if (allNil) return;

  // vsav_on_inter_san implicitly means EP3 is available
  // pse_on_inter_san implicitly means VSAV M is available
  const ep3Available = vsav_on_inter_san ?? 0;
  const vsavAvailable = pse_on_inter_san ?? 0;

  return (sapCount ?? 0) + ep3Available + vsavAvailable;
};

export const sapAllOmniMapCountGetter: MapCountGetter = (areaCounts) => {
  const sapCount = areaCounts?.availabilities.sap;

  const { vsav_on_inter_san, pse_on_inter_san, both_available } =
    areaCounts?.omnibus ?? {};

  const allNil = [
    sapCount,
    vsav_on_inter_san,
    pse_on_inter_san,
    both_available,
  ].every(R.isNil);

  if (allNil) return;

  // vsav_on_inter_san implicitly means EP3 is available
  // pse_on_inter_san implicitly means VSAV M is available
  const ep3Available = vsav_on_inter_san ?? 0;
  const vsavAvailable = pse_on_inter_san ?? 0;

  return (
    (sapCount ?? 0) + ep3Available + vsavAvailable + (both_available ?? 0) * 2
  );
};
