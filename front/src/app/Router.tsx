import { CoverOps } from "app/pages/CoverOps/CoverOps";
import { DetailedCover } from "app/pages/DetailedCover/DetailedCover";
import { DetailedOps } from "app/pages/DetailedOps/DetailedOps";
import { CoverSummary } from "app/pages/CoverSummary/CoverSummary";
import { VsavCover } from "app/pages/VsavCover/VsavCover";

import { createRouter, defineRoute } from "type-route";
import { CoverByVehicleRolePage } from "app/pages/CoverByVehicleRole/CoverByVehicleRole";

export const { RouteProvider, useRoute, routes } = createRouter({
  coverOps: defineRoute(["/", "/cover-ops"]),
  coverSummary: defineRoute("/cover-summary"),
  detailedCover: defineRoute({}, () => `/detailed-cover`),
  detailedOps: defineRoute({}, () => `/detailed-ops`),
  vsavCover: defineRoute({}, () => `/vsav-cover`),
  coverByVehicleRole: defineRoute("/cover-by-vehicle-role"),
});

export const Router = () => {
  const route = useRoute();

  return (
    <>
      {route.name !== "coverOps" && <div className="w-full h-12" />}
      {route.name === "detailedCover" && <DetailedCover route={route} />}
      {route.name === "coverSummary" && <CoverSummary route={route} />}
      {route.name === "coverOps" && <CoverOps route={route} />}
      {route.name === "detailedOps" && <DetailedOps route={route} />}
      {route.name === "vsavCover" && <VsavCover route={route} />}
      {route.name === "coverByVehicleRole" && (
        <CoverByVehicleRolePage route={route} />
      )}
    </>
  );
};
