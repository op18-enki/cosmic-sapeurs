import { Navigation } from "app/Navigation/Navigation";
import { Router } from "app/Router";
import {
  useBatchDispatchLoop,
  useListenToSocketDisconnection,
  useSubscriptionToAvailabilityChangedEvents,
  useSubscriptionToOmnibusBalanceChangedEvents,
  useSubscriptionToOngoingOpChangedEvents,
  useSubscriptionToResetCoverOpsEvents,
} from "app/subscription-hooks";
import { actions, Hours } from "core-logic";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

const useFetchCover = (hours: Hours, connectionDate: Date) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchCoverThunk(hours));
  }, [hours, connectionDate]);
};

const useFetchOps = (hours: Hours, connectionDate: Date) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchOpsThunk(hours));
  }, [hours, connectionDate]);
};

const useFetchFrontLinesHistory = (connectionDate: Date) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchFrontLinesHistory());
  }, [connectionDate]);
};

const useFetchOmnibus = (connectionDate: Date) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchOmnibusThunk());
  }, [connectionDate]);
};

export const App = () => {
  const maxNumberOfHoursToShow = 4;
  const connectionDate = useListenToSocketDisconnection();

  useSubscriptionToResetCoverOpsEvents(connectionDate);
  useBatchDispatchLoop(connectionDate);
  useFetchOps(maxNumberOfHoursToShow, connectionDate);
  useFetchCover(maxNumberOfHoursToShow, connectionDate);
  useFetchOmnibus(connectionDate);
  useFetchFrontLinesHistory(connectionDate);
  useSubscriptionToAvailabilityChangedEvents(connectionDate);
  useSubscriptionToOngoingOpChangedEvents(connectionDate);
  useSubscriptionToOmnibusBalanceChangedEvents(connectionDate);

  return (
    <>
      <Navigation />
      <Router />
    </>
  );
};
