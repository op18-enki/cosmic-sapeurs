import { sapAllOmniMapCountGetter, sapMapCountGetter } from "app/countGetters";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { contrastedMapColors, coverStyles } from "app/theme";
import { Valorization } from "core-logic";
import { ContrastedLeafletMap, LeafletMapInputs } from "ui";

const getSapColors = (valorization: Valorization) => {
  const colors = [
    contrastedMapColors.red,
    contrastedMapColors.orange,
    contrastedMapColors.green,
  ];

  if (valorization === "sap")
    return makeMapColorAndScale({
      colors,
      countGetter: sapAllOmniMapCountGetter,
    });

  return makeMapColorAndScale({
    colors,
    countGetter: sapMapCountGetter,
  });
};

export const CoverMapVsavWithOmnibusIncludedWithOthers = (props: {
  data: LeafletMapInputs;
  valorization: Valorization;
  mapContainerStyle?: React.CSSProperties;
  mapZoom?: number;
}) => {
  return (
    <ContrastedLeafletMap
      colors={getSapColors(props.valorization)}
      data={props.data}
      omnibusDisplay={
        props.valorization === "sap" ? "includeWithOthers" : "notShown"
      }
      subCategory="sap"
      titleColor={coverStyles.sap.mainColor}
      legendPosition="top"
      mapContainerStyle={props.mapContainerStyle}
      mapZoom={props.mapZoom}
    />
  );
};
