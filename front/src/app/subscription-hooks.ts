import { actions, AppThunk } from "core-logic";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "./redux-hooks";

const useSubscription = (
  subscribeThunk: () => AppThunk,
  unsubscribeThunk: () => AppThunk,
  connectionDate: Date,
): void => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(subscribeThunk());
    return () => {
      dispatch(unsubscribeThunk());
    };
  }, [connectionDate]);
};

export const useSubscriptionToAvailabilityChangedEvents = (
  connectionDate: Date,
) =>
  useSubscription(
    actions.subscribeToAvailabilityChangedEventsThunk,
    actions.unsubscribeFromAvailabilityChangedEventsThunk,
    connectionDate,
  );

export const useSubscriptionToOngoingOpChangedEvents = (connectionDate: Date) =>
  useSubscription(
    actions.subscribeToOngoingOpChangedEventsThunk,
    actions.unsubscribeFromOngoingOpChangedEventsThunk,
    connectionDate,
  );

export const useSubscriptionToOmnibusBalanceChangedEvents = (
  connectionDate: Date,
) =>
  useSubscription(
    actions.subscribeToOmnibusBalanceChangedEventsThunk,
    actions.unsubscribeFromOmnibusBalanceChangedEventsThunk,
    connectionDate,
  );

export const useSubscriptionToResetCoverOpsEvents = (connectionDate: Date) => {
  useSubscription(
    actions.subscribeToResetCoverOpsRequestedThunk,
    actions.unsubscribeFromResetCoverOpsRequestedThunk,
    connectionDate,
  );
  useSubscription(
    actions.subscribeToResetCoverOpsFinishedThunk,
    actions.unsubscribeFromResetCoverOpsFinishedThunk,
    connectionDate,
  );
};

export const useBatchDispatchLoop = (connectionDate: Date) =>
  useSubscription(
    actions.loopOverDispatchBatchThunk,
    actions.stopLoopOverDispatchBatchThunk,
    connectionDate,
  );

export const useListenToSocketDisconnection = () => {
  const [connectionDate, setConnectionDate] = useState<Date>(new Date());
  const [numberOfConnections, setNumberOfReconnections] = useState<number>(0);
  const pubSubStatus = useSelector((state) => state.interactions.pubSubStatus);
  const dispatch = useDispatch();

  const maxNumberOfConnectionTries = 6;

  useEffect(() => {
    if (
      pubSubStatus === "closed" &&
      numberOfConnections < maxNumberOfConnectionTries
    ) {
      dispatch(actions.pubSubStatusUpdated("tryingToReconnect"));
    }
    if (pubSubStatus === "tryingToReconnect") {
      if (numberOfConnections < maxNumberOfConnectionTries) {
        setTimeout(() => {
          setConnectionDate(new Date());
          setNumberOfReconnections(numberOfConnections + 1);
        }, 1_250 * 2 ** numberOfConnections);
      } else {
        dispatch(actions.pubSubStatusUpdated("closed"));
      }
    }

    if (pubSubStatus === "open") {
      setNumberOfReconnections(0);
    }
  }, [pubSubStatus]);

  return connectionDate;
};
