import { coverStyles } from "app/theme";
import { useCalculatedSums } from "app/useCalculatedSums";
import type { CoverOpsVehicleSubCategory } from "core-logic";

const epLabel = "EP dispo";
const sapLabel = "C. San dispo";

export const vehicleToSumUpLabel: Record<CoverOpsVehicleSubCategory, string> = {
  ep: epLabel,
  epWithOmni: epLabel,
  sap: sapLabel,
  sapWithOmni: sapLabel,
};

type SumUpNumberProps = {
  vehicleSubCategory: CoverOpsVehicleSubCategory;
};

export const SumUpNumber = ({ vehicleSubCategory }: SumUpNumberProps) => {
  const { omniTakenIntoAccount, socle, omni } = useCalculatedSums();

  const numSocle = socle[vehicleSubCategory];
  const numOmni = omni[vehicleSubCategory];

  return (
    <UiSumUpNumber
      numberToDisplay={omniTakenIntoAccount ? numSocle + numOmni : numSocle}
      numberOfOmnibus={numOmni}
      areOmnibusCounted={omniTakenIntoAccount}
      vehicleSubCategory={vehicleSubCategory}
    />
  );
};

type UiSumUpNumberProps = {
  areOmnibusCounted: boolean;
  numberToDisplay: number;
  numberOfOmnibus: number;
  vehicleSubCategory: CoverOpsVehicleSubCategory;
};

export const UiSumUpNumber = ({
  areOmnibusCounted,
  numberToDisplay,
  numberOfOmnibus,
  vehicleSubCategory,
}: UiSumUpNumberProps) => {
  const label = vehicleToSumUpLabel[vehicleSubCategory];
  const color = coverStyles[vehicleSubCategory].mainColor;

  return (
    <div className="px-5 text-6xl flex flex-col items-center" style={{ color }}>
      <span className="font-bold">{numberToDisplay}</span>
      <div className="text-sm w-44 text-center">
        <p>{label}</p>
        {areOmnibusCounted ? (
          <p>(dont {numberOfOmnibus} modulaires)</p>
        ) : (
          <p>(hors modulaires)</p>
        )}
      </div>
    </div>
  );
};
