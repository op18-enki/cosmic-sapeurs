import {
  epAllOmniMapCountGetter,
  epMapCountGetter,
  epOmniEpOnlyMapCountGetter,
} from "app/countGetters";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { useSelector } from "app/redux-hooks";
import { contrastedMapColors, coverStyles } from "app/theme";
import { takeOmnibusIntoAccountSelector } from "core-logic";
import { ContrastedLeafletMap, LeafletMapInputs } from "ui";
import { OmnibusDisplayOption } from "../ui/map/LeafletMap";

const getEpColors = (omnibusDisplay: OmnibusDisplayOption) => {
  const colors = [
    contrastedMapColors.red,
    contrastedMapColors.orange,
    contrastedMapColors.green,
  ];

  switch (omnibusDisplay) {
    case "greenLines":
      return makeMapColorAndScale({
        colors,
        countGetter: epOmniEpOnlyMapCountGetter,
      });
    case "includeWithOthers":
      return makeMapColorAndScale({
        colors,
        countGetter: epAllOmniMapCountGetter,
      });
    case "notShown":
      return makeMapColorAndScale({
        colors,
        countGetter: epMapCountGetter,
      });
    default:
      const exhaustiveCheck: never = omnibusDisplay;
      return exhaustiveCheck;
  }
};

export const CoverMapEp = (props: { data: LeafletMapInputs }) => {
  const takeOmnibusIntoAccount = useSelector(takeOmnibusIntoAccountSelector);
  const omnibusDisplay: OmnibusDisplayOption = takeOmnibusIntoAccount
    ? "includeWithOthers"
    : "notShown";

  return (
    <ContrastedLeafletMap
      colors={getEpColors(omnibusDisplay)}
      data={props.data}
      omnibusDisplay={omnibusDisplay}
      subCategory="ep"
      titleColor={coverStyles.ep.mainColor}
      legendPosition="bottom"
    />
  );
};
