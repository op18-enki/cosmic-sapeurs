import type { FlattenOnGoingOpEvent } from "app/pages/DetailedOps/DetailedOps";
import {
  coverStyles,
  omnibusBothAvailableColor,
  stripesColor,
} from "app/theme";
import {
  AvailabilityKind,
  AvailabilityKindWithoutOmnibusUnavailable,
  OmnibusBalanceKind,
  OperationCause,
} from "interfaces";
import { ColorsBy } from "ui";

type LabelAndColor = {
  label: string;
  color: string;
};

export const procedureToColorAndLabel: Partial<Record<string, LabelAndColor>> =
  {
    O: { label: "Orange", color: "#ff8c00" },
    V: { label: "Verte", color: "#90ef8f" },
    R: { label: "Rouge", color: "#ff0100" },
    B: { label: "Blanche", color: "white" },
    H: { label: "HMB", color: "#6a5bcd" },
    // H: { label: "HMB", color: "#6a5bcd80" },
    Normal: { label: "MEG", color: "cyan" },
    S: { label: "Retard", color: "#da7192" },
    P: { label: "INU", color: "#1d90ff" },
    I: { label: "Adresse inexistante", color: "silver" },
    E: { label: "Exercice", color: "magenta" },
  };

export type LabelOrStrongLabel =
  | { label: string; strongLabel?: string }
  | { label?: string; strongLabel: string };

export const simpleCountKindToLabel: Record<AvailabilityKind, string> = {
  available: "Disponible",
  on_operation: "Sur intervention",
  recoverable: "Récupérable",
  unavailable: "Indisponible",
  unavailable_omnibus: "Indisponible omnibus",
};

export const countKindToLabel: Record<
  AvailabilityKindWithoutOmnibusUnavailable,
  LabelOrStrongLabel
> = {
  available: { strongLabel: simpleCountKindToLabel.available },
  on_operation: { label: simpleCountKindToLabel.on_operation },
  recoverable: { label: simpleCountKindToLabel.recoverable },
  unavailable: { label: simpleCountKindToLabel.unavailable },
};

export const labelAndColorByBalanceOption: ColorsBy<OmnibusBalanceKind> = {
  both_on_inter_san: {
    color: coverStyles.sap.mainColor,
    stripes: stripesColor,
    label: "EP3 et VSAV M sur inter",
  },
  vsav_on_inter_san: {
    color: coverStyles.sap.mainColor,
    label: "VSAV M sur inter, ",
    strongLabel: "EP3 dispo",
  },
  pse_on_inter_san: {
    color: coverStyles.sap.mainColor,
    label: "EP3 sur inter, ",
    strongLabel: "VSAV M dispo",
  },
  both_available: {
    color: omnibusBothAvailableColor,
    strongLabel: "Ensemble constitué dispo",
  },
  pse_on_inter_fire: {
    color: coverStyles.ep.mainColor,
    label: "Ensemble sur inter pompe",
    stripes: stripesColor,
  },
  only_pse_on_inter_fire: {
    color: coverStyles.ep.mainColor,
    label: "Soclé EP6 sur inter pompe",
    stripes: stripesColor,
  },
  only_pse_available: {
    color: coverStyles.ep.mainColor,
    strongLabel: "Ensemble soclé EP6 dispo",
  },
  dangling: {
    color: "grey",
    label: "Etat instable",
  },
};

export const operationToLabel: Record<OperationCause, string> = {
  fire: "Feux (1••)",
  accident: "Accidents (2••)",
  victim: "SAV (3••)",
  people_protection: "Protection des personnes (4••)",
  animal: "Animaux (5••)",
  water_gaz_electricity: "Eau, Gaz, Electricité (6••)",
  property_protection: "Protection des biens (7••)",
  pollution: "Lutte contre la pollution (8••)",
  exploration: "Reconnaissance, recherches (9••)",
  other: "Autre",
};

type FieldToDisplay = Extract<
  keyof FlattenOnGoingOpEvent,
  | "address_area"
  | "address"
  | "departure_criteria"
  | "raw_procedure"
  | "raw_operation_id"
  | "opening_timestamp"
  | "raw_cause"
  | "affected_vehicles"
>;

export const ongoingOpEventFieldToLabel: Record<
  FieldToDisplay | "opened_since" | "affected_vehicles_count",
  string
> = {
  raw_operation_id: "ID d'opération",
  address: "Adresse",
  address_area: "CSTC",
  departure_criteria: "CMA",
  affected_vehicles_count: "Nombre d'engins",
  affected_vehicles: "Liste d'engins",
  opening_timestamp: "Heure d'ouverture",
  opened_since: "Ouverte depuis",
  raw_cause: "Motif",
  raw_procedure: "Procédure",
};
