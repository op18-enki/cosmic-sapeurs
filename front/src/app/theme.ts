import type { AvailabilityKindWithoutUnavailable } from "interfaces";
import type {
  CoverOpsOperationCause,
  CoverOpsVehicleSubCategory,
} from "core-logic";

const commonColors = {
  // unavailable: "gray",
  recoverable: { color: "gray" },
};

export type CoverStyle = {
  mainColor: string;
  byKind: Record<
    AvailabilityKindWithoutUnavailable,
    { color: string; stripes?: string }
  >;
  scale: string[];
};

const hexToRGBBuilder =
  (hexaColor: string) =>
  (opacity = 1) => {
    const r = parseInt(hexaColor.slice(1, 3), 16);
    const g = parseInt(hexaColor.slice(3, 5), 16);
    const b = parseInt(hexaColor.slice(5, 7), 16);
    return `rgba(${r}, ${g}, ${b}, ${opacity})`;
  };

type NumberOfColors = 3 | 4;

const makeScale = (
  hexaColor: string,
  numberOfColors?: NumberOfColors,
): string[] => {
  const hexToRgb = hexToRGBBuilder(hexaColor);
  switch (numberOfColors) {
    case 3:
      return [hexToRgb(0), hexToRgb(0.4), hexToRgb(1)];
    case 4:
    default:
      return [hexToRgb(0), hexToRgb(0.15), hexToRgb(0.6), hexToRgb(1)];
  }
};

const victimRescueMain = "#4CB9DB";
const pumpMain = "#ffa600";

export const contrastedMapColors = {
  red: "#ff988a",
  orange: "#f9ea46",
  green: "#b0f489",
};

export const omnibusBothAvailableColor = "#53a611";
export const stripesColor = "#404040";

const makeSubCategoryStyle = (
  mainColor: string,
  numberOfColorInScale: NumberOfColors,
): CoverStyle => ({
  mainColor,
  byKind: {
    ...commonColors,
    on_operation: { color: mainColor, stripes: stripesColor },
    available: { color: mainColor },
  },
  scale: makeScale(mainColor, numberOfColorInScale).reverse(),
});

const sapStyle = makeSubCategoryStyle(victimRescueMain, 4);
const epStyle = makeSubCategoryStyle(pumpMain, 3);

export const coverStyles: Record<CoverOpsVehicleSubCategory, CoverStyle> = {
  sap: sapStyle,
  sapWithOmni: sapStyle,
  ep: epStyle,
  epWithOmni: epStyle,
};

export type OpsStyle = {
  main: string;
  scale: string[];
};

const victimOpsMain = "#3A48C2";
const fireOpsMain = "#fa5e43";

export const opsColors: Record<CoverOpsOperationCause, OpsStyle> = {
  victim: {
    main: victimOpsMain,
    scale: makeScale(victimOpsMain),
  },
  fire: {
    main: fireOpsMain,
    scale: makeScale(fireOpsMain, 3),
  },
};
