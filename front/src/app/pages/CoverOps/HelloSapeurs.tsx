import { useSelector } from "app/redux-hooks";
import { useDispatch } from "react-redux";
import { actions } from "core-logic";
import { UiButton } from "ui";

const useHelloWorld = () => {
  const message = useSelector((state) => state.helloSapeur.message);
  const isFetching = useSelector((state) => state.helloSapeur.isFetching);
  return { message, isFetching };
};

export const HelloSapeurs = () => {
  const { message, isFetching } = useHelloWorld();
  const dispatch = useDispatch();

  return (
    <>
      <hr />
      <div className="flex items-center">
        <UiButton
          isLoading={isFetching}
          onClick={() => dispatch(actions.helloSapeurThunk())}
        >
          Hello sapeurs !
        </UiButton>
        <div>
          Message:{" "}
          {message ? <strong>{message}</strong> : "Pas encore de message..."}
        </div>
      </div>
    </>
  );
};
