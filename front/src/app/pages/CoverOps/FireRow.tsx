import { CoverMapEp } from "app/CoverMapEp";
import { operationToLabel } from "app/labels";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { useSelector } from "app/redux-hooks";
import { coverStyles, opsColors } from "app/theme";
import type { CoverOpsVehicleSubCategory } from "core-logic";
import {
  actions,
  epBarInputSelector,
  fireLineInputSelector,
  Hours,
  lastTimestampDateSelector,
  mapsInputsSelector,
} from "core-logic";
import { numberOfHoursToShowSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import type { OperationCause } from "interfaces";
import * as R from "ramda";
import { useDispatch } from "react-redux";
import type { OnMapHoverTextMaker } from "ui";
import { LeafletMap, LineStyleByVariable } from "ui";
import { AxisToStyle } from "ui/charts/line/lineChartHooks";
import { ChartsRow } from "./ChartsRow";
import { LineAndBar } from "./LineAndBar";

type Fire = Extract<OperationCause, "fire">;
type Pump = Extract<CoverOpsVehicleSubCategory, "ep">;

const fireLineStyleByVariable: LineStyleByVariable<Fire | Pump> = {
  fire: {
    color: opsColors.fire.main,
    axis: "left",
  },
  ep: {
    color: coverStyles.ep.mainColor,
    axis: "right",
  },
};

const axisToStyle: AxisToStyle = {
  left: {
    color: opsColors.fire.main,
    yMax: 30,
  },
  right: { color: coverStyles.ep.mainColor, yMax: 120 },
};

const nTicks = 6;

type FireRowProps = {
  chartHeight: string;
};

const onMapHoverFireOngoingOps: OnMapHoverTextMaker = ({
  area,
  areaCounts,
}) => {
  const count = areaCounts?.operations.fire;
  return `${area} <ul>${
    R.isNil(count) ? "" : `<li>${count} inter en cours</li>`
  }</ul>`;
};

const fireColors = makeMapColorAndScale({
  colors: opsColors.fire.scale,
  countGetter: (areaCounts) => areaCounts?.operations.fire,
});

export const FireRow = ({ chartHeight }: FireRowProps) => {
  const epBarInputs = useSelector(epBarInputSelector);
  const mapsInputs = useSelector(mapsInputsSelector);
  const fireLineInputs = useSelector(fireLineInputSelector);
  const hoursSliceToShow = useSelector(numberOfHoursToShowSelector);
  const lastTimestamp = useSelector(lastTimestampDateSelector);
  const dispatch = useDispatch();

  return (
    <ChartsRow
      elements={[
        <LeafletMap
          data={mapsInputs}
          onHoverTextMaker={onMapHoverFireOngoingOps}
          getColorForArea={fireColors.getColorForArea}
          scale={fireColors.scale}
          title={`Nbr d'inter. ${operationToLabel["fire"]} \n en cours par cstc`}
          legendPosition="bottom"
          omnibusDisplay="notShown"
        />,
        <LineAndBar<Pump | Fire>
          chartHeight={chartHeight}
          axisToStyle={axisToStyle}
          lineStyleByVariable={fireLineStyleByVariable}
          barChartData={epBarInputs}
          lineChartData={fireLineInputs}
          hoursToShow={hoursSliceToShow}
          nTicks={nTicks}
          vehicleSubCategory="ep"
          operationCause="fire"
          tMax={lastTimestamp}
          onSliderChange={(n: Hours) => {
            dispatch(actions.numberOfHoursToShowChanged(n));
          }}
          barLegendPosition="flex-end"
        />,
        <CoverMapEp data={mapsInputs} />,
      ]}
    />
  );
};
