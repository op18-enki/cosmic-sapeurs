import { MapCountGetter } from "app/countGetters";
import { AreaCounts, GetColorForArea } from "ui";
import { makeColorScale, Scale } from "utils";

type MakeMapColorAndScaleParams = {
  colors: string[];
  countGetter: MapCountGetter;
};

export const makeMapColorAndScale = (
  params: MakeMapColorAndScaleParams,
): { scale: Scale; getColorForArea: GetColorForArea } => {
  const { getColor, scale } = makeColorScale({
    colors: params.colors,
    max: params.colors.length - 1,
  });
  return {
    getColorForArea: (counts?: AreaCounts) =>
      getColor(params.countGetter(counts)),
    scale,
  };
};
