import { CoverMapVsav } from "app/CoverMapVsav";
import { operationToLabel } from "app/labels";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { useSelector } from "app/redux-hooks";
import { coverStyles, opsColors } from "app/theme";
import {
  actions,
  CoverOpsVehicleSubCategory,
  Hours,
  lastTimestampDateSelector,
  mapsInputsSelector,
  sapBarInputSelector,
  victimLineInputSelector,
} from "core-logic";
import { numberOfHoursToShowSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { OperationCause } from "interfaces";
import * as R from "ramda";
import { useDispatch } from "react-redux";
import type { OnMapHoverTextMaker } from "ui";
import { LeafletMap, LineStyleByVariable } from "ui";
import { AxisToStyle } from "ui/charts/line/lineChartHooks";
import { ChartsRow } from "./ChartsRow";
import { LineAndBar } from "./LineAndBar";

type Victim = Extract<OperationCause, "victim">;
type Sap = Extract<CoverOpsVehicleSubCategory, "sap">;

const victimLineStyleByVariable: LineStyleByVariable<Victim | Sap> = {
  victim: {
    color: opsColors.victim.main,
    axis: "left",
  },
  sap: {
    color: coverStyles.sap.mainColor,
    axis: "right",
  },
};

const nTicks = 6;

type VictimRowProps = {
  chartHeight: string;
};

const axisToStyle: AxisToStyle = {
  left: {
    color: opsColors.victim.main,
    yMax: 150,
  },
  right: { color: coverStyles.sap.mainColor, yMax: 150 },
};

const onMapHoverVictimOngoingOps: OnMapHoverTextMaker = ({
  area,
  areaCounts,
}) => {
  const count = areaCounts?.operations.victim;
  return `${area} <ul>${
    R.isNil(count) ? "" : `<li>${count} inter en cours</li>`
  }</ul>`;
};

const victimColors = makeMapColorAndScale({
  colors: opsColors.victim.scale,
  countGetter: (counts) => counts?.operations.victim,
});

export const VictimRow = ({ chartHeight }: VictimRowProps) => {
  const mapsInputs = useSelector(mapsInputsSelector);
  const vsavBarInput = useSelector(sapBarInputSelector);
  const victimLineInputs = useSelector(victimLineInputSelector);
  const hoursSliceToShow = useSelector(numberOfHoursToShowSelector);
  const lastTimestamp = useSelector(lastTimestampDateSelector);
  const dispatch = useDispatch();

  return (
    <ChartsRow
      elements={[
        <LeafletMap
          data={mapsInputs}
          onHoverTextMaker={onMapHoverVictimOngoingOps}
          getColorForArea={victimColors.getColorForArea}
          scale={victimColors.scale}
          title={`Nbr d'inter. ${operationToLabel["victim"]} en cours par cstc`}
          legendPosition="top"
          omnibusDisplay="notShown"
        />,
        <LineAndBar<Victim | Sap>
          chartHeight={chartHeight}
          axisToStyle={axisToStyle}
          lineStyleByVariable={victimLineStyleByVariable}
          barChartData={vsavBarInput}
          lineChartData={victimLineInputs}
          hoursToShow={hoursSliceToShow}
          nTicks={nTicks}
          vehicleSubCategory="sap"
          operationCause="victim"
          tMax={lastTimestamp}
          onSliderChange={(n: Hours) => {
            dispatch(actions.numberOfHoursToShowChanged(n));
          }}
          barLegendPosition="start"
        />,
        <CoverMapVsav data={mapsInputs} />,
      ]}
    />
  );
};
