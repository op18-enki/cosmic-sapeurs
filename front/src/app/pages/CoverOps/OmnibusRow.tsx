import { ChartsRow } from "./ChartsRow";
import { OmnibusBarChartHorizontal, UiSwitchWithCustomLabels } from "ui";
import { useSelector } from "app/redux-hooks";
import { omnibusBalanceKindOptions } from "interfaces";
import { useDispatch } from "react-redux";
import {
  actions,
  omnibusBalanceSelector,
  takeOmnibusIntoAccountSelector,
} from "core-logic";
import { labelAndColorByBalanceOption } from "app/labels";

type OmnibusRowProps = {
  chartHeight: string;
};

export const OmnibusRow = ({ chartHeight }: OmnibusRowProps) => {
  const omnibusBarInput = useSelector(omnibusBalanceSelector);
  const showOmnibusOnMap = useSelector(takeOmnibusIntoAccountSelector);
  const dispatch = useDispatch();
  return (
    <ChartsRow
      elements={[
        <div />,
        <OmnibusBarChartHorizontal
          chartHeight={chartHeight}
          data={omnibusBarInput}
          omnibusBalanceOptions={omnibusBalanceKindOptions}
          colorByBalance={labelAndColorByBalanceOption}
          barLabel={"Répartition omnibus"}
        />,
        <div
          className="absolute z-10 w-80"
          style={{ right: "100px", top: "475px" }}
        >
          <UiSwitchWithCustomLabels
            checked={showOmnibusOnMap}
            onChange={() => dispatch(actions.toggleShowOmnibusOnMap())}
            checkedLabel={"Modulaires inclus"}
            notCheckedLabel={"Modulaires non-inclus"}
          />
        </div>,
      ]}
    />
  );
};
