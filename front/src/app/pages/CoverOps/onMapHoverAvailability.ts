import { coverStyles, omnibusBothAvailableColor } from "app/theme";
import { OnMapHoverTextMaker } from "ui";
import * as R from "ramda";

const displayIf =
  (predicate: (value?: number) => boolean) =>
  (value: number | undefined, key: string = "", color: string): string =>
    predicate(value)
      ? `<li  style='color: ${color}'>${value} ${key} dispo</li>`
      : "";

const displayIfNotNil = displayIf((value) => !R.isNil(value));
const displayIfNot0 = displayIf((value) => !R.isNil(value) && value > 0);

export const onMapHoverAvailability: OnMapHoverTextMaker = ({
  area,
  areaCounts,
}) => {
  const sapColor = coverStyles.sap.mainColor;
  const epColor = coverStyles.ep.mainColor;

  const {
    pse_on_inter_san,
    vsav_on_inter_san,
    both_available,
    only_pse_available,
  } = areaCounts?.omnibus ?? {};

  const displayedCounts = [
    displayIfNotNil(areaCounts?.availabilities.sap, "VSAV", sapColor),
    displayIfNot0(pse_on_inter_san, "VSAVM", sapColor),
    displayIfNot0(vsav_on_inter_san, "EP3", sapColor),
    displayIfNot0(
      both_available,
      "Ensemble modulaire",
      omnibusBothAvailableColor,
    ),
    displayIfNot0(only_pse_available, "EP6 soclé pompe", epColor),
    displayIfNotNil(areaCounts?.availabilities.ep, "EP", epColor),
  ];

  const displayedCountsAsString = displayedCounts
    .filter((val) => val !== "")
    .join("");

  const formattedCounts = displayedCountsAsString
    ? displayedCountsAsString
    : "";

  return `<strong>${area}</strong><ul>${formattedCounts}</ul>`;
};
