import { coverStyles } from "app/theme";
import { CoverOpsVehicleSubCategory } from "core-logic";
import { countKindToLabel } from "app/labels";
import type { LabelOrStrongLabel } from "app/labels";

import type { AvailabilityKindWithoutUnavailable } from "interfaces";
import * as R from "ramda";

export const createLabelAndColorByAvailability = (
  role: CoverOpsVehicleSubCategory,
): Record<
  AvailabilityKindWithoutUnavailable,
  LabelOrStrongLabel & { color: string; stripes?: string }
> =>
  R.mapObjIndexed(
    (coverColor, availabilityKind) => ({
      color: coverColor.color,
      stripes: coverColor.stripes,
      ...countKindToLabel[availabilityKind],
    }),
    coverStyles[role].byKind,
  );
