import { useSelector } from "app/redux-hooks";
import { omnibusDetailsSelectorByBalanceKind } from "core-logic/useCases/cover-ops/selectors/omnibusDetailsSelectors";
import type { OmnibusDetail, OmnibusBalanceKind } from "interfaces";
import * as R from "ramda";

type OmnibusBarLegendHoverProps = {
  label: string;
  omnibusBalanceKind: OmnibusBalanceKind;
};

export const OmnibusBarLegendHover = ({
  label,
  omnibusBalanceKind,
}: OmnibusBarLegendHoverProps) => {
  const omnibusDetails: OmnibusDetail[] = useSelector(
    omnibusDetailsSelectorByBalanceKind[omnibusBalanceKind],
  );

  const areas = omnibusDetails.map(({ area }) => area);
  const columns = R.splitAt(areas.length / 2, areas);

  return (
    <div>
      <p className="text-lg underline">{label}</p>
      {!areas.length ? (
        <span className="text-base">Aucun</span>
      ) : (
        <div className="flex justify-around">
          {columns.map((column, i) => {
            return (
              <div key={i}>
                {column.map((area) => (
                  <div key={area} className="text-base">
                    {area}
                  </div>
                ))}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};
