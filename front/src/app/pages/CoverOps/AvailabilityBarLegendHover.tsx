import {
  CoverOpsVehicleSubCategory,
  availabilityEventByIdByAvailabilityKindByBySubCategory,
} from "core-logic";
import { AvailabilityChangedEvent, AvailabilityKind } from "interfaces";
import * as R from "ramda";
import { useSelector } from "react-redux";

type AvailabilityBarLegendHoverProps = {
  label: string;
  availabilityKind: AvailabilityKind;
  subCategory: CoverOpsVehicleSubCategory;
};

export const AvailabilityBarLegendHover = ({
  label,
  availabilityKind,
  subCategory,
}: AvailabilityBarLegendHoverProps) => {
  const countsByKind = useSelector(
    availabilityEventByIdByAvailabilityKindByBySubCategory[subCategory][
      availabilityKind
    ],
  );

  return <AvailabilityDetails label={label} countsByKind={countsByKind} />;
};

type AvailabilityDetailsProps = {
  label: string;
  countsByKind?: Record<string, AvailabilityChangedEvent[]>;
};

export const AvailabilityDetails = ({
  label,
  countsByKind,
}: AvailabilityDetailsProps) => {
  if (!countsByKind) return null;

  const countsByKindEntries = Object.entries(countsByKind);

  return (
    <div>
      <p className="text-lg underline">{label}</p>
      {!countsByKindEntries.length ? (
        <span className="text-base">Aucun</span>
      ) : (
        <ul>
          {countsByKindEntries.map(([status, events]) => {
            return (
              <li key={status} className="text-base mb-2">
                <strong>{events.length}</strong> {status}
                <br />
                <span className="text-xs">
                  {events.map(R.prop("home_area")).join(", ")}
                </span>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};
