import { CoverMapWithOmnibusIncludedWithOthers } from "app/CoverMapWithOmnibusIncludedWithOthers";
import { useSelector } from "app/redux-hooks";
import { actions, Valorization, valorizationSelector } from "core-logic";
import { useDispatch } from "react-redux";
import { valorizationOptions } from "../../../core-logic/useCases/cover-ops/interactions.slice";
import { UiRadio } from "../../../ui/UiRadio";
import { coverStyles } from "../../theme";

const valorizationToLabel: Record<Valorization, string> = {
  both: "Le meilleur des deux mondes",
  sap: "Valorisation C.San",
  ep: "Valorisation EP",
};

const valorizationToColor: Record<Valorization, string> = {
  both: "",
  sap: coverStyles.sap.mainColor,
  ep: coverStyles.ep.mainColor,
};

export const ContrastedCoverMaps = () => {
  const currentValorization = useSelector(valorizationSelector);
  const dispatch = useDispatch();

  return (
    <div className="flex-1 flex flex-col items-center relative">
      <CoverMapWithOmnibusIncludedWithOthers
        vehicleSubCategory="sap"
        legendPosition="top"
      />
      <CoverMapWithOmnibusIncludedWithOthers
        vehicleSubCategory="ep"
        legendPosition="bottom"
      />

      <div className="left-0 right-0 top-0 bottom-0 absolute flex flex-col justify-center z-30">
        <div className="">
          <UiRadio
            className="mt-20"
            options={valorizationOptions.map((value) => ({
              value,
              label: valorizationToLabel[value],
              color: valorizationToColor[value],
            }))}
            value={currentValorization}
            handleChange={(e) => {
              dispatch(actions.setValorization(e.target.value as Valorization));
            }}
          />
        </div>
      </div>
    </div>
  );
};
