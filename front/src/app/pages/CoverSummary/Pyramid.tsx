import { ReactNode } from "react";
import "./pyramid.css";

export type ColorAndText = {
  text: ReactNode;
  color: string;
};

type PyramideProps = {
  top: ColorAndText;
  middle: ColorAndText;
  bottom: ColorAndText;
};

export const Pyramid = ({ top, middle, bottom }: PyramideProps) => {
  return (
    <div className="pyramid">
      <div className="zone relative" style={{ backgroundColor: top.color }}>
        <div className="absolute left-0 right-0 bottom-4">{top.text}</div>
      </div>
      <div className="zone relative" style={{ backgroundColor: middle.color }}>
        <div className="absolute left-16 bottom-2">{middle.text}</div>
      </div>
      <div className="zone relative" style={{ backgroundColor: bottom.color }}>
        <div className="absolute left-16 bottom-8">{bottom.text}</div>
      </div>
    </div>
  );
};
