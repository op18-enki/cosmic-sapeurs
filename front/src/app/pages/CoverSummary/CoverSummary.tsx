import { ContrastedCoverMaps } from "app/pages/CoverSummary/ContrastedCoverMaps";
import { CoverageDistribution } from "app/pages/CoverSummary/CoverageDistribution";
import { PotentialValorizations } from "app/pages/CoverSummary/PotentialValorizations";
import { routes } from "app/Router";
import { Route } from "type-route";

type CoverSummaryProps = { route: Route<typeof routes.coverSummary> };

export const CoverSummary = (props: CoverSummaryProps) => {
  return (
    <div className="flex justify-between mx-5">
      <ContrastedCoverMaps />
      <div className="flex-1">
        <CoverageDistribution />
      </div>
      <PotentialValorizations />
    </div>
  );
};
