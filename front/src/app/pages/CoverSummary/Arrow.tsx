import { makeStyles } from "@material-ui/core";
import { ReactNode } from "react";

const useStyle = makeStyles({
  arrow: (props: { color: string }) => ({
    padding: "8px 0 8px 8px",
    height: "66px",
    minWidth: "180px",
    lineHeight: "50px",
    position: "relative",
    background: props.color,
    textAlign: "center",
  }),
  arrowRight: (props: { color: string }) => ({
    width: 0,
    height: 0,
    borderTop: "50px solid transparent",
    borderBottom: "50px solid transparent",
    borderLeft: `50px solid ${props.color}`,
  }),
});

type ArrowProps = { children: ReactNode; color: string };

export const Arrow = ({ children, color }: ArrowProps) => {
  const classes = useStyle({ color });

  return (
    <div className="flex items-center">
      <div className={classes.arrow}>{children}</div>
      <div className={classes.arrowRight} />
    </div>
  );
};
