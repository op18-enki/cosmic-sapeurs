import { StaticPie } from "app/pages/CoverSummary/StaticPie";
import { useSelector } from "app/redux-hooks";
import { coverStyles, omnibusBothAvailableColor } from "app/theme";
import {
  epBarInputSelector,
  omnibusBalanceSelector,
  sapBarInputSelector,
} from "core-logic";
import * as R from "ramda";

export const CoverageDistribution = () => {
  const epAvailability = useSelector(epBarInputSelector);
  const sapAvailability = useSelector(sapBarInputSelector);
  const omnibusBalance = useSelector(omnibusBalanceSelector);

  const omniInService = R.values(omnibusBalance).reduce(
    (acc, n) => acc! + (n ?? 0),
    0,
  );

  const omniPumpAvailable =
    (omnibusBalance.both_available ?? 0) +
    (omnibusBalance.only_pse_available ?? 0);

  const omniSanAvailable =
    (omnibusBalance.both_available ?? 0) * 2 +
    (omnibusBalance.pse_on_inter_san ?? 0) +
    (omnibusBalance.vsav_on_inter_san ?? 0);

  return (
    <div className="h-full flex items-center justify-center">
      <StaticPie
        data={[
          {
            label: (
              <div>
                EP soclés : <strong>{epAvailability.available}</strong>
              </div>
            ),
            labelPosition: { top: "120px", right: "40px" },
            value: 1,
            // value: epAvailability.available,
            color: coverStyles.ep.mainColor,
          },
          {
            label: (
              <>
                <div>
                  <strong>{omniInService} EPM</strong> en service
                </div>
                <br />
                <div>Modulaires disponibles :</div>
                <div>
                  <strong>{omniSanAvailable} C.San</strong> ou{" "}
                  <strong>{omniPumpAvailable} EP6</strong>
                </div>
              </>
            ),
            labelPosition: { bottom: "40px", left: "120px" },
            value: 1,
            // value: omniPumpAvailable,
            color: omnibusBothAvailableColor,
          },
          {
            label: (
              <div>
                VSAV soclés : <strong>{sapAvailability.available}</strong>
              </div>
            ),
            labelPosition: { left: "40px", top: "120px" },
            value: 1,
            // value: sapAvailability.available,
            color: coverStyles.sap.mainColor,
          },
        ]}
      />
    </div>
  );
};
