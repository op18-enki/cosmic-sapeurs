import { ReactNode } from "react";
import { PieChart } from "react-minimal-pie-chart";
import type { DataEntry } from "react-minimal-pie-chart/types/commonTypes";

type DataEntryWithLabel = DataEntry & {
  label: ReactNode;
  labelPosition: {
    top?: number | string;
    bottom?: number | string;
    left?: number | string;
    right?: number | string;
  };
};

type StaticPieProps = {
  data: DataEntryWithLabel[];
};

export const StaticPie = ({ data }: StaticPieProps) => (
  <div className="relative w-96">
    <PieChart data={data} startAngle={-90} />
    {data.map(({ label, labelPosition: position }) => (
      <div className="absolute" style={position}>
        {label}
      </div>
    ))}
  </div>
);
