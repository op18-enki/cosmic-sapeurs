import { AvailabilityBarLegendHover } from "app/pages/CoverOps/AvailabilityBarLegendHover";
import { Arrow } from "app/pages/CoverSummary/Arrow";
import { useSelector } from "app/redux-hooks";
import { UiSumUpNumber } from "app/SumUpNumber";
import { coverStyles } from "app/theme";
import { useCalculatedSums } from "app/useCalculatedSums";
import {
  CoverOpsVehicleSubCategory,
  epBarInputSelector,
  sapBarInputSelector,
  valorizationSelector,
} from "core-logic";
import { ReactNode } from "react";
import { UiTooltip } from "ui";
import { ColorAndText, Pyramid } from "./Pyramid";

const textWithColor = (
  subCategory: CoverOpsVehicleSubCategory,
  text: ReactNode,
): ColorAndText => ({
  text,
  color: subCategory.includes("ep")
    ? coverStyles.ep.mainColor
    : coverStyles.sap.mainColor,
});

export const PotentialValorizations = () => {
  const { socle, omni } = useCalculatedSums();
  const vsavBarInput = useSelector(sapBarInputSelector);
  const epBarInput = useSelector(epBarInputSelector);
  const valorization = useSelector(valorizationSelector);

  const isEpOmniDisplayed = valorization === "ep" || valorization === "both";
  const isSapOmniDisplayed = valorization === "sap" || valorization === "both";

  return (
    <div className="flex justify-around" style={{ flex: 2 }}>
      <div className="flex flex-col justify-around h-full">
        <div
          className={`flex items-center ${
            valorization === "ep" ? "opacity-50" : ""
          }`}
        >
          <Arrow color={coverStyles.sap.mainColor}>Valorisation VSAV</Arrow>
          <Pyramid
            top={textWithColor("ep", `EP: ${socle.ep}`)}
            middle={textWithColor(
              "sap",
              <div className="flex flex-col items-start">
                <div className="font-bold">VSAV: {socle.sap + omni.sap}</div>
                <div>= {socle.sap} (soclés)</div>
                <div>+ {omni.sap} (modulaires)</div>
              </div>,
            )}
            bottom={textWithColor(
              "sap",
              <UiTooltip
                placement="bottom"
                content={
                  <AvailabilityBarLegendHover
                    label={"VSAV Récupérables"}
                    availabilityKind={"recoverable"}
                    subCategory={"sap"}
                  />
                }
              >
                <>VSAV soclés récupérables: {vsavBarInput.recoverable}</>
              </UiTooltip>,
            )}
          />
          <UiSumUpNumber
            vehicleSubCategory="sap"
            numberToDisplay={
              isSapOmniDisplayed ? socle.sap + omni.sap : socle.sap
            }
            numberOfOmnibus={omni.sap}
            areOmnibusCounted={isSapOmniDisplayed}
          />
        </div>
        <div
          className={`flex items-center ${
            valorization === "sap" ? "opacity-50" : ""
          }`}
        >
          <Arrow color={coverStyles.ep.mainColor}>Valorisation EP </Arrow>
          <Pyramid
            top={textWithColor("sap", `VSAV: ${socle.sap}`)}
            middle={textWithColor(
              "ep",
              <div className="flex flex-col items-start">
                <div className="font-bold">EP: {socle.ep + omni.ep}</div>
                <div>= {socle.ep} (soclés)</div>
                <div>+ {omni.ep} (modulaires)</div>
              </div>,
            )}
            bottom={textWithColor(
              "ep",
              <UiTooltip
                placement="top"
                content={
                  <AvailabilityBarLegendHover
                    label={"EP Récupérables"}
                    availabilityKind={"recoverable"}
                    subCategory={"ep"}
                  />
                }
              >
                <>EP soclés récupérables: {epBarInput.recoverable}</>
              </UiTooltip>,
            )}
          />
          <UiSumUpNumber
            vehicleSubCategory="ep"
            numberToDisplay={isEpOmniDisplayed ? socle.ep + omni.ep : socle.ep}
            numberOfOmnibus={omni.ep}
            areOmnibusCounted={isEpOmniDisplayed}
          />
        </div>
      </div>
    </div>
  );
};
