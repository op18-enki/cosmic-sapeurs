import { OpsFilters } from "app/pages/DetailedOps/OpsFilters";
import { OpsMap } from "app/pages/DetailedOps/OpsMap";
import { useSelector } from "app/redux-hooks";
import { routes } from "app/Router";
import { filteredOnGoingOpEventSelector } from "core-logic";
import { OngoingOpChangedEvent } from "interfaces";
import { Route } from "type-route";

type DetailedOpsProps = { route: Route<typeof routes.detailedOps> };
export type FlattenOnGoingOpEvent = Omit<
  OngoingOpChangedEvent,
  "opening_infos"
> &
  OngoingOpChangedEvent["opening_infos"];

export const DetailedOps = (props: DetailedOpsProps) => {
  const filterSelector = useSelector(filteredOnGoingOpEventSelector);

  const lastOnGoingOpEventsFlatten: FlattenOnGoingOpEvent[] =
    filterSelector.map(
      ({ opening_infos, ...ongoingOpChangedEventWithoutOpeningInfos }) => ({
        ...ongoingOpChangedEventWithoutOpeningInfos,
        ...opening_infos,
      }),
    );

  // lastOnGoingOpEventsFlatten.push({
  //   timestamp: "2021-10-12",
  //   raw_operation_id: "123",
  //   address: "bob street",
  //   address_area: "STMR",
  //   latitude: 48.873792,
  //   longitude: 2.295028,
  //   bspp_ongoing_ops: 12,
  //   area_ongoing_ops: 3,
  //   status: "first_vehicle_affected",
  //   affected_vehicles: ["vehicle1", "vehicle2"],
  //   cause: "fire",
  //   opening_timestamp: "2021-01-10",
  //   departure_criteria: "122",
  //   raw_procedure: "H",
  //   raw_cause: "My raw cause",
  // }); // this is arc de triomphe position

  return (
    <div className="flex flex-col w-full">
      <div className="p-5">
        <OpsFilters data={lastOnGoingOpEventsFlatten} />
      </div>
      <div style={{ height: 800 }}>
        <OpsMap operations={lastOnGoingOpEventsFlatten} />
      </div>
    </div>
  );
};
