import { procedureToColorAndLabel } from "app/labels";
import { divIcon } from "leaflet";
import "./style.css";
const makeMarkerHtmlStyles = (
  color: string,
  isInuReinforced: boolean,
  shouldBlink: boolean,
  opacity: number,
) => `
  opacity: ${opacity};
  background-color: ${color};
  width: 2rem; 
  height: 2rem; 
  display: block;
  left: -1rem;
  top: -1rem;
  position: relative;
  border-radius: 2.5rem 2.5rem 0;
  transform: rotate(45deg);
  font-size: 0.5rem;
  font-weight: ${isInuReinforced ? "bold" : ""};
  margin-bottom: ${isInuReinforced ? "5px" : ""};
  border: ${isInuReinforced ? "3px solid red" : "1px solid grey"};
  ${shouldBlink ? "animation: blinker 1s linear infinite" : ""};`;

const markerContentStyle = `
  position: absolute;
  transform: rotate(-45deg);
  left: 10px;
  top: 0px;
  font-size: 20px;`;

export const getMarkerIcon = (
  procedure: string,
  departure_criteria: string,
  hasVehicles: boolean,
  isToLong: boolean,
  opacity: number,
) => {
  const color = procedureToColorAndLabel[procedure]?.color ?? "grey";

  const isInuReinforced = procedure === "P" && hasVehicles;
  const shouldInuBlink = procedure === "P" && !hasVehicles && isToLong;

  return divIcon({
    className: "my-custom-pin",
    iconAnchor: [0, 24],
    // labelAnchor: [-6, 0],
    popupAnchor: [0, -36],
    html: `<span style="position: relative">
      <span style="${makeMarkerHtmlStyles(
        color,
        isInuReinforced,
        shouldInuBlink,
        opacity,
      )}"/>
      <span style="${markerContentStyle}">${departure_criteria.charAt(0)}</span>
    </span>`,
  });
};
