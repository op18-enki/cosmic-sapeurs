import { FlattenOnGoingOpEvent } from "app/pages/DetailedOps/DetailedOps";
import { OpsTable } from "app/pages/DetailedOps/OpsTable";
import { useSelector } from "app/redux-hooks";
import {
  actions,
  activeOpsFiltersSelector,
  inuBlinkStartTimeSelector,
  procedureOptionsSelector,
} from "core-logic";
import type { FiltrableField } from "core-logic";
import { departureCriteriaOptionsSelector } from "core-logic";
import * as R from "ramda";
import { useDispatch } from "react-redux";
import { UiChip, UiSelect, UiListButton, UiDrawer, UiNumberSelect } from "ui";
import { procedureToColorAndLabel } from "app/labels";
import { withVehicleOnlySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { UiSwitch } from "ui/UiSwitch";
import { opsActions } from "core-logic/useCases/cover-ops/ops.slice";

const filtrableFieldToLabel: Record<FiltrableField, string> = {
  cause: "Famille code motif",
  departure_criteria: "Code motif",
  raw_cause: "Motif de l'opération",
  raw_procedure: "Procédure",
};

type OpsFiltersProps = {
  data: FlattenOnGoingOpEvent[];
};

export const OpsFilters = ({ data }: OpsFiltersProps) => {
  const withVehicleOnly = useSelector(withVehicleOnlySelector);
  const activeFilters = useSelector(activeOpsFiltersSelector);
  const procedureOptions = useSelector(procedureOptionsSelector);
  const departureCriteriaOptions = useSelector(
    departureCriteriaOptionsSelector,
  );
  const dispatch = useDispatch();

  const inuBlinkStartTime = useSelector(inuBlinkStartTimeSelector);

  const changeInuBlinkStartTime = (inuBlinkStartTimeValue: string) => {
    const inuBlinkStartTime = parseInt(inuBlinkStartTimeValue) || 0;
    dispatch(actions.setInuBlinkTime({ inuBlinkStartTime }));
  };

  return (
    <>
      <div className="flex items-center">
        <div className="flex">
          <UiSelect
            label="Procédure"
            options={procedureOptions}
            onChange={(selected) => {
              if (R.isNil(selected)) return;
              dispatch(
                actions.addOpsFilter({
                  field: "raw_procedure",
                  valueToMatch:
                    typeof selected === "string" ? selected : selected.value,
                }),
              );
            }}
          />
          <UiSelect
            label="Code motif"
            options={departureCriteriaOptions}
            onChange={(selected) => {
              if (R.isNil(selected)) return;

              dispatch(
                actions.addOpsFilter({
                  field: "departure_criteria",
                  valueToMatch:
                    typeof selected === "string" ? selected : selected.value,
                }),
              );
            }}
          />
          <UiNumberSelect
            label="délai (minutes) avant alerte INU"
            defaultNumber={inuBlinkStartTime}
            onChange={(e) => changeInuBlinkStartTime(e.target.value)}
          />
          <UiSwitch
            label="Afficher uniquement les opérations avec des véhicules affectés"
            checked={withVehicleOnly}
            onChange={() => dispatch(opsActions.withVehicleOnlyToggled())}
          />
        </div>

        <div className="flex flex-col flex-1">
          <div className="flex flex-1">
            {R.keys(activeFilters).map((filterableField) => {
              const filters = activeFilters[filterableField];

              return (
                <div key={filterableField} className="mx-10 my-2">
                  <h5> {filtrableFieldToLabel[filterableField]}</h5>
                  <hr />
                  <div className="my-2 mx-5 flex flex-wrap">
                    {filters?.map((filter) => (
                      <div key={filter} style={{ margin: "5px 5px 0 5px" }}>
                        <UiChip
                          color={
                            filterableField === "raw_procedure"
                              ? procedureToColorAndLabel[filter]?.color
                              : undefined
                          }
                          label={
                            filterableField === "raw_procedure"
                              ? procedureToColorAndLabel[filter]?.label ??
                                filter
                              : filter
                          }
                          onDelete={() => {
                            dispatch(
                              actions.removeOpsFilter({
                                field: filterableField,
                                valueToMatch: filter,
                              }),
                            );
                          }}
                        />
                      </div>
                    ))}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <UiDrawer content={<OpsTable data={data} />}>
          <UiListButton />
        </UiDrawer>
      </div>
    </>
  );
};
