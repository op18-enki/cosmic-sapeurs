import {
  ongoingOpEventFieldToLabel,
  procedureToColorAndLabel,
} from "app/labels";
import { FlattenOnGoingOpEvent } from "app/pages/DetailedOps/DetailedOps";
import { format } from "date-fns";
import { SimpleTable } from "ui";

type OpsTableProps = {
  data: FlattenOnGoingOpEvent[];
};
export const OpsTable = ({ data }: OpsTableProps) => {
  const relevantDatas = data.map(
    ({
      address_area,
      departure_criteria,
      raw_procedure,
      raw_operation_id,
      opening_timestamp,
      raw_cause,
      affected_vehicles,
      address,
    }) => ({
      [ongoingOpEventFieldToLabel.raw_procedure]: (
        <p
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <span
            style={{
              height: 20,
              width: 20,
              borderRadius: "100%",
              border: "1px solid gray",
              paddingRight: "5px",
              backgroundColor: procedureToColorAndLabel[raw_procedure]?.color,
            }}
          />
        </p>
      ),
      [ongoingOpEventFieldToLabel.raw_operation_id]: raw_operation_id,
      [ongoingOpEventFieldToLabel.address_area]: address_area,
      [ongoingOpEventFieldToLabel.address]: address,
      [ongoingOpEventFieldToLabel.departure_criteria]: departure_criteria,
      [ongoingOpEventFieldToLabel.opening_timestamp]: format(
        new Date(opening_timestamp),
        "dd/MM  HH:mm",
      ),
      [ongoingOpEventFieldToLabel.raw_cause]: raw_cause,
      [ongoingOpEventFieldToLabel.affected_vehicles]: affected_vehicles.length,
    }),
  );

  return <SimpleTable data={relevantDatas} />;
};
