import {
  ongoingOpEventFieldToLabel,
  procedureToColorAndLabel,
} from "app/labels";
import { getMarkerIcon } from "app/pages/DetailedOps/getMarkerIcon";
import { useSelector } from "app/redux-hooks";
import {
  inuBlinkStartTimeSelector,
  lastTimestampDateSelector,
} from "core-logic";
import { lastAvailabilityEventForEachVehicleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityEventForEachVehicleSelector";
import { differenceInMinutes } from "date-fns";
import { ENV } from "environmentVariables";
import * as L from "leaflet";
import "proj4leaflet";
import { propEq } from "ramda";
import { TiledMapLayer } from "react-esri-leaflet";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { DateString, makeSinceStrict, sinceInMinute } from "utils";
import type { FlattenOnGoingOpEvent } from "./DetailedOps";

const timeIsOverFormStartTime =
  (blinkStartTime: number) =>
  (dateOpening: string): boolean =>
    sinceInMinute(dateOpening) >= blinkStartTime;

const makeOpacityFromNow =
  (now: Date) =>
  (openingTimestamp?: DateString): number => {
    if (!openingTimestamp) return 1;

    const age = differenceInMinutes(now, new Date(openingTimestamp));

    switch (true) {
      case age <= 30:
        return 1;
      case age <= 60:
        return 0.82;
      case age <= 90:
        return 0.68;
      default:
        return 0.5;
    }
  };

type OpsMapProps = {
  operations: FlattenOnGoingOpEvent[];
};

const projectionInfos =
  "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";

const crs = new L.Proj.CRS("EPSG:2154", projectionInfos, {
  origin: [484447.0, 6999195.0],
  resolutions: [
    396.87579375158754, 264.5838625010584, 185.20870375074085,
    119.06273812547626, 92.60435187537043, 66.1459656252646, 39.687579375158755,
    26.458386250105836, 19.843789687579378, 13.229193125052918,
    9.260435187537043, 6.614596562526459, 3.9687579375158752,
    2.6458386250105836, 1.9843789687579376, 1.3229193125052918,
    1.0583354500042335, 0.7937515875031751, 0.6614596562526459,
    0.5291677250021167, 0.26458386250105836, 0.13229193125052918,
    0.06614596562526459,
  ],
});

const baseMapOptions: {
  className: string;
  center: L.LatLngTuple;
  zoom: number;
} = {
  className: "w-full h-full",
  center: [48.853, 2.35],
  zoom: 11,
};

export const OpsMap = ({ operations }: OpsMapProps) => {
  const timeOfLastEvent = useSelector(lastTimestampDateSelector);
  const availabilityEvents = useSelector(
    lastAvailabilityEventForEachVehicleSelector,
  );
  const sinceStrict = makeSinceStrict(timeOfLastEvent);
  const mapOptions = ENV.mapTilesUrl.includes("openstreetmap")
    ? { ...baseMapOptions }
    : { ...baseMapOptions, crs, zoom: 6 };

  const inuBlinkStartTime = useSelector(inuBlinkStartTimeSelector);
  const isTimeOver = timeIsOverFormStartTime(inuBlinkStartTime);
  const opacityFromNow = makeOpacityFromNow(timeOfLastEvent);

  return (
    <MapContainer {...mapOptions}>
      {ENV.mapTilesUrl.includes("openstreetmap") ? (
        <TileLayer url={ENV.mapTilesUrl} />
      ) : (
        <TiledMapLayer url={ENV.mapTilesUrl} useCors={false} tileSize={512} />
      )}
      {operations.map(
        ({
          latitude,
          longitude,
          raw_procedure,
          raw_cause,
          raw_operation_id,
          departure_criteria,
          address,
          affected_vehicles,
          opening_timestamp,
          address_area,
        }) => (
          <Marker
            key={raw_operation_id}
            icon={getMarkerIcon(
              raw_procedure,
              departure_criteria,
              affected_vehicles.length !== 0,
              isTimeOver(opening_timestamp),
              opacityFromNow(opening_timestamp),
            )}
            position={[latitude, longitude]}
          >
            <Popup>
              <div>
                {ongoingOpEventFieldToLabel.raw_operation_id} :{" "}
                {raw_operation_id}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.address} : {address}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.address_area} : {address_area}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.opened_since} :{" "}
                {sinceStrict(opening_timestamp)}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.raw_procedure} :{" "}
                {procedureToColorAndLabel[raw_procedure]?.label ??
                  raw_procedure}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.departure_criteria} :{" "}
                {departure_criteria}
              </div>
              <div>
                {ongoingOpEventFieldToLabel.raw_cause}: {raw_cause}
              </div>
              <div>
                <div>
                  {ongoingOpEventFieldToLabel.affected_vehicles_count} :{" "}
                  {affected_vehicles.length}
                </div>
                {affected_vehicles.length !== 0 && (
                  <div>
                    {ongoingOpEventFieldToLabel.affected_vehicles} :{" "}
                    <ul className="pl-4 w-3/4">
                      {affected_vehicles.map((rawVehicleId) => {
                        const eventData = availabilityEvents.find(
                          propEq("raw_vehicle_id", rawVehicleId),
                        )?.latest_event_data;
                        return (
                          eventData && (
                            <li className="list-disc " key={rawVehicleId}>
                              <div className="flex justify-between">
                                <div>{eventData.vehicle_name}</div>
                                <div> {eventData.raw_status}</div>
                              </div>
                            </li>
                          )
                        );
                      })}
                    </ul>
                  </div>
                )}
              </div>
            </Popup>
          </Marker>
        ),
      )}
    </MapContainer>
  );
};
