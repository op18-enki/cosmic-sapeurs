import { useSelector } from "app/redux-hooks";
import { routes } from "app/Router";
import { coverStyles } from "app/theme";
import { useCalculatedSums } from "app/useCalculatedSums";
import { mapsInputsSelector } from "core-logic";
import { Route } from "type-route";
import { CoverMapVsavWithOmnibusIncludedWithOthers } from "app/CoverMapVsavWithOmnibusIncludedWithOthers";
import { PubSubStatusChip } from "app/Navigation/PubSubStatusChip";

type VsavCoverProps = { route: Route<typeof routes.vsavCover> };

export const VsavCover = (props: VsavCoverProps) => {
  const mapsInputs = useSelector(mapsInputsSelector);
  const { socle, omni } = useCalculatedSums();

  const numSocle = socle.sapWithOmni;
  const numOmni = omni.sapWithOmni;

  const pubSubStatus = useSelector((state) => state.interactions.pubSubStatus);

  return (
    <div className="flex m-5 items-center justify-evenly gap-5">
      <div className="absolute top-4 right-4">
        <PubSubStatusChip status={pubSubStatus} />
      </div>
      <div>
        <CoverMapVsavWithOmnibusIncludedWithOthers
          data={mapsInputs}
          valorization={"sap"}
          mapContainerStyle={{ width: "850px", height: "850px" }}
          mapZoom={11}
        />
      </div>
      <div className="text-4xl">
        <p>
          <VsavNumber biggest>{numSocle + numOmni}</VsavNumber> VSAV disponibles
        </p>
        <br />
        <br />
        <p>
          dont <VsavNumber>{omni.sapWithOmni}</VsavNumber> modulaires
        </p>
      </div>
    </div>
  );
};

const VsavNumber = ({
  children,
  biggest,
}: {
  children: number;
  biggest?: boolean;
}) => (
  <span
    style={{
      color: coverStyles.sap.mainColor,
      fontSize: biggest ? "16rem" : "8rem",
    }}
  >
    {children}
  </span>
);
