import { simpleCountKindToLabel } from "app/labels";
import { AvailabilityDetails } from "app/pages/CoverOps/AvailabilityBarLegendHover";
import { useSelector } from "app/redux-hooks";
import { routes } from "app/Router";
import { actions, vehicleRoleClassification, vehiclesLabels } from "core-logic";
import {
  areVehicleLinkedSelector,
  selectedVehicleRolesSelector,
} from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import {
  lastAvailabilityEventsForVehicleRoleByRawStatusSelector,
  mapByVehicleRoleDataSelector,
} from "core-logic/useCases/cover-ops/selectors/lastAvailabilityEventForVehicleRoleSelector";
import { Availability, VehicleRole } from "interfaces";
import * as R from "ramda";
import { useDispatch } from "react-redux";
import { Route } from "type-route";
import {
  LeafletMapForAnyVehicle,
  Option,
  UiChip,
  UiSelect,
  UiTooltip,
} from "ui";
import { UiFab } from "ui/UiFab";
import DeleteIcon from "@material-ui/icons/Delete";
import { UiSwitch } from "../../../ui/UiSwitch";

type CoverByVehicleRoleProps = {
  route: Route<typeof routes.coverByVehicleRole>;
};

export const CoverByVehicleRolePage = ({ route }: CoverByVehicleRoleProps) => (
  <div className="flex flex-wrap">
    {[0, 1, 2, 3].map((index) => (
      <CoverByVehicleRole key={index} index={index} />
    ))}
  </div>
);

const CoverByVehicleRole = ({ index }: { index: number }) => {
  const dispatch = useDispatch();

  const countsByKind4times = useSelector(
    lastAvailabilityEventsForVehicleRoleByRawStatusSelector,
  );
  const countsByKind = countsByKind4times[index];

  const dataByArea4times = useSelector(mapByVehicleRoleDataSelector);
  const dataByArea = dataByArea4times[index];

  const selectedRoles4times = useSelector(selectedVehicleRolesSelector);
  const selectedVehicleRoles = selectedRoles4times[index];

  const areVehicleLinked4times = useSelector(areVehicleLinkedSelector);
  const areVehicleLinked = areVehicleLinked4times[index];

  return (
    <div
      className="m-2 relative bg-white"
      style={{
        minWidth: "49%",
        boxShadow:
          "rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px",
      }}
    >
      <div className="flex" style={{ height: "490px" }}>
        <div className="h-full">
          <div className="absolute top-0 left-2 z-10 flex">
            <VehicleCategoryFilter
              alreadySelectedRoles={selectedVehicleRoles}
              addRoles={(rolesToAdd) =>
                dispatch(
                  actions.addSelectedVehicleRole({
                    index,
                    rolesToAdd,
                  }),
                )
              }
            />
            <UiSwitch
              label="Vehicules liés (ex: FA/CA)"
              checked={areVehicleLinked}
              onChange={() => dispatch(actions.areVehicleLinkedToggled(index))}
            />
          </div>
          <div className="mt-12" style={{ zIndex: -1 }}>
            <LeafletMapForAnyVehicle
              title={
                !selectedVehicleRoles
                  ? "Aucun type de véhicule sélectionné"
                  : ""
              }
              titleColor="black"
              data={dataByArea}
              makePopupText={makePopupText}
              legendPosition="top"
            />
          </div>
        </div>
        {selectedVehicleRoles && (
          <div className="w-96 overflow-y-scroll h-full">
            <div className="p-4">
              {selectedVehicleRoles.length > 0 && (
                <UiFab
                  style={{ marginRight: "10px" }}
                  onClick={() =>
                    dispatch(actions.clearSelectedVehicleRoles(index))
                  }
                >
                  <UiTooltip content="Vider la sélection" placement="left">
                    <DeleteIcon />
                  </UiTooltip>
                </UiFab>
              )}
              {selectedVehicleRoles.map((role) => (
                <div key={role} className="inline-block m-0.5">
                  <UiChip
                    label={vehiclesLabels.roles[role]}
                    onDelete={() =>
                      dispatch(
                        actions.removeSelectedVehicleRole({
                          index,
                          roleToDelete: role,
                        }),
                      )
                    }
                  />
                </div>
              ))}
            </div>
            <AvailabilityDetails
              label="Sur intervention"
              countsByKind={countsByKind.on_operation}
            />
            <AvailabilityDetails
              label="Récupérable"
              countsByKind={countsByKind.recoverable}
            />
            <AvailabilityDetails
              label="Disponible"
              countsByKind={countsByKind.available}
            />
          </div>
        )}
      </div>
    </div>
  );
};

const makePopupText = ({
  area,
  availability,
}: {
  area: string;
  availability?: Availability;
}): string => {
  if (!availability) return `${area}: Aucun véhicule`;

  const formattedCounts = R.keys(availability)
    .map((key) => {
      const count = availability[key];
      const label = simpleCountKindToLabel[key];
      return `<li key={label}>${label}: ${count}</li>`;
    })
    .join("");

  return `<strong>${area}</strong><ul>${formattedCounts}</ul>`;
};

const vehicleRoleOptions: Option<VehicleRole[]>[] = R.keys(
  vehiclesLabels.roles,
).map((role) => ({
  label: vehiclesLabels.roles[role],
  value: [role],
}));

const familyOptions: Option<VehicleRole[]>[] = R.keys(
  vehicleRoleClassification,
).flatMap((category) =>
  R.keys(vehicleRoleClassification[category]).map(
    (subCategory): Option<VehicleRole[]> => ({
      label: "Catégorie : " + vehiclesLabels.subCategories[subCategory],
      value: vehicleRoleClassification[category][subCategory]!,
    }),
  ),
);

type VehicleCategoryFilterProps = {
  addRoles: (roles: VehicleRole[]) => void;
  alreadySelectedRoles: VehicleRole[];
};

const VehicleCategoryFilter = ({
  addRoles,
  alreadySelectedRoles,
}: VehicleCategoryFilterProps) => {
  const filteredVehicleRoleOptions = vehicleRoleOptions.filter(
    (roleOption) => !alreadySelectedRoles.includes(roleOption.value[0]),
  );

  return (
    <UiSelect
      label="Type de véhicule"
      options={[...filteredVehicleRoleOptions, ...familyOptions]}
      onChange={(selected) => {
        if (R.isNil(selected)) return;
        if (typeof selected === "string") return;
        addRoles(selected.value);
      }}
    />
  );
};
