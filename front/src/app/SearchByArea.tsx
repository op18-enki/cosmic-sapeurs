import { TextField } from "@material-ui/core";
import { bsppAreaOptions } from "interfaces";
import { useDispatch, useSelector } from "react-redux";
import { Autocomplete } from "@material-ui/lab";
import { UiDetailedVehicleTable, vehicleTableHeaders } from "ui";
import {
  actions,
  latestVehicleChangedEventsByIdForAreaSelector,
  selectedAreaSelector,
} from "core-logic";
import type { Area } from "interfaces";
import { addSinceField } from "utils";

const FilterForArea = ({ area }: { area: Area }) => {
  const latestVehicleChangedEventsByIdForArea = useSelector(
    latestVehicleChangedEventsByIdForAreaSelector,
  );
  const latestVehicleChangedEventsByIdForAreaWithSince = addSinceField(
    latestVehicleChangedEventsByIdForArea,
  );
  return (
    <UiDetailedVehicleTable
      data={latestVehicleChangedEventsByIdForAreaWithSince}
      headers={vehicleTableHeaders}
      title={`Véhicules affectés à ${area}`}
    />
  );
};

export const SearchByArea = () => {
  const dispatch = useDispatch();
  const selectedArea = useSelector(selectedAreaSelector);

  return (
    <div className="px-5" style={{ minWidth: "500px" }}>
      <Autocomplete
        freeSolo
        options={bsppAreaOptions}
        getOptionLabel={(option) => option}
        onChange={(event: any, newValue: string | null) => {
          dispatch(actions.setSelectedArea(newValue?.toUpperCase() as Area));
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Recherche par CSTC"
            margin="normal"
            variant="outlined"
            autoFocus
          />
        )}
      />
      {!!selectedArea && <FilterForArea area={selectedArea} />}
    </div>
  );
};
