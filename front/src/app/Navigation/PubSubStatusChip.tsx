import { PubSubStatus } from "core-logic";
import { StatusChip, StatusChipProps } from "ui";

const errorStyle = {
  color: "#ad0000",
  textColor: "#fff",
};

const styleByStatus: Record<PubSubStatus, StatusChipProps> = {
  notConnected: {
    label: "Socket Non connecté",
    color: "#fcc603",
  },
  closed: {
    label: "Socket Cloturé",
    ...errorStyle,
  },
  error: {
    label: "Socket Erreur",
    ...errorStyle,
  },
  open: {
    label: "Socket Connecté",
    color: "#07872b",
    textColor: "#fff",
  },
  tryingToReconnect: {
    label: "Reconnexion en cours...",
    color: "#fcc603",
  },
};

type PubSubStatusChipProps = {
  status: PubSubStatus;
};

export const PubSubStatusChip = ({ status }: PubSubStatusChipProps) => {
  const { color, label, textColor } = styleByStatus[status];
  return <StatusChip color={color} label={label} textColor={textColor} />;
};
