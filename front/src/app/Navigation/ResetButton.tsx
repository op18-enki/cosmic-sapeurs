import { useSelector } from "app/redux-hooks";
import { actions, isLoadingDataSelector } from "core-logic";
import { useDispatch } from "react-redux";
import RefreshIcon from "@material-ui/icons/Refresh";
import { UiFab } from "ui/UiFab";

export const ResetButton = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(isLoadingDataSelector);

  return (
    <UiFab
      // disabled={true}
      disabled={isLoading}
      onClick={() => {
        const hasConfirmed = window.confirm(
          "Attention, avez-vous la validation de l'officer de garde CO pour effectuer une resynchronisation avec adagio ?",
        );
        if (hasConfirmed) {
          dispatch(actions.triggerResetCoverOpsThunk());
        }
      }}
    >
      <RefreshIcon />
    </UiFab>
  );
};
