import { useSelector } from "app/redux-hooks";
import { routes, useRoute } from "app/Router";
import {
  UiTabs,
  LinkTab,
  UiAppBar,
  UiSearchButton,
  UiDrawer,
  SearchByArea,
} from "ui";
import { format } from "date-fns";
import { lastTimestampDateSelector } from "core-logic";
import { PubSubStatusChip } from "./PubSubStatusChip";
import logo from "./operations-18-logo.png";
import { ResetButton } from "app/Navigation/ResetButton";
import {
  isLoadingDataSelector,
  isResetingCoverOpsSelector,
} from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { CircularProgress } from "@material-ui/core";

export const Navigation = () => {
  const route = useRoute();
  const pubSubStatus = useSelector((state) => state.interactions.pubSubStatus);
  const lastTimestampDate = useSelector(lastTimestampDateSelector);
  const isLoading = useSelector(isLoadingDataSelector);
  const isResetingCoverOps = useSelector(isResetingCoverOpsSelector);

  if (route.name === "vsavCover") return null;

  return (
    <UiAppBar isLoading={isLoading}>
      {isResetingCoverOps && (
        <ObstructingMessage
          message={"En cours de synchronisation avec Adagio"}
          withLoader={true}
        />
      )}
      {pubSubStatus === "closed" && (
        <ObstructingMessage
          message={
            "Connection perdue avec le backend, veuillez tenter de recharger la page..."
          }
          withLoader={false}
        />
      )}
      <a className="flex items-center -ml-2 block" {...routes.coverOps().link}>
        <img
          src={logo}
          alt="logo"
          style={{
            height: "40px",
            backgroundColor: "white",
            borderRadius: "50%",
            padding: "2px",
          }}
        />
      </a>
      <UiTabs tabValue={route.name}>
        <LinkTab
          label="Couverture Opérationnelle"
          {...routes.coverOps().link}
          value={routes.coverOps().name}
        />
        <LinkTab
          label="Détail des disponibilités"
          {...routes.detailedCover().link}
          value={routes.detailedCover().name}
        />
        <LinkTab
          label="Détail des opérations"
          {...routes.detailedOps().link}
          value={routes.detailedOps().name}
        />
        <LinkTab
          label="Synthèse Couverture"
          {...routes.coverSummary().link}
          value={routes.coverSummary().name}
        />
        <LinkTab
          label="Couverture par véhicule"
          {...routes.coverByVehicleRole().link}
          value={routes.coverByVehicleRole().name}
        />
      </UiTabs>
      <div className="flex-grow text-right pr-5 flex items-center justify-end gap-4">
        <ResetButton />
        <UiDrawer
          variant="persistent"
          position="left"
          content={<SearchByArea />}
        >
          <UiSearchButton />
        </UiDrawer>
        <div className="ml-2">
          Dernier évènement : {format(lastTimestampDate, "dd/MM/yyyy  HH:mm")}
        </div>
      </div>
      <PubSubStatusChip status={pubSubStatus} />
    </UiAppBar>
  );
};

type ObstructingMessageProps = {
  message: string;
  withLoader: boolean;
};

const ObstructingMessage = ({
  message,
  withLoader,
}: ObstructingMessageProps) => (
  <div
    className="text-white font-bold text-4xl p-24 absolute top-64 left-2/4 flex flex-col items-center justify-between"
    style={{ transform: "translate(-50%)", backgroundColor: "#4050b5" }}
  >
    <div className="text-center">{message}</div>
    {withLoader && (
      <>
        <br />
        <div>
          <CircularProgress color="inherit" />
        </div>
      </>
    )}
  </div>
);
