import {
  epAllOmniMapCountGetter,
  epMapCountGetter,
  MapCountGetter,
  sapAllOmniMapCountGetter,
  sapMapCountGetter,
} from "app/countGetters";
import { makeMapColorAndScale } from "app/pages/CoverOps/makeMapColorAndScale";
import { useSelector } from "app/redux-hooks";
import { contrastedMapColors, coverStyles } from "app/theme";
import {
  mapsInputsSelector,
  Valorization,
  valorizationSelector,
  VehicleSubCategory,
} from "core-logic";
import { ContrastedLeafletMap } from "ui";
import { LegendPosition } from "../ui/map/MapLegend";

type ExtractFromExisting<T, K extends T> = Extract<T, K>;
type EpOrSap = ExtractFromExisting<VehicleSubCategory, "sap" | "ep">;

const gettersByEpOrSap: Record<
  EpOrSap,
  { omni: MapCountGetter; notOmni: MapCountGetter }
> = {
  sap: { notOmni: sapMapCountGetter, omni: sapAllOmniMapCountGetter },
  ep: { notOmni: epMapCountGetter, omni: epAllOmniMapCountGetter },
};

const getVehicleColors = (
  valorization: Valorization,
  vehicleSubCategory: EpOrSap,
): ReturnType<typeof makeMapColorAndScale> => {
  const colors = [
    contrastedMapColors.red,
    contrastedMapColors.orange,
    contrastedMapColors.green,
  ];

  const getters = gettersByEpOrSap[vehicleSubCategory];

  switch (valorization) {
    case "both":
    case vehicleSubCategory:
      return makeMapColorAndScale({ countGetter: getters.omni, colors });
    default:
      return makeMapColorAndScale({ countGetter: getters.notOmni, colors });
  }
};

const vehicleSubCategoryToColor: Record<EpOrSap, string> = {
  ep: coverStyles.ep.mainColor,
  sap: coverStyles.sap.mainColor,
};

const both: Valorization = "both";

type CoverMapWithOmnibusIncludedWithOthersProps = {
  vehicleSubCategory: ExtractFromExisting<VehicleSubCategory, "sap" | "ep">;
  legendPosition: LegendPosition;
};

export const CoverMapWithOmnibusIncludedWithOthers = ({
  vehicleSubCategory,
  legendPosition,
}: CoverMapWithOmnibusIncludedWithOthersProps) => {
  const valorization = useSelector(valorizationSelector);
  const mapsInputs = useSelector(mapsInputsSelector);

  return (
    <ContrastedLeafletMap
      colors={getVehicleColors(valorization, vehicleSubCategory)}
      data={mapsInputs}
      omnibusDisplay={
        [vehicleSubCategory, both].includes(valorization)
          ? "includeWithOthers"
          : "notShown"
      }
      subCategory={vehicleSubCategory}
      titleColor={vehicleSubCategoryToColor[vehicleSubCategory]}
      legendPosition={legendPosition}
    />
  );
};
