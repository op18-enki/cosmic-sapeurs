import * as R from "ramda";

const grayColors = ["white", "gainsboro", "silver", "dimgray", "black"];

type MakeColorScaleParams = {
  colors: string[];
  max: number;
};

export type Scale = { color: string; label: string }[];

export const makeColorScale = ({
  colors = grayColors,
  max,
}: MakeColorScaleParams) => {
  const step = Math.floor(max / (colors.length - 1));

  const scale: Scale = colors.map((color, i) => {
    const getLabel = () => {
      if (colors.length === 1) return "0";
      if (i === colors.length - 1) return `${max}+`;
      return `${i * step}`;
    };

    return { color, label: getLabel() };
  });

  const getColor = (value?: number) => {
    if (R.isNil(value)) return "lightgray";
    if (value < 0) return "lightgreen";
    const foundColor = colors.find(
      (color, k) => k * step <= value && value < (k + 1) * step,
    );
    return foundColor || colors[colors.length - 1];
  };

  return {
    scale,
    getColor,
  };
};
