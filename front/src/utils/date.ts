import {
  differenceInMinutes,
  formatDistance,
  formatDistanceStrict,
} from "date-fns";
import { fr } from "date-fns/locale";
import type { WithTimestamp } from "utils/array";
import { Flavor } from "./flavor";

export type DateString = Flavor<string, "DateString">;

export const addSinceField = <T extends WithTimestamp>(array: T[]) =>
  array.map((item) => ({
    ...item,
    since: since(item.timestamp),
  }));

const since = (dateAsString: string) =>
  formatDistance(new Date(), new Date(dateAsString), {
    locale: fr,
  });

export const makeSinceStrict =
  (referenceDate: Date) => (dateAsString: string) =>
    formatDistanceStrict(referenceDate, new Date(dateAsString), {
      locale: fr,
    });

export const sinceStrict = (dateAsString: string) =>
  makeSinceStrict(new Date())(dateAsString);

export const sinceInMinute = (dateAsString: string) =>
  differenceInMinutes(new Date(), new Date(dateAsString));
