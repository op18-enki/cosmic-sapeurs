import { makeColorScale } from "./color";

describe("Make Color Scale", () => {
  it("Returns undefined when empty color array is given", () => {
    const { getColor } = makeColorScale({ colors: [], max: 4 });
    const color = getColor(2);
    expect(color).toBeUndefined();
  });

  it("Always returns the only color when a 1D color array is given", () => {
    const { getColor, scale } = makeColorScale({ colors: ["black"], max: 4 });
    const color = getColor(2);
    expect(scale).toEqual([{ color: "black", label: "0" }]);
    expect(color).toBe("black");
  });

  it("Returns the color index when range equals color array length", () => {
    const { getColor, scale } = makeColorScale({
      colors: ["green", "yellow", "orange", "red"],
      max: 3,
    });
    expect(scale).toEqual([
      { color: "green", label: "0" },
      { color: "yellow", label: "1" },
      { color: "orange", label: "2" },
      { color: "red", label: "3+" },
    ]);

    expect(getColor(0)).toBe("green");
    expect(getColor(0.8)).toBe("green");
    expect(getColor(1.2)).toBe("yellow");
    expect(getColor(2)).toBe("orange");
    expect(getColor(3)).toBe("red");
    expect(getColor(4)).toBe("red");
  });
});
