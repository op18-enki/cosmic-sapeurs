import { replaceAt, replaceLast, sortByTimestamp } from "./array";

describe("ReplaceLast utils", () => {
  it("Replaces last value when array is of length 1", () => {
    expect(replaceLast(["last"], "newLast")).toEqual(["newLast"]);
  });
  it("Replaces last value when array is of length 3", () => {
    expect(replaceLast(["first", "second", "last"], "newLast")).toEqual([
      "first",
      "second",
      "newLast",
    ]);
  });
});

describe("ReplaceAt utils", () => {
  it("Replaces first value with array of length 1", () => {
    expect(replaceAt(["last"], 0, "newLast")).toEqual(["newLast"]);
  });
  it("Replaces third value when array is of length 4", () => {
    expect(
      replaceAt(["first", "second", "third", "forth"], 2, "newThird"),
    ).toEqual(["first", "second", "newThird", "forth"]);
  });
  it("?? if index out of range", () => {
    expect(
      replaceAt(["first", "second", "third", "forth"], 8, "outOfRange"),
    ).toEqual(["first", "second", "third", "forth"]);
  });
});

describe("sortByTimestamp utils", () => {
  it("Sorts inputs by timestamp descending", () => {
    expect(
      sortByTimestamp(
        [
          { timestamp: "2020-10-21T15:10:59.024Z" },
          { timestamp: "2020-10-21T16:10:59.024Z" },
          { timestamp: "2020-10-21T14:10:59.024Z" },
        ],
        "desc",
      ),
    ).toEqual([
      { timestamp: "2020-10-21T16:10:59.024Z" },
      { timestamp: "2020-10-21T15:10:59.024Z" },
      { timestamp: "2020-10-21T14:10:59.024Z" },
    ]);
  });
  it("Sorts inputs by timestamp ascending", () => {
    expect(
      sortByTimestamp(
        [
          { timestamp: "2020-10-21T15:10:59.024Z" },
          { timestamp: "2020-10-21T16:10:59.024Z" },
          { timestamp: "2020-10-21T14:10:59.024Z" },
        ],
        "asc",
      ),
    ).toEqual([
      { timestamp: "2020-10-21T14:10:59.024Z" },
      { timestamp: "2020-10-21T15:10:59.024Z" },
      { timestamp: "2020-10-21T16:10:59.024Z" },
    ]);
  });
});
