import * as R from "ramda";

type GroupBy = <T, K extends string>(
  fn: (a: T) => K,
) => (list: readonly T[]) => { [k in K]: T[] };

export const groupBy = R.groupBy as GroupBy;

export const replaceAt = <T>(array: T[], index: number, newValue: T) => {
  if (index > array.length) {
    console.warn("Index greater than array length. ");
    return array;
  }
  return [...array.slice(0, index), newValue, ...array.slice(index + 1)];
};

export const removeAtIndex = <T>(array: T[], index: number) =>
  array.splice(index, 1);

export const replaceLast = <T>(array: T[], newValue: T): T[] => {
  if (array.length === 1) return [newValue];
  return [...array.slice(0, -1), newValue];
};

export const mutateLast = <T>(array: T[], newValue: T): void => {
  array.splice(-1, 1, newValue);
};

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

export const rejectIsNil = <T>(array: (T | null | undefined)[]): T[] => {
  return array.filter(notEmpty);
};

export const findBackwards = <T>(
  array: T[],
  cb: (element: T) => boolean,
): [T | undefined, number | undefined] => {
  for (let index = array.length - 1; index >= 0; index--) {
    const element = array[index];
    if (cb(element)) return [element, index];
  }
  return [undefined, undefined];
};

export type WithTimestamp = {
  timestamp: string;
};
export const sortByTimestamp = <T extends WithTimestamp>(
  array: T[],
  order: "asc" | "desc",
) =>
  array.sort(({ timestamp: a }, { timestamp: b }) => {
    return order === "desc"
      ? new Date(b).getTime() - new Date(a).getTime()
      : new Date(a).getTime() - new Date(b).getTime();
  });

export const getLatest = <T extends WithTimestamp>(array: T[]): T | undefined =>
  sortByTimestamp(array, "desc")[0];
