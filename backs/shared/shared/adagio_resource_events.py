from dataclasses import dataclass
from typing import Literal, Optional

from shared.adagio_vehicle_events import AdagioAbregeStatutOperationnel, AdagioArea
from shared.external_event import ExternalEvent
from shared.helpers.date import DateStr
from shared.marshmallow_validate import with_from_dict_classmethod

AdagioAbregeFamilleRessourcesPartagees = Literal[
    "bamr",
    "ban",
    "bap",
    "bars",
    "bec",
    "bel",
    "bem",
    "besi",
    "boitelec",
    "bpe",
    "bpm",
    "bsn",
    "bspc",
    "btrt",
    "bumd",
    "cahp",
    "emf",
    "esi",
    "gvc",
    "linc",
    "lot nrbc",
    "lot san",
    "mpe",
    "mpr",
    "o2",
    "osec",
    "pesi",
    "rac",
    "rp",
    "rtra",
    "smg",
    "spcr",
]


@with_from_dict_classmethod()
@dataclass
class AdagioResourceChangedStatusEventData:
    timestamp: DateStr
    IdRessourcesPartagee: int
    AbregeRessourcesPartagee: Optional[str]
    LibelleRessourcesPartagee: Optional[str]
    IdFamilleRessourcesType: Optional[int]
    AbregeFamilleRessourcesType: str
    LibelleFamilleRessourcesType: str
    ImmatriculationBSPP: str
    ImmatriculationAdministrative: Optional[str]
    Libelle_GTA: Optional[str]
    IdFamilleRessourcesPartagees: Optional[int]
    AbregeFamilleRessourcesPartagees: AdagioAbregeFamilleRessourcesPartagees
    LibelleFamilleRessourcesPartagees: str
    IdAffectationOperationnelle: Optional[int]
    IdAffectationAdministrative: Optional[int]
    IdStatutOperationnel: Optional[int]
    IdSelectionIntervention: Optional[int]
    AdresseIntervention: Optional[str]
    IdIntervention: Optional[int]
    DateStatutOperationnelRessourcePartagee: str
    OrdreGTA: Optional[int]
    IdAffectationStationnement: Optional[int]
    LibelleAffectationStationnement: Optional[str]
    Disponible: Optional[int]
    IdMMASelection: Optional[int]
    RPReduiteAttachee: Optional[int]
    LibelleAffectationOperationnelle: AdagioArea
    LibelleAffectationAdministrative: Optional[str]  # TODO should be Optional[Area]
    AbregeStatutOperationnel: AdagioAbregeStatutOperationnel
    LibelleStatutOperationnel: str
    Cstc: Optional[str]  # Optional[AdagioArea]


TopicAdagioResourceChangedStatus = Literal["adagio_resource_changed_status"]


@with_from_dict_classmethod()
@dataclass
class AdagioResourceChangedStatusEvent(ExternalEvent):
    data: AdagioResourceChangedStatusEventData
    topic: TopicAdagioResourceChangedStatus = "adagio_resource_changed_status"
