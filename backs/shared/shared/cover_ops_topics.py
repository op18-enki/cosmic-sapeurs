from typing import List, Literal, Union

TopicResetCoverOpsRequested = Literal["resetCoverOpsRequested"]
TopicResetCoverOpsFinished = Literal["resetCoverOpsFinished"]
TopicTriggerResetFromSnapshot = Literal["trigger_reset_from_snapshot"]
TopicSetRepositoriesFromSnapshotExecuted = Literal[
    "set_repositories_from_snapshot_executed"
]
TopicConvertersCatchesUpAccumulatedEvents = Literal[
    "converters_catches_up_accumulated_events"
]
TopicConvertersInitializedLastStatusOfAllVehicles = Literal[
    "converters_initialized_last_status_of_all_vehicles"
]
TopicVehicleChangedStatus = Literal["vehicle_changed_status"]
TopicOperationChangedStatus = Literal["operation_changed_status"]
TopicAvailabilityChanged = Literal["availabilityChanged"]
TopicOmnibusBalanceChanged = Literal["omnibusBalanceChanged"]
TopicOngoingOpChanged = Literal["ongoingOpChanged"]


ListenedByFrontCoverOpsDomainTopic = Union[
    TopicAvailabilityChanged,
    TopicOngoingOpChanged,
    TopicOmnibusBalanceChanged,
    TopicResetCoverOpsFinished,
    TopicResetCoverOpsRequested,
]

# TODO : Rename ExternalTopic into StatusChangedTopic
ExternalTopic = Union[TopicVehicleChangedStatus, TopicOperationChangedStatus]
external_topic_options: List[ExternalTopic] = [
    "vehicle_changed_status",
    "operation_changed_status",
]

TopicAreaOngoingOpCountBecameNegative = Literal["area_ongoing_op_count_became_negative"]

CoverOpsDomainTopic = Union[
    ListenedByFrontCoverOpsDomainTopic,
    TopicVehicleChangedStatus,
    TopicOperationChangedStatus,
    TopicConvertersCatchesUpAccumulatedEvents,
    TopicConvertersInitializedLastStatusOfAllVehicles,
    TopicAreaOngoingOpCountBecameNegative,
    TopicResetCoverOpsFinished,
    TopicResetCoverOpsRequested,
    TopicSetRepositoriesFromSnapshotExecuted,
    TopicTriggerResetFromSnapshot,
]
