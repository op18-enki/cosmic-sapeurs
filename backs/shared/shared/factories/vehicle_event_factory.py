from typing import Optional, Union

from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    OmnibusInfos,
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
    VehicleStatus,
    vehicle_status_options,
)
from shared.event_source import EventSource
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import VehicleEvent

default_timestamp = DateStr("2020-01-01T12:00:00.000Z")


def make_vehicle_event_data(
    timestamp: DateStr = None,
    raw_vehicle_id: str = None,
    raw_operation_id: str = None,
    status: VehicleStatus = None,
    raw_status: str = None,
    home_area: Area = None,
    role: VehicleRole = None,
    vehicle_name: str = None,
    omnibus: Optional[OmnibusInfos] = None,
) -> VehicleEventData:
    if status and status not in vehicle_status_options:
        raise ValueError(f"Provided status not in {vehicle_status_options}")
    home_area = home_area or "STOU"
    role = role or "vsav_solo"
    new_raw_vehicle_id: RawVehicleId = RawVehicleId(
        raw_vehicle_id if raw_vehicle_id else "404"
    )

    vehicle_event_data = VehicleEventData(
        timestamp=timestamp or default_timestamp,
        raw_vehicle_id=new_raw_vehicle_id,
        raw_operation_id=RawOperationId(raw_operation_id) if raw_operation_id else None,
        status=status or "selected",
        raw_status=raw_status or "some raw status",
        home_area=home_area,
        role=role,
        vehicle_name=vehicle_name or "default vehicle name",
        omnibus=omnibus,
    )
    return vehicle_event_data


def make_vehicle_event(
    dispatched_at: Optional[DateStr] = None,
    uuid: Optional[str] = None,
    source: Optional[EventSource] = None,
    # Following args are for make_vehicle_event_data
    timestamp: DateStr = None,
    raw_vehicle_id: Optional[Union[RawVehicleId, str]] = None,
    raw_operation_id: Optional[str] = None,
    status: Optional[VehicleStatus] = None,
    raw_status: Optional[str] = None,
    home_area: Optional[Area] = None,
    role: Optional[VehicleRole] = None,
    vehicle_name: Optional[str] = None,
    omnibus: Optional[OmnibusInfos] = None,
) -> VehicleEvent:
    vehicle_event_data = make_vehicle_event_data(
        raw_vehicle_id=raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        raw_status=raw_status,
        home_area=home_area,
        role=role,
        vehicle_name=vehicle_name,
        timestamp=timestamp,
        omnibus=omnibus,
    )
    uuid = uuid or uuid4()
    return VehicleEvent(
        dispatched_at=dispatched_at,
        uuid=uuid,
        data=vehicle_event_data,
        source=source or "converters_online",
    )
