from typing import List, Optional, Union

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationEventData,
    OperationStatus,
    RawOperationId,
    operation_just_opened,
    operation_reinforced,
    operation_status_options,
)
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.event_source import EventSource
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import OperationEvent

default_timestamp = DateStr("2020-01-01T12:00:00.000Z")


def make_opening_infos_kwargs(
    address: str = "1 place jules renard 75017 Paris",
    address_area: Area = "STOU",
    cause: OperationCause = "victim",
    raw_cause: str = "Vélo ...",
    raw_procedure: str = "blanche",
    departure_criteria: str = "Sans casque",
    longitude: float = 32,
    latitude: float = 98,
) -> dict:
    return {
        "address": address,
        "address_area": address_area,
        "cause": cause,
        "raw_cause": raw_cause,
        "raw_procedure": raw_procedure,
        "departure_criteria": departure_criteria,
        "longitude": longitude,
        "latitude": latitude,
    }


def make_operation_event_data(
    timestamp: Optional[DateStr] = None,
    raw_operation_id: Optional[Union[RawOperationId, str]] = None,
    status: OperationStatus = "opened",
    address: Optional[str] = None,
    address_area: Optional[Area] = None,
    cause: Optional[OperationCause] = None,
    raw_cause: Optional[str] = None,
    raw_procedure: Optional[str] = None,
    departure_criteria: Optional[str] = None,
    longitude: Optional[float] = None,
    latitude: Optional[float] = None,
    affected_vehicles: Optional[List[RawVehicleId]] = [],
) -> OperationEventData:
    if status and status not in operation_status_options:
        raise ValueError(f"Provided status not in {operation_status_options}")
    raw_operation_id = RawOperationId(raw_operation_id or "404")
    operation_is_just_opened_or_reinforced = status in [
        operation_just_opened,
        operation_reinforced,
    ]
    operation_event_data = OperationEventData(
        timestamp=timestamp or default_timestamp,
        raw_operation_id=raw_operation_id,
        status=status,
        address=address
        or (
            "1 place jules renard 75017 Paris"
            if operation_is_just_opened_or_reinforced
            else None
        ),
        address_area=address_area
        or ("STOU" if operation_is_just_opened_or_reinforced else None),
        cause=cause or ("victim" if operation_is_just_opened_or_reinforced else None),
        raw_cause=raw_cause
        or (
            "Some emergency description"
            if operation_is_just_opened_or_reinforced
            else None
        ),
        raw_procedure=raw_procedure,
        departure_criteria=departure_criteria,
        latitude=latitude,
        longitude=longitude,
        affected_vehicles=affected_vehicles or [],
    )
    return operation_event_data


def make_operation_event(
    dispatched_at: Optional[DateStr] = None,
    uuid: Optional[str] = None,
    source: Optional[EventSource] = None,
    # Following args are for make_operation_event_data
    timestamp: Optional[DateStr] = None,
    raw_operation_id: Optional[Union[RawOperationId, str]] = None,
    status: OperationStatus = "opened",
    address: Optional[str] = None,
    address_area: Optional[Area] = None,
    cause: Optional[OperationCause] = None,
    raw_cause: Optional[str] = None,
    raw_procedure: Optional[str] = None,
    departure_criteria: Optional[str] = None,
    longitude: Optional[float] = None,
    latitude: Optional[float] = None,
    affected_vehicles: Optional[List[RawVehicleId]] = [],
) -> OperationEvent:
    operation_event_data = make_operation_event_data(
        timestamp=timestamp,
        raw_operation_id=raw_operation_id,
        status=status,
        address=address,
        address_area=address_area,
        cause=cause,
        raw_cause=raw_cause,
        raw_procedure=raw_procedure,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        affected_vehicles=affected_vehicles,
    )
    uuid = uuid or uuid4()
    return OperationEvent(
        dispatched_at=dispatched_at,
        source=source or "converters_online",
        uuid=uuid,
        data=operation_event_data,
    )
