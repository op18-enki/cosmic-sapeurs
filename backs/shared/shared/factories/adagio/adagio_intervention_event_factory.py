from typing import Optional

from shared.adagio_intervention_events import (
    AdagioInterventionChangedStatusEvent,
    AdagioInterventionChangedStatusEventData,
    AdagioInterventionCreatedEvent,
    AdagioInterventionCreatedEventData,
    AdagioInterventionReinforcedEvent,
    AdagioInterventionReinforcedEventData,
    AdagioLibelleStatutIntervention,
    AdagioProcedure,
    Adresse,
    Cstc,
    Emetteur,
    Intervention,
    OdeResume,
    StatutIntervention,
)
from shared.data_transfert_objects.area import Area
from shared.helpers.date import DateStr


def make_adagio_intervention_created_event_data(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    raw_cause: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
    address_x: float = None,
    address_y: float = None,
) -> AdagioInterventionCreatedEventData:
    id_intervention = id_intervention or 12019366

    if timestamp is None:
        timestamp = "2020-12-09T17:35:24.000Z"
    else:
        timestamp = str(timestamp)

    return AdagioInterventionCreatedEventData(
        timestamp=timestamp,
        OdeResume=OdeResume(
            LibelleMotif=raw_cause or "Feu de ... ",
            Delestage=0,
            Procedure=adagio_procedure or "R",
            Precurseur=False,
            AlerteCoordination=False,
            RenfortMoyenDeleste=False,
            RenfortMoyenNonDeleste=False,
            Emetteur=Emetteur(Libelle="blondela"),
            EtatIntervention=timestamp,
            Cstc=Cstc(Libelle=cstc or "STHO"),
            Motif=motif or "111",
            NbEnginsDepart=4,
            ListeEnginsDepart="\nMMA sélectionnés =EPA BSPP_1_BSLT-PSE_1_BSLT-VLR BSPP_1_STHO-FA_1_LAND-\nObservations =MMA manquants: \n\n\nObservations sur le motif d'alerte: CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
            NbEnginsPartis=0,
            NbEnginsPresentes=0,
            Adresse=Adresse(
                IdObjetGeo=4278312,
                CodeTypeReseau="ROUTIER",
                CodeTypeAdresse="ADR_VP",
                Commentaire="",
                X=address_x or 649833.5,
                Y=address_y or 6863363.0,
                LibelleComplet=address or "1 RUE DE L'ELYSEE 75008 8EME ARRONDISSEMENT",
                TypeInfrastructure="",
                CategorieFonctionnelle="RUE",
            ),
            IdIntervention=id_intervention,
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention,
            RenseignementComplementaire="",
        ),
    )


def make_adagio_intervention_reinforced_event_data(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    raw_cause: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
    address_x: float = None,
    address_y: float = None,
) -> AdagioInterventionReinforcedEventData:
    id_intervention = id_intervention or 12019366
    if timestamp is None:
        timestamp = "2020-12-09T17:35:24.000Z"
    else:
        timestamp = str(timestamp)

    return AdagioInterventionReinforcedEventData(
        timestamp=timestamp,
        OdeResume=OdeResume(
            LibelleMotif=raw_cause or "Feu de ... ",
            Delestage=0,
            Procedure=adagio_procedure or "R",
            Precurseur=False,
            AlerteCoordination=False,
            RenfortMoyenDeleste=False,
            RenfortMoyenNonDeleste=False,
            Emetteur=Emetteur(Libelle="blondela"),
            EtatIntervention=timestamp,
            Cstc=Cstc(Libelle=cstc or "STHO"),
            Motif=motif or "111",
            NbEnginsDepart=4,
            ListeEnginsDepart="\nMMA sélectionnés =EPA BSPP_1_BSLT-PSE_1_BSLT-VLR BSPP_1_STHO-FA_1_LAND-\nObservations =MMA manquants: \n\n\nObservations sur le motif d'alerte: CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
            NbEnginsPartis=0,
            NbEnginsPresentes=0,
            Adresse=Adresse(
                IdObjetGeo=4278312,
                CodeTypeReseau="ROUTIER",
                CodeTypeAdresse="ADR_VP",
                Commentaire="",
                X=address_x or 649833.5,
                Y=address_y or 6863363.0,
                LibelleComplet=address or "1 RUE DE L'ELYSEE 75008 8EME ARRONDISSEMENT",
                TypeInfrastructure="",
                CategorieFonctionnelle="RUE",
            ),
            IdIntervention=id_intervention,
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention,
            RenseignementComplementaire="",
        ),
    )


def make_adagio_intervention_changed_status_event_data(
    id_intervention: int = None,
    libelle_statut_intervention: Optional[AdagioLibelleStatutIntervention] = None,
    timestamp: Optional[DateStr] = None,
) -> AdagioInterventionChangedStatusEventData:
    if timestamp is None:
        if timestamp is None:
            timestamp = "2020-12-15T10:38:23.453Z"
        else:
            timestamp = str(timestamp)

    return AdagioInterventionChangedStatusEventData(
        timestamp=timestamp,
        IdHistoriqueIntervention=27,
        StatutIntervention=StatutIntervention(
            IdStatutIntervention=4,
            AbregeStatutIntervention="VAM",
            LibelleStatutIntervention=libelle_statut_intervention
            or "Validation manuelle",
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention or 12434,
            RenseignementComplementaire="",
        ),
        GroupeHoraireModificationStatut=timestamp,
        GroupeHoraireModificationStatutGMT=timestamp,
    )


def make_adagio_intervention_created_event(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    raw_cause: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
    address_x: float = None,
    address_y: float = None,
) -> AdagioInterventionCreatedEvent:
    data = make_adagio_intervention_created_event_data(
        id_intervention=id_intervention,
        cstc=cstc,
        motif=motif,
        raw_cause=raw_cause,
        timestamp=timestamp,
        address=address,
        adagio_procedure=adagio_procedure,
        address_x=address_x,
        address_y=address_y,
    )
    return AdagioInterventionCreatedEvent(
        topic="adagio_intervention_created", data=data
    )


def make_adagio_intervention_reinforced_event(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    raw_cause: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    address_x: float = None,
    address_y: float = None,
) -> AdagioInterventionReinforcedEvent:
    return AdagioInterventionReinforcedEvent(
        topic="adagio_intervention_reinforced",
        data=make_adagio_intervention_reinforced_event_data(
            id_intervention=id_intervention,
            cstc=cstc,
            motif=motif,
            raw_cause=raw_cause,
            timestamp=timestamp,
            address=address,
            address_x=address_x,
            address_y=address_y,
        ),
    )


def make_adagio_intervention_changed_status_event(
    id_intervention: int = None,
    libelle_statut_intervention: Optional[AdagioLibelleStatutIntervention] = None,
    timestamp: Optional[DateStr] = None,
) -> AdagioInterventionChangedStatusEvent:

    return AdagioInterventionChangedStatusEvent(
        data=make_adagio_intervention_changed_status_event_data(
            id_intervention=id_intervention,
            libelle_statut_intervention=libelle_statut_intervention,
            timestamp=timestamp,
        ),
        topic="adagio_intervention_changed_status",
    )
