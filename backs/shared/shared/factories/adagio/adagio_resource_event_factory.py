from typing import Optional
from shared.adagio_resource_events import (
    AdagioAbregeFamilleRessourcesPartagees,
    AdagioResourceChangedStatusEvent,
    AdagioResourceChangedStatusEventData,
)

from shared.adagio_vehicle_events import (
    AdagioAbregeStatutOperationnel,
    AdagioArea,
)
from shared.helpers.date import DateStr


def make_adagio_resource_event(
    id_intervention: int,
    id_resource_partagee: Optional[int] = None,
    abrege_famille_ressources_partagees: Optional[
        AdagioAbregeFamilleRessourcesPartagees
    ] = None,
    timestamp: Optional[str] = None,
    libelle_affectation_operationnelle: Optional[AdagioArea] = None,
    libelle_statut_operationnel: Optional[str] = None,
    abrege_statut_operationnel: Optional[AdagioAbregeStatutOperationnel] = None,
    immatriculation_bspp: Optional[str] = None,
) -> AdagioResourceChangedStatusEvent:
    data = AdagioResourceChangedStatusEventData(
        timestamp=DateStr(timestamp)
        if timestamp
        else DateStr("2020-12-15T17:46:09.850Z"),
        IdRessourcesPartagee=id_resource_partagee or 103,
        AbregeRessourcesPartagee=None,
        LibelleRessourcesPartagee=None,
        IdFamilleRessourcesType=None,
        AbregeFamilleRessourcesType="",
        LibelleFamilleRessourcesType="",
        ImmatriculationBSPP=immatriculation_bspp or "EPA 103",
        ImmatriculationAdministrative=None,
        Libelle_GTA=None,
        IdFamilleRessourcesPartagees=None,
        AbregeFamilleRessourcesPartagees=abrege_famille_ressources_partagees or "bap",
        LibelleFamilleRessourcesPartagees="BAP",
        IdAffectationOperationnelle=None,
        IdAffectationAdministrative=None,
        IdStatutOperationnel=None,
        IdSelectionIntervention=None,
        AdresseIntervention=None,
        IdIntervention=id_intervention,
        DateStatutOperationnelRessourcePartagee="2020-12-15T17:46:09.850Z",
        OrdreGTA=None,
        IdAffectationStationnement=None,
        LibelleAffectationStationnement=None,
        Disponible=None,
        IdMMASelection=None,
        RPReduiteAttachee=None,
        LibelleAffectationOperationnelle=libelle_affectation_operationnelle or "STOU",
        LibelleAffectationAdministrative=None,
        AbregeStatutOperationnel=abrege_statut_operationnel or "RENTRE",
        LibelleStatutOperationnel=libelle_statut_operationnel or "Rentré",
        Cstc=None,
    )
    return AdagioResourceChangedStatusEvent(
        topic="adagio_resource_changed_status",
        data=data,
    )
