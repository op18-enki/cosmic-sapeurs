import abc
from typing import List

from shared.events.event_entity import EventData


class AbstractEventsRepository(abc.ABC):
    _purge_older_than = 6  # hours

    def __init__(self, purge: bool):
        self._should_purge = purge

    def add(self, event_datas: List[EventData]) -> None:
        self._add(event_datas)
        if self._should_purge:
            for event_data in event_datas:
                self.purge_old_similar_events(event_data)

    @abc.abstractmethod
    def _add(self, event_datas: List[EventData]) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def purge_old_similar_events(self, event: EventData):
        raise NotImplementedError
