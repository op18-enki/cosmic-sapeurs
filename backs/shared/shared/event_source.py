from typing import Literal

EventSource = Literal[
    "converters_catch_up",
    "converters_online",
    "converters_reset",
    "sapeurs_notify",
    "sapeurs_resync_cover_from_cache",
    "sapeurs_resync_ops_from_cache",
    "sapeurs_update_ops",
    "sapeurs_update_cover",
    "sapeurs_update_omnibus",
]
