from typing import Dict


def get_in_dict(path: str, dictionary: Dict):
    keys = path.split(".")
    rv = dictionary
    for key in keys:
        rv = rv.get(key)
        if rv is None:
            return
    return rv


def set_in_dict(path: str, dictionary: Dict, new_value) -> None:
    keys = path.split(".")
    for key in keys[:-1]:
        dictionary = dictionary.setdefault(key, {})
    dictionary[keys[-1]] = new_value
