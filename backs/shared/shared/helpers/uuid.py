import uuid as uuid_lib
from abc import ABC, abstractclassmethod
from typing import List


def uuid4() -> str:
    return str(uuid_lib.uuid4())


def pad_string_to_uuid4_length(s: str) -> str:
    target_length = 36  # len(uuid4())
    actual_length = len(s)
    if actual_length < target_length:
        return s + "x" * (target_length - actual_length)
    return s[: target_length - actual_length]


class AbstractUuid(ABC):
    @abstractclassmethod
    def make() -> str:
        raise NotImplementedError


class CustomUuid(AbstractUuid):
    def __init__(self, uuid: str = None) -> None:
        self._next_uuids = []
        if uuid:
            self._next_uuids = [uuid]

    def make(self) -> str:
        real_uuid4 = uuid4()
        return (
            self._next_uuids.pop(0)
            if self._next_uuids
            else pad_string_to_uuid4_length(f"generated_from_custom_uuid_{real_uuid4}")
        )

    def set_next_uuid(self, uuid: str):
        self._next_uuids = [uuid]

    def set_next_uuids(self, uuids: List[str]):
        self._next_uuids = uuids


class RealUuid(AbstractUuid):
    def make(self) -> str:
        return uuid4()
