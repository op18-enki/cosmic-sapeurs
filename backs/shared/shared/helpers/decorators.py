import asyncio
from functools import wraps
from time import time
from typing import Callable, Optional, TypeVar

from .logger import logger

ReturnedValue = TypeVar("ReturnedValue")


def withDuration(message: str, cb: Callable[[], ReturnedValue]) -> ReturnedValue:
    start = time()
    res = cb()
    queryDuration = (time() - start) * 1000
    print(f"{message} : ", queryDuration)
    return res


def timeit(func_name: str = "function", min_duration: Optional[float] = None):
    def _timeit_decorator(func: Callable):
        @wraps(func)
        def _timeit_wrapper(*args, **kwargs):
            start = time()
            try:
                return func(*args, **kwargs)
            finally:
                duration = (time() - start) * 1000
                if min_duration is None or duration > min_duration:
                    logger.info(f"\n[{func_name}] Execution took {duration} ms")

        return _timeit_wrapper

    return _timeit_decorator


def async_timeit(func_name: str = "function", min_duration: Optional[float] = None):
    """See : https://gist.github.com/Integralist/77d73b2380e4645b564c28c53fae71fb"""

    def _timeit_decorator(func):
        async def process(func, *args, **params):
            if asyncio.iscoroutinefunction(func):
                return await func(*args, **params)
            else:
                return func(*args, **params)

        async def helper(*args, **params):
            start = time()
            result = await process(func, *args, **params)

            # Test normal function route...
            # result = await process(lambda *a, **p: print(*a, **p), *args, **params)
            duration = (time() - start) * 1000
            if min_duration is None or duration > min_duration:
                logger.info(f"[{func_name}] Async execution took {duration} ms")
            return result

        return helper

    return _timeit_decorator
