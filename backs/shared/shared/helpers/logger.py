import logging
import os
from datetime import datetime
from pathlib import Path
from typing import Union

from shared.helpers.mkdir_if_relevent import mkdir_if_relevant
from shared.helpers.prepare_get_env_variable import prepare_get_env_variable


class Logger:
    def __init__(self):
        get_env_variable = prepare_get_env_variable(os.environ.get)
        self.mode = get_env_variable("LOGGER_MODE", ["INFO", "WARN", "ERROR"], "ERROR")
        formatter = logging.Formatter("%(asctime)s: %(levelname)-8s %(message)s")

        console = logging.StreamHandler()
        now = datetime.now().strftime("%d-%m-%YT%H.%M.%S")
        mkdir_if_relevant(Path("./data"))
        file = logging.FileHandler(
            f"./data/{now}__logger.log",
            mode="a",
        )
        console.setFormatter(formatter)
        file.setFormatter(formatter)
        self.logger = logging.getLogger()

        if self.mode == "WARN":
            self.logger.setLevel(logging.WARN)
        elif self.mode == "ERROR":
            self.logger.setLevel(logging.ERROR)
        else:
            self.logger.setLevel(logging.INFO)

        self.logger.addHandler(console)
        self.logger.addHandler(file)

    def info(self, msg, *argv: Union[str, object]):
        self.logger.info(msg, *argv)
        if self.mode in ["INFO", "WARN", "ERROR"]:
            print("[INFO] ", msg, *argv)

    def warn(self, msg, *argv: Union[str, object]):
        self.logger.warning(msg, *argv)
        print("[WARN] ", msg, *argv)


logger = Logger()
