import os
from pathlib import Path


def find_file_matching(path: Path, pattern: str) -> Path:
    matching_paths = [path / f for f in os.listdir(path) if Path(f).match(pattern)]
    if not matching_paths:
        raise FileNotFoundError(
            f"Could not find files matching pattern {pattern} in path {path}"
        )
    return sorted(matching_paths)[-1]
