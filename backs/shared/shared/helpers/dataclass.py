import json
from dataclasses import asdict
from typing import Dict


def dataclass_to_dict(dataclass) -> Dict:
    if hasattr(dataclass, "as_dict"):
        return dataclass.as_dict()

    return asdict(dataclass)


def dumps_dataclass(dataclass) -> str:
    return json.dumps(dataclass_to_dict(dataclass))
