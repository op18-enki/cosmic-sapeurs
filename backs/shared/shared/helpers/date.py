import re
from datetime import datetime, timedelta
from time import strftime
from typing import NewType, Optional

from dateutil import tz
from marshmallow.exceptions import ValidationError
from marshmallow.fields import Field

date_format = "%Y-%m-%dT%H:%M:%S.%f%z"
DateStr = NewType("DateStr", str)


def deserialize_datestr(value: str) -> Optional[DateStr]:
    if value == "NoneStr":
        return None
    if isinstance(value, str):
        if is_datestr_format(value):
            return DateStr(value)
        else:
            raise ValidationError(
                f"Could not convert value {value} to DateStr: Expects format {date_format}, got {value}"
            )
    else:
        raise ValidationError(
            f"Could not convert value {value} to DateStr: Expects type str, got {type(value)}"
        )


class DateStrField(Field):
    def _serialize(self, value, attr, obj, **kwargs):
        return str(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return deserialize_datestr(value)


class InvalidDateFormat(Exception):
    pass


def is_datestr_format(date: str) -> bool:
    try:
        datetime.strptime(date, date_format)
        return True
    except:
        return False


def from_datetime(date: datetime) -> DateStr:
    dateAsString = (
        date.strftime("%Y-%m-%dT%H:%M:%S")
        + date.strftime(".%f")[:4]
        + date.strftime("%z")
    ).replace("+0000", "") + "Z"
    return DateStr(dateAsString)


def to_datetime(date: DateStr) -> datetime:
    return datetime.strptime(date, date_format)


def sub_milliseconds(date: DateStr, ms: int) -> DateStr:
    return from_datetime(to_datetime(date) - timedelta(milliseconds=ms))


def add_milliseconds(date: DateStr, ms: int) -> DateStr:
    return from_datetime(to_datetime(date) + timedelta(milliseconds=ms))


def hours_between(t0: DateStr, t1: DateStr) -> int:
    return (to_datetime(t1) - to_datetime(t0)).total_seconds() / 3600
