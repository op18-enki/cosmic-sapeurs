import csv
from pathlib import Path
from typing import List, Literal, Tuple, Union


def writerow(csv_path: Path, row: List, mode: Literal["a", "w"] = "a"):
    with csv_path.open(mode, newline="") as f:
        writer = csv.writer(f)
        writer.writerow(row)


def delete_file(file_path: Path):
    try:
        file_path.unlink()
    except FileNotFoundError:
        pass


def reset_file(file_path: Path):
    try:
        f = open(file_path, "w+")
        f.close()
    except FileNotFoundError:
        pass


def read_csv(csv_path: Union[Path, str]) -> Tuple[List, List]:
    with open(csv_path, "r") as file:
        reader = csv.reader(file)
        headers = next(reader)
        rows = [row for row in reader]
    return headers, rows
