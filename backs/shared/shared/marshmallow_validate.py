from typing import Any, Dict, Type, TypeVar, Union

import marshmallow_dataclass
from marshmallow.exceptions import ValidationError

from shared.exceptions import InvalidEventFormatError


def marshmallow_validate(dict_candidate: Dict, SchemaClass=None):
    schema = marshmallow_dataclass.class_schema(SchemaClass)()
    try:
        return schema.load(dict_candidate)
    except ValidationError as marshmallow_validation_error:
        raise InvalidEventFormatError(marshmallow_validation_error.messages)


from dataclasses import fields as datafields


def dataclass_from_dict(
    dataclass_object: Any, dataclass_as_dict: Dict
) -> Union[Dict, Any]:
    try:
        fieldtypes = {f.name: f.type for f in datafields(dataclass_object)}
        return dataclass_object(
            **{
                f: dataclass_from_dict(fieldtypes[f], dataclass_as_dict[f])
                for f in dataclass_as_dict
            }
        )
    except:
        return dataclass_as_dict


def with_from_dict_classmethod():
    """Decorator to add `from_dict` classmethod to dataclass

    Example :

    @with_from_dict_classmethod()
    @dataclass
    class MyClass:
        a: int

    my_class_from_dict = MyClass.from_dict({"a": 1})

    >> print(my_class_from_dict)
    MyClass(a=1)

    """
    _T_SchemaClass = TypeVar("_T_SchemaClass")

    def decorator(cls):
        @classmethod
        def from_dict(
            cls: Type[_T_SchemaClass],
            input: Dict,
            skip_marshmallow_validation: bool = False,
        ) -> _T_SchemaClass:
            if skip_marshmallow_validation:
                return dataclass_from_dict(cls, input)
            else:
                return marshmallow_validate(SchemaClass=cls, dict_candidate=input)

        setattr(cls, "from_dict", from_dict)

        return cls

    return decorator
