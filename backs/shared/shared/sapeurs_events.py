from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional, Type, TypeVar

from shared.cover_ops_topics import (
    CoverOpsDomainTopic,
    TopicAvailabilityChanged,
    TopicOngoingOpChanged,
)
from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.event_source import EventSource
from shared.exceptions import InvalidEventFormatError
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import deserialize_datestr
from shared.marshmallow_validate import with_from_dict_classmethod

from .cover_ops_topics import (
    TopicAreaOngoingOpCountBecameNegative,
    TopicConvertersCatchesUpAccumulatedEvents,
    TopicConvertersInitializedLastStatusOfAllVehicles,
    TopicOmnibusBalanceChanged,
    TopicOperationChangedStatus,
    TopicResetCoverOpsFinished,
    TopicResetCoverOpsRequested,
    TopicSetRepositoriesFromSnapshotExecuted,
    TopicTriggerResetFromSnapshot,
    TopicVehicleChangedStatus,
)
from .data_transfert_objects.operation_event_data import OperationEventData
from .data_transfert_objects.reset_cover_ops_finished_event_data import (
    ResetCoverOpsFinishedEventData,
)
from .data_transfert_objects.vehicle_event_data import VehicleEventData
from .helpers.date import DateStr, DateStrField

_T_SchemaClass = TypeVar("_T_SchemaClass")


@dataclass
class Event:
    uuid: str
    data: Any  # = None
    topic: CoverOpsDomainTopic
    source: Optional[EventSource] = None
    dispatched_at: Optional[DateStr] = field(
        metadata={
            "marshmallow_field": DateStrField(),
        },  # Custom marshmallow field
        default_factory=lambda: None,
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at)
            if self.dispatched_at
            else "NoneStr",
            "source": self.source,
            "topic": self.topic,
            "uuid": self.uuid,
            "data": dataclass_to_dict(self.data),
        }

    @classmethod
    def from_dict(
        cls: Type[_T_SchemaClass], input: Dict, use_marshmallow: bool = True
    ) -> _T_SchemaClass:
        raise NotImplementedError


operation_changed_status: TopicOperationChangedStatus = "operation_changed_status"


@with_from_dict_classmethod()
@dataclass
class OperationEvent(Event):
    source: EventSource
    data: OperationEventData
    topic: TopicOperationChangedStatus = operation_changed_status


vehicle_changed_status: TopicVehicleChangedStatus = "vehicle_changed_status"


@dataclass
class VehicleEvent(Event):
    source: EventSource
    data: VehicleEventData
    topic: TopicVehicleChangedStatus = vehicle_changed_status

    @classmethod
    def from_dict(
        cls,
        input: Dict,
        skip_marshmallow_validation: bool = False,
    ) -> "VehicleEvent":
        if input.get("topic") != vehicle_changed_status:
            raise InvalidEventFormatError(
                {"topic": ["Must be equal to vehicle_changed_status."]}
            )
        dispatched_at_value = input.get("dispatched_at")
        data = input.get("data")
        if not data:
            raise InvalidEventFormatError(
                {"data": ["Missing data for required field."]}
            )
        return VehicleEvent(
            uuid=input["uuid"],
            dispatched_at=deserialize_datestr(dispatched_at_value)
            if dispatched_at_value
            else None,
            source=input["source"],
            data=VehicleEventData.from_dict(
                data,
                skip_marshmallow_validation=skip_marshmallow_validation,
            ),
        )


@with_from_dict_classmethod()
@dataclass
class ConvertersVehiclesInitEvent(Event):
    data: List[Event]
    topic: TopicConvertersInitializedLastStatusOfAllVehicles = (
        "converters_initialized_last_status_of_all_vehicles"
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": [event.as_dict() for event in self.data],
        }


@with_from_dict_classmethod()
@dataclass
class ConvertersCatchesUpEvent(Event):
    data: List[Event]
    topic: TopicConvertersCatchesUpAccumulatedEvents = (
        "converters_catches_up_accumulated_events"
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": [event.as_dict() for event in self.data],
        }


availability_changed: TopicAvailabilityChanged = "availabilityChanged"
ongoing_op_changed: TopicOngoingOpChanged = "ongoingOpChanged"
omnibus_balance_changed: TopicOmnibusBalanceChanged = "omnibusBalanceChanged"


@dataclass
class AvailabilityChangedEvent(Event):
    data: AvailabilityChangedEventData
    source: EventSource
    topic: TopicAvailabilityChanged = availability_changed

    @classmethod
    def from_dict(
        cls, input: Dict, skip_marshmallow_validation: bool
    ) -> "AvailabilityChangedEvent":
        if not skip_marshmallow_validation:
            raise NotImplementedError
        return cls(
            uuid=input["uuid"],
            dispatched_at=input.get("dispatched_at"),
            source=input["source"],
            data=AvailabilityChangedEventData.from_dict(input["data"]),
        )


@with_from_dict_classmethod()
@dataclass
class OngoingOpChangedEvent(Event):
    data: OngoingOpChangedEventData
    source: EventSource
    topic: TopicOngoingOpChanged = ongoing_op_changed


@dataclass
class OmnibusBalanceChangedEvent(Event):
    data: OmnibusBalanceChangedEventData
    source: EventSource
    topic: TopicOmnibusBalanceChanged = omnibus_balance_changed

    # TODO : Should this class method be required in the parent Event (so that we can count on it everywhere)
    @classmethod
    def from_dict(
        cls, input: Dict, skip_marshmallow_validation: bool
    ) -> "OmnibusBalanceChangedEvent":
        if not skip_marshmallow_validation:
            raise NotImplementedError
        return cls(
            uuid=input["uuid"],
            dispatched_at=input.get("dispatched_at"),
            source=input["source"],
            data=OmnibusBalanceChangedEventData.from_dict(input["data"]),
        )


@dataclass
class AreaOngoingOpCountBecameNegativeEventData:
    area: Optional[Area]


@with_from_dict_classmethod()
@dataclass
class AreaOngoingOpCountBecameNegativeEvent(Event):
    data: AreaOngoingOpCountBecameNegativeEventData
    topic: TopicAreaOngoingOpCountBecameNegative = (
        "area_ongoing_op_count_became_negative"
    )


@dataclass
class ResetCoverOpsRequestedEvent(Event):
    topic: TopicResetCoverOpsRequested = "resetCoverOpsRequested"
    data: None = None


@dataclass
class TriggerResetFromSnapshot(Event):
    topic: TopicTriggerResetFromSnapshot = "trigger_reset_from_snapshot"
    data: None = None


@dataclass
class ResetCoverOpsFinishedEvent(Event):
    topic: TopicResetCoverOpsFinished = "resetCoverOpsFinished"
    data: ResetCoverOpsFinishedEventData


@dataclass
class SetRepositoriesFromSnapshotExecutedEvent(Event):
    topic: TopicSetRepositoriesFromSnapshotExecuted = (
        "set_repositories_from_snapshot_executed"
    )
    data: None = None
