from dataclasses import dataclass
from typing import Dict, Union

from shared.adagio_topics import TopicAdagioEvent
from shared.cover_ops_topics import ExternalTopic


@dataclass
class DateInfos:
    date_path_in_data: str
    date_format_regex_in_data: str


TopicsToReplay = Union[ExternalTopic, TopicAdagioEvent]

topic_to_timestamp_path: Dict[TopicsToReplay, str] = {
    "vehicle_changed_status": "timestamp",
    "operation_changed_status": "timestamp",
    "adagio_vehicle_changed_status": "timestamp",
    "adagio_intervention_created": "timestamp",
    "adagio_intervention_reinforced": "timestamp",
    "adagio_intervention_changed_status": "timestamp",
}
