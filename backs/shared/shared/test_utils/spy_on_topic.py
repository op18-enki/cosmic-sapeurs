from typing import Any, List

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.event_bus import AbstractEventBus


async def spy_on_topic(
    event_bus: AbstractEventBus, topic: CoverOpsDomainTopic
) -> List[Any]:
    published_events = []

    async def spy(e):
        print(f"SPY RECEIVED EVENT WITH TOPIC {e.topic} while spying topic {topic}")
        published_events.append(e)

    await event_bus.subscribe(topic, spy)
    return published_events
