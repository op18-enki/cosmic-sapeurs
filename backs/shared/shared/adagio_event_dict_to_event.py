from typing import Dict

from shared.adagio_intervention_events import (
    AdagioInterventionChangedStatusEvent,
    AdagioInterventionChangedStatusEventData,
    AdagioInterventionCreatedEvent,
    AdagioInterventionCreatedEventData,
    AdagioInterventionEvent,
    AdagioInterventionReinforcedEvent,
    AdagioInterventionReinforcedEventData,
    TopicAdagioInterventionChangedStatus,
    TopicAdagioInterventionReinforced,
)
from shared.adagio_topics import (
    TopicAdagioInterventionCreated,
    TopicAdagioInterventionSnapshot,
)
from shared.adagio_resource_events import AdagioResourceChangedStatusEventData
from shared.adagio_vehicle_events import (
    AdagioVehicleChangedStatusEvent,
    AdagioVehicleChangedStatusEventData,
    adagio_vehicle_topic_options,
)
from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger

topic_adagio_intervention_created: TopicAdagioInterventionCreated = (
    "adagio_intervention_created"
)
topic_adagio_intervention_reinforced: TopicAdagioInterventionReinforced = (
    "adagio_intervention_reinforced"
)
topic_adagio_intervention_changed_status: TopicAdagioInterventionChangedStatus = (
    "adagio_intervention_changed_status"
)
topic_adagio_intervention_snapshot: TopicAdagioInterventionSnapshot = (
    "adagio_intervention_snapshot"
)

adagio_intervention_topic_options = [
    topic_adagio_intervention_created,
    topic_adagio_intervention_reinforced,
    topic_adagio_intervention_changed_status,
    topic_adagio_intervention_snapshot,
]


def adagio_intervention_dict_to_data(
    data_as_dict: Dict, topic: str
) -> AdagioInterventionChangedStatusEventData:

    if topic in [
        topic_adagio_intervention_created,
        topic_adagio_intervention_snapshot,
    ]:
        return AdagioInterventionCreatedEventData.from_dict(data_as_dict)

    elif topic == topic_adagio_intervention_changed_status:
        return AdagioInterventionChangedStatusEventData.from_dict(data_as_dict)
    elif topic == topic_adagio_intervention_reinforced:
        return AdagioInterventionReinforcedEventData.from_dict(data_as_dict)
    else:
        message = f"Received unknown topic {topic}, expected {adagio_intervention_topic_options}"
        logger.warn(message)
        raise InvalidEventFormatError(message)


def adagio_intervention_event_dict_to_event(
    event_as_dict: Dict,
) -> AdagioInterventionEvent:
    topic = event_as_dict["topic"]
    if topic not in adagio_intervention_topic_options:
        message = f"Received unknown topic {topic}, expected {adagio_intervention_topic_options}"
        logger.warn(message)
        raise InvalidEventFormatError(message)

    if topic == topic_adagio_intervention_created:
        return AdagioInterventionCreatedEvent.from_dict(event_as_dict)

    elif topic == topic_adagio_intervention_changed_status:
        return AdagioInterventionChangedStatusEvent.from_dict(event_as_dict)
    else:  # topic == topic_adagio_intervention_reinforced:
        return AdagioInterventionReinforcedEvent.from_dict(event_as_dict)


adagio_vehicle_event_dict_to_event = AdagioVehicleChangedStatusEvent.from_dict
adagio_vehicle_event_dict_to_event_data = AdagioVehicleChangedStatusEventData.from_dict
adagio_ressource_event_dict_to_event_data = (
    AdagioResourceChangedStatusEventData.from_dict
)


def adagio_event_dict_to_event(event_dict):
    topic = event_dict.get("topic")
    if topic in adagio_intervention_topic_options:
        return adagio_intervention_event_dict_to_event(event_dict)
    elif topic in adagio_vehicle_topic_options:
        return adagio_vehicle_event_dict_to_event(event_dict)
    else:
        raise InvalidEventFormatError(f"Event with invalid topic {topic}")
