from datetime import datetime
from time import sleep

from sqlalchemy.engine import create_engine


def wait_for_pg(pg_url: str):
    engine = create_engine(
        pg_url,
        isolation_level="REPEATABLE READ",
    )
    attempt = 1
    while attempt < 5:
        print(
            f"[{datetime.now().strftime('%H:%M:%S')}] Trying to connect to Postgres with url {pg_url} for the {attempt}th time..."
        )
        try:
            connection = engine.connect()
            print(f"Successfully connected to Postgres!")
            return
        except Exception as e:
            print(e)
            attempt += 1
            print(f"Will sleep for 15 seconds and retry Postgres connection...")
            sleep(15)
            print("Wake up")
