from dataclasses import asdict, dataclass
from typing import Any, Dict, Type, TypedDict, TypeVar


class ExternalEventAsDict(TypedDict):
    topic: str
    data: Dict[Any, Any]


_T_SchemaClass = TypeVar("_T_SchemaClass")


@dataclass
class ExternalEvent:
    data: Any
    topic: str

    def as_dict(self) -> ExternalEventAsDict:
        return asdict(self)

    @classmethod
    def from_dict(cls: Type[_T_SchemaClass], input: Dict) -> _T_SchemaClass:
        raise NotImplementedError
