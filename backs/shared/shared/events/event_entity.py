import abc
from dataclasses import dataclass
from typing import Dict, Generic, Type, TypeVar, Union

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData
from shared.helpers.dataclass import dataclass_to_dict

GenericEventData = TypeVar("GenericEventData")

_T_SchemaClass = TypeVar("_T_SchemaClass")


@dataclass
class EventEntity(abc.ABC, Generic[GenericEventData]):
    data: GenericEventData
    uuid: str
    source: str
    topic: str

    def as_dict(self):
        return {
            "uuid": self.uuid,
            "topic": self.topic,
            "source": self.source,
            "data": dataclass_to_dict(self.data),
        }

    @classmethod
    def from_dict(cls: Type[_T_SchemaClass], input: Dict) -> _T_SchemaClass:
        raise NotImplementedError


EventData = Union[
    VehicleEventData,
    AvailabilityChangedEventData,
    OperationEventData,
    OngoingOpChangedEventData,
]
