from dataclasses import dataclass
from typing import List

from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData


@dataclass
class ConvertersSnapshot:
    vehicles: List[VehicleEventData]
    operations: List[OperationEventData]

    @classmethod
    def from_dict(cls, d: dict) -> "ConvertersSnapshot":
        return cls(
            vehicles=[
                VehicleEventData.from_dict(d_vehicle) for d_vehicle in d["vehicles"]
            ],
            operations=[
                OperationEventData.from_dict(d_operation)
                for d_operation in d["operations"]
            ],
        )
