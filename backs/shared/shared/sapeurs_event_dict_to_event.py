from typing import Dict

from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger
from shared.sapeurs_events import (
    AreaOngoingOpCountBecameNegativeEvent,
    AvailabilityChangedEvent,
    ConvertersCatchesUpEvent,
    ConvertersVehiclesInitEvent,
    Event,
    OmnibusBalanceChangedEvent,
    OngoingOpChangedEvent,
    OperationEvent,
    VehicleEvent,
    availability_changed,
    omnibus_balance_changed,
    ongoing_op_changed,
    operation_changed_status,
    vehicle_changed_status,
)


def ConvertersVehiclesInitEvent_from_dict(
    event_as_dict: Dict,
) -> ConvertersVehiclesInitEvent:
    return ConvertersVehiclesInitEvent(
        uuid=event_as_dict["uuid"],
        data=[
            vehicle_or_operation_event_dict_to_event(nested_event_as_dict)
            for nested_event_as_dict in event_as_dict["data"]
        ],
    )


def ConvertersCatchesUpEvent_from_dict(event_as_dict: Dict) -> ConvertersCatchesUpEvent:
    return ConvertersCatchesUpEvent(
        uuid=event_as_dict["uuid"],
        data=[
            vehicle_or_operation_event_dict_to_event(nested_event_as_dict)
            for nested_event_as_dict in event_as_dict["data"]
        ],
    )


def vehicle_or_operation_event_dict_to_event(event_as_dict: Dict) -> Event:
    topic = event_as_dict["topic"]
    if topic == vehicle_changed_status:
        return VehicleEvent.from_dict(event_as_dict)
    elif topic == operation_changed_status:
        return OperationEvent.from_dict(event_as_dict)
    else:
        message = f"Received unknown topic {topic}, expected {[vehicle_changed_status, operation_changed_status]}"
        logger.warn(message)
        raise InvalidEventFormatError(message)


def sapeurs_event_dict_to_event(
    event_as_dict: Dict,
    skip_marshmallow_validation: bool = False,
) -> Event:
    topic = event_as_dict["topic"]
    if topic == vehicle_changed_status:
        return VehicleEvent.from_dict(event_as_dict, skip_marshmallow_validation)
    elif topic == operation_changed_status:
        return OperationEvent.from_dict(event_as_dict, skip_marshmallow_validation)
    elif topic == availability_changed:
        return AvailabilityChangedEvent.from_dict(
            event_as_dict, skip_marshmallow_validation
        )
    elif topic == omnibus_balance_changed:
        return OmnibusBalanceChangedEvent.from_dict(
            event_as_dict, skip_marshmallow_validation
        )
    elif topic == ongoing_op_changed:
        return OngoingOpChangedEvent.from_dict(
            event_as_dict, skip_marshmallow_validation
        )
    elif topic == "converters_catches_up_accumulated_events":
        return ConvertersCatchesUpEvent_from_dict(event_as_dict)
    elif topic == "converters_initialized_last_status_of_all_vehicles":
        return ConvertersVehiclesInitEvent_from_dict(event_as_dict)
    elif topic == "area_ongoing_op_count_became_negative":
        return AreaOngoingOpCountBecameNegativeEvent.from_dict(event_as_dict)
    else:
        message = f"Received dict with unknown topic {topic}."
        logger.warn(message)
        raise InvalidEventFormatError(message)
