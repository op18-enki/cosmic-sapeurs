from dataclasses import dataclass, field
from typing import List, Literal, Optional

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.helpers.date import DateStr
from shared.marshmallow_validate import with_from_dict_classmethod

OperationCause = Literal[
    "fire",  # 1**
    "accident",  # 2**
    "victim",  # 3**
    "people_protection",  # 4**
    "animal",  # 5**
    "property_protection",  # 6**
    "water_gaz_electricity",  # 7**
    "pollution",  # 8**
    "exploration",  # 9**
    "other",
]
operation_cause_options: List[OperationCause] = [
    "fire",  # 1**
    "accident",  # 2**
    "victim",  # 3**
    "people_protection",  # 4**
    "animal",  # 5**
    "property_protection",  # 6**
    "water_gaz_electricity",  # 7**
    "pollution",  # 8**
    "exploration",  # 9**
    "other",
]

OperationRelevantStatus = Literal[
    "opened",
    "first_vehicle_affected",
    "all_vehicles_released",
    "some_affected_vehicle_changed",
    "closed",
    "opened_by_snapshot",
]
OperationStatus = Literal[
    "reinforced",
    "validated",
    "finished",
    "closed",
    "opened",
    "ongoing",
    "received",
    "sent",
    "first_vehicle_affected",
    "all_vehicles_released",
    "some_affected_vehicle_changed",
    "opened_by_snapshot",
]

operation_status_options: List[OperationStatus] = [
    "opened",
    "reinforced",
    "validated",
    "finished",
    "closed",
    "first_vehicle_affected",
    "all_vehicles_released",
    "some_affected_vehicle_changed",
    "opened_by_snapshot",
]

operation_closed: OperationStatus = "closed"
operation_just_opened: OperationRelevantStatus = "opened"
operation_reinforced: OperationStatus = "reinforced"
operation_has_first_affected_vehicle: OperationRelevantStatus = "first_vehicle_affected"
operation_has_all_vehicles_released: OperationRelevantStatus = "all_vehicles_released"
operation_has_some_affected_vehicle_changed: OperationRelevantStatus = (
    "some_affected_vehicle_changed"
)
operation_opened_by_snapshot: OperationRelevantStatus = "opened_by_snapshot"

operation_relevant_ongoing_status_options: List[OperationRelevantStatus] = [
    operation_opened_by_snapshot,
    operation_just_opened,
    operation_has_first_affected_vehicle,
    operation_has_some_affected_vehicle_changed,
]

operation_relevant_status_options: List[OperationRelevantStatus] = [
    *operation_relevant_ongoing_status_options,
    operation_has_all_vehicles_released,
    operation_closed,
]


@with_from_dict_classmethod()
@dataclass
class OperationEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    timestamp: DateStr  # From 'Operation-Software'
    raw_operation_id: RawOperationId  # 'Operation-Software'  vehicle ID
    status: OperationStatus
    address: Optional[str]
    address_area: Optional[
        Area
    ]  # Reference on a partitioned map of where the address belongs
    cause: Optional[OperationCause]
    raw_cause: Optional[str]  # Cause label from 'Operation-Software'
    raw_procedure: Optional[str]
    departure_criteria: Optional[str]
    longitude: Optional[float]
    latitude: Optional[float]
    affected_vehicles: List[RawVehicleId] = field(default_factory=lambda: [])
