from dataclasses import dataclass
from typing import Any, List

from dataclasses_jsonschema import JsonSchemaMixin


@dataclass
class FrontLinesHistory(JsonSchemaMixin):
    victim: List[Any]
    fire: List[Any]
