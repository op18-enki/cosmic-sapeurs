from dataclasses import dataclass
from typing import Literal

from dataclasses_jsonschema import JsonSchemaMixin


@dataclass
class SapeursRoutes(JsonSchemaMixin, allow_additional_props=False):
    converted_event: Literal["converted_event"]
    hello: Literal["hello"]
    reset: Literal["reset"]
    get_cover: Literal["cover"]
    get_ops: Literal["ops"]
    get_front_history: Literal["front_history"]
    get_omnibus: Literal["omnibus"]
    prefix: Literal["api"]
    ping: Literal["ping"]
