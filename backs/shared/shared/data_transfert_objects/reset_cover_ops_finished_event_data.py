from dataclasses import dataclass
from typing import Any, Dict, List

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.front_lines_history import FrontLinesHistory
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.helpers.dataclass import dataclass_to_dict


@dataclass
class ResetCoverOpsFinishedEventData(JsonSchemaMixin, allow_additional_props=False):
    omnibus_details: List[OmnibusDetail]
    availabilities: List[AvailabilityChangedEventData]
    ongoing_ops: List[OngoingOpChangedEventData]
    front_lines_history: FrontLinesHistory

    def as_dict(self) -> Dict[Any, Any]:
        return {
            "omnibus_details": [
                dataclass_to_dict(omnibus_detail)
                for omnibus_detail in self.omnibus_details
            ],
            "availabilities": [
                dataclass_to_dict(availability) for availability in self.availabilities
            ],
            "ongoing_ops": [
                dataclass_to_dict(ongoing_op) for ongoing_op in self.ongoing_ops
            ],
            "front_lines_history": dataclass_to_dict(self.front_lines_history),
        }
