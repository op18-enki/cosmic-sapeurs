from copy import copy
from dataclasses import dataclass
from typing import Dict, Optional, Type, TypeVar

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import DateStr
from shared.marshmallow_validate import marshmallow_validate


@dataclass
class Availability(JsonSchemaMixin, allow_additional_props=False):
    available: int
    unavailable: int
    on_operation: int
    recoverable: int
    unavailable_omnibus: int


_T_AvailabilityChangedEventData = TypeVar("_T_AvailabilityChangedEventData")


@dataclass()
class AvailabilityChangedEventData(JsonSchemaMixin, allow_additional_props=False):
    timestamp: DateStr
    bspp_availability: Availability
    area_availability: Availability
    role: VehicleRole
    home_area: Area
    raw_vehicle_id: RawVehicleId
    latest_event_data: VehicleEventData
    availability_changed: bool
    previous_raw_operation_id: Optional[RawOperationId] = None

    def as_dict(self):
        return {
            "timestamp": str(self.timestamp),
            "bspp_availability": dataclass_to_dict(self.bspp_availability),
            "area_availability": dataclass_to_dict(self.area_availability),
            "role": self.role,
            "home_area": self.home_area,
            "raw_vehicle_id": str(self.raw_vehicle_id),
            "previous_raw_operation_id": str(self.previous_raw_operation_id)
            if self.previous_raw_operation_id
            else None,
            "latest_event_data": dataclass_to_dict(self.latest_event_data),
            "availability_changed": self.availability_changed,
        }

    @classmethod
    def from_dict(
        cls: Type[_T_AvailabilityChangedEventData],
        input: Dict,
    ) -> _T_AvailabilityChangedEventData:
        input = copy(input)
        del input["latest_event_data"]["availability_kind"]
        return marshmallow_validate(SchemaClass=cls, dict_candidate=input)
