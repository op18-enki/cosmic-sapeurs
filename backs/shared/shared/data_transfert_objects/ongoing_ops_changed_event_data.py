from dataclasses import dataclass
from typing import List, Optional

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
)
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import DateStr


@dataclass
class OperationOpeningInfos(JsonSchemaMixin, allow_additional_props=False):
    cause: OperationCause
    raw_cause: Optional[str]
    address_area: Area
    address: Optional[str]
    raw_procedure: str
    departure_criteria: str
    longitude: float
    latitude: float
    opening_timestamp: DateStr


@dataclass
class OngoingOpChangedEventData(JsonSchemaMixin, allow_additional_props=False):
    timestamp: DateStr
    status: OperationStatus
    raw_operation_id: RawOperationId
    affected_vehicles: List[RawVehicleId]
    opening_infos: OperationOpeningInfos

    def asdict(self):
        return {
            "timestamp": str(self.timestamp),
            "status": self.status,
            "affected_vehicles": self.affected_vehicles,
            "opening_infos": dataclass_to_dict(self.opening_infos),
        }
