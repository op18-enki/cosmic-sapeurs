from typing import NewType

RawVehicleId = NewType("RawVehicleId", str)
RawOperationId = NewType("RawOperationId", str)
