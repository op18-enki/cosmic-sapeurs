import abc
import asyncio
from asyncio.events import AbstractEventLoop
from collections import defaultdict
from typing import Any, Callable, Coroutine, Dict, Generic, List, TypeVar, Union

from shared.helpers.clock import AbstractClock
from shared.sapeurs_events import Event

EventCallback = Union[
    Callable[[Any], Coroutine[Any, Any, Any]], Callable[[], Coroutine[Any, Any, Any]]
]
GenericTopic = TypeVar("GenericTopic")


class AbstractEventBus(abc.ABC, Generic[GenericTopic]):
    def __init__(self, clock: AbstractClock) -> None:
        self.clock = clock

    async def publish(self, event: Event) -> None:
        event_with_dispatched_at = self.set_dispatched_at(event)
        await self._publish(event_with_dispatched_at)

    @abc.abstractmethod
    async def _publish(self, event_with_dispatched_at: Event) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def unsubscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        raise NotImplementedError

    def set_dispatched_at(self, event: Event) -> Event:
        event.dispatched_at = self.clock.get_now()
        return event

    @abc.abstractmethod
    async def start_on_loop(self, loop: AbstractEventLoop) -> None:
        raise NotImplementedError


class InMemoryEventBus(AbstractEventBus, Generic[GenericTopic]):
    def __init__(self, clock: AbstractClock) -> None:
        super().__init__(clock=clock)
        Subscriptions = Dict[GenericTopic, List[EventCallback]]
        self._subscriptions: Subscriptions = defaultdict(lambda: [])

    async def unsubscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        filtered = [cb for cb in self._subscriptions[topic] if cb != callback]
        self._subscriptions[topic] = filtered

    async def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        self._subscriptions[topic].append(callback)

    async def _publish(self, event_with_dispatched_at: Event) -> None:
        if self._subscriptions[event_with_dispatched_at.topic]:
            await asyncio.wait(
                [
                    asyncio.create_task(callback())
                    if callback.__code__.co_argcount == 0
                    else asyncio.create_task(callback(event_with_dispatched_at))
                    for callback in self._subscriptions[event_with_dispatched_at.topic]
                ]
            )

    async def start_on_loop(self, loop: AbstractEventLoop) -> None:
        pass
