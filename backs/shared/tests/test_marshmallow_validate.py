from dataclasses import dataclass

import pytest

from shared.exceptions import InvalidEventFormatError
from shared.marshmallow_validate import with_from_dict_classmethod


@with_from_dict_classmethod()
@dataclass
class MyClass:
    a: int


def test_with_from_dict_classmethod():
    my_class_from_dict = MyClass.from_dict({"a": 1})
    my_class_expected = MyClass(a=1)
    assert my_class_from_dict == my_class_expected


def test_with_from_dict_classmethod_raises():
    with pytest.raises(InvalidEventFormatError):
        my_class_from_dict = MyClass.from_dict({"a": "incorrect_type_for_field_a"})
