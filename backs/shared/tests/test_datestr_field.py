from dataclasses import dataclass, field

import pytest
from dataclasses_jsonschema import JsonSchemaMixin

from shared.exceptions import InvalidEventFormatError
from shared.helpers.date import DateStr, DateStrField
from shared.marshmallow_validate import marshmallow_validate


@dataclass
class DataWithDateStrField(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    date_str: DateStr = field(
        metadata={"marshmallow_field": DateStrField()}  # Custom marshmallow field
    )


def test_str_with_correct_type_and_format_is_deserialized():
    data_with_str_field = marshmallow_validate(
        SchemaClass=DataWithDateStrField,
        dict_candidate={"date_str": "2020-10-01T12:00:00.000Z"},
    )
    assert data_with_str_field.date_str == DateStr("2020-10-01T12:00:00.000Z")


def test_str_with_incorrect_format_raises_validation_error():
    with pytest.raises(InvalidEventFormatError):
        marshmallow_validate(
            SchemaClass=DataWithDateStrField,
            dict_candidate={"date_str": "2020-10-01T12:00:00.0000"},
        )
