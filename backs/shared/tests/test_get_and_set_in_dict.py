from shared.helpers.get_and_set_in_dict import get_in_dict, set_in_dict


def test_find_in_dict():
    dictionary = {
        "app": {
            "Garden": {
                "Flowers": {
                    "Red flower": "Rose",
                    "White Flower": "Jasmine",
                    "Yellow Flower": "Marigold",
                }
            },
            "Fruits": {},
        }
    }
    assert get_in_dict("app.Garden.Flowers.White Flower", dictionary) == "Jasmine"


def test_set_in_dict():
    dictionary = {
        "app": {
            "Garden": {
                "Flowers": {
                    "Red flower": "Rose",
                    "White Flower": "Jasmine",
                    "Yellow Flower": "Marigold",
                }
            },
            "Fruits": {},
        }
    }
    set_in_dict("app.Garden.Flowers.White Flower", dictionary, new_value="Lala")
    assert dictionary == {
        "app": {
            "Garden": {
                "Flowers": {
                    "Red flower": "Rose",
                    "White Flower": "Lala",
                    "Yellow Flower": "Marigold",
                }
            },
            "Fruits": {},
        }
    }
