import os


def set_variable_for_context(local_value, prod_value, CI_value):
    running_in_CI = os.environ.get("CI", False)
    if running_in_CI:
        return CI_value
    is_in_docker_compose = os.environ.get("DOCKER_COMPOSE", False) == "true"
    if is_in_docker_compose:
        return prod_value
    return local_value
