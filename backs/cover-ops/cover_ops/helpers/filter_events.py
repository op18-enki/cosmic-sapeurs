from typing import Callable, List, Tuple, TypeVar

from shared.events.event_entity import EventEntity

T = TypeVar("T")


def filter_events(events: List[T], filter: Callable[[T], bool]) -> Tuple[List[T], int]:
    filtered_vehicle_events = [
        vehicle_repo_event
        for vehicle_repo_event in events
        if filter(vehicle_repo_event)
    ]
    nb_of_removed_events = len(events) - len(filtered_vehicle_events)
    return (filtered_vehicle_events, nb_of_removed_events)
