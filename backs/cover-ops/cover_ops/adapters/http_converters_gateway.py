import requests
from cover_ops.domain.ports.converter_gateway import (
    AbstractConvertersGateway,
    ConvertersSnapshot,
)

from shared.helpers.logger import logger
from shared.routes import route_cover_ops_snapshot


class HttpConvertersGateway(AbstractConvertersGateway):
    def __init__(self, http_converters_url: str) -> None:
        self.http_converters_url = http_converters_url

    async def get_cover_ops_snapshot(self) -> ConvertersSnapshot:
        logger.info(
            f"\nGetting last status of all vehicles and operations, from {self.http_converters_url}..."
        )
        r = requests.get(
            f"{self.http_converters_url}/{route_cover_ops_snapshot}", timeout=15
        )
        logger.info(f"Response status was : {r.status_code}")
        body = r.json()
        if r.status_code == 200:
            return ConvertersSnapshot.from_dict(body)
        else:
            logger.warn(
                f"Could not get snapshot, http_code: {r.status_code}",
            )
            return ConvertersSnapshot(vehicles=[], operations=[])
