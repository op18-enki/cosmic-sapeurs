from typing import Callable, Dict, Optional, Union, cast

from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.schema import Table

from shared.cover_ops_topics import (
    TopicAvailabilityChanged,
    TopicOmnibusBalanceChanged,
    TopicOngoingOpChanged,
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from shared.events.event_entity import EventData, EventEntity
from shared.helpers.dataclass import dataclass_to_dict
from shared.sapeurs_events import (
    AvailabilityChangedEvent,
    OngoingOpChangedEvent,
    OperationEvent,
    VehicleEvent,
)

EventTopic = Union[
    TopicAvailabilityChanged,
    TopicOngoingOpChanged,
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
    TopicOmnibusBalanceChanged,
]


def event_to_id(event: EventEntity) -> Optional[int]:
    event_to_id_getter: Dict[EventTopic, Callable[[EventEntity], Optional[int]]] = {
        "vehicle_changed_status": lambda e: int(
            cast(VehicleEvent, e).data.raw_vehicle_id
        ),
        "availabilityChanged": lambda e: int(
            cast(AvailabilityChangedEvent, e).data.raw_vehicle_id
        ),
        "operation_changed_status": lambda e: int(
            cast(OperationEvent, e).data.raw_operation_id
        ),
        "ongoingOpChanged": lambda e: int(
            cast(OngoingOpChangedEvent, e).data.raw_operation_id
        ),
        "omnibusBalanceChanged": lambda e: None,
    }
    return event_to_id_getter[cast(EventTopic, event.topic)](event)


class PgRepository:
    def __init__(self, event_data_class, engine: Engine, table: Table):
        self.engine = engine
        self.connection = engine.connect()
        self.table = table
        self.event_data_class = event_data_class

    def _add(self, event_data: EventData) -> None:
        insertion = self.table.insert().values(
            data=dataclass_to_dict(event_data),
            event_timestamp=event_data.timestamp,  # type: ignore
        )
        self.connection.execute(insertion)
