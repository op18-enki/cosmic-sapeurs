from typing import List, Optional, Set, Tuple

from cover_ops.adapters.postgres.db import operation_events_table
from cover_ops.adapters.postgres.pg_repository import PgRepository
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.expression import text

from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    OperationRelevantStatus,
    OperationStatus,
    RawOperationId,
    operation_closed,
    operation_has_all_vehicles_released,
    operation_relevant_status_options,
)
from shared.helpers.decorators import timeit


class PgOperationEventsRepository(PgRepository, AbstractOperationEventsRepository):
    def __init__(self, engine: Engine, purge: bool) -> None:
        AbstractOperationEventsRepository.__init__(self, purge=purge)
        PgRepository.__init__(
            self,
            engine=engine,
            event_data_class=OperationEventData,
            table=operation_events_table,
        )

    def reset(self):
        raise NotImplementedError

    @timeit("OperationEvent: get_datas_for_id_with_status_in")
    def get_datas_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationRelevantStatus]] = None,
    ) -> List[OperationEventData]:
        if statuses is not None:
            query = text(
                """
                SELECT uuid, source, topic, data
                FROM operation_events
                WHERE event_id = :raw_operation_id
                AND data ->> 'status' IN :statuses
                """
            )
        else:
            query = text(
                """
                SELECT uuid, source, topic, data
                FROM operation_events
                WHERE event_id = :raw_operation_id
                """,
            )

        rows = self.connection.execute(
            query,
            {
                "raw_operation_id": int(raw_operation_id),
                "statuses": tuple(statuses or []),
            },
        ).all()
        return [OperationEventData.from_dict(row[0]) for row in rows]

    @timeit("OperationEvent: get_ongoing_operation_datas_with_relevant_status")
    def get_ongoing_operation_datas_with_relevant_status(
        self,
    ) -> List[OperationEventData]:
        ongoing_op_ids = self.connection.execute(
            text(
                """
                    SELECT event_id
                    FROM
                    (
                        SELECT DISTINCT ON (event_timestamp) *
                        FROM operation_events
                        ORDER BY
                            event_id,
                            event_timestamp DESC
                        ) AS operation_latest_relevant_event
                    WHERE data ->> 'status' NOT IN :closing_statuses
                """
            ),
            {
                "closing_statuses": (
                    operation_closed,
                    operation_has_all_vehicles_released,
                ),
            },
        ).all()

        if not ongoing_op_ids:
            return []

        rows = self.connection.execute(
            text(
                """
                SELECT uuid, source, topic, data
                FROM operation_events
                WHERE event_id IN :ongoing_op_ids
                AND data ->> 'status' IN :relevant_statuses
                """
            ),
            {
                "ongoing_op_ids": tuple(ongoing_op_ids[0]),
                "relevant_statuses": tuple(operation_relevant_status_options),
            },
        )

        return [OperationEventData.from_dict(row[0]) for row in rows]

    def purge_old_similar_events(self, event: OperationEventData):
        raise NotImplementedError

    def get_latest_event_data_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationEventData]:
        raise NotImplementedError

    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        raise NotImplementedError

    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        raise NotImplementedError

    def get_operation_opening_infos_for_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[Tuple[OperationOpeningInfos, OperationStatus]]:
        raise NotImplementedError
