from typing import Dict, List, Optional

from cover_ops.adapters.postgres.db import ongoing_events_table
from cover_ops.adapters.postgres.pg_repository import PgRepository
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.expression import text

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationCause
from shared.helpers.decorators import timeit


class PgOngoingOpEventsRepository(PgRepository, AbstractOngoingOpEventsRepository):
    def __init__(self, engine: Engine, purge: bool) -> None:
        AbstractOngoingOpEventsRepository.__init__(self, purge=purge)
        PgRepository.__init__(
            self,
            engine=engine,
            event_data_class=OngoingOpChangedEventData,
            table=ongoing_events_table,
        )

    def reset(self):
        raise NotImplementedError

    @timeit("OnGoingOpEvents: get_all_currently_ongoing_ops")
    def get_all_ongoing_ops(self) -> List[OngoingOpChangedEventData]:
        rows = self.connection.execute(
            text(
                """
                SELECT DISTINCT ON (event_timestamp)
                    data
                FROM ongoing_op_changed_events
                WHERE data ->> 'status' in ('opened', 'reinforced', 'first_vehicle_affected', 'all_vehicles_released', 'some_affected_vehicle_changed')
                ORDER BY
                    event_timestamp DESC
                ;
            """
            ),
        ).all()

        if not rows:
            return []
        return self._rows_to_datas(rows)

    @timeit("OnGoingOpEvents: get_latest_event_data_for_area_by_cause")
    def get_latest_event_data_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        query = text(
            """
                SELECT data
                FROM ongoing_op_changed_events
                WHERE (data -> 'opening_infos' ->> 'address_area') = :area
                AND (data -> 'opening_infos' ->> 'cause') = :cause
                ORDER BY data ->> 'timestamp'
                DESC LIMIT 1;
            """
        )

        rows = self.connection.execute(
            query,
            {"area": area, "cause": cause},
        ).all()
        return OngoingOpChangedEventData.from_dict(rows[0][0]) if rows else None

    def get_latest_event_data_for_all_area_for_cause_fire_and_victim(
        self,
    ) -> List[OngoingOpChangedEventData]:
        query = text(
            """
            with splitJson as (
                SELECT
                    (data -> 'opening_infos' ->> 'address_area') as address_area,
                    (data -> 'opening_infos' ->> 'cause') as cause,
                    event_timestamp as timestamp,
                    data
                FROM ongoing_op_changed_events
            ),
            windowing as (
                SELECT
                    sub.*,
                    rank() OVER (
                        PARTITION BY address_area,
                        cause
                        ORDER BY
                            timestamp DESC
                    ) as row_number
                FROM splitJson sub
                WHERE cause in ('fire', 'victim')
            )
            SELECT data
            FROM windowing
            WHERE row_number = 1
            """
        )

        rows = self.connection.execute(query).all()
        if not rows:
            return []

        return [OngoingOpChangedEventData.from_dict(row[0]) for row in rows]

    @staticmethod
    def _rows_to_datas(rows: List[Dict]) -> List[OngoingOpChangedEventData]:
        return [OngoingOpChangedEventData.from_dict(row[0]) for row in rows]

    def purge_old_similar_events(self, event: OngoingOpChangedEventData):
        raise NotImplementedError

    def get_ongoing_ops_counts_for_cause(self, cause: OperationCause) -> Optional[int]:
        raise NotImplementedError

    def get_latest_event_data_for_operation_id(
        self, operation_id: RawOperationId
    ) -> Optional[OngoingOpChangedEventData]:
        raise NotImplementedError
