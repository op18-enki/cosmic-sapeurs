from datetime import timedelta
from typing import List, Optional

from cover_ops.adapters.postgres.db import availability_changed_events_table
from cover_ops.adapters.postgres.pg_repository import PgRepository
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.expression import text

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.decorators import timeit, withDuration


class PgAvailabilityEventsRepository(
    PgRepository, AbstractAvailabilityEventsRepository
):
    def __init__(
        self,
        engine: Engine,
        purge: bool,
    ) -> None:
        AbstractAvailabilityEventsRepository.__init__(self, purge=purge)
        PgRepository.__init__(
            self,
            engine=engine,
            event_data_class=AvailabilityChangedEventData,
            table=availability_changed_events_table,
        )

    def reset(self):
        raise NotImplementedError

    def purge_old_similar_events(self, event: AvailabilityChangedEventData):
        raise NotImplementedError

    @timeit("AvailabilityEvents : get_availability_event_data_since")
    def get_availability_event_data_since(self) -> List[AvailabilityChangedEventData]:
        last_availability_data_in_array = self.connection.execute(
            text(
                """
            SELECT event_timestamp
            FROM availability_changed_events
            ORDER BY event_timestamp DESC LIMIT 1;
        """
            )
        ).all()

        if not last_availability_data_in_array:
            return []

        last_timestamp = last_availability_data_in_array[0][0]

        until = last_timestamp - timedelta(hours=6)

        rows = withDuration(
            "Querry events until",
            lambda: self.connection.execute(
                text(
                    """
                SELECT data
                FROM availability_changed_events
                WHERE event_timestamp >= :until;
                """
                ),
                {"until": until},
            ).all(),
        )

        return withDuration(
            "Create actual AvailabilityEvents",
            lambda: [AvailabilityChangedEventData.from_dict(row[0]) for row in rows],
        )

    @timeit(
        "AvailabilityEvents : list_latest_availability_event_data_by_vehicle_from_repo"
    )
    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:
        rows = withDuration(
            "List latest availability event Query",
            lambda: self.connection.execute(
                """
            SELECT DISTINCT ON (event_timestamp)
                data
            FROM
                availability_changed_events
            ORDER BY
                event_timestamp DESC
            ;
            """
            ).all(),
        )

        return withDuration(
            "Process actual events",
            lambda: sorted(
                [AvailabilityChangedEventData.from_dict(row[0]) for row in rows],
                key=lambda event_data: event_data.timestamp,
            ),
        )

    @property
    def last_availability_event(self) -> Optional[AvailabilityChangedEventData]:
        raise NotImplementedError

    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    def get_latest_vehicle_event_data_affected_to_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        raise NotImplementedError
