from sqlalchemy import Column, MetaData, String, Table
from sqlalchemy.dialects.postgresql import INTEGER, JSONB, TIMESTAMP
from sqlalchemy.sql.sqltypes import CHAR

metadata = MetaData()


def make_events_table(name: str) -> Table:
    return Table(
        name,
        metadata,
        Column("data", JSONB),
        Column("event_timestamp", TIMESTAMP(timezone=True), index=True),
    )


vehicle_events_table = make_events_table("vehicle_events")
availability_changed_events_table = make_events_table("availability_changed_events")
operation_events_table = make_events_table("operation_events")
ongoing_events_table = make_events_table("ongoing_op_changed_events")
omnibus_events_table = make_events_table("omnibus_balance_changed_events")


def initialize_db(engine):
    metadata.create_all(engine)


def drop_db(engine):
    metadata.drop_all(engine)


def reset_db(connection):
    connection.execute(
        """
        DELETE FROM vehicle_events;
        """
    )
    connection.execute(
        """
        DELETE FROM availability_changed_events;
        """
    )

    connection.execute(
        """
        DELETE FROM operation_events;
        """
    )
    connection.execute(
        """
        DELETE FROM ongoing_op_changed_events;
        """
    )
    connection.execute(
        """
        DELETE FROM omnibus_balance_changed_events;
        """
    )
    # query = text(
    #     """
    #     DELETE FROM :table
    #     """,
    # )
    # for table in [
    #     "vehicle_events",
    #     "availability_changed_events",
    #     "operation_events",
    #     "ongoing_op_changed_events",
    #     "omnibus_balance_changed_events",
    # ]:
    #     connection.execute(
    #         query,
    #         {"table": table},
    #     )
