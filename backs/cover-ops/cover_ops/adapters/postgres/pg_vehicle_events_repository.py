from typing import Dict, List, Optional

from cover_ops.adapters.postgres.db import vehicle_events_table
from cover_ops.adapters.postgres.pg_repository import PgRepository
from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from sqlalchemy.engine.base import Engine

from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
)
from shared.helpers.decorators import timeit


class PgVehicleEventsRepository(PgRepository, AbstractVehicleEventsRepository):
    def __init__(
        self,
        purge: bool,
        engine: Engine,
    ) -> None:
        AbstractVehicleEventsRepository.__init__(self, purge=purge)
        PgRepository.__init__(
            self,
            engine=engine,
            event_data_class=VehicleEventData,
            table=vehicle_events_table,
        )

    def reset(self):
        raise NotImplementedError

    def purge_old_similar_events(self, event: VehicleEventData):
        raise NotImplementedError

    @timeit("VehicleEvents : get_latest_event_by_raw_vehicle_id")
    def get_latest_data_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventData]:
        rows = self.connection.execute(
            """
            SELECT DISTINCT ON (event_timestamp)
                uuid, source, topic, data
            FROM
                vehicle_events
            ORDER BY
                event_timestamp DESC
            ;
            """
        ).all()

        eventsByRawVehicleId = {}

        for row in rows:
            data = VehicleEventData.from_dict(row[3])
            eventsByRawVehicleId[data.raw_vehicle_id] = data

        return eventsByRawVehicleId

    def get_latest_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        raise NotImplementedError
