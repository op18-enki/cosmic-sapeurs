import json
from asyncio.events import AbstractEventLoop
from dataclasses import dataclass
from typing import Any, Dict, List

from aio_pika.channel import Channel
from aio_pika.connection import Connection, connect
from aio_pika.exchange import ExchangeType
from aio_pika.message import DeliveryMode, Message
from cover_ops.adapters.rabbitmq_utils import make_rabbitmq_url
from cover_ops.domain.ports.domain_event_bus import DomainEventBus

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.event_bus import EventCallback
from shared.helpers.clock import AbstractClock
from shared.helpers.dataclass import dataclass_to_dict
from shared.sapeurs_event_dict_to_event import sapeurs_event_dict_to_event
from shared.sapeurs_events import Event


@dataclass
class RabbitChannelInfos:
    channel: Channel
    events_exchange: Any
    queues: List[str]


async def create_channel(
    topic: CoverOpsDomainTopic, connection: Connection
) -> RabbitChannelInfos:
    channel = await connection.channel()
    await channel.set_qos(prefetch_count=1)
    events_exchange = await channel.declare_exchange(topic, ExchangeType.FANOUT)
    return RabbitChannelInfos(channel, events_exchange, [])


def make_callback_with_deserialization(
    callback: EventCallback,
):
    async def callback_with_deserialization(message):
        message_body = message.body.decode("utf-8")
        event_as_dict = json.loads(message_body)
        event = sapeurs_event_dict_to_event(
            event_as_dict, skip_marshmallow_validation=True
        )
        await callback(event)

    return callback_with_deserialization


class NoActiveRabbitConnection(Exception):
    pass


class RabbitMqDomainEventBus(DomainEventBus):
    def __init__(
        self,
        clock: AbstractClock,
    ) -> None:
        super().__init__(clock=clock)
        self._connection = None
        self.channels_infos_by_topic: Dict[CoverOpsDomainTopic, RabbitChannelInfos] = {}

    async def start_on_loop(self, loop: AbstractEventLoop) -> None:
        self._connection = await connect(make_rabbitmq_url(), loop=loop)

    async def _publish(self, event_with_dispatched_at: Event) -> None:
        self._raise_if_no_connection()
        if not self._connection:
            raise NoActiveRabbitConnection

        topic = event_with_dispatched_at.topic
        rabbit_channel = self.channels_infos_by_topic.get(topic)
        if not rabbit_channel:
            return
        message_body = json.dumps(dataclass_to_dict(event_with_dispatched_at)).encode(
            "utf-8"
        )
        message = Message(
            message_body,
            delivery_mode=DeliveryMode.PERSISTENT,
            content_encoding="utf-8",
            content_type="text/plain",
        )
        await rabbit_channel.events_exchange.publish(message, routing_key=topic)

    async def subscribe(
        self, topic: CoverOpsDomainTopic, callback: EventCallback
    ) -> None:
        self._raise_if_no_connection()
        if topic not in self.channels_infos_by_topic:
            self.channels_infos_by_topic[topic] = await create_channel(
                topic, self._connection  # type: ignore
            )

        # One queue per callback
        channel = self.channels_infos_by_topic[topic].channel
        events_exchange = self.channels_infos_by_topic[topic].events_exchange
        queue_name = self.make_queue_name(topic, callback)
        queue = await channel.declare_queue(
            queue_name,
            durable=False,  # Durability : queue should not survive when broker restarts
            exclusive=True,  # Exclusive queues may only be accessed by the current connection
            auto_delete=True,  # Delete queue when channel will be closed.
        )
        # Binding the queue to the exchange (ie. the topic's channel)
        await queue.bind(events_exchange)
        # Add callback to the queue
        callback_with_deserialization = make_callback_with_deserialization(callback)
        # Start listening the queue
        await queue.consume(
            callback_with_deserialization, no_ack=True
        )  # no_ack so that we don't need to acknowledge the messages
        # Add queue name to channel infos
        self.channels_infos_by_topic[topic].queues.append(queue_name)

    async def unsubscribe(
        self, topic: CoverOpsDomainTopic, callback: EventCallback
    ) -> None:
        self._raise_if_no_connection()
        channel_infos = self.channels_infos_by_topic.get(topic)
        if channel_infos:
            queue_name = self.make_queue_name(topic, callback)
            await channel_infos.channel.queue_delete(queue_name)

    @staticmethod
    def make_queue_name(topic, callback):
        return f"queue_{topic}_{id(callback)}"

    def _raise_if_no_connection(self):
        if not self._connection:
            raise NoActiveRabbitConnection
