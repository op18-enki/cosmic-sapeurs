import os
from pathlib import Path
from typing import List

from cover_ops.adapters.csv_event_entity_writer import CsvEventDataWriter
from cover_ops.domain.entities.event_entities import VehicleEventData
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData


class CsvAvailabilityEventsRepository(
    InMemoryAvailabilityEventsRepository, CsvEventDataWriter
):
    def __init__(self, csv_path: Path, purge: bool) -> None:
        InMemoryAvailabilityEventsRepository.__init__(self, purge=purge)
        CsvEventDataWriter.__init__(
            self,
            csv_path=csv_path,
            data_from_dict=AvailabilityChangedEventData.from_dict,
        )
        if os.path.isfile(self.csv_path):
            self.availability_datas = self._from_csv()
        else:
            self.initialize_csv()

    def reset(self):
        super().reset()
        self.initialize_csv()

    def _add(self, event_datas: List[AvailabilityChangedEventData]):
        super()._add(event_datas)
        for event_data in event_datas:
            self.writerow_from_event_data(event_data)

    def _from_csv(self) -> List[AvailabilityChangedEventData]:
        return self.retrieve_entities_from_csv()

    def purge_old_similar_events(self, event: AvailabilityChangedEventData):
        nb_of_purged_events = super().purge_old_similar_events(event)
        if not nb_of_purged_events:
            return
        self.initialize_csv()
        for event in self.availability_datas:
            self.writerow_from_event_data(event)


class CsvOngoingOpEventsRepository(
    InMemoryOngoingOpEventsRepository, CsvEventDataWriter
):
    def __init__(self, csv_path: Path, purge: bool) -> None:
        InMemoryOngoingOpEventsRepository.__init__(self, purge=purge)
        CsvEventDataWriter.__init__(
            self,
            csv_path=csv_path,
            data_from_dict=OngoingOpChangedEventData.from_dict,
        )
        if os.path.isfile(self.csv_path):
            self.ongoing_op_datas = self._from_csv()
        else:
            self.initialize_csv()

    def purge_old_similar_events(self, data: OngoingOpChangedEventData):
        nb_of_purged_events = super().purge_old_similar_events(data)
        if not nb_of_purged_events:
            return
        self.initialize_csv()
        for event in self.ongoing_op_datas:
            self.writerow_from_event_data(event)

    def _add(self, event_datas: List[OngoingOpChangedEventData]):
        super()._add(event_datas)
        for event_data in event_datas:
            self.writerow_from_event_data(event_data)

    def _from_csv(self) -> List[OngoingOpChangedEventData]:
        return self.retrieve_entities_from_csv()


class CsvVehicleEventsRepository(InMemoryVehicleEventsRepository, CsvEventDataWriter):
    def __init__(self, csv_path: Path, purge: bool) -> None:
        InMemoryVehicleEventsRepository.__init__(self, purge=purge)
        CsvEventDataWriter.__init__(
            self,
            csv_path=csv_path,
            data_from_dict=VehicleEventData.from_dict,
        )
        if os.path.isfile(self.csv_path):
            self.vehicle_entities = self._from_csv()
        else:
            self.initialize_csv()

    def _add(self, event_datas: List[VehicleEventData]):
        super()._add(event_datas)
        for event_data in event_datas:
            self.writerow_from_event_data(event_data)

    def _from_csv(self) -> List[VehicleEventData]:
        return self.retrieve_entities_from_csv()

    def purge_old_similar_events(self, event: VehicleEventData):
        nb_of_purged_events = super().purge_old_similar_events(event)
        if not nb_of_purged_events:
            return
        self.initialize_csv()
        for event_data in self.vehicle_datas:
            self.writerow_from_event_data(event_data)


class CsvOperationEventsRepository(
    InMemoryOperationEventsRepository, CsvEventDataWriter
):
    def __init__(self, csv_path: Path, purge: bool) -> None:
        InMemoryOperationEventsRepository.__init__(self, purge=purge)
        CsvEventDataWriter.__init__(
            self, csv_path=csv_path, data_from_dict=OperationEventData.from_dict
        )
        if os.path.isfile(self.csv_path):
            self._operation_datas = self._from_csv()
        else:
            self.initialize_csv()

    def _add(self, event_datas: List[OperationEventData]):
        super()._add(event_datas)
        for event_data in event_datas:
            self.writerow_from_event_data(event_data)

    def _from_csv(self) -> List[OperationEventData]:
        return self.retrieve_entities_from_csv()

    def purge_old_similar_events(self, event: OperationEventData):
        nb_of_purged_events = super().purge_old_similar_events(event)
        if not nb_of_purged_events:
            return
        self.initialize_csv()
        for event in self.operation_datas:
            self.writerow_from_event_data(event)


class CsvOmnibusEventsRepository(InMemoryOmnibusEventsRepository, CsvEventDataWriter):
    def __init__(self, csv_path: Path):
        InMemoryOmnibusEventsRepository.__init__(self)
        CsvEventDataWriter.__init__(
            self,
            csv_path=csv_path,
            data_from_dict=lambda d: OmnibusDetail(
                area=d["area"],
                balance_kind=d["balance_kind"],
                pse_id=d["pse_id"],
                vsav_id=d["vsav_id"],
            ),
        )
        self._omnibus_details: List[OmnibusDetail] = []

    def remove_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        super().remove_omnibus(omnibus_detail)
        self.initialize_csv()
        for omnibus_detail in self._omnibus_details:
            self.writerow_from_event_data(omnibus_detail)

    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        super().add_omnibus(omnibus_detail)
        self.writerow_from_event_data(omnibus_detail)
