import csv
import json
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, Dict, List

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.events.event_entity import EventEntity
from shared.helpers.csv import reset_file, writerow
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.logger import logger
from shared.helpers.mkdir_if_relevent import mkdir_if_relevant


class CsvEventDataWriter:
    def __init__(
        self,
        csv_path: Path,
        data_from_dict: Callable[[Dict], Any],
    ):
        self.csv_path = csv_path
        self.data_from_dict = data_from_dict

    def initialize_csv(self):
        mkdir_if_relevant(self.csv_path.parent)
        reset_file(self.csv_path)
        writerow(self.csv_path, ["data"])

    def writerow_from_event_data(self, event_data: Any):
        writerow(
            self.csv_path,
            [
                json.dumps(dataclass_to_dict(event_data)),
            ],
        )

    def retrieve_entities_from_csv(self) -> List[Any]:
        datas = []
        with self.csv_path.open("r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                data_as_dict = json.loads(row["data"])
                datas.append(self.data_from_dict(data_as_dict))

                # topic = row["topic"]
                # if topic == self.event_topic:
                #     row["data"] = json.loads(row["data"])
                #     entities.append(self.entity_from_dict(row))
                # else:
                #     logger.warn(
                #         f"Found row with wrong topic {topic} in csv : {self.csv_path}"
                #     )
        return datas
