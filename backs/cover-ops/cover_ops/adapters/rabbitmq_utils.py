import os

from cover_ops.helpers.set_variable_for_context import set_variable_for_context


def make_rabbitmq_url():
    user = os.environ.get("RABBITMQ_USER", "guest")
    password = os.environ.get("RABBITMQ_PASS", "guest")
    host = set_variable_for_context("localhost", "rabbitmq", "rabbitmq")
    return f"amqp://{user}:{password}@{host}"
