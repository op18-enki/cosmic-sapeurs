import json
from typing import Any, Dict, List, Optional

from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from redis import Redis

from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
)
from shared.helpers.dataclass import dumps_dataclass
from shared.helpers.decorators import timeit


class RedisVehicleEventsRepository(AbstractVehicleEventsRepository):
    prefix = "latest_event_by_raw_vehicle_id"

    def __init__(self, purge: bool, redis_instance: Redis):
        super().__init__(purge)
        self.r = redis_instance

    @timeit("Redis Vehice repo: reset ")
    def reset(self):
        for k in self.r.scan_iter(self._make_latest_vehicle_event_data_for_id_key("*")):
            self.r.delete(k)

    def get_latest_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        binary_event = self.r.get(
            self._make_latest_vehicle_event_data_for_id_key(raw_vehicle_id)
        )
        return self.data_from_binary_event_data(binary_event) if binary_event else None

    def get_latest_data_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventData]:
        latest_vehicle_event_keys = self.r.keys(f"{self.prefix}*")
        binaries = [self.r.get(k) for k in latest_vehicle_event_keys]
        event_datas = [self.data_from_binary_event_data(binary) for binary in binaries]

        return {event_data.raw_vehicle_id: event_data for event_data in event_datas}

    def set_latest_vehicle_event_data_for_vehicle_id(
        self, vehicle_event_data: VehicleEventData
    ):
        vehicle_id = vehicle_event_data.raw_vehicle_id
        redis_key = self._make_latest_vehicle_event_data_for_id_key(vehicle_id)

        latest_stored_data_for_this_vehicle = self.get_latest_data_for_vehicle_id(
            vehicle_id
        )

        self.r.set(
            redis_key,
            dumps_dataclass(vehicle_event_data),
        )

        if latest_stored_data_for_this_vehicle:
            previous_raw_operation_id = (
                latest_stored_data_for_this_vehicle.raw_operation_id
            )
            if previous_raw_operation_id:
                self.r.srem(f"raw_operation_id:{previous_raw_operation_id}", redis_key)

        current_raw_operation_id = vehicle_event_data.raw_operation_id
        if current_raw_operation_id is not None:
            self.r.sadd(f"raw_operation_id:{current_raw_operation_id}", redis_key)

    def _add(self, event_datas: List[VehicleEventData]) -> None:
        for event_data in event_datas:
            self.set_latest_vehicle_event_data_for_vehicle_id(event_data)
        # self.r.hset(
        #     self.latest_event_by_raw_vehicle_id,
        #     event_entity.data.raw_vehicle_id,
        #     dumps_dataclass(event_entity),
        # )

    def purge_old_similar_events(self, event: VehicleEventData):
        # Since we only keep the latest event for each vehicle, no need to purge!
        pass

    @staticmethod
    def data_from_binary_data(binary: Any) -> VehicleEventData:
        decoded = binary.decode()
        deserialized = json.loads(decoded)
        return VehicleEventData.from_dict(deserialized["data"])

    @staticmethod
    def data_from_binary_event_data(binary: Any) -> VehicleEventData:
        decoded = binary.decode()
        deserialized = json.loads(decoded)
        return VehicleEventData.from_dict(deserialized)

    def _make_latest_vehicle_event_data_for_id_key(self, vehicle_id: str) -> str:
        return f"{self.prefix}_{vehicle_id}"
