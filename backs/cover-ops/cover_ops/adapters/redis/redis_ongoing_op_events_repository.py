import json
from typing import Any, List, Optional

from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from redis import Redis

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
    OperationCause,
)
from shared.data_transfert_objects.operation_event_data import (
    RawOperationId,
    operation_relevant_ongoing_status_options,
    operation_closed,
)
from cover_ops.domain.helpers.flatten_list import flatten_list
from shared.helpers.dataclass import dumps_dataclass
from shared.helpers.date import hours_between
from shared.helpers.decorators import timeit


class RedisOngoingOpEventsRepository(AbstractOngoingOpEventsRepository):
    prefix = "ongoing_ops"
    ongoing_operation_id_key = "ongoing_operation_ids"

    def __init__(self, purge: bool, redis_instance: Redis):
        super().__init__(purge)
        self.r = redis_instance

    @timeit("Redis ongoing op repo: reset")
    def reset(self) -> None:
        self.r.delete(self.ongoing_operation_id_key)
        for k in self.r.scan_iter(self._make_operation_id_key("*")):
            self.r.delete(k)
        for k in self.r.scan_iter(self._make_area_cause_key("*", "*")):
            self.r.delete(k)

    def _add(self, event_datas: List[OngoingOpChangedEventData]) -> None:

        serialized_datas = [dumps_dataclass(data) for data in event_datas]

        for (data, serialized_data) in zip(event_datas, serialized_datas):
            raw_operation_id = data.raw_operation_id
            is_ongoing = data.status in operation_relevant_ongoing_status_options
            is_closed = data.status == operation_closed

            key_list_operation_datas = self._make_operation_id_key(raw_operation_id)
            if is_closed:
                # keep only the closing event in the list
                self.r.delete(key_list_operation_datas)

            self.r.rpush(
                key_list_operation_datas,
                serialized_data,
            )

            if is_ongoing:
                if raw_operation_id not in self._get_ongoing_operation_ids():
                    self.r.rpush(self.ongoing_operation_id_key, raw_operation_id)
            else:
                self.r.lrem(self.ongoing_operation_id_key, 1, raw_operation_id)

        self.r.mset(
            {
                self._make_area_cause_key(
                    data.opening_infos.address_area,
                    data.opening_infos.cause,
                ): serialized_data
                for (data, serialized_data) in zip(event_datas, serialized_datas)
            }
        )

    def purge_old_similar_events(self, event_data: OngoingOpChangedEventData):
        timestamp = event_data.timestamp
        key = self._make_operation_id_key(event_data.raw_operation_id)
        binary_datas_of_same_operation = self.r.lrange(key, 0, -1)
        if len(binary_datas_of_same_operation) == 1:
            return

        count_to_purge = 0
        for binary_data in binary_datas_of_same_operation:
            repo_event_data_timestamp = json.loads(binary_data.decode())["timestamp"]

            repo_data_age = hours_between(repo_event_data_timestamp, timestamp)
            if repo_data_age > self._purge_older_than:
                count_to_purge += 1
            else:
                break
            self.r.lrem(key, count_to_purge, event_data.raw_operation_id)

    def get_all_ongoing_ops(self) -> List[OngoingOpChangedEventData]:
        ongoing_operation_ids = self._get_ongoing_operation_ids()
        latest_event_keys = [
            self._make_operation_id_key(ongoing_op_id)
            for ongoing_op_id in ongoing_operation_ids
        ]
        return list(
            sorted(
                flatten_list(
                    [
                        [
                            self._data_from_binary_data(binary_data)
                            for binary_data in self.r.lrange(key, 0, -1)
                        ]
                        for key in latest_event_keys
                    ],
                ),
                key=lambda event_data: event_data.timestamp,
            )
        )

    def get_latest_event_data_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        binary_event_data = self.r.get(self._make_area_cause_key(area, cause))
        if binary_event_data is None:
            return
        return self._data_from_binary_data(binary_event_data)

    def get_latest_event_data_for_all_area_for_cause_fire_and_victim(
        self,
    ) -> List[OngoingOpChangedEventData]:
        latest_victim_event_keys = (
            self.r.keys(self._make_area_cause_key("*", "victim")) or []
        )
        latest_fire_event_keys = (
            self.r.keys(self._make_area_cause_key("*", "fire")) or []
        )
        return [
            self._data_from_binary_data(self.r.get(key))
            for key in latest_victim_event_keys + latest_fire_event_keys
        ]

    def get_latest_event_data_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> Optional[OngoingOpChangedEventData]:
        latest_binary_event = self.r.lindex(
            self._make_operation_id_key(raw_operation_id), -1
        )

        return (
            self._data_from_binary_data(latest_binary_event)
            if latest_binary_event
            else None
        )

    def _make_operation_id_key(self, raw_operation_id: str) -> str:
        return f"{self.prefix}_by_id_{raw_operation_id}"

    def _make_area_cause_key(self, area: str, cause: str) -> str:
        return f"{self.prefix}_by_area_and_cause_{area}_{cause}"

    @staticmethod
    def _data_from_binary_data(binary: Any) -> OngoingOpChangedEventData:
        decoded = binary.decode()
        deserialized = json.loads(decoded)
        return OngoingOpChangedEventData.from_dict(deserialized)

    def _get_ongoing_operation_ids(self):
        return [
            ongoing_operation_id.decode()
            for ongoing_operation_id in self.r.lrange(
                self.ongoing_operation_id_key, 0, -1
            )
        ]
