from cover_ops.adapters.redis.redis_utils import deserialize_value_from_redis
from cover_ops.domain.ports.api_state_cache import AbstractApiStateCache
from redis import Redis

is_ready_key = "is_ready"


class RedisApiStateCache(AbstractApiStateCache):
    def __init__(self, redis_instance: Redis):
        self.r = redis_instance
        super().__init__()

    def get_is_ready(self) -> bool:
        value_in_cache = self.r.get(is_ready_key)
        return (
            bool(deserialize_value_from_redis(value_in_cache))
            if value_in_cache
            else False
        )

    def set_is_ready(self, is_ready: bool) -> None:
        self.r.set(is_ready_key, int(is_ready))
