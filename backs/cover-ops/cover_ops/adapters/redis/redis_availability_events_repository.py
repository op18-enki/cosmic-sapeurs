import json
from typing import Any, List

from cover_ops.domain.helpers.flatten_list import flatten_list
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
    default_availability_stored_infos,
)
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from redis import Redis

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.dataclass import dumps_dataclass
from shared.helpers.date import hours_between
from shared.helpers.decorators import timeit


class RedisAvailabilityEventsRepository(AbstractAvailabilityEventsRepository):
    prefix_list = "availabilities_list"
    prefix_latest = "availabilities_latest"

    def __init__(self, purge: bool, redis_instance: Redis):
        super().__init__(purge)
        self.r = redis_instance

    @timeit("Redis availability reop: reset")
    def reset(self) -> None:
        for k in self.r.scan_iter(self._make_vehicle_key_by_id("*")):
            self.r.delete(k)
        for k in self.r.scan_iter(self._make_vehicle_key_by_area_by_role("*", "*")):  # type: ignore
            self.r.delete(k)

    def get_availability_event_data_since(self) -> List[AvailabilityChangedEventData]:
        availailities_keys = self.r.keys(self._make_vehicle_key_by_id("*"))

        binary_events = flatten_list(
            [
                self.r.lrange(availailities_key, 0, -1)
                for availailities_key in availailities_keys
            ]
        )
        return self.sort_availability_data_by_timestamp(
            [
                self.event_data_from_binary(binary_event)
                for binary_event in binary_events
            ]
        )

    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        binary = self.r.get(self._make_vehicle_key_by_role(role))
        if binary is None:
            return default_availability_stored_infos
        availability_event_data = self.event_data_from_binary(binary)
        return AvailabilityStoredInfos(
            availability_event_data.bspp_availability,
            latest_event_data=availability_event_data.latest_event_data,
        )

    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        binary = self.r.get(self._make_vehicle_key_by_area_by_role(area, role))
        if binary is None:
            return default_availability_stored_infos

        availability_event_data = self.event_data_from_binary(binary)
        return AvailabilityStoredInfos(
            availability_event_data.area_availability,
            latest_event_data=availability_event_data.latest_event_data,
        )

    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:

        availailities_keys = self.r.keys(f"{self.prefix_list}*")

        binary_events = [
            self.r.lindex(availailities_key, -1)
            for availailities_key in availailities_keys
        ]

        return self.sort_availability_data_by_timestamp(
            [
                self.event_data_from_binary(binary_event)
                for binary_event in binary_events
            ],
        )

    def purge_old_similar_events(self, event_data: AvailabilityChangedEventData):
        event_timestamp = event_data.timestamp
        key = self._make_vehicle_key_by_id(event_data.raw_vehicle_id)
        binary_events_of_same_vehicle = self.r.lrange(key, 0, -1)
        if len(binary_events_of_same_vehicle) == 1:
            return

        count_to_purge = 0
        for binary_event in binary_events_of_same_vehicle:
            repo_event_timestamp = json.loads(binary_event.decode())["timestamp"]

            repo_event_age = hours_between(repo_event_timestamp, event_timestamp)
            if repo_event_age > self._purge_older_than:
                count_to_purge += 1
            else:
                break

        # purge those indexes (LPOP more than one, in redis lib not implemented)
        self.r.execute_command(f"LPOP {key} {count_to_purge}")

    def get_latest_vehicle_event_data_affected_to_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        affected_vehicle_event_data = []
        latest_event_data_keys = self.r.keys(f"availability_latest_event_data_for_*")
        for latest_event_data_key in latest_event_data_keys:
            latest_event_data_operation = VehicleEventData.from_dict(
                json.loads(self.r.get(latest_event_data_key).decode())  # type: ignore
            )
            if latest_event_data_operation.raw_operation_id == raw_operation_id:
                affected_vehicle_event_data.append(latest_event_data_operation)
        return self.sort_vehicle_data_by_timestamp(affected_vehicle_event_data)

    def _add(self, event_datas: List[AvailabilityChangedEventData]) -> None:
        serialized_event_datas = [
            dumps_dataclass(event_data) for event_data in event_datas
        ]
        for (serialized_event_data, event_data) in zip(
            serialized_event_datas, event_datas
        ):
            self.r.rpush(
                self._make_vehicle_key_by_id(event_data.raw_vehicle_id),
                serialized_event_data,
            )

        self.r.mset(
            {
                self._make_vehicle_key_by_area_by_role(
                    event_data.home_area, event_data.role
                ): serialized_event_data
                for (serialized_event_data, event_data) in zip(
                    serialized_event_datas, event_datas
                )
            }
        )

        self.r.mset(
            {
                self._make_vehicle_key_by_role(event_data.role): serialized_event_data
                for (serialized_event_data, event_data) in zip(
                    serialized_event_datas, event_datas
                )
            }
        )
        self.r.mset(
            {
                f"availability_latest_event_data_for_{event_data.raw_vehicle_id}": dumps_dataclass(
                    event_data.latest_event_data
                )
                for event_data in event_datas
            }
        )

    def _make_vehicle_key_by_id(self, raw_vehicle_id: str) -> str:
        return f"{self.prefix_list}_{raw_vehicle_id}"

    def _make_vehicle_key_by_area_by_role(self, area: Area, role: VehicleRole) -> str:
        return f"{self.prefix_latest}_{area}_{role}"

    def _make_vehicle_key_by_role(self, role: VehicleRole) -> str:
        return f"{self.prefix_latest}_{role}"

    @staticmethod
    def event_data_from_binary(binary: Any) -> AvailabilityChangedEventData:
        decoded = binary.decode()
        deserialized = json.loads(decoded)
        return AvailabilityChangedEventData.from_dict(deserialized)

    @staticmethod
    def sort_availability_data_by_timestamp(
        event_datas: List[AvailabilityChangedEventData],
    ) -> List[AvailabilityChangedEventData]:
        return list(
            sorted(
                event_datas,
                key=lambda event_data: event_data.timestamp,
            )
        )

    @staticmethod
    def sort_vehicle_data_by_timestamp(
        event_datas: List[VehicleEventData],
    ) -> List[VehicleEventData]:
        return list(
            sorted(
                event_datas,
                key=lambda event_data: event_data.timestamp,
            )
        )
