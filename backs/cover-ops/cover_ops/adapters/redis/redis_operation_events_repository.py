import json
from typing import Any, List, Optional, Set, Tuple

from cover_ops.adapters.redis.redis_availability_events_repository import flatten_list
from cover_ops.domain.entities.event_entities import OperationEventData
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
    sort_operation_datas_by_timestamp,
)
from redis import Redis

from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationRelevantStatus,
    OperationStatus,
    operation_closed,
    operation_relevant_ongoing_status_options,
)
from shared.helpers.dataclass import dumps_dataclass
from shared.helpers.date import hours_between
from shared.helpers.decorators import timeit


class RedisOperationEventsRepository(AbstractOperationEventsRepository):
    prefix = "operation"
    active_operation_ids = "active_operation_ids"
    operations_notified_from_vehicle_changes_key = (
        "operations_notified_from_vehicle_changes_key"
    )

    def __init__(self, purge: bool, redis_instance: Redis):
        super().__init__(purge)
        self.r = redis_instance

    @timeit("Redis Operation repo: reset ")
    def reset(self) -> None:
        self.r.delete(self.operations_notified_from_vehicle_changes_key)
        self.r.delete(self.active_operation_ids)
        for k in self.r.scan_iter(self._make_opening_info_for_operation_id_key("*")):
            self.r.delete(k)
        for k in self.r.scan_iter(self._make_data_for_operation_id_key("*")):
            self.r.delete(k)

    def _add(self, event_datas: List[OperationEventData]):
        for event_data in event_datas:
            raw_operation_id = event_data.raw_operation_id
            is_ongoing = event_data.status in operation_relevant_ongoing_status_options
            is_closed = event_data.status == operation_closed

            self.r.rpush(
                self._make_data_for_operation_id_key(raw_operation_id),
                dumps_dataclass(event_data),
            )
            if is_ongoing:
                if raw_operation_id not in self._get_ongoing_operation_ids():
                    self.r.rpush(self.active_operation_ids, raw_operation_id)
            else:
                self.r.lrem(self.active_operation_ids, 1, raw_operation_id)

            has_opening_info = (
                event_data.cause is not None and event_data.address_area is not None
            )
            if has_opening_info:
                self.r.set(
                    self._make_opening_info_for_operation_id_key(raw_operation_id),
                    dumps_dataclass(event_data),
                )
            if is_closed:
                self.r.delete(
                    self._make_opening_info_for_operation_id_key(raw_operation_id),
                )

    def purge_old_similar_events(self, event_data: OperationEventData):
        timestamp = event_data.timestamp
        key = self._make_data_for_operation_id_key(event_data.raw_operation_id)
        binary_datas_of_same_operation = self.r.lrange(key, 0, -1)
        if len(binary_datas_of_same_operation) == 1:
            return

        count_to_purge = 0
        for binary_data in binary_datas_of_same_operation:
            repo_data_timestamp = json.loads(binary_data.decode())["timestamp"]

            repo_event_age = hours_between(repo_data_timestamp, timestamp)
            if repo_event_age > self._purge_older_than:
                count_to_purge += 1
            else:
                break

        # purge those indexes (LPOP more than one, in redis lib not implemented)
        self.r.execute_command(f"LPOP {key} {count_to_purge}")

    def get_latest_event_data_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationEventData]:
        latest_binary_data = self.r.lindex(
            self._make_data_for_operation_id_key(raw_operation_id), -1
        )
        if latest_binary_data is None:
            return
        return self._event_data_from_binary_data(latest_binary_data)

    def get_datas_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationRelevantStatus]] = None,
    ) -> List[OperationEventData]:
        binary_datas_for_operation = self.r.lrange(
            self._make_data_for_operation_id_key(raw_operation_id), 0, -1
        )
        event_datas = [
            self._event_data_from_binary_data(binary_data)
            for binary_data in binary_datas_for_operation
        ]
        if statuses is None:
            return event_datas
        return [
            event_data for event_data in event_datas if event_data.status in statuses
        ]

    def get_ongoing_operation_datas_with_relevant_status(
        self,
    ) -> List[OperationEventData]:
        ongoing_operation_ids = self._get_ongoing_operation_ids()

        binary_datas = flatten_list(
            [
                self.r.lrange(
                    self._make_data_for_operation_id_key(ongoing_operation_id), 0, -1
                )
                for ongoing_operation_id in ongoing_operation_ids
            ]
        )
        return sort_operation_datas_by_timestamp(
            [
                self._event_data_from_binary_data(binary_data)
                for binary_data in binary_datas
            ]
        )

    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        self.r.set(
            self.operations_notified_from_vehicle_changes_key,
            json.dumps(list(operation_ids)),
        )

    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        value_in_cache = self.r.get(self.operations_notified_from_vehicle_changes_key)
        if not value_in_cache:
            return set()
        return set(json.loads(value_in_cache.decode()))

    def get_operation_opening_infos_for_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationOpeningInfos]:
        value_in_cache = self.r.get(
            self._make_opening_info_for_operation_id_key(raw_operation_id)
        )
        if not value_in_cache:
            return
        event_data = self._event_data_from_binary_data(value_in_cache)
        opening_infos = self.make_opening_info_from_event_data(event_data)
        return opening_infos

    def _make_data_for_operation_id_key(self, raw_operation_id: str) -> str:
        return f"{self.prefix}_event_datas_{raw_operation_id}"

    def _make_opening_info_for_operation_id_key(self, raw_operation_id: str) -> str:
        return f"{self.prefix}_opening_{raw_operation_id}"

    @staticmethod
    def _event_data_from_binary_data(binary: Any) -> OperationEventData:
        decoded = binary.decode()
        deserialized = json.loads(decoded)
        return OperationEventData.from_dict(deserialized)

    def _get_ongoing_operation_ids(self):
        return [
            ongoing_operation_id.decode()
            for ongoing_operation_id in self.r.lrange(self.active_operation_ids, 0, -1)
        ]
