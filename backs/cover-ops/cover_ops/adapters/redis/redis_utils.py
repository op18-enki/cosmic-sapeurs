import json
from typing import Any, List, Literal, Optional, Union

from cover_ops.helpers.set_variable_for_context import set_variable_for_context
from redis import Redis

redis_port: int = set_variable_for_context(
    local_value=6379, prod_value=6379, CI_value=6379
)
redis_host: Literal["localhost", "redis"] = set_variable_for_context(
    local_value="localhost", prod_value="redis", CI_value="redis"
)


def make_redis_instance() -> Redis:
    return Redis(host=redis_host, port=redis_port, db=0)


def deserialize_value_from_redis(serialized_value):
    return json.loads(serialized_value) if serialized_value else None


def redis_get_and_loads(r: Redis, key: str) -> Optional[Any]:
    serialized_value = r.get(key)
    return deserialize_value_from_redis(serialized_value)


def redis_get_many_and_loads(r: Redis, keys: List[str]) -> List[Any]:
    serialized_values = r.mget(keys)
    return [deserialize_value_from_redis(v) for v in serialized_values]
