from typing import Optional

from cover_ops.adapters.redis.redis_utils import deserialize_value_from_redis
from cover_ops.domain.ports.front_lines_history_cache import (
    AbstractFrontLinesHistoryCache,
)
from redis import Redis

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory
from shared.helpers.dataclass import dumps_dataclass

front_lines_history_key = "front_lines_history"


class RedisFrontLinesHistoryCache(AbstractFrontLinesHistoryCache):
    def __init__(self, redis_instance: Redis):
        self.r = redis_instance
        super().__init__()

    def get_front_lines_history(self) -> FrontLinesHistory:
        value_in_cache = self.r.get(front_lines_history_key)
        deserialized_value = deserialize_value_from_redis(value_in_cache)
        return (
            FrontLinesHistory(**deserialized_value)
            if deserialized_value
            else FrontLinesHistory([], [])
        )

    def set_front_lines_history(self, front_lines_history: FrontLinesHistory) -> None:
        self.r.set(front_lines_history_key, dumps_dataclass(front_lines_history))
