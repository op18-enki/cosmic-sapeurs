from typing import List, Optional

from cover_ops.adapters.redis.redis_utils import (
    redis_get_and_loads,
    redis_get_many_and_loads,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from redis import Redis

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.helpers.dataclass import dumps_dataclass
from shared.helpers.decorators import timeit
from shared.helpers.logger import logger


class RedisOmnibusEventsRepository(AbstractOmnibusEventsRepository):
    omnibus_detail_key = "omnibus_detail"

    def __init__(self, redis_instance: Redis):
        self.r = redis_instance

    @timeit("Redis omnibus reop: reset")
    def reset(self):
        for k in self.r.scan_iter(self._make_omnibus_detail_for_ids_key("*", "*")):
            self.r.delete(k)

    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        self.r.set(
            self._make_omnibus_detail_for_ids_key(
                omnibus_detail.vsav_id, omnibus_detail.pse_id
            ),
            dumps_dataclass(omnibus_detail),
        )

    def get_omnibus_details(self) -> List[OmnibusDetail]:
        keys = self.r.keys(f"{self.omnibus_detail_key}*")
        details_as_dict = redis_get_many_and_loads(self.r, keys)
        return [OmnibusDetail(**detail_as_dict) for detail_as_dict in details_as_dict]

    def remove_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        key = self.get_omnibus_detail_key_for_ids(
            omnibus_detail.vsav_id, omnibus_detail.pse_id
        )
        if not key:
            logger.warn(
                f"Omnibus detail {omnibus_detail} not in cache. Cannot remove. "
            )
            return
        self.r.delete(key)

    def get_omnibus_detail(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[OmnibusDetail]:
        key = self.get_omnibus_detail_key_for_ids(raw_vehicle_id, raw_vehicle_id)
        if key:
            detail_as_dict = redis_get_and_loads(self.r, key)
            return OmnibusDetail(**detail_as_dict) if detail_as_dict else None

    def _make_omnibus_detail_for_ids_key(
        self, vsav_id: Optional[str], pse_id: Optional[str]
    ) -> str:
        return f"{self.omnibus_detail_key}_{vsav_id}_{pse_id}"

    def get_omnibus_detail_key_for_ids(
        self,
        vsav_id: Optional[RawVehicleId],
        pse_id: Optional[RawVehicleId],
    ) -> Optional[str]:
        if not any([vsav_id, pse_id]):
            logger.warn("Either vsav_id or pse_id should be given")
            return
        if vsav_id:
            vsav_match = self.r.keys(
                self._make_omnibus_detail_for_ids_key(vsav_id, "*")
            )
            if vsav_match:
                return vsav_match[0]
        if pse_id:
            pse_match = self.r.keys(self._make_omnibus_detail_for_ids_key("*", pse_id))
            if pse_match:
                return pse_match[0]
