from dataclasses import is_dataclass
from typing import Union

from shared.events.event_entity import EventData, EventEntity
from shared.helpers.dataclass import dataclass_to_dict
from shared.sapeurs_events import Event


def serialize_data(data: EventData):
    if is_dataclass(data):
        payload = dataclass_to_dict(data)
    else:
        payload = data
    return payload


def backend_event_to_frontend_event(event: Union[Event, EventEntity]):
    return {
        "payload": serialize_data(event.data) if event.data else None,
    }
