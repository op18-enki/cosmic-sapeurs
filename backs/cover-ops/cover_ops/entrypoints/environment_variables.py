import os
from dataclasses import dataclass
from typing import List, Literal, Optional

from dotenv import load_dotenv

from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

load_dotenv()

get_env_variable = prepare_get_env_variable(os.environ.get)

ExceptionMode = Literal["RAISE", "PASS"]
exception_mode_options: List[ExceptionMode] = ["RAISE", "PASS"]

EventsRepositories = Literal["CSV", "REDIS"]  # "PG" (no more PG)
events_repositories_options: List[EventsRepositories] = [
    "CSV",
    "REDIS",
]  #  "PG" (no more PG)

CacheOption = Literal["IN_MEMORY", "REDIS"]
cache_options: List[CacheOption] = ["IN_MEMORY", "REDIS"]

ConvertersGatewayOption = Literal["IN_MEMORY", "HTTP"]
converters_gateway_options: List[ConvertersGatewayOption] = ["IN_MEMORY", "HTTP"]

DomainEventBusOption = Literal["IN_MEMORY", "RABBITMQ"]
domain_event_bus_options: List[DomainEventBusOption] = ["IN_MEMORY", "RABBITMQ"]

# TODO : purge should be an env variable
@dataclass
class EnvironmentVariables:
    exception_mode: ExceptionMode
    events_repositories: EventsRepositories
    cache: CacheOption
    domain_event_bus: DomainEventBusOption
    converters_gateway: ConvertersGatewayOption
    converters_url: str
    pg_url: Optional[str] = None

    def __str__(self):
        return f"\
            exception_mode: {self.exception_mode} \n\
            events_repositories: {self.events_repositories} \n\
            cache: {self.cache} \n\
            domain_event_bus: {self.domain_event_bus} \n\
            # converters_url: {self.converters_url}\n\
            pg_url: {self.pg_url} \n"


def get_env_variables() -> EnvironmentVariables:
    events_repositories = get_env_variable(
        "EVENTS_REPOSITORIES", events_repositories_options
    )
    cache = get_env_variable("CACHE", cache_options)
    domain_event_bus = get_env_variable("DOMAIN_EVENT_BUS", domain_event_bus_options)
    converters_gateway = get_env_variable(
        "CONVERTERS_GATEWAY", converters_gateway_options
    )

    pg_url: Optional[str] = None
    if events_repositories == "PG":
        pg_url = os.environ.get("PG_URL")
        if not pg_url:
            raise Exception("When REPOSITORIES is PG, a PG_URL is needed")

    return EnvironmentVariables(
        exception_mode=get_env_variable("EXCEPTION_MODE", exception_mode_options),
        events_repositories=events_repositories,
        cache=cache,
        domain_event_bus=domain_event_bus,
        pg_url=pg_url,
        converters_gateway=converters_gateway,
        converters_url=os.environ.get("CONVERTERS_URL", "missing-converter-url"),
    )
