from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.front_lines_history_cache import (
    AbstractFrontLinesHistoryCache,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.get_cover import GetCover
from cover_ops.domain.use_cases.get_ops import GetOps
from cover_ops.domain.use_cases.notify_when_vehicle_availability_changed_affects_operation import (
    NotifyWhenVehicleAvailabilityChangedAffectsOperation,
)
from cover_ops.domain.use_cases.set_cover_ops_repositories_from_snapshot import (
    SetCoverOpsRepositoriesFromSnapshot,
)
from cover_ops.domain.use_cases.transfert_events_batch_to_repositories import (
    TransfertEventsBatchToRepositories,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.domain.use_cases.update_ops import UpdateOps

from shared.data_transfert_objects.reset_cover_ops_finished_event_data import (
    ResetCoverOpsFinishedEventData,
)
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import (
    AvailabilityChangedEvent,
    ConvertersVehiclesInitEvent,
    OperationEvent,
    ResetCoverOpsFinishedEvent,
    VehicleEvent,
)


async def subscribe_to_vehicle_changed_status(
    domain_event_bus: DomainEventBus,
    update_cover: UpdateCover,
    update_omnibus: UpdateOmnibus,
    add_events_to_repositories: AddEventsToRepositories,
):
    async def update_omnibus_and_cover_then_add_to_repo(event: VehicleEvent):
        await update_omnibus.execute(event)
        await update_cover.execute(event)
        await add_events_to_repositories.execute(event)

    await domain_event_bus.subscribe(
        "vehicle_changed_status", update_omnibus_and_cover_then_add_to_repo
    )


async def subscribe_to_operation_changed_status(
    domain_event_bus: DomainEventBus,
    update_ops: UpdateOps,
    add_events_to_repositories: AddEventsToRepositories,
):
    async def update_ops_and_add_to_repo(event: OperationEvent):
        await update_ops.execute(event)
        await add_events_to_repositories.execute(event)

    await domain_event_bus.subscribe(
        "operation_changed_status", update_ops_and_add_to_repo
    )


async def subscribe_to_availability_changed(
    domain_event_bus: DomainEventBus,
    notify_when_vehicle_availability_changed_affects_operation: NotifyWhenVehicleAvailabilityChangedAffectsOperation,
    add_events_to_repositories: AddEventsToRepositories,
):
    async def notify_and_add_to_repo(event: AvailabilityChangedEvent):
        await notify_when_vehicle_availability_changed_affects_operation.execute(event)
        await add_events_to_repositories.execute(event)

    await domain_event_bus.subscribe("availabilityChanged", notify_and_add_to_repo)


async def subscribe_to_converters_initialized_last_status_of_all_vehicles(
    domain_event_bus: DomainEventBus,
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories,
):
    async def transfert_event_batch_and_resync_from_repo(
        event: ConvertersVehiclesInitEvent,
    ):
        transfert_events_batch_to_repositories.execute(event.data)

    await domain_event_bus.subscribe(
        "converters_initialized_last_status_of_all_vehicles",
        transfert_event_batch_and_resync_from_repo,
    )


async def subscribe_to_reset_repositories_from_snapshot_executed(
    domain_event_bus: DomainEventBus,
    uuid: AbstractUuid,
    get_cover: GetCover,
    get_ops: GetOps,
    front_lines_history_cache: AbstractFrontLinesHistoryCache,
    omnibus_events_repo: AbstractOmnibusEventsRepository,
):
    async def get_cover_and_ops_and_publish_for_front():
        omnibus_details = omnibus_events_repo.get_omnibus_details()
        availabilities = get_cover.execute()
        ongoing_ops = get_ops.execute()
        front_lines_history = front_lines_history_cache.get_front_lines_history()
        # FOR REPLAY ONLY ... -------
        # if front_lines_history.victim:
        #     last_timestamp_front_history = front_lines_history.victim[-1]["timestamp"]
        #     snapshot_timestamp = add_milliseconds(last_timestamp_front_history, 1200000)
        #     for availability in availabilities:
        #         availability.timestamp = snapshot_timestamp

        #     for ongoing_op in ongoing_ops:
        #         ongoing_op.timestamp = snapshot_timestamp
        # --------------
        await domain_event_bus.publish(
            ResetCoverOpsFinishedEvent(
                uuid=uuid.make(),
                data=ResetCoverOpsFinishedEventData(
                    omnibus_details, availabilities, ongoing_ops, front_lines_history
                ),
            )
        )

    await domain_event_bus.subscribe(
        "set_repositories_from_snapshot_executed",
        get_cover_and_ops_and_publish_for_front,
    )


async def subscribe_to_domain_events(
    uuid: AbstractUuid,
    domain_event_bus: DomainEventBus,
    front_lines_history_cache: AbstractFrontLinesHistoryCache,
    update_cover: UpdateCover,
    update_ops: UpdateOps,
    update_omnibus: UpdateOmnibus,
    add_events_to_repositories: AddEventsToRepositories,
    notify_when_vehicle_availability_changed_affects_operation: NotifyWhenVehicleAvailabilityChangedAffectsOperation,
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories,
    set_coverops_repositories_from_snapshot: SetCoverOpsRepositoriesFromSnapshot,
    get_cover: GetCover,
    get_ops: GetOps,
    omnibus_events_repo: AbstractOmnibusEventsRepository,
):
    await subscribe_to_vehicle_changed_status(
        domain_event_bus, update_cover, update_omnibus, add_events_to_repositories
    )

    await subscribe_to_operation_changed_status(
        domain_event_bus, update_ops, add_events_to_repositories
    )

    await subscribe_to_converters_initialized_last_status_of_all_vehicles(
        domain_event_bus,
        transfert_events_batch_to_repositories,
    )
    await subscribe_to_availability_changed(
        domain_event_bus,
        notify_when_vehicle_availability_changed_affects_operation,
        add_events_to_repositories,
    )

    await domain_event_bus.subscribe(
        "trigger_reset_from_snapshot", set_coverops_repositories_from_snapshot.execute
    )

    await subscribe_to_reset_repositories_from_snapshot_executed(
        domain_event_bus,
        uuid,
        get_cover,
        get_ops,
        front_lines_history_cache,
        omnibus_events_repo,
    )

    await domain_event_bus.subscribe(
        "ongoingOpChanged", add_events_to_repositories.execute
    )

    await domain_event_bus.subscribe(
        "omnibusBalanceChanged", add_events_to_repositories.execute
    )
