import asyncio
from asyncio.events import AbstractEventLoop
from time import sleep

from aio_pika.connection import connect
from cover_ops.adapters.rabbitmq_utils import make_rabbitmq_url

from shared.helpers.logger import logger


async def wait_for_rabbitmq(loop: AbstractEventLoop):
    rabbit_url = make_rabbitmq_url()
    attempt = 1
    while attempt < 5:
        logger.info(
            f"Trying to connect to RabbitMQ with url {rabbit_url} for the {attempt}th time..."
        )
        try:
            await connect(rabbit_url, loop=loop)
            logger.info(f"Successfully connected to RabbitMQ!")
            return
        except Exception as e:
            attempt += 1
            logger.info(
                f"Failed connecting to RabbitMQ -> {e}. \nWill sleep for 10 seconds and retry..."
            )
            sleep(10)
