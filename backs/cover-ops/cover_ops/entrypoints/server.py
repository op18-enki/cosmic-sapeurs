import asyncio
import os
from asyncio.events import AbstractEventLoop
from typing import Any, Callable, Tuple, Union

import aiohttp_cors
import click
from aiohttp import web
from aiohttp.web_app import Application
from cover_ops.domain.use_cases.set_cover_ops_repositories_from_snapshot import (
    SetCoverOpsRepositoriesFromSnapshot,
)
from cover_ops.entrypoints.backend_to_frontend import serialize_data
from cover_ops.entrypoints.config import Config
from cover_ops.entrypoints.environment_variables import ExceptionMode, get_env_variables
from cover_ops.entrypoints.socket_handlers import (
    SapeurWebSocketResponse,
    send_events_to_front,
)

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.logger import logger
from shared.routes import make_url, sapeurs_routes
from shared.sapeurs_events import (
    OperationEvent,
    ResetCoverOpsRequestedEvent,
    TriggerResetFromSnapshot,
    VehicleEvent,
)
from shared.wait_for_pg import wait_for_pg

web_json_response_missing_body = web.json_response(
    {"success": "false"}, status=400, reason="Missing body"
)

web_json_response_unavailable = web.json_response(
    {"success": "false"}, status=503, reason="API not ready"
)


route_converted_event = make_url(sapeurs_routes.prefix, sapeurs_routes.converted_event)


def make_app(config: Config) -> Tuple[Application, SetCoverOpsRepositoriesFromSnapshot]:

    routes = web.RouteTableDef()

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.hello))
    async def hello(request):
        logger.info("Received Hello Sapeur request")
        return web.json_response({"message": "Hello, world"})

    @routes.post(make_url(sapeurs_routes.prefix, sapeurs_routes.reset))
    async def resetCoverOps(request):
        logger.info("Received Reset CoverOps request")
        history = await request.json()

        if history["victim"] | history["fire"]:
            config.front_lines_history.set_front_lines_history(
                front_lines_history=FrontLinesHistory(
                    history["victim"], history["fire"]
                )
            )

        await config.domain_event_bus.publish(
            ResetCoverOpsRequestedEvent(config.uuid.make())
        )
        await config.domain_event_bus.publish(
            TriggerResetFromSnapshot(config.uuid.make())
        )
        return web.json_response({"response": "reset request taken into account"})

    async def publish_in_domain(e: Union[VehicleEvent, OperationEvent]):
        await config.domain_event_bus.publish(e)

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_cover))
    async def get_last_status_of_all_vehicles_route(request):
        availability_events_data = config.use_cases.get_cover.execute()
        serialized_events = [
            serialize_data(event_data) for event_data in availability_events_data
        ]
        return web.json_response(serialized_events)

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_ops))
    async def get_last_status_of_all_on_going_ops_route(request):
        on_going_op_events_data = config.use_cases.get_ops.execute()
        serialized_events = [
            serialize_data(event_data) for event_data in on_going_op_events_data
        ]
        return web.json_response(serialized_events)

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_omnibus))
    async def get_last_omnibus_balance_route(request):
        omnibus_details = config.omnibus_events_repo.get_omnibus_details()
        omnibus_event_data = OmnibusBalanceChangedEventData(
            details=omnibus_details, timestamp=config.clock.get_now()
        )
        return web.json_response(dataclass_to_dict(omnibus_event_data))

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_front_history))
    async def get_front_lines_history_route(request):
        front_lines_history = config.front_lines_history.get_front_lines_history()
        return web.json_response(dataclass_to_dict(front_lines_history))

    @routes.post(route_converted_event)
    async def converted_event(request):
        if not config.api_state_cache.get_is_ready():
            logger.info("Converters tries to reach me, but I'm not ready...")
            return web_json_response_unavailable

        if not request.body_exists:
            return web_json_response_missing_body

        sapeurs_event = await request.json()
        await config.use_cases.publish_converters_events_to_domain_event_bus.execute(
            sapeurs_event
        )
        return web.json_response({"success": "true"})

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.ping))
    async def cover_ops_is_ready(request):
        return web.json_response({"is_ready": config.api_state_cache.get_is_ready()})

    @routes.get(make_url(sapeurs_routes.prefix, "ws"))
    async def websocket_handler(request):
        ws = await send_events_to_front(
            SapeurWebSocketResponse(),
            config.domain_event_bus,
            topics=[
                "availabilityChanged",
                "ongoingOpChanged",
                "omnibusBalanceChanged",
                "resetCoverOpsRequested",
                "resetCoverOpsFinished",
            ],
        )
        await ws.prepare(request)
        await ws.run()
        return ws

    # loop = asyncio.get_event_loop()
    app = web.Application(client_max_size=64 * 1024 * 1024)
    app.add_routes(routes)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
        },
    )

    for route in list(app.router.routes()):
        cors.add(route)
    return app, config.use_cases.set_coverops_repositories_from_snapshot


def make_exception_handler(
    exception_mode: ExceptionMode,
) -> Callable[[AbstractEventLoop, Any], None]:
    def exception_handler(loop: AbstractEventLoop, context):
        loop.default_exception_handler(context)

        exception = context.get("exception")
        # if isinstance(exception, SapeursError):
        logger.warn(f"[Cover-Ops] Got a Sapeurs-Error {exception}. Will stop the loop!")
        if exception_mode == "RAISE":
            loop.stop()

    return exception_handler


@click.command()
@click.option("--wait-for-services", is_flag=True)
def main(wait_for_services: bool = False):
    loop = asyncio.get_event_loop()
    env = get_env_variables()

    loop.set_exception_handler(make_exception_handler(env.exception_mode))

    if wait_for_services:
        pg_url = os.getenv("PG_URL")
        if pg_url:
            wait_for_pg(pg_url)
        # loop.run_until_complete(wait_for_rabbitmq(loop))

    config = Config(env=env)

    loop.run_until_complete(config.domain_event_bus.start_on_loop(loop))
    app, reset_coverops_repo = make_app(config)
    loop.run_until_complete(config.bus_subscribes_to_domain_events())
    loop.create_task(reset_coverops_repo.execute())
    runner = web.AppRunner(app)
    loop.run_until_complete(runner.setup())
    site = web.TCPSite(runner)
    loop.run_until_complete(site.start())
    loop.run_forever()


if __name__ == "__main__":
    # execute only if run as a script
    main()
