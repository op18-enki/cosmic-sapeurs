from typing import List, Union

import aiohttp
from aiohttp import web
from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.entrypoints.backend_to_frontend import backend_event_to_frontend_event

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.helpers.logger import logger
from shared.sapeurs_events import AvailabilityChangedEvent


class SapeurWebSocketResponse(web.WebSocketResponse):
    def __init__(self) -> None:
        super().__init__()

    async def send_to_front(
        self,
        event: Union[
            AvailabilityChangedEvent,
            AvailabilityChangedEventEntity,
        ],
    ):
        if self._writer is None:
            logger.warn("Web Socket is closed... Cannot send to frontend ! ")
            return
        message = backend_event_to_frontend_event(event)
        message["topic"] = event.topic  # type: ignore
        await self.send_json(message)

    async def run(self):
        async for msg in self:
            logger.info(f"\n Incomming message : {msg.data}")
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == "close":
                    logger.info("Closing socket ...")
                    await self.close()
            elif msg.type == aiohttp.WSMsgType.ERROR:
                logger.info("ws connection closed with exception %s" % self.exception())
        logger.info("Websocket connection ran")


async def send_events_to_front(
    ws: SapeurWebSocketResponse,
    domain_event_bus: DomainEventBus,
    topics: List[CoverOpsDomainTopic],
) -> SapeurWebSocketResponse:
    async def send_to_front_if_socket_open(e):
        async def unsubscribe_from_topics():
            for topic in topics:
                await domain_event_bus.unsubscribe(topic, send_to_front_if_socket_open)
            return

        try:
            if ws.closed:
                await unsubscribe_from_topics()
                return
            await ws.send_to_front(e)
        except Exception as error:
            logger.warn("Send to front error : ", error)
            await unsubscribe_from_topics()

    for topic in topics:
        await domain_event_bus.subscribe(topic, send_to_front_if_socket_open)
    return ws
