import os
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Dict, Optional, Tuple, cast

from cover_ops.adapters.csv_events_repositories import (
    CsvAvailabilityEventsRepository,
    CsvOmnibusEventsRepository,
    CsvOngoingOpEventsRepository,
    CsvOperationEventsRepository,
    CsvVehicleEventsRepository,
)
from cover_ops.adapters.http_converters_gateway import HttpConvertersGateway
from cover_ops.adapters.rabbitmq_domain_event_bus import RabbitMqDomainEventBus
from cover_ops.adapters.redis.redis_api_state_cache import RedisApiStateCache
from cover_ops.adapters.redis.redis_availability_events_repository import (
    RedisAvailabilityEventsRepository,
)
from cover_ops.adapters.redis.redis_front_lines_history_cache import (
    RedisFrontLinesHistoryCache,
)
from cover_ops.adapters.redis.redis_omnibus_events_repository import (
    RedisOmnibusEventsRepository,
)
from cover_ops.adapters.redis.redis_ongoing_op_events_repository import (
    RedisOngoingOpEventsRepository,
)
from cover_ops.adapters.redis.redis_operation_events_repository import (
    RedisOperationEventsRepository,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance
from cover_ops.adapters.redis.redis_vehicle_events_repository import (
    RedisVehicleEventsRepository,
)
from cover_ops.domain.ports.api_state_cache import (
    AbstractApiStateCache,
    InMemoryApiStateCache,
)
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.converter_gateway import (
    AbstractConvertersGateway,
    InMemoryConvertersGateway,
)
from cover_ops.domain.ports.domain_event_bus import (
    DomainEventBus,
    InMemoryDomainEventBus,
)
from cover_ops.domain.ports.front_lines_history_cache import (
    AbstractFrontLinesHistoryCache,
    InMemoryFrontLinesHistoryCache,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.get_cover import GetCover
from cover_ops.domain.use_cases.get_ops import GetOps
from cover_ops.domain.use_cases.notify_when_vehicle_availability_changed_affects_operation import (
    NotifyWhenVehicleAvailabilityChangedAffectsOperation,
)
from cover_ops.domain.use_cases.publish_converters_events_to_domain_event_bus import (
    PublishConvertersEventsToDomainEventBus,
)
from cover_ops.domain.use_cases.set_cover_ops_repositories_from_snapshot import (
    SetCoverOpsRepositoriesFromSnapshot,
)
from cover_ops.domain.use_cases.transfert_events_batch_to_repositories import (
    TransfertEventsBatchToRepositories,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.domain.use_cases.update_ops import UpdateOps
from cover_ops.entrypoints.environment_variables import (
    CacheOption,
    DomainEventBusOption,
    EnvironmentVariables,
    EventsRepositories,
)
from cover_ops.entrypoints.subscribe_to_domain_events import subscribe_to_domain_events
from dotenv import load_dotenv
from redis import Redis

from shared.helpers.clock import AbstractClock, RealClock
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid, RealUuid

load_dotenv()


clock = RealClock()
server_started_at = clock.get_now().replace(
    ":", " "
)  # this line replaces ":" by " " for Windows compatibility


def get_csv_repositories(
    csv_base_path: Path,
) -> Tuple[
    AbstractVehicleEventsRepository,
    AbstractOperationEventsRepository,
    AbstractAvailabilityEventsRepository,
    AbstractOngoingOpEventsRepository,
    AbstractOmnibusEventsRepository,
]:
    (
        vehicle_csv_path,
        operation_csv_path,
        availability_csv_path,
        ongoing_op_csv_path,
        omnibus_csv_path,
    ) = infer_repository_and_cache_paths(csv_base_path, server_started_at)

    return (
        CsvVehicleEventsRepository(vehicle_csv_path, purge=True),
        CsvOperationEventsRepository(operation_csv_path, purge=True),
        CsvAvailabilityEventsRepository(availability_csv_path, purge=True),
        CsvOngoingOpEventsRepository(ongoing_op_csv_path, purge=True),
        CsvOmnibusEventsRepository(omnibus_csv_path),
    )


def get_redis_repositories(
    redis_instance: Redis,
) -> Tuple[
    AbstractVehicleEventsRepository,
    AbstractOperationEventsRepository,
    AbstractAvailabilityEventsRepository,
    AbstractOngoingOpEventsRepository,
    AbstractOmnibusEventsRepository,
]:
    return (
        RedisVehicleEventsRepository(redis_instance=redis_instance, purge=True),
        RedisOperationEventsRepository(redis_instance=redis_instance, purge=True),
        RedisAvailabilityEventsRepository(redis_instance=redis_instance, purge=True),
        RedisOngoingOpEventsRepository(redis_instance=redis_instance, purge=True),
        RedisOmnibusEventsRepository(redis_instance=redis_instance),
    )


def get_front_lines_history_redis_cache(
    redis_instance: Redis,
) -> AbstractFrontLinesHistoryCache:
    return RedisFrontLinesHistoryCache(redis_instance)


def get_api_redis_cache(
    redis_instance: Redis,
) -> AbstractApiStateCache:
    return RedisApiStateCache(redis_instance)


env_var_to_domain_event_bus: Dict[
    DomainEventBusOption, Callable[[AbstractClock], DomainEventBus]
] = {
    "IN_MEMORY": lambda clock: InMemoryDomainEventBus(clock=clock),
    "RABBITMQ": lambda clock: RabbitMqDomainEventBus(clock),
}


repo_and_cache_file_names = [
    "vehicle_events_repo.csv",
    "operation_events_repo.csv",
    "availability_events_repo.csv",
    "ongoing_op_events_repo.csv",
    "omnibus_events_repo.csv",
]


def infer_repository_latest_date(
    csv_base_path: Path,
) -> Optional[str]:
    p = csv_base_path.glob("**/*.csv")
    file_names = [x.name for x in p if x.is_file()]
    if not file_names:
        return None
    latest_server_started_at = max(
        [file_name.split("__")[0] for file_name in file_names]
    )

    repo_with_latest_date = [
        f"{latest_server_started_at}__{repo_name}"
        for repo_name in repo_and_cache_file_names
    ]

    if set(repo_with_latest_date).issubset(set(file_names)):
        return latest_server_started_at
    else:
        return


def infer_repository_and_cache_paths(
    csv_base_path: Path, current_server_started_at: str
):
    prefix_date = infer_repository_latest_date(csv_base_path)
    if prefix_date is None:
        prefix_date = current_server_started_at
        logger.info(
            f"No previous repositories to use. Create one with prefix {prefix_date}"
        )
    return tuple(
        [
            csv_base_path / f"{prefix_date}__{file_name}"
            for file_name in repo_and_cache_file_names
        ]
    )


@dataclass
class UseCases:
    update_cover: UpdateCover
    update_ops: UpdateOps
    update_omnibus: UpdateOmnibus
    notify_when_vehicle_availability_changed_affects_operation: NotifyWhenVehicleAvailabilityChangedAffectsOperation
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories
    add_events_to_repositories: AddEventsToRepositories
    get_cover: GetCover
    get_ops: GetOps
    publish_converters_events_to_domain_event_bus: PublishConvertersEventsToDomainEventBus
    set_coverops_repositories_from_snapshot: SetCoverOpsRepositoriesFromSnapshot


class Config:
    vehicle_events_repo: AbstractVehicleEventsRepository
    operation_events_repo: AbstractOperationEventsRepository
    ongoing_op_events_repo: AbstractOngoingOpEventsRepository
    availability_events_repo: AbstractAvailabilityEventsRepository
    omnibus_events_repo: AbstractOmnibusEventsRepository
    api_state_cache: AbstractApiStateCache
    front_lines_history: AbstractFrontLinesHistoryCache
    uuid: AbstractUuid
    domain_event_bus: DomainEventBus
    use_cases: UseCases

    def __init__(
        self,
        env: EnvironmentVariables,
        csv_base_path: Path = Path("data"),
    ) -> None:
        self.ENV = env
        logger.info(self.ENV)

        self.redis_instance = self._make_redis_instance_if_necessary(
            self.ENV.events_repositories, self.ENV.cache
        )
        self.clock = clock
        self.domain_event_bus = env_var_to_domain_event_bus[env.domain_event_bus](clock)

        self.csv_base_path = csv_base_path
        if not os.path.exists(csv_base_path):
            os.mkdir(csv_base_path)

        pg_url = self.ENV.pg_url or "No URL PROVIDED"
        (
            self.vehicle_events_repo,
            self.operation_events_repo,
            self.availability_events_repo,
            self.ongoing_op_events_repo,
            self.omnibus_events_repo,
        ) = self.get_processed_events_repo()
        self.uuid = RealUuid()

        self.api_state_cache = self.get_api_state_cache()
        self.front_lines_history = self.get_front_lines_history_cache()
        self._converters_gateway = self.get_converters_gateway()

        self._prepare_json_path = (
            f"data/{server_started_at}_latest_vehicle_changed_status_events.json"
        )
        self._converters_gateway = self.get_converters_gateway()

        self.use_cases = UseCases(
            update_cover=UpdateCover(
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                vehicle_repo=self.vehicle_events_repo,
                availability_repo=self.availability_events_repo,
            ),
            update_ops=UpdateOps(
                ongoing_op_events_repo=self.ongoing_op_events_repo,
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                operation_events_repo=self.operation_events_repo,
            ),
            update_omnibus=UpdateOmnibus(
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                omnibus_repo=self.omnibus_events_repo,
                vehicle_repo=self.vehicle_events_repo,
            ),
            get_cover=GetCover(
                availability_events_repo=self.availability_events_repo,
            ),
            get_ops=GetOps(ongoing_op_events_repo=self.ongoing_op_events_repo),
            publish_converters_events_to_domain_event_bus=PublishConvertersEventsToDomainEventBus(
                domain_event_bus=self.domain_event_bus,
            ),
            notify_when_vehicle_availability_changed_affects_operation=NotifyWhenVehicleAvailabilityChangedAffectsOperation(
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                availability_repo=self.availability_events_repo,
                operation_repo=self.operation_events_repo,
            ),
            transfert_events_batch_to_repositories=TransfertEventsBatchToRepositories(
                vehicle_events_repo=self.vehicle_events_repo,
                operation_events_repo=self.operation_events_repo,
            ),
            add_events_to_repositories=AddEventsToRepositories(
                vehicle_events_repo=self.vehicle_events_repo,
                operation_events_repo=self.operation_events_repo,
                availability_events_repo=self.availability_events_repo,
                ongoing_op_events_repo=self.ongoing_op_events_repo,
            ),
            set_coverops_repositories_from_snapshot=SetCoverOpsRepositoriesFromSnapshot(
                uuid=self.uuid,
                domain_event_bus=self.domain_event_bus,
                omnibus_events_repo=self.omnibus_events_repo,
                vehicle_events_repo=self.vehicle_events_repo,
                availability_events_repo=self.availability_events_repo,
                operation_events_repo=self.operation_events_repo,
                ongoingops_events_repo=self.ongoing_op_events_repo,
                converters_gateway=self._converters_gateway,
                api_state_cache=self.api_state_cache,
            ),
        )

    async def bus_subscribes_to_domain_events(self):
        await subscribe_to_domain_events(
            self.uuid,
            self.domain_event_bus,
            self.front_lines_history,
            self.use_cases.update_cover,
            self.use_cases.update_ops,
            self.use_cases.update_omnibus,
            self.use_cases.add_events_to_repositories,
            self.use_cases.notify_when_vehicle_availability_changed_affects_operation,
            self.use_cases.transfert_events_batch_to_repositories,
            self.use_cases.set_coverops_repositories_from_snapshot,
            self.use_cases.get_cover,
            self.use_cases.get_ops,
            self.omnibus_events_repo,
        )

    @staticmethod
    def _make_redis_instance_if_necessary(
        repo: EventsRepositories, cache: CacheOption
    ) -> Optional[Redis]:
        if repo == "REDIS" or cache == "REDIS":
            redis_instance = make_redis_instance()
            redis_instance.flushdb()
            return redis_instance

    def get_api_state_cache(
        self,
    ) -> AbstractApiStateCache:
        option = self.ENV.cache
        if option == "IN_MEMORY":
            return InMemoryApiStateCache()
        if option == "REDIS":
            return get_api_redis_cache(cast(Redis, self.redis_instance))
        raise ValueError

    def get_front_lines_history_cache(self) -> AbstractFrontLinesHistoryCache:
        option = self.ENV.cache
        if option == "IN_MEMORY":
            return InMemoryFrontLinesHistoryCache()
        if option == "REDIS":
            return get_front_lines_history_redis_cache(cast(Redis, self.redis_instance))
        raise ValueError

    def get_processed_events_repo(
        self,
    ) -> Tuple[
        AbstractVehicleEventsRepository,
        AbstractOperationEventsRepository,
        AbstractAvailabilityEventsRepository,
        AbstractOngoingOpEventsRepository,
        AbstractOmnibusEventsRepository,
    ]:
        option = self.ENV.events_repositories
        if option == "CSV":
            return get_csv_repositories(self.csv_base_path)
        # elif option == "PG":
        #     return get_pg_repositories(self.ENV.pg_url)  # type: ignore
        elif option == "REDIS":
            return get_redis_repositories(cast(Redis, self.redis_instance))
        else:
            raise ValueError()

    def get_converters_gateway(
        self,
    ) -> AbstractConvertersGateway:
        option = self.ENV.converters_gateway
        if option == "IN_MEMORY":
            return InMemoryConvertersGateway()
        if option == "HTTP":
            return HttpConvertersGateway(self.ENV.converters_url)
        raise ValueError
