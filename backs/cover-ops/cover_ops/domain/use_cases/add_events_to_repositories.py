from csv import writer

from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.helpers.dataclass import dumps_dataclass
from shared.sapeurs_events import (
    AvailabilityChangedEvent,
    Event,
    OngoingOpChangedEvent,
    OperationEvent,
    VehicleEvent,
)


class AddEventsToRepositories:
    def __init__(
        self,
        operation_events_repo: AbstractOperationEventsRepository,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        availability_events_repo: AbstractAvailabilityEventsRepository,
        ongoing_op_events_repo: AbstractOngoingOpEventsRepository,
    ):
        self.operation_events_repo = operation_events_repo
        self.vehicle_events_repo = vehicle_events_repo
        self.availability_events_repo = availability_events_repo
        self.ongoing_op_events_repo = ongoing_op_events_repo

    async def execute(
        self,
        event: Event,
    ):
        if isinstance(event, VehicleEvent):
            self.vehicle_events_repo.add([event.data])
        elif isinstance(event, OperationEvent):
            self.operation_events_repo.add([event.data])
        elif isinstance(event, AvailabilityChangedEvent):
            self.availability_events_repo.add([event.data])
        elif isinstance(event, OngoingOpChangedEvent):
            self.ongoing_op_events_repo.add([event.data])

        # debug : save to csv
        with open(f"./data/{event.topic}.csv", "a") as file:
            writer_to_file = writer(file)
            writer_to_file.writerow([dumps_dataclass(event.data)])
            file.close()
