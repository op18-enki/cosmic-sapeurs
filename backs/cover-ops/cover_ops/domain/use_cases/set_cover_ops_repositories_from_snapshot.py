from collections import Counter, defaultdict
from itertools import groupby
from typing import Dict, List, Tuple

from cover_ops.domain.entities.event_entities import (
    OngoingOpChangedEventData,
    OperationEventData,
    VehicleEventData,
)
from cover_ops.domain.ports.api_state_cache import AbstractApiStateCache
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.converter_gateway import AbstractConvertersGateway
from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_omnibus import OmnibusDatas, infer_balance_key

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationStatus,
    RawOperationId,
    operation_has_all_vehicles_released,
    operation_has_first_affected_vehicle,
    operation_has_some_affected_vehicle_changed,
)
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    RawVehicleId,
    VehicleRole,
)
from shared.helpers.decorators import async_timeit, timeit
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import SetRepositoriesFromSnapshotExecutedEvent


class SetCoverOpsRepositoriesFromSnapshot:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
        omnibus_events_repo: AbstractOmnibusEventsRepository,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        availability_events_repo: AbstractAvailabilityEventsRepository,
        operation_events_repo: AbstractOperationEventsRepository,
        ongoingops_events_repo: AbstractOngoingOpEventsRepository,
        converters_gateway: AbstractConvertersGateway,
        api_state_cache: AbstractApiStateCache,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.uuid = uuid
        self.omnibus_events_repo = omnibus_events_repo
        self.vehicle_events_repo = vehicle_events_repo
        self.availability_events_repo = availability_events_repo
        self.operation_events_repo = operation_events_repo
        self.ongoingops_events_repo = ongoingops_events_repo
        self.converters_gateway = converters_gateway
        self.api_state_cache = api_state_cache

    @async_timeit("execute reset ")
    async def execute(self):
        snapshot = await self.converters_gateway.get_cover_ops_snapshot()
        self.reset_repositories()

        # Filter vehicle events
        bspp_snapshot_vehicles = [
            vehicle_data
            for vehicle_data in snapshot.vehicles
            if vehicle_data.home_area in bspp_area_options
        ]

        # Compute affected vehicles and infer statuses accordingly
        affected_vehicle_ids_per_operation_id: Dict[
            RawOperationId, List[RawVehicleId]
        ] = {
            operation_data.raw_operation_id: [
                vehicles_data.raw_vehicle_id
                for vehicles_data in bspp_snapshot_vehicles
                if vehicles_data.raw_operation_id == operation_data.raw_operation_id
                and vehicles_data.availability_kind == "on_operation"
            ]
            for operation_data in snapshot.operations
        }
        snapshot_status_per_operation_id: Dict[
            RawOperationId, List[OperationStatus]
        ] = {
            operation_data.raw_operation_id: operation_data.status
            for operation_data in snapshot.operations
        }

        infered_operation_status_per_operation_id: Dict[
            RawOperationId, OperationStatus
        ] = {
            raw_operation_id: self.infer_status_from_affected_vehicles_count(
                len(affected_vehicles),
                snapshot_status_per_operation_id[raw_operation_id],
            )
            for (
                raw_operation_id,
                affected_vehicles,
            ) in affected_vehicle_ids_per_operation_id.items()
        }

        # Set repositories
        self.set_vehicle_events(bspp_snapshot_vehicles)
        self.set_availability_events(bspp_snapshot_vehicles)
        self.set_omnibus_events(bspp_snapshot_vehicles)
        self.set_operation_events(
            snapshot.operations,
            infered_operation_status_per_operation_id,
        )
        self.set_ongoingops_events(
            snapshot.operations,
            infered_operation_status_per_operation_id,
            affected_vehicle_ids_per_operation_id,
        )

        operations_notified_from_vehicle_changes = set(
            [
                operation_id
                for operation_id, infered_status in infered_operation_status_per_operation_id.items()
                if infered_status != operation_has_all_vehicles_released
            ]
        )
        self.operation_events_repo.set_operations_notified_from_vehicle_changes(
            operations_notified_from_vehicle_changes
        )

        # Set cover-ops ready
        self.api_state_cache.set_is_ready(True)

        # Publish an event to say it's finished
        await self.domain_event_bus.publish(
            SetRepositoriesFromSnapshotExecutedEvent(self.uuid.make())
        )

    @timeit("reset all repositories")
    def reset_repositories(self):
        self.vehicle_events_repo.reset()
        self.availability_events_repo.reset()
        self.omnibus_events_repo.reset()
        self.operation_events_repo.reset()
        self.ongoingops_events_repo.reset()

    @timeit("vehicle_events")
    def set_vehicle_events(self, vehicle_event_datas: List[VehicleEventData]) -> None:
        for vehicle_event_data in vehicle_event_datas:
            self.vehicle_events_repo.add([vehicle_event_data])

    @timeit("availability_events")
    def set_availability_events(
        self, vehicle_event_datas: List[VehicleEventData]
    ) -> None:
        (
            bspp_availabilities_by_role,
            availabilities_by_role_by_area,
        ) = compute_bspp_and_area_availabilities_from_vehicles(vehicle_event_datas)

        availability_datas = []
        for vehicle_data in vehicle_event_datas:
            availablity_event_data = create_availability_changed_event_data_given_vehicle_data_and_availabilities(
                vehicle_data,
                bspp_availability=bspp_availabilities_by_role[vehicle_data.role],
                area_availability=availabilities_by_role_by_area[vehicle_data.role][
                    vehicle_data.home_area
                ],
            )
            availability_datas.append(availablity_event_data)

        self.availability_events_repo.add(availability_datas)

    @timeit("set_omnibus")
    def set_omnibus_events(self, vehicle_event_datas: List[VehicleEventData]):
        vehicle_data_by_id: Dict[RawVehicleId, VehicleEventData] = {
            vehicle_data.raw_vehicle_id: vehicle_data
            for vehicle_data in vehicle_event_datas
        }

        vsav_omni_role: VehicleRole = "vsav_omni"
        omnibus_details: List[OmnibusDetail] = [
            OmnibusDetail(
                vsav_id=vehicle_id,
                pse_id=vehicle_data.omnibus.partner_raw_vehicle_id,  # type: ignore (we know omnibus is not None since we just filtered  on this condition)
                balance_kind=infer_balance_key(
                    OmnibusDatas(
                        vehicle_data,
                        vehicle_data_by_id[
                            vehicle_data.omnibus.partner_raw_vehicle_id  # type: ignore (we know omnibus is not None since we just filtered  on this condition)
                        ],
                    )
                ),
                area=vehicle_data.home_area,
            )
            for vehicle_id, vehicle_data in vehicle_data_by_id.items()
            if vehicle_data.role == vsav_omni_role
            and vehicle_data.omnibus is not None
            and vehicle_data.omnibus.partner_raw_vehicle_id in vehicle_data_by_id
        ]

        for omnibus_detail in omnibus_details:
            self.omnibus_events_repo.add_omnibus(omnibus_detail)

    @timeit("ongoingops_events")
    def set_ongoingops_events(
        self,
        operation_datas: List[OperationEventData],
        infered_operation_status_per_operation_id: Dict[
            RawOperationId, OperationStatus
        ],
        affected_vehicle_ids_per_operation_id: Dict[RawOperationId, List[RawVehicleId]],
    ):

        ongoingops_datas = []
        for operation_data in operation_datas:
            raw_operation_id = operation_data.raw_operation_id
            cause = operation_data.cause
            address_area = operation_data.address_area
            if (
                operation_data.longitude is None
                or operation_data.latitude is None
                or cause is None
                or address_area is None
                or operation_data.departure_criteria is None
                or operation_data.raw_procedure is None
            ):
                logger.warn(
                    f"Opening infos should not be None in snapshot data, missing data for operation {operation_data.raw_operation_id}"
                )
                break

            affected_vehicles = affected_vehicle_ids_per_operation_id[raw_operation_id]
            ongoingops_datas.append(
                OngoingOpChangedEventData(
                    timestamp=operation_data.timestamp,
                    raw_operation_id=raw_operation_id,
                    status=infered_operation_status_per_operation_id[raw_operation_id],
                    affected_vehicles=affected_vehicles,
                    opening_infos=OperationOpeningInfos(
                        opening_timestamp=operation_data.timestamp,
                        departure_criteria=operation_data.departure_criteria,
                        cause=cause,
                        address=operation_data.address,
                        address_area=address_area,
                        longitude=operation_data.longitude,
                        latitude=operation_data.latitude,
                        raw_procedure=operation_data.raw_procedure,
                        raw_cause=operation_data.raw_cause,
                    ),
                )
            )

        self.ongoingops_events_repo.add(ongoingops_datas)

    @timeit("operation_events")
    def set_operation_events(
        self,
        operation_datas: List[OperationEventData],
        infered_operation_status_per_operation_id: Dict[
            RawOperationId, OperationStatus
        ],
    ):
        operation_datas_with_relevent_status = [
            OperationEventData(
                timestamp=operation_data.timestamp,
                raw_operation_id=operation_data.raw_operation_id,
                status=infered_operation_status_per_operation_id[
                    operation_data.raw_operation_id
                ],
                address=operation_data.address,
                address_area=operation_data.address_area,
                cause=operation_data.cause,
                raw_cause=operation_data.raw_cause,
                raw_procedure=operation_data.raw_procedure,
                departure_criteria=operation_data.departure_criteria,
                longitude=operation_data.longitude,
                latitude=operation_data.latitude,
                affected_vehicles=operation_data.affected_vehicles,
            )
            for operation_data in operation_datas
        ]
        self.operation_events_repo.add(operation_datas_with_relevent_status)  # type: ignore

    @staticmethod
    def infer_status_from_affected_vehicles_count(
        count: int, status: OperationStatus
    ) -> OperationStatus:
        if count == 0:
            return status
        elif count == 1:
            return operation_has_first_affected_vehicle
        return operation_has_some_affected_vehicle_changed


@timeit("compute_bspp_and_area_availabilities_from_vehicles")
def compute_bspp_and_area_availabilities_from_vehicles(
    vehicle_datas: List[VehicleEventData],
) -> Tuple[
    Dict[VehicleRole, Availability], Dict[VehicleRole, Dict[Area, Availability]]
]:

    # Compute availabilities
    get_role = lambda vehicle_data: (vehicle_data.role)
    get_area = lambda vehicle_data: (vehicle_data.home_area)

    bspp_availabilities: Dict[VehicleRole, Availability] = {}
    area_availabilities: Dict[VehicleRole, Dict[Area, Availability]] = defaultdict(
        lambda: dict()
    )

    # Set
    for role, datas_for_role in groupby(
        sorted(vehicle_datas, key=get_role), key=get_role
    ):
        list_datas_for_role = list(datas_for_role)
        bspp_availabilities[role] = compute_availability_from_datas(list_datas_for_role)

        for area, datas_for_area_role in groupby(
            sorted(list_datas_for_role, key=get_area), key=get_area
        ):
            list_datas_for_area_for_role = list(datas_for_area_role)
            area_availabilities[role][area] = compute_availability_from_datas(
                list_datas_for_area_for_role
            )

    return bspp_availabilities, area_availabilities


def compute_availability_from_datas(
    vehicle_datas: List[VehicleEventData],
) -> Availability:
    availability_count = Counter(
        [vehicle_data.availability_kind for vehicle_data in vehicle_datas]
    )
    return Availability(
        available=availability_count["available"],
        on_operation=availability_count["on_operation"],
        unavailable=availability_count["unavailable"],
        recoverable=availability_count["recoverable"],
        unavailable_omnibus=availability_count["unavailable_omnibus"],
    )


def create_availability_changed_event_data_given_vehicle_data_and_availabilities(
    vehicle_event_data: VehicleEventData,
    bspp_availability: Availability,
    area_availability: Availability,
) -> AvailabilityChangedEventData:
    return AvailabilityChangedEventData(
        timestamp=vehicle_event_data.timestamp,
        area_availability=area_availability,
        bspp_availability=bspp_availability,
        home_area=vehicle_event_data.home_area,
        latest_event_data=vehicle_event_data,
        raw_vehicle_id=vehicle_event_data.raw_vehicle_id,
        role=vehicle_event_data.role,
        previous_raw_operation_id=vehicle_event_data.raw_operation_id,
        availability_changed=True,
    )
