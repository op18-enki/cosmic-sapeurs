from typing import List

from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationCause


class GetOps:
    def __init__(
        self, ongoing_op_events_repo: AbstractOngoingOpEventsRepository
    ) -> None:
        self.ongoing_op_events_repo = ongoing_op_events_repo

    def execute(self) -> List[OngoingOpChangedEventData]:
        ongoing_ops_events_data_from_date = [
            event_data
            for event_data in self.ongoing_op_events_repo.get_all_ongoing_ops()
        ]
        latest_ops_event_datas_per_area = (
            self.ongoing_op_events_repo.get_latest_event_data_for_all_area_for_cause_fire_and_victim()
        )

        return latest_ops_event_datas_per_area + ongoing_ops_events_data_from_date
