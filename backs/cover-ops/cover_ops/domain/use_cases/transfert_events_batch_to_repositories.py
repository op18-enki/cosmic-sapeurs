from typing import List

from cover_ops.domain.entities.event_entities import (
    OperationEventEntity,
    VehicleEventData,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.helpers.logger import logger
from shared.sapeurs_events import (
    Event,
    operation_changed_status,
    vehicle_changed_status,
)


class TransfertEventsBatchToRepositories:
    def __init__(
        self,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        operation_events_repo: AbstractOperationEventsRepository,
    ) -> None:
        self.vehicle_events_repo = vehicle_events_repo
        self.operation_events_repo = operation_events_repo

    def execute(self, events: List[Event]):
        logger.info(f"Transfering {len(events)} events to repositories")
        for event in events:
            topic = event.topic
            source = event.source
            if source is None or topic not in [
                vehicle_changed_status,
                operation_changed_status,
            ]:
                logger.warn(
                    f"\n TransfertEventsBatchToRepositories expects vehicle or operation event with specified source. Event #{event.uuid} had topic {topic}, and source {source}. Skipping !"
                )
                return
            if topic == vehicle_changed_status:
                self.vehicle_events_repo.add([event.data])
            elif topic == operation_changed_status:
                self.operation_events_repo.add([event.data])

        logger.info("Transfert events batch to repositories finished.")
