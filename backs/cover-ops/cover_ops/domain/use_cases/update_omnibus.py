from dataclasses import dataclass
from typing import Optional

from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
    OmnibusBalanceKind,
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import (
    AvailabilityKind,
    VehicleEventData,
    VehicleRole,
    vehicle_role_options,
)
from shared.helpers.date import DateStr
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import OmnibusBalanceChangedEvent, VehicleEvent


def infer_balance_key_for_mixed_availability(
    vsav_availability_kind: AvailabilityKind,
    pse_role: VehicleRole,
    pse_availability_kind: AvailabilityKind,
) -> OmnibusBalanceKind:
    # Here : pse and vsav have different availability kind
    if pse_availability_kind == "unavailable":
        return "dangling"

    if (
        pse_role == "pse_omni_san" and pse_availability_kind == "on_operation"
    ):  # vsav_omni is either available or recoverable
        return "pse_on_inter_san"

    if (
        pse_role == "pse_omni_san" and vsav_availability_kind == "on_operation"
    ):  # pse_omni is either available or recoverable
        return "vsav_on_inter_san"
    if (
        pse_role == "pse_omni_pump"
        and pse_availability_kind == "on_operation"
        and vsav_availability_kind == "unavailable_omnibus"
    ):
        return "pse_on_inter_fire"

    if (
        pse_role == "pse_omni_pump"
        and pse_availability_kind == "on_operation"
        and vsav_availability_kind != "available"
    ):
        return "only_pse_on_inter_fire"

    if (
        pse_role == "pse_omni_pump"
        and pse_availability_kind == "available"
        and vsav_availability_kind
        in ["unavailable", "recoverable", "unavailable_omnibus"]
    ):
        return "only_pse_available"

    logger.warn(
        f"\nDangling state: vsav is {vsav_availability_kind} / pse as {pse_role} is {pse_availability_kind} "
    )
    return "dangling"


@dataclass
class OmnibusDatas:
    vsav_data: VehicleEventData
    pse_data: VehicleEventData


def infer_omnibus_datas(
    data_0: VehicleEventData, data_1: VehicleEventData
) -> OmnibusDatas:
    if data_0.role == "vsav_omni":
        return OmnibusDatas(data_0, data_1)
    return OmnibusDatas(data_1, data_0)


def infer_balance_key(omnibus_datas: OmnibusDatas) -> Optional[OmnibusBalanceKind]:
    vsav_availability_kind = omnibus_datas.vsav_data.availability_kind
    pse_availability_kind = omnibus_datas.pse_data.availability_kind

    if (
        vsav_availability_kind == "on_operation"
        and pse_availability_kind == "on_operation"
    ):
        return "both_on_inter_san"

    if vsav_availability_kind == "available" and pse_availability_kind == "available":
        return "both_available"

    return infer_balance_key_for_mixed_availability(
        vsav_availability_kind=vsav_availability_kind,
        pse_availability_kind=pse_availability_kind,
        pse_role=omnibus_datas.pse_data.role,
    )


def infer_dangling_omnibus_infos_from_orphan_vehicle_event_data(
    event_data: VehicleEventData,
):
    if event_data.role == "vsav_omni":
        return OmnibusDetail(
            vsav_id=event_data.raw_vehicle_id,
            pse_id=None,
            area=event_data.home_area,
            balance_kind="dangling",
        )
    return OmnibusDetail(
        vsav_id=None,
        pse_id=event_data.raw_vehicle_id,
        area=event_data.home_area,
        balance_kind="dangling",
    )


class UpdateOmnibus:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
        vehicle_repo: AbstractVehicleEventsRepository,
        omnibus_repo: AbstractOmnibusEventsRepository,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.uuid = uuid
        self.omnibus_repo = omnibus_repo
        self.vehicle_repo = vehicle_repo

    async def execute(self, event: VehicleEvent) -> None:

        if event.data.role not in vehicle_role_options:
            return

        omnibus_infos = event.data.omnibus
        previous_omnibus_detail = self.omnibus_repo.get_omnibus_detail(
            event.data.raw_vehicle_id
        )
        if not omnibus_infos:
            # This vehicle is not involved in an omnibus.
            if not previous_omnibus_detail:
                return
            # This vehicle was involved in an omnibus, but is not anymore.
            detail_of_just_dissociated_omnibus = previous_omnibus_detail
            self.omnibus_repo.remove_omnibus(detail_of_just_dissociated_omnibus)
            dangling_vsav_id = (
                None
                if detail_of_just_dissociated_omnibus.vsav_id
                == event.data.raw_vehicle_id
                else detail_of_just_dissociated_omnibus.vsav_id
            )
            dangling_pse_id = (
                None
                if detail_of_just_dissociated_omnibus.pse_id
                == event.data.raw_vehicle_id
                else detail_of_just_dissociated_omnibus.pse_id
            )
            if any([dangling_vsav_id, dangling_pse_id]):
                self.omnibus_repo.add_omnibus(
                    OmnibusDetail(
                        vsav_id=dangling_vsav_id,
                        pse_id=dangling_pse_id,
                        balance_kind="dangling",
                        area=previous_omnibus_detail.area,
                    )
                )
            await self.publish(event.data.timestamp)
            return

        # This vehicle is involved in an omnibus.
        partner_raw_vehicle_id = omnibus_infos.partner_raw_vehicle_id

        previous_partner_omnibus_detail = self.omnibus_repo.get_omnibus_detail(
            partner_raw_vehicle_id
        )

        previous_partner_event_data = self.vehicle_repo.get_latest_data_for_vehicle_id(
            partner_raw_vehicle_id
        )

        if not previous_partner_event_data:
            logger.warn(
                f"[Update Omnibus] Could not retrieve event data from vehicle {omnibus_infos.partner_raw_vehicle_id}"
            )
            orphan_dangling_detail = (
                infer_dangling_omnibus_infos_from_orphan_vehicle_event_data(event.data)
            )
            self.omnibus_repo.add_omnibus(orphan_dangling_detail)

            await self.publish(event.data.timestamp)
            return

        current_event_datas = infer_omnibus_datas(
            event.data, previous_partner_event_data
        )
        current_balance_key = infer_balance_key(current_event_datas)

        if current_balance_key:
            # This vehicle is involved in an omnibus
            if previous_omnibus_detail:
                # This vehicle was involved in an omnibus before : remove
                self.omnibus_repo.remove_omnibus(previous_omnibus_detail)
            if (
                previous_partner_omnibus_detail
                and previous_partner_omnibus_detail != previous_omnibus_detail
            ):
                # The partner of this vehicle was involved in an omnibus before : remove
                self.omnibus_repo.remove_omnibus(previous_partner_omnibus_detail)

            self.omnibus_repo.add_omnibus(
                OmnibusDetail(
                    vsav_id=current_event_datas.vsav_data.raw_vehicle_id,
                    pse_id=current_event_datas.pse_data.raw_vehicle_id,
                    balance_kind=current_balance_key,
                    area=current_event_datas.vsav_data.home_area,
                )
            )
            await self.publish(event.data.timestamp)

    async def publish(self, timestamp: DateStr):
        data = OmnibusBalanceChangedEventData(
            timestamp=timestamp,
            details=list(self.omnibus_repo.get_omnibus_details()),
        )
        uuid = self.uuid.make()
        await self.domain_event_bus.publish(
            OmnibusBalanceChangedEvent(
                uuid=uuid, data=data, source="sapeurs_update_omnibus"
            )
        )
