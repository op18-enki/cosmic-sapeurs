from typing import Dict, List, Optional, Tuple

from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
    AvailabilityStoredInfos,
    VehicleEventData,
)

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    AvailabilityKind,
    VehicleRole,
    vehicle_role_options,
)
from shared.helpers.date import DateStr, add_milliseconds
from shared.helpers.decorators import timeit
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import AvailabilityChangedEvent, VehicleEvent


def availability_unchanged_for_role_for_area(
    previous_event_data: VehicleEventData, current_event_data: VehicleEventData
) -> bool:
    return (
        previous_event_data.availability_kind == current_event_data.availability_kind
        and previous_event_data.role == current_event_data.role
        and previous_event_data.home_area == current_event_data.home_area
    )


class UnknownRoleError(Exception):
    pass


class UpdateCover:
    def __init__(
        self,
        vehicle_repo: AbstractVehicleEventsRepository,
        availability_repo: AbstractAvailabilityEventsRepository,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
    ) -> None:
        self.availability_repo = availability_repo
        self.vehicle_repo = vehicle_repo
        self.domain_event_bus = domain_event_bus
        self.uuid = uuid

    async def execute(self, event: VehicleEvent) -> None:
        """Store event in repository

        Args:
            event (VehicleEvent): event published on vehicle change
        """

        cover_updated_event_datas = self.process_vehicle_event(event)

        if cover_updated_event_datas is not None:
            for cover_updated_event_data in cover_updated_event_datas:
                uuid = self.uuid.make()
                availability_event = AvailabilityChangedEvent(
                    uuid=uuid,
                    data=cover_updated_event_data,
                    source="sapeurs_update_cover",
                )
                self.availability_repo.add([availability_event.data])
                await self.domain_event_bus.publish(availability_event)

    @timeit("process_vehicle_event", min_duration=50)
    def process_vehicle_event(
        self, event: VehicleEvent
    ) -> Optional[List[AvailabilityChangedEventData]]:
        if self._unknown_home_or_cause(event.data):
            return

        previous_vehicle_event_data = self.vehicle_repo.get_latest_data_for_vehicle_id(
            event.data.raw_vehicle_id
        )
        (
            cover_updated_events_data,
            compensation_needed,
        ) = self._get_first_availability_and_tells_if_compensation_is_needed(
            previous_vehicle_event_data,
            event.data,
        )

        # ----------- Role or area changed ---
        # decrement from role / area
        if compensation_needed:
            # If previous_vehicle_event_data were None, then previous_vehicle_event_data = current_vehicle_event_data
            # But compensation_needed = home_area changed OR role changed
            # Hence: if previous_vehicle_event_data is None, then compensation_needed is False
            assert previous_vehicle_event_data is not None

            # get previous_bspp_stored_infos_for_previous_area_for_previous_role
            previous_role = previous_vehicle_event_data.role
            # previous_home_area = previous_vehicle_event_data.home_area
            if previous_role == event.data.role:
                previous_bspp_stored_infos_for_previous_area_for_previous_role = (
                    AvailabilityStoredInfos(
                        availability=cover_updated_events_data.bspp_availability,
                        latest_event_data=cover_updated_events_data.latest_event_data,
                    )
                )
            else:
                previous_bspp_stored_infos_for_previous_area_for_previous_role = (
                    self.availability_repo.get_bspp_availability_stored_infos_for_role(
                        previous_role
                    )
                )

            compensation_availability_changed_data = (
                self._on_area_or_role_changed_decrement_availability_of_previous_area(
                    event,
                    previous_vehicle_event_data,
                    event.data,
                    previous_bspp_stored_infos_for_previous_area_for_previous_role,
                )
            )
            if compensation_availability_changed_data:

                return [
                    cover_updated_events_data,
                    compensation_availability_changed_data,
                ]
        # ------------------------------------
        return [cover_updated_events_data]

    def _on_area_or_role_changed_decrement_availability_of_previous_area(
        self,
        event: VehicleEvent,
        previous_vehicle_event_data: VehicleEventData,
        current_vehicle_stored_infos: VehicleEventData,
        previous_bspp_stored_infos_for_previous_area_for_previous_role: AvailabilityStoredInfos,
        # previous_availability_for_previous_area_for_previous_role: Availability,
    ) -> Optional[AvailabilityChangedEventData]:
        previous_home_area = previous_vehicle_event_data.home_area
        current_home_area = current_vehicle_stored_infos.home_area
        previous_role = previous_vehicle_event_data.role
        current_role = current_vehicle_stored_infos.role

        if (
            previous_role not in vehicle_role_options
            or previous_home_area not in bspp_area_options
        ):
            return

        logger.info(
            f"\n Will compensate for role from {previous_role} to {current_role}; area from {previous_home_area} to {current_home_area} ",
        )

        delta_previous_availability_decreased: Dict[AvailabilityKind, int] = {
            previous_vehicle_event_data.availability_kind: -1
        }

        compensated_bspp_availability_for_role = self.apply_delta(
            previous_bspp_stored_infos_for_previous_area_for_previous_role.availability,
            delta_previous_availability_decreased,
        )

        previous_availability_for_previous_area_for_previous_role = (
            self.availability_repo.get_availability_stored_infos_for_area_for_role(
                previous_home_area, previous_role
            ).availability
        )

        compensated_area_availability_for_role = self.apply_delta(
            previous_availability_for_previous_area_for_previous_role,
            delta_previous_availability_decreased,
        )

        compensation_availability_changed_event_data = (
            self._make_availability_changed_data(
                timestamp=add_milliseconds(event.data.timestamp, 1),
                role=previous_role,
                home_area=previous_home_area,
                area_availability=compensated_area_availability_for_role,
                bspp_availability=compensated_bspp_availability_for_role,
                raw_vehicle_id=event.data.raw_vehicle_id,
                latest_event_data=previous_vehicle_event_data,
                previous_raw_operation_id=None,
                availability_changed=True,
            )
        )

        return compensation_availability_changed_event_data

    def _get_availabilities_for_role(
        self,
        compensation_needed: bool,
        current_area: Area,
        current_role: VehicleRole,
        previous_available_kind: AvailabilityKind,
        current_availability_kind: AvailabilityKind,
    ) -> Tuple[Availability, Availability]:
        current_stored_infos_for_area_for_role = (
            self.availability_repo.get_availability_stored_infos_for_area_for_role(
                current_area, current_role
            )
        )
        current_stored_infos_for_bspp_for_role = (
            self.availability_repo.get_bspp_availability_stored_infos_for_role(
                current_role
            )
        )

        # do not decrease for this role, the compensation (for previous role !) will be handled by `_on_area_or_role_changed_decrement_availability_of_previous_area`
        availability_delta_for_role: Dict[AvailabilityKind, int] = {
            previous_available_kind: -1 if not compensation_needed else 0,
            current_availability_kind: +1,
        }

        vehicle_availability_by_area = self.apply_delta(
            current_stored_infos_for_area_for_role.availability,
            availability_delta_for_role,
        )

        bspp_vehicle_availability = self.apply_delta(
            current_stored_infos_for_bspp_for_role.availability,
            availability_delta_for_role,
        )
        return vehicle_availability_by_area, bspp_vehicle_availability

    @staticmethod
    def apply_delta(
        availability: Availability,
        delta: Dict[AvailabilityKind, int],
    ) -> Availability:
        return Availability(
            available=availability.available + delta.get("available", 0),
            unavailable=availability.unavailable + delta.get("unavailable", 0),
            recoverable=availability.recoverable + delta.get("recoverable", 0),
            on_operation=availability.on_operation + delta.get("on_operation", 0),
            unavailable_omnibus=availability.unavailable_omnibus
            + delta.get("unavailable_omnibus", 0),
        )

    def _make_availability_changed_data(
        self,
        latest_event_data: VehicleEventData,
        raw_vehicle_id: RawVehicleId,
        timestamp: DateStr,
        role: VehicleRole,
        home_area: Area,
        area_availability: Availability,
        bspp_availability: Availability,
        previous_raw_operation_id: Optional[RawOperationId],
        availability_changed: bool,
    ) -> AvailabilityChangedEventData:
        return AvailabilityChangedEventData(
            timestamp=timestamp,
            role=role,
            home_area=home_area,
            area_availability=area_availability,
            bspp_availability=bspp_availability,
            raw_vehicle_id=raw_vehicle_id,
            latest_event_data=latest_event_data,
            previous_raw_operation_id=previous_raw_operation_id,
            availability_changed=availability_changed,
        )

    def _get_first_availability_and_tells_if_compensation_is_needed(
        self,
        previous_vehicle_event_data: Optional[VehicleEventData],
        current_vehicle_event_data: VehicleEventData,
    ) -> Tuple[AvailabilityChangedEventData, bool,]:
        raw_vehicle_id = current_vehicle_event_data.raw_vehicle_id

        current_home_area = current_vehicle_event_data.home_area
        current_role = current_vehicle_event_data.role

        if previous_vehicle_event_data and availability_unchanged_for_role_for_area(
            previous_vehicle_event_data, current_vehicle_event_data
        ):
            new_area_availability_for_role = (
                self.availability_repo.get_availability_stored_infos_for_area_for_role(
                    area=current_home_area, role=current_role
                ).availability
            )
            new_bspp_availability_for_role = (
                self.availability_repo.get_bspp_availability_stored_infos_for_role(
                    role=current_role
                ).availability
            )

            compensation_needed = False
            availability_changed = False

        else:
            # status is set to "unavailable" when vehicle is new
            if previous_vehicle_event_data is None:
                logger.warn(
                    f"Update cover received a new {current_role} with id {raw_vehicle_id}"
                )
                # New vehicle
                previous_vehicle_event_data = current_vehicle_event_data

            # ----------- Systematic --
            home_area_has_changed = (
                previous_vehicle_event_data.home_area != current_home_area
            )
            role_has_changed = previous_vehicle_event_data.role != current_role
            compensation_needed = role_has_changed or home_area_has_changed

            (
                new_area_availability_for_role,
                new_bspp_availability_for_role,
            ) = self._get_availabilities_for_role(
                compensation_needed=compensation_needed,
                current_area=current_home_area,
                current_role=current_role,
                previous_available_kind=previous_vehicle_event_data.availability_kind,
                current_availability_kind=current_vehicle_event_data.availability_kind,
            )
            availability_changed = True

        availability_changed_event_data = self._make_availability_changed_data(
            timestamp=current_vehicle_event_data.timestamp,
            role=current_role,
            home_area=current_home_area,
            area_availability=new_area_availability_for_role,
            bspp_availability=new_bspp_availability_for_role,
            raw_vehicle_id=raw_vehicle_id,
            latest_event_data=current_vehicle_event_data,
            previous_raw_operation_id=previous_vehicle_event_data.raw_operation_id,
            availability_changed=availability_changed,
        )

        return (
            availability_changed_event_data,
            compensation_needed,
        )

    @staticmethod
    def _unknown_home_or_cause(event_data: VehicleEventData):
        if event_data.home_area not in bspp_area_options:
            logger.warn(
                f"\n Skipping vehicle event with home area {event_data.home_area} not in {bspp_area_options}. \n "
            )
            return True

        if event_data.role not in vehicle_role_options:
            # received vehicle with role "other"
            logger.info(f"\n Skipping vehicle event with role {event_data.role}. \n")
            return True

        return False
