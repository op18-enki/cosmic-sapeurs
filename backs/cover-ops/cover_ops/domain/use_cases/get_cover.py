from typing import List

from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)


class GetCover:
    def __init__(
        self,
        availability_events_repo: AbstractAvailabilityEventsRepository,
    ) -> None:
        self.availability_events_repo = availability_events_repo

    def execute(self) -> List[AvailabilityChangedEventData]:

        latest_availability_event_data_by_vehicle_from_repo = (
            self.availability_events_repo.list_latest_availability_event_data_by_vehicle_from_repo()
        )

        return sorted(
            latest_availability_event_data_by_vehicle_from_repo,
            key=lambda event_data: event_data.timestamp,
        )
        # TODO : We should indeed filter duplicated events data, but the following implementation (in-memory) took too much time to process.
