from typing import List

from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)

from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    OperationStatus,
)
from shared.helpers.date import DateStr
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import AvailabilityChangedEvent, OperationEvent


class NotifyWhenVehicleAvailabilityChangedAffectsOperation:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
        availability_repo: AbstractAvailabilityEventsRepository,
        operation_repo: AbstractOperationEventsRepository,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.uuid = uuid
        self.availability_repo = availability_repo
        self.operation_repo = operation_repo

    async def execute(self, availability_event: AvailabilityChangedEvent) -> None:

        vehicle_event_data = availability_event.data.latest_event_data
        previous_raw_operation_id = availability_event.data.previous_raw_operation_id

        raw_operation_id = vehicle_event_data.raw_operation_id
        timestamp = vehicle_event_data.timestamp

        operations_notified_from_vehicle_changes = (
            self.operation_repo.get_operations_notified_from_vehicle_changes()
        )
        # previous
        if (
            previous_raw_operation_id is not None
            and previous_raw_operation_id != raw_operation_id
            and self.are_all_vehicle_released(
                previous_raw_operation_id,
            )
        ):
            if previous_raw_operation_id in operations_notified_from_vehicle_changes:
                operations_notified_from_vehicle_changes.remove(
                    previous_raw_operation_id
                )
                await self.publish_operation_event(
                    timestamp=timestamp,
                    raw_operation_id=previous_raw_operation_id,
                    status="all_vehicles_released",
                    affected_vehicles=[],
                )

        if raw_operation_id is None:
            return

        # current
        if raw_operation_id not in operations_notified_from_vehicle_changes:
            if vehicle_event_data.availability_kind == "on_operation":
                await self.publish_operation_event(
                    timestamp=timestamp,
                    raw_operation_id=raw_operation_id,
                    status="first_vehicle_affected",
                    affected_vehicles=[vehicle_event_data.raw_vehicle_id],
                )
                operations_notified_from_vehicle_changes.add(raw_operation_id)

        elif self.are_all_vehicle_released(raw_operation_id):
            await self.publish_operation_event(
                timestamp=timestamp,
                raw_operation_id=raw_operation_id,
                status="all_vehicles_released",
                affected_vehicles=[],
            )
            operations_notified_from_vehicle_changes.remove(raw_operation_id)
        else:
            if availability_event.data.availability_changed:
                await self.publish_operation_event(
                    timestamp=timestamp,
                    raw_operation_id=raw_operation_id,
                    status="some_affected_vehicle_changed",
                    affected_vehicles=self.get_vehicles_on_operation(raw_operation_id),
                )

        self.operation_repo.set_operations_notified_from_vehicle_changes(
            operations_notified_from_vehicle_changes
        )

    def are_all_vehicle_released(
        self,
        raw_operation_id: RawOperationId,
    ) -> bool:
        return len(self.get_vehicles_on_operation(raw_operation_id)) == 0

    def get_vehicles_on_operation(
        self, raw_operation_id: RawOperationId
    ) -> List[RawVehicleId]:
        vehicle_ids = [
            vehicle_event_data.raw_vehicle_id
            for vehicle_event_data in self.availability_repo.get_latest_vehicle_event_data_affected_to_operation_id(
                raw_operation_id=raw_operation_id
            )
            if vehicle_event_data.availability_kind == "on_operation"
        ]
        return sorted(vehicle_ids)

    async def publish_operation_event(
        self,
        timestamp: DateStr,
        raw_operation_id: RawOperationId,
        status: OperationStatus,
        affected_vehicles: List[RawVehicleId],
    ):
        operation_changed_status_event = OperationEvent(
            uuid=self.uuid.make(),
            source="sapeurs_notify",
            data=OperationEventData(
                address=None,
                address_area=None,
                cause=None,
                raw_procedure=None,
                timestamp=timestamp,
                raw_operation_id=raw_operation_id,
                status=status,
                latitude=None,
                longitude=None,
                raw_cause=None,
                departure_criteria=None,
                affected_vehicles=affected_vehicles,
            ),
        )
        await self.domain_event_bus.publish(operation_changed_status_event)
