from typing import Optional

from cover_ops.domain.ports.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import (
    OngoingOpChangedEvent,
    OperationEvent,
    OperationEventData,
)
from shared.helpers.logger import logger
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.data_transfert_objects.operation_event_data import (
    operation_relevant_status_options,
    operation_just_opened,
)


class UpdateOps:
    def __init__(
        self,
        ongoing_op_events_repo: AbstractOngoingOpEventsRepository,
        operation_events_repo: AbstractOperationEventsRepository,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.ongoing_op_events_repo = ongoing_op_events_repo
        self.operation_events_repo = operation_events_repo
        self.uuid = uuid

    async def execute(self, event: OperationEvent) -> None:
        """Store event in repository

        Args:
            event (OperationEvent): event published on operation status change
        """
        uuid = self.uuid.make()
        ongoing_op_changed_event_data = self.process_event(event.data)

        if ongoing_op_changed_event_data is None:
            return

        event_to_publish = OngoingOpChangedEvent(
            uuid=uuid, data=ongoing_op_changed_event_data, source="sapeurs_update_ops"
        )

        await self.domain_event_bus.publish(event_to_publish)

    def process_event(
        self, event_data: OperationEventData
    ) -> Optional[OngoingOpChangedEventData]:

        current_status = event_data.status

        if current_status not in operation_relevant_status_options:
            return

        raw_operation_id = event_data.raw_operation_id

        if current_status in [operation_just_opened, operation_just_opened]:
            address_area = event_data.address_area
            address = event_data.address
            cause = event_data.cause
            raw_cause = event_data.raw_cause
            raw_procedure = event_data.raw_procedure
            departure_criteria = event_data.departure_criteria
            longitude = event_data.longitude
            latitude = event_data.latitude
            if (
                cause is None
                or address_area is None
                or address is None
                or raw_cause is None
                or raw_procedure is None
                or departure_criteria is None
                or longitude is None
                or latitude is None
            ):
                logger.warn(
                    f"Cause or address_area or procedure or raw_cause was None for operation #{raw_operation_id}, raw_cause: {raw_cause} "
                    f"with status {current_status}: cause: {cause}, address_area: {address_area}, address: {address}, procedure: {raw_procedure} "
                    f"departure_criteria: {departure_criteria}, longitude: {longitude}, latitude: {latitude}"
                    f"Keeping in cache."
                )
                return

            opening_infos = OperationOpeningInfos(
                cause=cause,
                raw_cause=raw_cause,
                address=address,
                address_area=address_area,
                opening_timestamp=event_data.timestamp,
                raw_procedure=raw_procedure,
                longitude=longitude,
                latitude=latitude,
                departure_criteria=departure_criteria,
            )

        else:  # operation_closed | first_vehicle_affected | all_vehicles_released | some_affected_vehicle_changed
            opening_infos = (
                self.operation_events_repo.get_operation_opening_infos_for_id(
                    raw_operation_id
                )
            )

            if opening_infos is None:
                logger.warn(
                    "[OPS] "
                    f"Received status {current_status} for #{raw_operation_id} "
                    f"but no opening infos. Handle cache and return. "
                )
                return

        return OngoingOpChangedEventData(
            status=current_status,
            raw_operation_id=event_data.raw_operation_id,
            timestamp=event_data.timestamp,
            affected_vehicles=event_data.affected_vehicles,
            opening_infos=opening_infos,
        )
