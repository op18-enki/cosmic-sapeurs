from typing import Any, Dict

from cover_ops.domain.ports.domain_event_bus import DomainEventBus

from shared.cover_ops_topics import (
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from shared.sapeurs_event_dict_to_event import sapeurs_event_dict_to_event
from shared.sapeurs_events import VehicleEvent

operation_changed_status: TopicOperationChangedStatus = "operation_changed_status"
vehicle_changed_status: TopicVehicleChangedStatus = "vehicle_changed_status"


class PublishConvertersEventsToDomainEventBus:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
    ):
        self.domain_event_bus = domain_event_bus

    async def execute(self, event_as_dict: Dict[Any, Any]):

        # /!\ If topic is unknown,the following method will raise a Sapeurs Error
        # event = sapeurs_event_dict_to_event(event_as_dict)
        event = sapeurs_event_dict_to_event(event_as_dict)
        await self.domain_event_bus.publish(event)
