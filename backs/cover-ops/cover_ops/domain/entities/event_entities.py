from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from shared.cover_ops_topics import (
    TopicAvailabilityChanged,
    TopicOmnibusBalanceChanged,
    TopicOngoingOpChanged,
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData
from shared.event_source import EventSource
from shared.events.event_entity import EventEntity
from shared.marshmallow_validate import with_from_dict_classmethod


@with_from_dict_classmethod()
@dataclass
class VehicleEventEntity(JsonSchemaMixin, EventEntity):
    # for IDE happyness
    data: VehicleEventData
    source: EventSource
    topic: TopicVehicleChangedStatus = "vehicle_changed_status"


@with_from_dict_classmethod()
@dataclass
class AvailabilityChangedEventEntity(JsonSchemaMixin, EventEntity):
    # for IDE happyness
    data: AvailabilityChangedEventData
    source: EventSource
    topic: TopicAvailabilityChanged = "availabilityChanged"


@with_from_dict_classmethod()
@dataclass
class OperationEventEntity(EventEntity, JsonSchemaMixin):
    # for IDE happyness
    data: OperationEventData
    source: EventSource
    topic: TopicOperationChangedStatus = "operation_changed_status"


@with_from_dict_classmethod()
@dataclass
class OngoingOpChangedEventEntity(JsonSchemaMixin, EventEntity):
    # for IDE happyness
    data: OngoingOpChangedEventData
    source: EventSource
    topic: TopicOngoingOpChanged = "ongoingOpChanged"


@with_from_dict_classmethod()
@dataclass
class OmnibusBalanceChangedEventEntity(JsonSchemaMixin, EventEntity):
    data: OmnibusBalanceChangedEventData
    source: EventSource
    topic: TopicOmnibusBalanceChanged = "omnibusBalanceChanged"
