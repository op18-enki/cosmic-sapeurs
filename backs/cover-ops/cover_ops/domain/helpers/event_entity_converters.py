from cover_ops.domain.entities.event_entities import EventEntity

from shared.event_bus import Event


def event_to_entity(event: Event) -> EventEntity:
    return EventEntity(uuid=event.uuid, data=event.data, topic=event.topic)
