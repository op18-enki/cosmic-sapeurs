from typing import List


def flatten_list(l: List) -> List:
    return [item for sublist in l for item in sublist]
