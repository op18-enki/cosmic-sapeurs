import abc
from typing import List, Optional

from cover_ops.domain.ports.vehicle_events_repository import VehicleEventDataById

from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
    OmnibusDetail,
)


class AbstractOmnibusEventsRepository(abc.ABC):
    @abc.abstractmethod
    def remove_omnibus(self, omnibus: OmnibusDetail) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_omnibus_detail(
        self,
        raw_vehicle_id: RawVehicleId,
    ) -> Optional[OmnibusDetail]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_omnibus_details(self) -> List[OmnibusDetail]:
        raise NotImplementedError

    @abc.abstractmethod
    def reset(self) -> None:
        raise NotImplementedError


class InMemoryOmnibusEventsRepository(AbstractOmnibusEventsRepository):
    _latest_omnibus_event_data: Optional[OmnibusBalanceChangedEventData]

    def __init__(self):
        self._latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById = {}
        self._omnibus_details: List[OmnibusDetail] = []
        self._latest_omnibus_event_data = None

    def reset(self) -> None:
        self._latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById = {}
        self._omnibus_details: List[OmnibusDetail] = []
        self._latest_omnibus_event_data = None

    def remove_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        vsav_id = omnibus_detail.vsav_id
        pse_id = omnibus_detail.pse_id

        if not vsav_id and not pse_id:
            raise ValueError("Either pse_id or vsav_id should be given.")
        if vsav_id:
            self._omnibus_details = [
                detail for detail in self._omnibus_details if detail.vsav_id != vsav_id
            ]
        if pse_id:
            self._omnibus_details = [
                detail for detail in self._omnibus_details if detail.pse_id != pse_id
            ]

    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        self._omnibus_details.append(omnibus_detail)

    def get_omnibus_detail(
        self,
        raw_vehicle_id: RawVehicleId,
    ) -> Optional[OmnibusDetail]:
        matches = [
            detail
            for detail in self._omnibus_details
            if raw_vehicle_id in [detail.pse_id, detail.vsav_id]
        ]
        return matches[0] if matches else None

    def get_omnibus_details(self) -> List[OmnibusDetail]:
        return self._omnibus_details

    # next methods are only for test purposes
    def set_omnibus_details(self, omnibus_details: List[OmnibusDetail]):
        self._omnibus_details = omnibus_details
