import abc


class AbstractApiStateCache(abc.ABC):
    def __init__(self):
        self.set_is_ready(True)

    @abc.abstractmethod
    def set_is_ready(
        self,
        is_ready: bool,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_is_ready(
        self,
    ) -> bool:
        raise NotImplementedError


class InMemoryApiStateCache(AbstractApiStateCache):
    _is_ready: bool

    def set_is_ready(
        self,
        is_ready: bool,
    ) -> None:
        self._is_ready = is_ready

    def get_is_ready(
        self,
    ) -> bool:
        return self._is_ready
