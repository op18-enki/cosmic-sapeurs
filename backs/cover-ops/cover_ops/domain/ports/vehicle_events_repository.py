import abc
from dataclasses import dataclass
from itertools import groupby
from typing import Dict, List, Optional, Union

from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.helpers.filter_events import filter_events

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.date import hours_between
from shared.ports.abstract_events_repository import AbstractEventsRepository


@dataclass
class AvailabilityStoredInfos:
    availability: Availability
    latest_event_data: Union[VehicleEventData, None] = None

    @classmethod
    def from_dict(
        cls,
        input: Dict,
    ) -> "AvailabilityStoredInfos":

        latest_event_data_dict = input["latest_event_data"]
        latest_event_data = (
            VehicleEventData(**latest_event_data_dict)
            if latest_event_data_dict
            else None
        )

        return cls(
            availability=Availability(**input["availability"]),
            latest_event_data=latest_event_data,
        )


AvailabilityStoredInfoByRole = Dict[VehicleRole, AvailabilityStoredInfos]
AvailabilityStoredInfoByAreaByRole = Dict[Area, AvailabilityStoredInfoByRole]

VehicleEventDataById = Dict[RawVehicleId, VehicleEventData]
AvailabilityStoredInfosByVehicleId = Dict[RawVehicleId, AvailabilityStoredInfos]


class AbstractVehicleEventsRepository(
    AbstractEventsRepository,
    abc.ABC,
):
    @abc.abstractmethod
    def get_latest_data_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def reset(self) -> None:
        raise NotImplementedError

    # the following method is used only in resync, hence no more !
    # def list_latest_events_by_availability_by_role_by_area_and_for_bspp(
    #     self,
    # ) -> Tuple[
    #     Dict[Area, Dict[VehicleRole, Dict[AvailabilityKind, VehicleEventEntitiesList]]],
    #     Dict[VehicleRole, Dict[AvailabilityKind, VehicleEventEntitiesList]],
    # ]:

    #     events_by_availability_by_role = make_typed_record(
    #         vehicle_role_options,
    #         lambda: make_typed_record(
    #             vehicle_availability_kind_options, make_vehicle_event_entities_list
    #         ),
    #     )
    #     events_by_availability_by_area_by_role = make_typed_record(
    #         bspp_area_options,
    #         lambda: make_typed_record(
    #             vehicle_role_options,
    #             lambda: make_typed_record(
    #                 vehicle_availability_kind_options,
    #                 make_vehicle_event_entities_list,
    #             ),
    #         ),
    #     )

    #     latest_events_by_raw_vehicle_id = self.get_latest_event_by_raw_vehicle_id()
    #     for (home_area, role, availability_kind), matching_latest_events in groupby(
    #         latest_events_by_raw_vehicle_id.values(),
    #         lambda event: (
    #             event.data.home_area,
    #             event.data.role,
    #             event.data.availability_kind,
    #         ),
    #     ):
    #         if home_area not in bspp_area_options:
    #             continue
    #         if role not in vehicle_role_options:
    #             continue

    #         list_matching_latest_events = list(matching_latest_events)

    #         events_by_availability_by_area_by_role[home_area][role][
    #             availability_kind
    #         ] += list_matching_latest_events
    #         events_by_availability_by_role[role][
    #             availability_kind
    #         ] += list_matching_latest_events

    #     return (
    #         events_by_availability_by_area_by_role,
    #         events_by_availability_by_role,
    #     )  # type: ignore


def make_vehicle_event_entities_list() -> List[VehicleEventData]:
    return []


class InMemoryVehicleEventsRepository(AbstractVehicleEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self._vehicle_datas: List[VehicleEventData] = []

    def reset(self) -> None:
        self._vehicle_datas = []

    def get_all(self) -> List[VehicleEventData]:
        return self._vehicle_datas

    def get_latest_data_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventData]:
        latest_events_by_raw_vehicle_id: Dict[RawVehicleId, VehicleEventData] = {}
        for raw_vehicle_id, group in groupby(
            sorted(
                self._vehicle_datas,
                key=lambda event_data: event_data.raw_vehicle_id,
            ),
            lambda event_data: event_data.raw_vehicle_id,
        ):
            group_sorted_by_timestamp = list(
                sorted(
                    group,
                    key=lambda event_data: event_data.timestamp,
                ),
            )
            latest_event_data = group_sorted_by_timestamp[-1]
            latest_events_by_raw_vehicle_id[
                latest_event_data.raw_vehicle_id
            ] = latest_event_data
        return latest_events_by_raw_vehicle_id

    def _add(self, vehicle_event_datas: List[VehicleEventData]):
        self._vehicle_datas += vehicle_event_datas

    def purge_old_similar_events(self, event_data: VehicleEventData) -> int:
        def should_keep(repo_event_data: VehicleEventData) -> bool:
            same_id = repo_event_data.raw_vehicle_id == event_data.raw_vehicle_id
            if not same_id:
                return True
            event_age = hours_between(repo_event_data.timestamp, event_data.timestamp)
            return event_age < self._purge_older_than

        self._vehicle_datas, nb_of_purged_events = filter_events(
            self._vehicle_datas, should_keep
        )
        return nb_of_purged_events

    def get_latest_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        latest_events_by_id = self.get_latest_data_by_raw_vehicle_id()
        return (
            latest_events_by_id[raw_vehicle_id]
            if raw_vehicle_id in latest_events_by_id
            else None
        )

    @staticmethod
    def sort_data_by_timestamp(
        event_datas: List[VehicleEventData],
    ) -> List[VehicleEventData]:
        return list(
            sorted(
                event_datas,
                key=lambda event_data: event_data.timestamp,
            )
        )

    # next methods are only for test purposes
    @property
    def vehicle_datas(self) -> List[VehicleEventData]:
        return self._vehicle_datas

    @vehicle_datas.setter
    def vehicle_datas(self, vehicle_datas: List[VehicleEventData]):
        self._vehicle_datas = vehicle_datas
