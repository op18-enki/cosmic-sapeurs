import abc
from itertools import groupby
from typing import Callable, List, Optional

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from cover_ops.helpers.filter_events import filter_events

from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.date import hours_between
from shared.ports.abstract_events_repository import AbstractEventsRepository

default_availability_stored_infos = AvailabilityStoredInfos(
    availability=Availability(0, 0, 0, 0, 0)
)


class AbstractAvailabilityEventsRepository(AbstractEventsRepository):
    @abc.abstractmethod
    def get_availability_event_data_since(self) -> List[AvailabilityChangedEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    @abc.abstractmethod
    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_vehicle_event_data_affected_to_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def reset(self) -> None:
        raise NotImplementedError


def sorted_groupby(l: List, key_function: Callable):
    return groupby(
        sorted(l, key=key_function),
        key_function,
    )


class InMemoryAvailabilityEventsRepository(AbstractAvailabilityEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self._availability_datas: List[AvailabilityChangedEventData] = []

    def _add(self, availability_changed_datas: List[AvailabilityChangedEventData]):
        self._availability_datas += availability_changed_datas

    def reset(self) -> None:
        self._availability_datas = []

    def get_latest_vehicle_event_data_affected_to_operation_id(
        self, raw_operation_id: RawOperationId
    ):
        return [
            event_data.latest_event_data
            for event_data in self.list_latest_availability_event_data_by_vehicle_from_repo()
            if event_data.latest_event_data
            and event_data.latest_event_data.raw_operation_id == raw_operation_id
        ]

    def get_availability_event_data_since(self) -> List[AvailabilityChangedEventData]:
        return self._availability_datas

    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:
        if not self._availability_datas:
            return []

        latest_availability_event_data_by_vehicle = []

        for _, group in sorted_groupby(
            self._availability_datas,
            lambda event_data: event_data.raw_vehicle_id,
        ):
            latest_availability_event_data_by_vehicle.append(
                get_latest_data(list(group))
            )
        return latest_availability_event_data_by_vehicle

    def purge_old_similar_events(self, event_data: AvailabilityChangedEventData) -> int:
        def should_keep(repo_data: AvailabilityChangedEventData):
            same_id = repo_data.raw_vehicle_id == event_data.raw_vehicle_id
            if not same_id:
                return True
            event_age = hours_between(repo_data.timestamp, event_data.timestamp)
            return event_age < self._purge_older_than

        self._availability_datas, nb_of_purged_events = filter_events(
            self._availability_datas, should_keep
        )
        return nb_of_purged_events

    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        availability_data_for_role = [
            event_data
            for event_data in self._availability_datas
            if event_data.role == role
        ]
        if not availability_data_for_role:
            return default_availability_stored_infos

        latest_event_data = availability_data_for_role[-1]
        return AvailabilityStoredInfos(
            availability=latest_event_data.bspp_availability,
            latest_event_data=latest_event_data.latest_event_data,
        )

    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        availability_data_for_area_for_role = [
            event_data
            for event_data in self._availability_datas
            if event_data.role == role and event_data.home_area == area
        ]
        if not availability_data_for_area_for_role:
            return default_availability_stored_infos

        latest_event_data = availability_data_for_area_for_role[-1]
        return AvailabilityStoredInfos(
            availability=latest_event_data.area_availability,
            latest_event_data=latest_event_data.latest_event_data,
        )

    # next methods are only for test purposes
    @property
    def availability_datas(self) -> List[AvailabilityChangedEventData]:
        return self._availability_datas

    @property
    def last_availability_data(self) -> Optional[AvailabilityChangedEventData]:
        return self._availability_datas[-1] if self._availability_datas else None

    @availability_datas.setter
    def availability_datas(self, datas: List[AvailabilityChangedEventData]):
        self._availability_datas = datas


def sort_datas_by_timestamp(
    datas: List[AvailabilityChangedEventData],
) -> List[AvailabilityChangedEventData]:
    return list(
        sorted(
            datas,
            key=lambda data: data.timestamp,
        ),
    )


def get_latest_data(
    datas: List[AvailabilityChangedEventData],
) -> AvailabilityChangedEventData:
    return sort_datas_by_timestamp(datas)[-1]
