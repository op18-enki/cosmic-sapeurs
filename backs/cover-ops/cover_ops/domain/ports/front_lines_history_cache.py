import abc

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory


class AbstractFrontLinesHistoryCache(abc.ABC):
    @abc.abstractmethod
    def set_front_lines_history(self, front_lines_history: FrontLinesHistory) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_front_lines_history(
        self,
    ) -> FrontLinesHistory:
        raise NotImplementedError


class InMemoryFrontLinesHistoryCache(AbstractFrontLinesHistoryCache):
    _front_lines_history: FrontLinesHistory

    def set_front_lines_history(self, front_lines_history: FrontLinesHistory) -> None:
        self._front_lines_history = front_lines_history

    def get_front_lines_history(
        self,
    ) -> FrontLinesHistory:
        return self._front_lines_history
