from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.event_bus import AbstractEventBus, InMemoryEventBus

DomainEventBus = AbstractEventBus[CoverOpsDomainTopic]

InMemoryDomainEventBus = InMemoryEventBus[CoverOpsDomainTopic]
