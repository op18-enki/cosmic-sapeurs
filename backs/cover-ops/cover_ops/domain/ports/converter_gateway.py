import abc
from typing import Optional

from shared.converters_snapshot import ConvertersSnapshot


class AbstractConvertersGateway(abc.ABC):
    @abc.abstractmethod
    async def get_cover_ops_snapshot(self) -> ConvertersSnapshot:
        raise NotImplementedError


class InMemoryConvertersGateway(AbstractConvertersGateway):
    def __init__(self, snapshot: Optional[ConvertersSnapshot] = None) -> None:
        self._snapshot = snapshot or ConvertersSnapshot([], [])

    async def get_cover_ops_snapshot(self) -> ConvertersSnapshot:
        return self._snapshot

    def set_snapshot(self, snapshot: ConvertersSnapshot):
        self._snapshot = snapshot
