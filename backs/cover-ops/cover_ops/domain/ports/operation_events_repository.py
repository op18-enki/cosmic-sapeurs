import abc
from itertools import groupby
from typing import Callable, Dict, List, Optional, Set, Tuple

from cover_ops.helpers.filter_events import filter_events

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationEventData,
    OperationRelevantStatus,
    OperationStatus,
    operation_closed,
    operation_has_all_vehicles_released,
    operation_just_opened,
    operation_relevant_status_options,
)
from shared.helpers.date import hours_between
from shared.helpers.logger import logger
from shared.ports.abstract_events_repository import AbstractEventsRepository

OngoingOpsCountByArea = Dict[Area, int]
OngoingOpsCountByCauseByArea = Dict[OperationCause, OngoingOpsCountByArea]


def sort_operation_datas_by_timestamp(
    datas: List[OperationEventData],
) -> List[OperationEventData]:
    return list(
        sorted(
            datas,
            key=lambda data: data.timestamp,
        ),
    )


def filter_operation_datas(
    event_datas: List[OperationEventData],
    predicate: Callable[[OperationEventData], bool],
) -> List[OperationEventData]:
    filtered_datas = []
    for data in event_datas:
        if predicate(data):
            filtered_datas.append(data)
    return filtered_datas


class AbstractOperationEventsRepository(
    AbstractEventsRepository,
    abc.ABC,
):
    @abc.abstractmethod
    def reset(
        self,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_ongoing_operation_datas_with_relevant_status(
        self,
    ) -> List[OperationEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_datas_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationRelevantStatus]] = None,
    ) -> List[OperationEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_event_data_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_operation_opening_infos_for_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationOpeningInfos]:
        raise NotImplementedError

    @staticmethod
    def make_opening_info_from_event_data(
        event_data: OperationEventData,
    ) -> Optional[OperationOpeningInfos]:
        cause = event_data.cause
        raw_cause = event_data.raw_cause
        address_area = event_data.address_area
        address = event_data.address
        raw_procedure = event_data.raw_procedure
        departure_criteria = event_data.departure_criteria
        longitude = event_data.longitude
        latitude = event_data.latitude
        opening_timestamp = event_data.timestamp

        if (
            not cause
            or not address_area
            or not address
            or not raw_procedure
            or not raw_cause
            or not departure_criteria
            or not longitude
            or not latitude
        ):
            # Should not really happen ...
            logger.warn(
                f"Found in repo an opening event for operation #{event_data.raw_operation_id} with either address or no cause."
            )
            return

        return OperationOpeningInfos(
            cause=cause,
            raw_cause=raw_cause,
            address_area=address_area,
            address=address,
            raw_procedure=raw_procedure,
            departure_criteria=departure_criteria,
            longitude=longitude,
            latitude=latitude,
            opening_timestamp=opening_timestamp,
        )


class InMemoryOperationEventsRepository(AbstractOperationEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self._operation_datas: List[OperationEventData] = []
        self._operations_notified_from_vehicle_changes: Set[RawOperationId] = set()
        AbstractOperationEventsRepository.__init__(self, purge)

    def reset(
        self,
    ) -> None:
        self._operation_datas = []
        self._operations_notified_from_vehicle_changes = set()

    def purge_old_similar_events(self, event_data: OperationEventData) -> int:
        def should_keep(repo_event_data: OperationEventData) -> bool:
            same_id = repo_event_data.raw_operation_id == event_data.raw_operation_id
            if not same_id:
                return True
            event_age = hours_between(repo_event_data.timestamp, event_data.timestamp)
            return event_age < self._purge_older_than

        self._operation_datas, nb_of_purged_events = filter_events(
            self._operation_datas, should_keep
        )
        return nb_of_purged_events

    def get_latest_event_data_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationEventData]:
        filtered = filter_operation_datas(
            self._operation_datas,
            predicate=lambda event_data: event_data.raw_operation_id
            == raw_operation_id,
        )
        return filtered[-1] if filtered else None

    def get_datas_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationRelevantStatus]] = None,
    ) -> List[OperationEventData]:
        events_with_relevant_status_for_this_operation = filter_operation_datas(
            event_datas=self._operation_datas,
            predicate=lambda event_data: event_data.raw_operation_id == raw_operation_id
            and (event_data.status in statuses if statuses else True),
        )
        return events_with_relevant_status_for_this_operation

    def get_ongoing_operation_datas_with_relevant_status(
        self,
    ) -> List[OperationEventData]:
        operation_events_with_relevant_and_closing_status = filter_operation_datas(
            self._operation_datas,
            lambda event_data: event_data.status in operation_relevant_status_options,
        )
        ongoing_operation_events = []
        for _, group in groupby(
            sorted(
                operation_events_with_relevant_and_closing_status,
                key=lambda event_data: event_data.raw_operation_id,
            ),
            lambda event_data: event_data.raw_operation_id,
        ):
            l = list(group)
            latest_event_data = l[-1]
            if latest_event_data.status not in [
                operation_has_all_vehicles_released,
                operation_closed,
            ]:
                ongoing_operation_events += l
        return sort_operation_datas_by_timestamp(ongoing_operation_events)

    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        self._operations_notified_from_vehicle_changes = operation_ids

    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        return self._operations_notified_from_vehicle_changes

    def get_operation_opening_infos_for_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[OperationOpeningInfos]:
        filtered_datas_with_opening_infos = [
            data
            for data in self._operation_datas
            if data.raw_operation_id == raw_operation_id and data.cause
        ]
        if not filtered_datas_with_opening_infos:
            return

        latest_data_with_opening_info = filtered_datas_with_opening_infos[-1]

        opening_infos = self.make_opening_info_from_event_data(
            latest_data_with_opening_info
        )

        return opening_infos

    def _add(self, operation_event_datas: List[OperationEventData]):
        self._operation_datas += operation_event_datas

    # next methods are only for test purposes
    @property
    def operation_datas(self) -> List[OperationEventData]:
        return self._operation_datas

    @operation_datas.setter
    def operation_datas(self, operation_datas: List[OperationEventData]):
        self._operation_datas = operation_datas
