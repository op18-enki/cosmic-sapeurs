import abc
from typing import List, Optional

from cover_ops.helpers.filter_events import filter_events

from shared.data_transfert_objects.area import Area, bspp_area_options
from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    operation_closed,
    operation_relevant_ongoing_status_options,
)
from shared.helpers.date import hours_between
from shared.ports.abstract_events_repository import AbstractEventsRepository


class AbstractOngoingOpEventsRepository(AbstractEventsRepository):
    @abc.abstractmethod
    def reset(
        self,
    ) -> None:
        raise NotImplementedError

    @abc.abstractclassmethod
    def get_all_ongoing_ops(self) -> List[OngoingOpChangedEventData]:
        raise NotImplementedError

    @abc.abstractclassmethod
    def get_latest_event_data_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        raise NotImplementedError

    @abc.abstractclassmethod
    def get_latest_event_data_for_all_area_for_cause_fire_and_victim(
        self,
    ) -> List[OngoingOpChangedEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_event_data_for_operation_id(
        self, operation_id: RawOperationId
    ) -> Optional[OngoingOpChangedEventData]:
        raise NotImplementedError


class InMemoryOngoingOpEventsRepository(AbstractOngoingOpEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self._ongoing_op_datas: List[OngoingOpChangedEventData] = []

    def reset(
        self,
    ) -> None:
        self._ongoing_op_datas = []

    def _add(self, ongoing_op_changed_datas: List[OngoingOpChangedEventData]):
        self._ongoing_op_datas += ongoing_op_changed_datas

    def get_all_ongoing_ops(self) -> List[OngoingOpChangedEventData]:
        return [
            data
            for data in self._ongoing_op_datas
            if data.status in operation_relevant_ongoing_status_options
        ]

    def get_latest_event_data_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> Optional[OngoingOpChangedEventData]:
        sorted_selection = sorted(
            [
                event_data
                for event_data in self.ongoing_op_datas
                if event_data.raw_operation_id == raw_operation_id
            ],
            key=lambda event_data: event_data.timestamp,
        )
        return sorted_selection[-1] if sorted_selection else None

    def get_latest_event_data_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        sorted_selection = sorted(
            [
                event_data
                for event_data in self.ongoing_op_datas
                if event_data.opening_infos.address_area == area
                and event_data.opening_infos.cause == cause
            ],
            key=lambda event_data: event_data.timestamp,
        )
        return sorted_selection[-1] if sorted_selection else None

    def get_latest_event_data_for_all_area_for_cause_fire_and_victim(self):
        causes_to_fetch: List[OperationCause] = ["fire", "victim"]
        latest_ops_event_datas_per_area = []
        for area in bspp_area_options:
            for cause in causes_to_fetch:
                latest_event_data = self.get_latest_event_data_for_area_for_cause(
                    area, cause
                )
                if latest_event_data:
                    latest_ops_event_datas_per_area.append(latest_event_data)
        return latest_ops_event_datas_per_area

    def purge_old_similar_events(self, event_data: OngoingOpChangedEventData):
        def should_keep(repo_event_data: OngoingOpChangedEventData) -> bool:

            if repo_event_data == event_data:
                return True

            same_cause = (
                repo_event_data.opening_infos.cause == event_data.opening_infos.cause
            )
            same_address_area = (
                repo_event_data.opening_infos.address_area
                == event_data.opening_infos.address_area
            )
            if not same_cause and not same_address_area:
                return True

            # If operation is closed, purge all old events with same `raw_operation_id`
            if (
                event_data.status == operation_closed
                and event_data.raw_operation_id == repo_event_data.raw_operation_id
            ):
                return False

            event_age = hours_between(repo_event_data.timestamp, event_data.timestamp)
            if event_age < self._purge_older_than:
                return True

            # At this stage, the event is old enough to be purged but needs to meet some criteria :
            #   - Be closed (for the ongoing ops detailed map)
            #   - Not last of area of cause
            if not repo_event_data.status == operation_closed:
                return True

            if not self._is_last_of_area_of_cause(repo_event_data):
                return False

            return True

        self._ongoing_op_datas, nb_of_purged_events = filter_events(
            self._ongoing_op_datas, should_keep
        )
        return nb_of_purged_events

    def _is_last_of_area_of_cause(self, event_data: OngoingOpChangedEventData):
        filtered = [
            repo_event_data
            for repo_event_data in self.ongoing_op_datas
            if (
                repo_event_data.opening_infos.address_area
                == event_data.opening_infos.address_area
            )
            and (repo_event_data.opening_infos.cause == event_data.opening_infos.cause)
        ]
        return len(filtered) == 1

    # next methods are only for test purposes
    @property
    def ongoing_op_datas(self) -> List[OngoingOpChangedEventData]:
        return self._ongoing_op_datas

    @ongoing_op_datas.setter
    def ongoing_op_datas(self, datas: List[OngoingOpChangedEventData]):
        self._ongoing_op_datas = datas
