from typing import List, Optional

from cover_ops.domain.entities.event_entities import OngoingOpChangedEventEntity
from cover_ops.domain.ports.operation_events_repository import OperationOpeningInfos

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
)
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import OngoingOpChangedEvent

default_timestamp = DateStr("2020-10-31T12:00:00.000Z")


def make_ongoing_ops_changed_event_data(
    timestamp: Optional[DateStr] = None,
    opening_timestamp: Optional[DateStr] = None,
    cause: Optional[OperationCause] = None,
    address_area: Optional[Area] = None,
    address: Optional[str] = None,
    raw_operation_id: Optional[str] = None,
    raw_cause: Optional[str] = None,
    status: Optional[OperationStatus] = None,
    raw_procedure: Optional[str] = None,
    departure_criteria: Optional[str] = None,
    latitude: Optional[float] = None,
    longitude: Optional[float] = None,
    affected_vehicles: Optional[List[RawVehicleId]] = None,
) -> OngoingOpChangedEventData:
    cause = cause or "victim"
    address_area = address_area or "CHPT"
    address = address or "1 place jules renard 75017 Paris"
    raw_operation_id = RawOperationId(raw_operation_id or "404")
    raw_cause = raw_cause or "Feu de ..."
    return OngoingOpChangedEventData(
        timestamp=timestamp or default_timestamp,
        affected_vehicles=affected_vehicles or [],
        status=status or "opened",
        raw_operation_id=raw_operation_id,
        opening_infos=OperationOpeningInfos(
            cause=cause,
            address_area=address_area,
            address=address,
            raw_cause=raw_cause,
            raw_procedure=raw_procedure or "R",
            departure_criteria=departure_criteria or "301 (default)",
            longitude=longitude or 60000.25,
            latitude=latitude or 60000.25,
            opening_timestamp=opening_timestamp or DateStr(""),
        ),
    )


def make_ongoing_op_changed_event_entity(
    uuid: Optional[str] = None,
    *,
    # Following args are for make_ongoing_ops_changed_event_data
    timestamp: Optional[DateStr] = None,
    cause: Optional[OperationCause] = None,
    address_area: Optional[Area] = None,
    raw_operation_id: Optional[str] = None,
    status: Optional[OperationStatus] = None,
    raw_procedure: Optional[str] = None,
    departure_criteria: Optional[str] = None,
    latitude: Optional[float] = None,
    longitude: Optional[float] = None,
) -> OngoingOpChangedEventEntity:
    data = make_ongoing_ops_changed_event_data(
        timestamp=timestamp,
        cause=cause,
        address_area=address_area,
        raw_operation_id=raw_operation_id,
        raw_procedure=raw_procedure,
        status=status,
        departure_criteria=departure_criteria,
        latitude=latitude,
        longitude=longitude,
    )
    uuid = uuid or uuid4()
    return OngoingOpChangedEventEntity(
        uuid=uuid, data=data, source="sapeurs_update_ops"
    )


def make_ongoing_op_changed_event(
    uuid: Optional[str] = None,
    *,
    # Following args are for make_ongoing_ops_changed_event_data
    timestamp: Optional[DateStr] = None,
    cause: Optional[OperationCause] = None,
    address_area: Optional[Area] = None,
    address: Optional[str] = None,
    raw_operation_id: Optional[str] = None,
    raw_cause: Optional[str] = None,
    status: Optional[OperationStatus] = None,
    dispatched_at: Optional[DateStr] = None,
) -> OngoingOpChangedEvent:
    data = make_ongoing_ops_changed_event_data(
        timestamp=timestamp,
        cause=cause,
        address_area=address_area,
        address=address,
        raw_operation_id=raw_operation_id,
        raw_cause=raw_cause,
        status=status,
    )
    uuid = uuid or uuid4()
    return OngoingOpChangedEvent(
        uuid=uuid,
        data=data,
        dispatched_at=dispatched_at or None,
        source="sapeurs_update_ops",
    )
