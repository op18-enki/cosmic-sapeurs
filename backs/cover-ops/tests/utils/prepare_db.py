from typing import Any, List

from cover_ops.adapters.postgres.db import drop_db, initialize_db
from cover_ops.adapters.postgres.pg_repository import event_to_id
from sqlalchemy.engine import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.schema import Table

from shared.helpers.dataclass import dataclass_to_dict
from shared.test_utils.test_pg_url import test_pg_url


def prepare_db():
    engine = create_engine(
        test_pg_url,
        isolation_level="REPEATABLE READ",
    )
    drop_db(engine)
    initialize_db(engine)
    return engine


def insert_in_table(engine: Engine, table: Table, event_datas: List[Any]):
    events_to_insert = [
        {
            "data": dataclass_to_dict(event_data),
            "event_timestamp": event_data.timestamp,
        }
        for event_data in event_datas
    ]
    insertion = table.insert().values(events_to_insert)
    engine.connect().execute(insertion)
