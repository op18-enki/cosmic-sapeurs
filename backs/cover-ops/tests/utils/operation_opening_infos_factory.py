from typing import Optional

from cover_ops.domain.ports.operation_events_repository import OperationOpeningInfos

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import OperationCause
from shared.helpers.date import DateStr

default_timestamp = DateStr("2020-10-31T12:00:00.0")


def make_operation_opening_infos(
    opening_timestamp: Optional[DateStr] = None,
    cause: Optional[OperationCause] = None,
    address_area: Optional[Area] = None,
    address: Optional[str] = None,
    raw_cause: Optional[str] = None,
    raw_procedure: Optional[str] = None,
    departure_criteria: Optional[str] = None,
    latitude: Optional[float] = None,
    longitude: Optional[float] = None,
) -> OperationOpeningInfos:

    cause = cause or "victim"
    address_area = address_area or "CHPT"
    address = address or "1 place jules renard 75017 Paris"
    raw_cause = raw_cause or "Feu de ..."

    return OperationOpeningInfos(
        cause=cause,
        address_area=address_area,
        address=address,
        raw_cause=raw_cause,
        raw_procedure=raw_procedure or "R",
        departure_criteria=departure_criteria or "301 (default)",
        longitude=longitude or 60000.25,
        latitude=latitude or 60000.25,
        opening_timestamp=opening_timestamp or DateStr(""),
    )
