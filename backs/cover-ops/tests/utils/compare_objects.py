from typing import Dict


def compare_dict(dict_0: Dict, dict_1: Dict) -> None:
    assert len(dict_0) == len(dict_1)
    assert dict_0.keys() == dict_1.keys()
    for key_0, value_0 in dict_0.items():
        value_1 = dict_1[key_0]
        assert (
            value_0 == value_1
        ), f"Difference in key {key_0}: {value_0 } != {value_1} "


def compare_list(list_0: Dict, list_1: Dict) -> None:
    assert len(list_0) == len(
        list_1
    ), f"Lists are not of same length: left has {len(list_0)}, right has {len(list_1)}"

    for k, value_k_0 in enumerate(list_0):
        value_1 = list_1[k]
        assert (
            value_k_0 == value_1
        ), f"Difference in {k}iest item : {value_k_0 } != {value_1} "
