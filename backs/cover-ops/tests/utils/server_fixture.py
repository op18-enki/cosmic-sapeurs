from asyncio.events import AbstractEventLoop
from typing import Optional

import pytest
from cover_ops.entrypoints.server import make_app
from tests.utils.prepare_test_config import prepare_test_config


def make_server_fixture():
    config = prepare_test_config()

    @pytest.fixture
    def server_fixture(loop, aiohttp_client):
        """Start server for end-to-end tests"""
        loop.run_until_complete(config.domain_event_bus.start_on_loop(loop))
        app, _ = make_app(config)
        loop.run_until_complete(config.bus_subscribes_to_domain_events())
        return loop.run_until_complete(aiohttp_client(app))

    return server_fixture, config
