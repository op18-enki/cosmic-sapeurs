from typing import Dict, List


def assert_subset_of(subset=dict, expected=dict):
    for key, value in expected.items():
        assert (
            value == subset[key]
        ), f"key : {key} \n expected is: {value} \n value was: {subset[key]}"


def assert_list_events_have_uuid(
    actual_events: List,  # TODO : type this.
    expected_uuids: List[str],
):
    assert len(actual_events) == len(expected_uuids)
    assert set([event.uuid for event in actual_events]) == set(expected_uuids)


def assert_dicts_equal(dict_0: Dict, dict_1: Dict) -> None:
    assert len(dict_0) == len(dict_1)
    assert dict_0.keys() == dict_1.keys()
    for key_0, value_0 in dict_0.items():
        value_1 = dict_1[key_0]
        assert (
            value_0 == value_1
        ), f"Difference in key {key_0}: {value_0 } != {value_1} "


def assert_lists_equal(list_0: List, list_1: List) -> None:
    assert len(list_0) == len(
        list_1
    ), f"Lists are not of same length: left has {len(list_0)}, right has {len(list_1)} !"

    for k, value_0 in enumerate(list_0):
        value_1 = list_1[k]
        assert (
            value_0 == value_1
        ), f"Difference in {k}iest item : {value_0 } != {value_1} ! "
