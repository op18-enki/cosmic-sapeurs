import asyncio


async def post_and_sleep(server, route: str, data: str):
    resp = await server.post(route, data=data)
    await asyncio.sleep(0.1)
    return resp
