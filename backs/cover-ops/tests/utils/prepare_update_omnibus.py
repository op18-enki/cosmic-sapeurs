import asyncio
from typing import List, Literal, Optional

from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.domain_event_bus import InMemoryDomainEventBus
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)

from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import (
    OmnibusInfos,
    VehicleEventData,
    VehicleStatus,
)
from shared.factories.vehicle_event_factory import (
    make_vehicle_event,
    make_vehicle_event_data,
)
from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid, RealUuid
from shared.sapeurs_events import VehicleEvent
from shared.test_utils.spy_on_topic import spy_on_topic

vsav_raw_vehicle_id = RawVehicleId("vsav_190")
pse_raw_vehicle_id = RawVehicleId("ps_237")

loop = asyncio.get_event_loop()


async def prepare_update_omnibus(
    initial_vehicle_datas: Optional[List[VehicleEventData]] = None,
    initial_omnibus_details: Optional[List[OmnibusDetail]] = None,
):
    clock = CustomClock()
    vehicle_repo = InMemoryVehicleEventsRepository(purge=True)
    domain_event_bus = InMemoryDomainEventBus(clock=clock)
    custom_uuid = CustomUuid()
    omnibus_repo = InMemoryOmnibusEventsRepository()
    update_omnibus = UpdateOmnibus(
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        vehicle_repo=vehicle_repo,
        omnibus_repo=omnibus_repo,
    )
    if initial_vehicle_datas:
        vehicle_repo.vehicle_datas = initial_vehicle_datas
    if initial_omnibus_details:
        omnibus_repo.set_omnibus_details(initial_omnibus_details)

    published_events = await spy_on_topic(domain_event_bus, "omnibusBalanceChanged")
    return (
        update_omnibus,
        published_events,
        domain_event_bus,
        omnibus_repo,
        vehicle_repo,
    )


def execute_update_omnibus_with_initialized_balance_assert_result(
    initial_vehicle_events: List[VehicleEvent],
    initial_omnibus_details: List[OmnibusDetail],
    vehicle_event: VehicleEvent,
    expected_omnibus_details: Optional[List[OmnibusDetail]],
):
    (
        update_omnibus,
        published_events,
        domain_event_bus,
        omnibus_repo,
        _,
    ) = loop.run_until_complete(
        prepare_update_omnibus(
            initial_vehicle_datas=[event.data for event in initial_vehicle_events],
            initial_omnibus_details=initial_omnibus_details,
        )
    )

    asyncio.run(update_omnibus.execute(vehicle_event))

    if expected_omnibus_details is None:
        assert len(published_events) == 0
        return

    expected_omnibus_event_data = OmnibusBalanceChangedEventData(
        timestamp=vehicle_event.data.timestamp,
        details=expected_omnibus_details,
    )
    assert len(published_events) == 1
    assert published_events[0].data == expected_omnibus_event_data
    assert published_events[0].source == "sapeurs_update_omnibus"


def make_vsav_event(status: VehicleStatus):
    return make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_raw_vehicle_id,
        ),
        status=status,
    )


def make_pse_event(
    status: VehicleStatus, role: Literal["pse_omni_san", "pse_omni_pump"]
):
    return make_vehicle_event(
        raw_vehicle_id=pse_raw_vehicle_id,
        role=role,
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=vsav_raw_vehicle_id,
        ),
        status=status,
    )


def make_pse_event_data(
    status: VehicleStatus, role: Literal["pse_omni_san", "pse_omni_pump"]
):
    return make_vehicle_event_data(
        raw_vehicle_id=pse_raw_vehicle_id,
        role=role,
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=vsav_raw_vehicle_id,
        ),
        status=status,
    )


vsav_affected_event = make_vsav_event("selected")
pse_as_pse_home_event = make_pse_event("arrived_at_home", "pse_omni_pump")
pse_as_vsav_home_event = make_pse_event("arrived_at_home", "pse_omni_san")
vsav_lacks_staff_omnibus_event = make_vsav_event(status="unavailable_omnibus")
vsav_lacks_staff_event = make_vsav_event(status="unavailable_lacks_staff")
pse_to_inter_san_event = make_pse_event("departed_to_intervention", "pse_omni_san")
pse_to_inter_fire_event = make_pse_event("departed_to_intervention", "pse_omni_pump")
vsav_back_home_event = make_vsav_event("arrived_at_home")
pse_san_back_home_event = make_pse_event("arrived_at_home", "pse_omni_san")
pse_pump_back_home_event = make_pse_event("arrived_at_home", "pse_omni_pump")
pse_downgraded_event = make_pse_event(status="arrived_at_home", role="pse_omni_san")
pse_affected_as_pump_event = make_pse_event(status="selected", role="pse_omni_pump")
