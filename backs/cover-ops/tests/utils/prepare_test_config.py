from pathlib import Path

from cover_ops.adapters.csv_events_repositories import CsvVehicleEventsRepository
from cover_ops.adapters.postgres.db import reset_db
from cover_ops.adapters.postgres.pg_vehicle_events_repository import (
    PgVehicleEventsRepository,
)
from cover_ops.entrypoints.config import Config
from cover_ops.entrypoints.environment_variables import EnvironmentVariables

from shared.helpers.remove_folder_content import remove_folder_content
from shared.test_utils.test_pg_url import test_pg_url


def prepare_test_config():
    test_env = EnvironmentVariables(
        exception_mode="RAISE",
        events_repositories="CSV",
        cache="IN_MEMORY",
        domain_event_bus="IN_MEMORY",
        converters_url="fake-converter-url",
        pg_url=test_pg_url,
        converters_gateway="IN_MEMORY",
    )
    remove_folder_content(Path("tests/1__integration/temp_data"))
    config = Config(env=test_env, csv_base_path=Path("tests/1__integration/temp_data"))
    return config


def reset_test_config(config: Config):
    if isinstance(config.vehicle_events_repo, CsvVehicleEventsRepository):
        remove_folder_content(Path("data"))
    elif isinstance(config.vehicle_events_repo, PgVehicleEventsRepository):
        reset_db(config.vehicle_events_repo.connection)
