from typing import List, Optional

from cover_ops.domain.ports.domain_event_bus import InMemoryDomainEventBus
from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.use_cases.update_ops import UpdateOps

from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid
from shared.sapeurs_events import OngoingOpChangedEvent
from shared.test_utils.spy_on_topic import spy_on_topic


async def prepare_update_ops(
    operation_events_repo: Optional[InMemoryOperationEventsRepository] = None,
):
    custom_clock = CustomClock()
    operation_events_repo = operation_events_repo or InMemoryOperationEventsRepository(
        purge=False
    )
    ongoing_ops_events_repo = InMemoryOngoingOpEventsRepository(purge=False)
    domain_event_bus = InMemoryDomainEventBus(custom_clock)
    custom_uuid = CustomUuid()

    update_ops = UpdateOps(
        ongoing_op_events_repo=ongoing_ops_events_repo,
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        operation_events_repo=operation_events_repo,
    )

    ongoing_op_published_events: List[OngoingOpChangedEvent] = await spy_on_topic(
        domain_event_bus, "ongoingOpChanged"
    )

    return (
        update_ops,
        ongoing_ops_events_repo,
        custom_uuid,
        ongoing_op_published_events,
        operation_events_repo,
        domain_event_bus,
    )
