from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.domain_event_bus import InMemoryDomainEventBus
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover

from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid
from shared.test_utils.spy_on_topic import spy_on_topic


async def prepare_update_cover(
    vehicle_repo: InMemoryVehicleEventsRepository = None,
):
    custom_clock = CustomClock()
    vehicle_repo = vehicle_repo or InMemoryVehicleEventsRepository(purge=False)
    availability_repo = InMemoryAvailabilityEventsRepository(purge=True)
    domain_event_bus = InMemoryDomainEventBus(custom_clock)
    custom_uuid = CustomUuid()
    update_cover = UpdateCover(
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        vehicle_repo=vehicle_repo,
        availability_repo=availability_repo,
    )
    availability_published_events = await spy_on_topic(
        domain_event_bus, "availabilityChanged"
    )
    return (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    )
