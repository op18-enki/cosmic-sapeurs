import asyncio

from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from tests.utils.availability_changed_event_factory import (
    make_availability,
    make_availability_changed_event,
    make_availability_changed_event_data,
)
from tests.utils.prepare_update_cover import prepare_update_cover

from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.sapeurs_events import VehicleEvent

operation_id_1 = RawOperationId("operation_1")
operation_id_2 = RawOperationId("operation_2")
vehicle_id_A = RawVehicleId("vehicle_A")
vehicle_id_B = RawVehicleId("vehicle_B")

loop = asyncio.get_event_loop()


def execute_usecase_and_add_to_vehicle_repo(
    update_cover, vehicle_repo: AbstractVehicleEventsRepository, event: VehicleEvent
):
    asyncio.run(update_cover.execute(event=event))
    vehicle_repo.add([event.data])


def test_vehicle_event_gets_stored_and_cover_updated_event_gets_published():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())

    timestamp = DateStr("2020-10-01T12:00:00.000Z")
    dispatched_at = DateStr("2020-11-30T12:00:00.000Z")
    custom_clock.set_next_date(dispatched_at)

    incoming_vehicle_event = make_vehicle_event(
        uuid="vehicle_uuid",
        status="selected",
        role="vsav_solo",
        timestamp=timestamp,
        dispatched_at=dispatched_at,
        home_area="STOU",
        raw_vehicle_id="1",
    )

    cover_uuid = "cover_uuid"

    expected_cover_updated_event = make_availability_changed_event(
        timestamp=timestamp,
        uuid=cover_uuid,
        role="vsav_solo",
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=1),
        home_area="STOU",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event.data,
        dispatched_at=dispatched_at,
    )

    custom_uuid.set_next_uuid(cover_uuid)

    asyncio.run(update_cover.execute(event=incoming_vehicle_event))

    assert len(availability_published_events) == 1
    published_event = availability_published_events[0]
    assert published_event.uuid == cover_uuid
    assert published_event.dispatched_at == dispatched_at
    assert published_event.data.area_availability == make_availability(on_operation=1)

    assert availability_published_events == [expected_cover_updated_event]


# TODO : decide how to do the resync :
# not of Vehicle Cache from Vehicle Repo !
# but rather of Availability Repo from Vehicle Repo

# def test_vehicle_event_gets_stored_and_cover_updated_event_gets_published_when_repo_has_data():

#     old_timestamp_1 = DateStr("2021-01-01T11:00:00.000Z")
#     old_event_entity_1 = make_vehicle_event_entity(
#         raw_vehicle_id=vehicle_id_A,
#         status="arrived_at_home",
#         role="vsav_solo",
#         home_area="STOU",
#         timestamp=old_timestamp_1,
#         raw_operation_id=operation_id_1,
#     )
#     old_timestamp_2 = DateStr("2021-01-01T12:00:00.000Z")
#     old_event_entity_2 = make_vehicle_event_entity(
#         raw_vehicle_id=vehicle_id_A,
#         status="departed_to_intervention",
#         role="vsav_solo",
#         home_area="STOU",
#         timestamp=old_timestamp_2,
#         raw_operation_id=operation_id_2,
#     )
#     vehicle_event_repo = InMemoryVehicleEventsRepository(purge=True)
#     vehicle_event_repo.vehicle_events = [old_event_entity_1, old_event_entity_2]
#     (
#         vehicle_repo,
#         availability_repo,
#         update_cover,
#         domain_event_bus,
#         custom_uuid,
#         custom_clock,
#         availability_published_events,
#     ) = loop.run_until_complete(prepare_update_cover(vehicle_event_repo))

#     # assert resync does not influence availability events repo
#     assert len(availability_published_events) == 0

#     timestamp = DateStr("2020-10-01T12:00:00.000Z")
#     vehicle_dispatched_at = DateStr("2020-11-30T12:00:00.000Z")
#     availability_dispatched_at = DateStr("2020-11-30T12:00:00.000Z")
#     incoming_vehicle_event = make_vehicle_event(
#         dispatched_at=vehicle_dispatched_at,
#         uuid="vehicle_uuid",
#         status="selected",
#         role="vsav_solo",
#         raw_vehicle_id=vehicle_id_B,
#         timestamp=timestamp,
#         home_area="STOU",
#         raw_operation_id=None,
#     )
#     custom_clock.set_next_date(availability_dispatched_at)
#     expected_availability = make_availability(recoverable=0, on_operation=2)

#     cover_uuid = "cover_uuid"
#     expected_cover_updated_event = make_availability_changed_event(
#         timestamp=timestamp,
#         uuid=cover_uuid,
#         role="vsav_solo",
#         area_availability=expected_availability,
#         bspp_availability=expected_availability,
#         home_area="STOU",
#         raw_vehicle_id=vehicle_id_B,
#         latest_event_data=incoming_vehicle_event.data,
#         previous_raw_operation_id=None,
#         dispatched_at=availability_dispatched_at,
#     )
#     custom_uuid.set_next_uuid(cover_uuid)

#     asyncio.run(update_cover.execute(event=incoming_vehicle_event))

#     assert len(availability_published_events) == 1
#     assert availability_published_events[-1] == expected_cover_updated_event


def test_cover_correctly_updated_when_availability_kind_remains_unchanged():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())
    timestamp = DateStr("2020-10-01T12:00:00.000Z")
    incoming_vehicle_event_1 = make_vehicle_event(
        status="selected",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="STOU",
    )
    incoming_vehicle_event_2 = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="STOU",
    )
    expected_availability = make_availability(on_operation=1)

    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_1
    )
    custom_uuid.set_next_uuid("uuid_2")
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_2
    )

    assert len(availability_published_events) == 2

    availability_event_data_0 = availability_published_events[0].data
    availability_event_data_1 = availability_published_events[1].data

    assert availability_event_data_0.area_availability == expected_availability
    assert availability_event_data_1.area_availability == expected_availability

    assert availability_event_data_0.latest_event_data.status == "selected"
    assert availability_event_data_1.latest_event_data.status == "arrived_at_hospital"


def test_receives_event_with_role_other():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())

    incoming_vehicle_event = make_vehicle_event(role="other", raw_vehicle_id="1")
    assert asyncio.run(update_cover.execute(event=incoming_vehicle_event)) == None


def test_cover_correctly_updated_from_different_areas():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())
    timestamp = DateStr("2020-10-01T12:00:00.000Z")
    incoming_vehicle_event_1 = make_vehicle_event(
        status="selected",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="PARM",
    )

    incoming_vehicle_event_sdis = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="sdis",
        timestamp=timestamp,
        home_area="SDIS",
    )

    incoming_vehicle_event_3 = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="3",
        timestamp=timestamp,
        home_area="ANTO",
    )

    custom_uuid.set_next_uuid("uuid_1")
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_1
    )
    custom_uuid.set_next_uuid("uuid_sdis")
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_sdis
    )
    custom_uuid.set_next_uuid("uuid_3")
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_3
    )

    assert availability_published_events[
        0
    ].data == make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_1.data,
    )
    assert availability_published_events[
        1
    ].data == make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=2),
        home_area="ANTO",
        role="vsav_solo",
        raw_vehicle_id="3",
        latest_event_data=incoming_vehicle_event_3.data,
    )


def test_cover_correctly_updated_when_vehicle_changes_home_area_same_status():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())
    parm_timestamp = DateStr("2020-10-01T12:00:00.000Z")
    incoming_vehicle_event_parm = make_vehicle_event(
        status="arrived_on_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=parm_timestamp,
        home_area="PARM",
    )
    incoming_vehicle_event_anto = make_vehicle_event(
        status="arrived_on_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=DateStr("2020-10-01T13:00:00.000Z"),
        home_area="ANTO",
    )

    custom_uuid.set_next_uuids(
        [
            "uuid_parm",
            "uuid_anto",
            "uuid_anto_compensation",
        ]
    )

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_parm))
    vehicle_repo.add([incoming_vehicle_event_parm.data])
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_anto))
    vehicle_repo.add([incoming_vehicle_event_anto.data])

    assert len(availability_published_events) == 3

    expected_vsav_arrived_at_parm_event_data = make_availability_changed_event_data(
        timestamp=parm_timestamp,
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_parm.data,
    )
    expected_vsav_arrived_at_anto_event_data = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T13:00:00.000Z"),
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=2),
        home_area="ANTO",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_anto.data,
    )
    expected_vsav_left_parm_event_data = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T13:00:00.001Z"),
        area_availability=make_availability(on_operation=0),
        bspp_availability=make_availability(on_operation=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_parm.data,
    )
    expected_event_data = [
        expected_vsav_arrived_at_parm_event_data,
        expected_vsav_arrived_at_anto_event_data,
        expected_vsav_left_parm_event_data,
    ]

    assert [
        event.data for event in availability_published_events
    ] == expected_event_data

    # availabilities_by_area_by_role = vehicle_repo._availabilities_by_area_by_role

    # assert (
    #     availabilities_by_area_by_role["ANTO"]["vsav_solo"]
    # ).availability == make_availability(on_operation=1)

    # assert (
    #     availabilities_by_area_by_role["PARM"]["vsav_solo"]
    # ).availability == make_availability(on_operation=0)

    # assert vehicle_repo._bspp_availabilities_by_role[
    #     "vsav_solo"
    # ].availability == make_availability(on_operation=1)


def test_cover_correctly_updated_when_vehicle_changes_role_same_status():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())

    incoming_pse_timestamp = DateStr("2020-10-01T12:00:00.000Z")
    incoming_vehicle_event_pse = make_vehicle_event(
        status="arrived_at_home",
        role="pse_solo",
        raw_vehicle_id="1",
        timestamp=incoming_pse_timestamp,
        home_area="STOU",
    )

    incoming_vsav_timestamp = DateStr("2020-10-01T12:05:00.000Z")
    incoming_vehicle_event_vsav = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=incoming_vsav_timestamp,
        home_area="STOU",
    )

    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_pse
    )
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_vsav
    )

    assert len(availability_published_events) == 3

    expected_incoming_pse = make_availability_changed_event_data(
        timestamp=incoming_pse_timestamp,
        area_availability=make_availability(available=1),
        bspp_availability=make_availability(available=1),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )
    expected_incoming_vsav = make_availability_changed_event_data(
        timestamp=incoming_vsav_timestamp,
        area_availability=make_availability(available=1),
        bspp_availability=make_availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )

    expected_compensated_pse = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:05:00.001Z"),
        area_availability=make_availability(available=0),
        bspp_availability=make_availability(available=0),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )

    expected_event_data = [
        expected_incoming_pse,
        expected_incoming_vsav,
        expected_compensated_pse,
    ]
    assert [
        event.data for event in availability_published_events
    ] == expected_event_data

    assert [
        event.data for event in availability_published_events
    ] == expected_event_data

    # availabilities_STOU_vsav = vehicle_repo._availabilities_by_area_by_role["STOU"][
    #     "vsav_solo"
    # ]
    # assert availabilities_STOU_vsav.availability == make_availability(available=1)

    # assert (
    #     availabilities_STOU_vsav.latest_event_data == incoming_vehicle_event_vsav.data
    # )

    # assert vehicle_repo._bspp_availabilities_by_role[
    #     "pse_solo"
    # ].availability == make_availability(available=0)

    # assert (
    #     vehicle_repo._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
    #     == incoming_vehicle_event_vsav.data
    # )


def test_cover_correctly_updated_when_vehicle_changes_role_and_status():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())
    timestamp_vsav = DateStr("2020-10-01T12:00:00.000Z")

    incoming_vehicle_event_vsav = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_vsav,
        home_area="STOU",
    )
    timestamp_pse = DateStr("2020-10-01T12:08:00.000Z")
    incoming_vehicle_event_pse = make_vehicle_event(
        status="departed_to_intervention",
        role="pse_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_pse,
        home_area="STOU",
    )

    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_vsav
    )
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_pse
    )

    assert len(availability_published_events) == 3

    expected_availability_vsav = make_availability_changed_event_data(
        timestamp=timestamp_vsav,
        area_availability=make_availability(available=1),
        bspp_availability=make_availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )
    expected_availability_pse = make_availability_changed_event_data(
        timestamp=timestamp_pse,
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=1),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )
    expected_availability_vsav_compensated = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:08:00.001Z"),
        area_availability=make_availability(available=0),
        bspp_availability=make_availability(available=0),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )

    expected_event_data = [
        expected_availability_vsav,
        expected_availability_pse,
        expected_availability_vsav_compensated,
    ]

    assert [
        event.data for event in availability_published_events
    ] == expected_event_data

    # availabilities_STOU_vsav = vehicle_repo._availabilities_by_area_by_role["STOU"][
    #     "vsav_solo"
    # ]
    # assert availabilities_STOU_vsav.availability == make_availability(available=0)

    # assert vehicle_repo._bspp_availabilities_by_role[
    #     "pse_solo"
    # ].availability == make_availability(on_operation=1)

    # assert (
    #     vehicle_repo._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
    #     == incoming_vehicle_event_pse.data
    # )


def test_cover_correctly_updated_when_vehicle_changes_area_and_status():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())

    timestamp_stou = DateStr("2020-10-01T12:00:00.000Z")
    incoming_vehicle_event_stou = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_stou,
        home_area="STOU",
    )

    timestamp_chpt = DateStr("2020-10-01T12:03:00.000Z")
    incoming_vehicle_event_chpt = make_vehicle_event(
        status="departed_to_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_chpt,
        home_area="CHPT",
    )

    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_stou
    )
    execute_usecase_and_add_to_vehicle_repo(
        update_cover, vehicle_repo, incoming_vehicle_event_chpt
    )

    expected_availability_stou = make_availability_changed_event_data(
        timestamp=timestamp_stou,
        area_availability=make_availability(available=1),
        bspp_availability=make_availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_stou.data,
    )
    expected_availability_chpt = make_availability_changed_event_data(
        timestamp=timestamp_chpt,
        area_availability=make_availability(on_operation=1),
        bspp_availability=make_availability(on_operation=1, available=1),
        home_area="CHPT",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_chpt.data,
    )
    expected_availability_stou_compensated = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:03:00.001Z"),
        area_availability=make_availability(available=0),
        bspp_availability=make_availability(on_operation=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_stou.data,
    )

    expected_event_data = [
        expected_availability_stou,
        expected_availability_chpt,
        expected_availability_stou_compensated,
    ]
    assert [event.data.timestamp for event in availability_published_events] == [
        "2020-10-01T12:00:00.000Z",
        "2020-10-01T12:03:00.000Z",
        "2020-10-01T12:03:00.001Z",
    ]
    # assert [
    #     event.data for event in availability_published_events
    # ] == expected_event_data

    assert [
        event.data for event in availability_published_events
    ] == expected_event_data

    # assert vehicle_repo._availabilities_by_area_by_role["STOU"][
    #     "vsav_solo"
    # ].availability == make_availability(available=0)
    # assert vehicle_repo._availabilities_by_area_by_role["CHPT"][
    #     "vsav_solo"
    # ].availability == make_availability(on_operation=1)
    # assert vehicle_repo._bspp_availabilities_by_role[
    #     "vsav_solo"
    # ].availability == make_availability(on_operation=1)

    # assert (
    #     vehicle_repo._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
    #     == incoming_vehicle_event_chpt.data
    # )
