import asyncio
from typing import List

from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
    OperationOpeningInfos,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)
from tests.utils.prepare_update_ops import prepare_update_ops

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationStatus,
)
from shared.factories.operation_event_factory import (
    make_operation_event,
    make_operation_event_data,
)
from shared.helpers.date import DateStr

operation_id_1 = RawOperationId("operation_1")
vehicle_id_a = RawVehicleId("vehicle_a")
vehicle_id_b = RawVehicleId("vehicle_b")
timestamp_open = DateStr("2020-10-01T12:00.000Z")
timestamp_affected = DateStr("2020-10-01T12:30.000Z")
timestamp_released = DateStr("2020-10-01T13:00.000Z")
ongoing_op_uuid = "ongoing_op_uuid"
raw_procedure_r = "R"
departure_criteria = "301 (default)"
longitude = 60000.25
latitude = 60000.25
address = "1 place jules renard 75017 Paris"
raw_cause = "Feu de ..."

operation_victim_1_stou_opened = make_operation_event(
    uuid="operation_uuid",
    status="opened",
    cause="victim",
    timestamp=timestamp_open,
    address_area="STOU",
    address=address,
    raw_operation_id=operation_id_1,
    raw_cause="Feu de ...",
    raw_procedure=raw_procedure_r,
    departure_criteria=departure_criteria,
    longitude=longitude,
    latitude=latitude,
)

operation_victim_1_stou_first_vehicle_affected = make_operation_event(
    status="first_vehicle_affected",
    cause=None,
    address_area=None,
    address=address,
    raw_operation_id=operation_id_1,
    raw_cause="Feu de ...",
    timestamp=timestamp_affected,
    raw_procedure=raw_procedure_r,
    departure_criteria=departure_criteria,
    longitude=longitude,
    latitude=latitude,
    affected_vehicles=[vehicle_id_a],
)

operation_victim_1_stou_some_affected_vehicle_changed = make_operation_event(
    status="some_affected_vehicle_changed",
    cause=None,
    address_area=None,
    address=address,
    raw_operation_id=operation_id_1,
    raw_cause="Feu de ...",
    timestamp=timestamp_affected,
    raw_procedure=raw_procedure_r,
    departure_criteria=departure_criteria,
    longitude=longitude,
    latitude=latitude,
    affected_vehicles=[vehicle_id_a, vehicle_id_b],
)

operation_victim_1_stou_all_vehicles_released = make_operation_event(
    status="all_vehicles_released",
    cause=None,
    address_area=None,
    address=address,
    raw_operation_id=operation_id_1,
    raw_cause="Feu de ...",
    timestamp=timestamp_released,
    raw_procedure=raw_procedure_r,
    departure_criteria=None,
    longitude=None,
    latitude=None,
    affected_vehicles=[],
)

operation_victim_1_stou_closed = make_operation_event(
    status="closed",
    cause=None,
    address_area=None,
    address=address,
    raw_operation_id=operation_id_1,
    raw_cause="Feu de ...",
    timestamp=timestamp_released,
    raw_procedure=raw_procedure_r,
    departure_criteria=departure_criteria,
    longitude=longitude,
    latitude=latitude,
)
loop = asyncio.get_event_loop()


def test_only_add_to_repo_events_with_relevant_status():
    (
        update_ops,
        ongoing_ops_events_repo,
        _,
        ongoing_op_published_events,
        _,
        _,
    ) = loop.run_until_complete(prepare_update_ops())
    status_options: List[OperationStatus] = ["validated", "finished", "reinforced"]
    for status in status_options:
        event_with_irrelevent_status = make_operation_event(status=status)
        loop.run_until_complete(update_ops.execute(event=event_with_irrelevent_status))
        assert len(ongoing_op_published_events) == 0


def test_operation_just_opened():
    (
        update_ops,
        ongoing_ops_events_repo,
        custom_uuid,
        ongoing_op_published_events,
        _,
        _,
    ) = loop.run_until_complete(prepare_update_ops())

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_opened))

    assert len(ongoing_op_published_events) == 1
    expected_ongoing_ops_changed_event_data = OngoingOpChangedEventData(
        timestamp=timestamp_open,
        affected_vehicles=[],
        raw_operation_id=operation_id_1,
        status="opened",
        opening_infos=OperationOpeningInfos(
            cause="victim",
            address_area="STOU",
            address=address,
            raw_cause="Feu de ...",
            raw_procedure=raw_procedure_r,
            departure_criteria=departure_criteria,
            longitude=longitude,
            latitude=latitude,
            opening_timestamp=timestamp_open,
        ),
    )

    # Event well published
    published_event = ongoing_op_published_events[0]
    assert published_event.uuid == ongoing_op_uuid
    assert published_event.data == expected_ongoing_ops_changed_event_data


def test_first_vehicle_affected_to_opened_operation():
    (
        update_ops,
        ongoing_ops_events_repo,
        custom_uuid,
        ongoing_op_published_events,
        operation_events_repo,
        _,
    ) = loop.run_until_complete(prepare_update_ops())

    departure_criteria = "Broken hand ! Lot's of blood ... "
    longitude = latitude = 199
    address = "7 rue guillaume tell"
    raw_cause = "Victim rescue ... "

    operation_events_repo.add(
        [
            make_operation_event_data(
                raw_operation_id=operation_id_1,
                raw_cause=raw_cause,
                address_area="STOU",
                cause="victim",
                raw_procedure=raw_procedure_r,
                timestamp=timestamp_open,
                status="opened",
                address=address,
                latitude=latitude,
                longitude=longitude,
                departure_criteria=departure_criteria,
            )
        ]
    )

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        timestamp=timestamp_affected,
        raw_operation_id=operation_id_1,
        status="first_vehicle_affected",
        cause="victim",
        raw_cause=raw_cause,
        raw_procedure=raw_procedure_r,
        opening_timestamp=timestamp_open,
        affected_vehicles=[vehicle_id_a],
        address=address,
        latitude=latitude,
        longitude=longitude,
        departure_criteria=departure_criteria,
    )

    # Event published
    published_event = ongoing_op_published_events[0]

    assert published_event.data == expected_ongoing_ops_changed_event_data


def test_ops_does_not_publish_when_first_operation_status_is_first_vehicle_affected():
    (
        update_ops,
        ongoing_ops_events_repo,
        custom_uuid,
        ongoing_op_published_events,
        _,
        _,
    ) = loop.run_until_complete(prepare_update_ops())
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    assert len(ongoing_op_published_events) == 0


def test_ops_does_not_publish_when_first_operation_status_is_all_vehicles_released():
    (
        update_ops,
        ongoing_ops_events_repo,
        custom_uuid,
        ongoing_op_published_events,
        _,
        _,
    ) = loop.run_until_complete(prepare_update_ops())
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))
    assert len(ongoing_op_published_events) == 0


def test_ops_is_closed():
    operation_events_repo = InMemoryOperationEventsRepository(purge=False)
    operation_events_repo.operation_datas = [
        make_operation_event_data(
            raw_operation_id=operation_id_1,
            status="opened",
            cause="victim",
            address_area="STOU",
            raw_procedure=raw_procedure_r,
            departure_criteria=departure_criteria,
            longitude=longitude,
            latitude=latitude,
        ),
    ]

    (
        update_ops,
        ongoing_ops_events_repo,
        _,
        ongoing_op_published_events,
        _,
        _,
    ) = loop.run_until_complete(
        prepare_update_ops(operation_events_repo=operation_events_repo)
    )
    loop.run_until_complete(update_ops.execute(event=operation_victim_1_stou_closed))

    assert len(ongoing_op_published_events) == 1
