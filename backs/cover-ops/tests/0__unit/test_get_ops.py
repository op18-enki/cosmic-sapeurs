from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.use_cases.get_ops import GetOps
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)

from shared.helpers.date import DateStr


def test_get_ops_if_empty():
    ongoing_op_events_repo = InMemoryOngoingOpEventsRepository(purge=True)
    ongoing_op_events_repo.ongoing_op_datas
    getOps = GetOps(ongoing_op_events_repo=ongoing_op_events_repo)
    ongoing_op_changed_event_entities = getOps.execute()
    assert len(ongoing_op_changed_event_entities) == 0


def test_get_ops():
    ongoing_op_events_repo = InMemoryOngoingOpEventsRepository(purge=True)
    victim_1_stou_25_sept = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="1",
        timestamp=DateStr("2020-09-25T13:00:00.000Z"),
        raw_procedure="red",
    )
    victim_4_stou_21_sept = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="4",
        timestamp=DateStr("2020-09-21T13:00:00.000Z"),
    )
    fire_2_chpt_20_sept = make_ongoing_ops_changed_event_data(
        address_area="CHPT",
        cause="fire",
        raw_operation_id="2",
        timestamp=DateStr("2020-09-20T12:00:00.000Z"),
    )

    still_ongoing_other_3_parm_15_sept = make_ongoing_ops_changed_event_data(
        address_area="PARM",
        cause="other",
        raw_operation_id="3",
        timestamp=DateStr("2020-09-15T12:00:00.000Z"),
        status="first_vehicle_affected",
    )

    finished_victim_4_parm_15_sept = make_ongoing_ops_changed_event_data(
        address_area="PARM",
        cause="victim",
        raw_operation_id="4",
        timestamp=DateStr("2020-09-15T11:00:00.000Z"),
        status="first_vehicle_affected",
    )

    finished_fire_4_parm_15_sept_previous = make_ongoing_ops_changed_event_data(
        address_area="PARM",
        cause="fire",
        raw_operation_id="4",
        timestamp=DateStr("2020-09-15T12:00:00.000Z"),
        status="first_vehicle_affected",
    )
    finished_fire_4_parm_15_sept_latest = make_ongoing_ops_changed_event_data(
        address_area="PARM",
        cause="fire",
        raw_operation_id="4",
        timestamp=DateStr("2020-09-15T13:00:00.000Z"),
        status="all_vehicles_released",
    )

    ongoing_op_events_repo.ongoing_op_datas = [
        still_ongoing_other_3_parm_15_sept,
        finished_victim_4_parm_15_sept,
        finished_fire_4_parm_15_sept_previous,
        finished_fire_4_parm_15_sept_latest,
        victim_1_stou_25_sept,
        victim_4_stou_21_sept,
        fire_2_chpt_20_sept,
    ]
    getOps = GetOps(ongoing_op_events_repo=ongoing_op_events_repo)
    actual_ongoing_op_changed_event_data = getOps.execute()  # six days
    assert (
        len(actual_ongoing_op_changed_event_data) == 10
    )  # Actually, it might me 4 if we considered the operation that were opened before the "from_date"
    # and did not changed since then. But it's not very important for our usage, since at worth, one CSTC will be gray.

    expected_ongoing_op_changed_event_data = [
        victim_1_stou_25_sept,
        finished_fire_4_parm_15_sept_latest,
        finished_victim_4_parm_15_sept,
        fire_2_chpt_20_sept,
        still_ongoing_other_3_parm_15_sept,
        finished_victim_4_parm_15_sept,
        finished_fire_4_parm_15_sept_previous,
        victim_1_stou_25_sept,
        victim_4_stou_21_sept,
        fire_2_chpt_20_sept,
    ]

    assert (
        actual_ongoing_op_changed_event_data == expected_ongoing_op_changed_event_data
    )
