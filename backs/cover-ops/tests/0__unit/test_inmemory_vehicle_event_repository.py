import pytest
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_data,
)


def test_can_add_to_vehicle_events_repository():
    vehicle_events_repository = InMemoryVehicleEventsRepository(purge=True)
    vehicle_event_data = make_vehicle_event_data()
    vehicle_events_repository.add([vehicle_event_data])

    assert vehicle_events_repository.get_all() == [vehicle_event_data]
