from dataclasses import asdict
from typing import List

from cover_ops.domain.entities.event_entities import VehicleEventData
from cover_ops.domain.ports.api_state_cache import InMemoryApiStateCache
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.converter_gateway import (
    ConvertersSnapshot,
    InMemoryConvertersGateway,
)
from cover_ops.domain.ports.domain_event_bus import InMemoryDomainEventBus
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.set_cover_ops_repositories_from_snapshot import (
    SetCoverOpsRepositoriesFromSnapshot,
)
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_data,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)

from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    OmnibusInfos,
    RawVehicleId,
    VehicleRole,
    VehicleStatus,
)
from shared.factories.operation_event_factory import (
    make_opening_infos_kwargs,
    make_operation_event_data,
)
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr
from shared.helpers.uuid import CustomUuid
from shared.test_utils.spy_on_topic import spy_on_topic


async def prepare_use_case():
    domain_event_bus = InMemoryDomainEventBus(clock=CustomClock())
    omnibus_repo = InMemoryOmnibusEventsRepository()
    vehicle_repo = InMemoryVehicleEventsRepository(purge=True)
    availablitiy_repo = InMemoryAvailabilityEventsRepository(purge=True)
    operation_repo = InMemoryOperationEventsRepository(purge=True)
    ongoingops_repo = InMemoryOngoingOpEventsRepository(purge=True)
    api_state_cache = InMemoryApiStateCache()
    converters_gateway = InMemoryConvertersGateway()

    use_case = SetCoverOpsRepositoriesFromSnapshot(
        uuid=CustomUuid(),
        domain_event_bus=domain_event_bus,
        vehicle_events_repo=vehicle_repo,
        availability_events_repo=availablitiy_repo,
        omnibus_events_repo=omnibus_repo,
        operation_events_repo=operation_repo,
        ongoingops_events_repo=ongoingops_repo,
        converters_gateway=converters_gateway,
        api_state_cache=api_state_cache,
    )

    return (
        use_case,
        vehicle_repo,
        availablitiy_repo,
        omnibus_repo,
        operation_repo,
        ongoingops_repo,
        converters_gateway,
        domain_event_bus,
    )


async def test_add_to_vehicle_repository_and_operation_repository_entities_from_bspp_areas():
    # Prepare
    (
        use_case,
        vehicle_repo,
        _,
        _,
        operation_repo,
        _,
        converters_gateway,
        domain_event_bus,
    ) = await prepare_use_case()

    vehicle_data_from_bspp = make_vehicle_event_data()
    snapshot_vehicle_event_datas = [
        vehicle_data_from_bspp,
        make_vehicle_event_data(home_area="SDIS"),
    ]
    snapshot_operation_event_datas = [
        make_operation_event_data(),
    ]
    converters_gateway.set_snapshot(
        ConvertersSnapshot(snapshot_vehicle_event_datas, snapshot_operation_event_datas)
    )

    # Act
    await use_case.execute()

    # Assert
    assert vehicle_repo.vehicle_datas == [vehicle_data_from_bspp]

    assert len(operation_repo.operation_datas) == 1

    expected_operation_data = snapshot_operation_event_datas[0]
    expected_operation_data.status = "opened"
    assert operation_repo.operation_datas[0] == expected_operation_data


async def test_compute_availabilities_and_add_them_to_repo():
    # Prepare
    (
        use_case,
        _,
        availablitiy_repo,
        _,
        _,
        _,
        converters_gateway,
        domain_event_bus,
    ) = await prepare_use_case()

    snapshot_timestamp = DateStr("2020-01-01T12:00:00.000Z")

    # Snapshot says :
    # - STOU : 2 VSAV available, 1 PSE in service
    # - CHPT : 1 VSAV available, 1 EPA not available
    # - SDIS : 1 VSAV available

    data_1 = make_vehicle_event_data(
        raw_vehicle_id="vsav_stou_1",
        role="vsav_solo",
        status="arrived_at_home",
        timestamp=snapshot_timestamp,
        home_area="STOU",
    )
    data_2 = make_vehicle_event_data(
        raw_vehicle_id="vsav_stou_2",
        role="vsav_solo",
        status="arrived_at_home",
        home_area="STOU",
    )

    data_3 = make_vehicle_event_data(
        raw_vehicle_id="pse_stou_1",
        role="pse_solo",
        status="departed_to_intervention",
        timestamp=snapshot_timestamp,
        home_area="STOU",
    )

    data_4 = make_vehicle_event_data(
        raw_vehicle_id="vsav_chpt_1",
        role="vsav_solo",
        status="misc_available",
        timestamp=snapshot_timestamp,
        home_area="CHPT",
    )

    data_5 = make_vehicle_event_data(
        raw_vehicle_id="epa_chpt_1",
        role="epa_epsa",
        status="misc_unavailable",
        timestamp=snapshot_timestamp,
        home_area="CHPT",
    )

    data_6 = make_vehicle_event_data(
        raw_vehicle_id="vsav_sdis",
        role="vsav_solo",
        status="misc_available",
        timestamp=snapshot_timestamp,
        home_area="SDIS",
    )
    snapshot_vehicle_event_datas = [data_1, data_2, data_3, data_4, data_5, data_6]
    converters_gateway.set_snapshot(
        ConvertersSnapshot(snapshot_vehicle_event_datas, [])
    )

    # Act
    await use_case.execute()

    # Assert
    vsav_stou_availability = Availability(
        available=2, on_operation=0, unavailable=0, unavailable_omnibus=0, recoverable=0
    )
    vsav_chpt_availability = Availability(
        available=1, on_operation=0, unavailable=0, unavailable_omnibus=0, recoverable=0
    )
    vsav_bspp_availability = Availability(
        available=3, on_operation=0, unavailable=0, unavailable_omnibus=0, recoverable=0
    )
    pse_stou_availability = Availability(
        available=0, on_operation=1, unavailable=0, unavailable_omnibus=0, recoverable=0
    )
    pse_bspp_availability = Availability(
        available=0, on_operation=1, unavailable=0, unavailable_omnibus=0, recoverable=0
    )
    epa_chpt_availability = Availability(
        available=0, on_operation=0, unavailable=1, unavailable_omnibus=0, recoverable=0
    )
    epa_bspp_availability = Availability(
        available=0, on_operation=0, unavailable=1, unavailable_omnibus=0, recoverable=0
    )

    def make_availability_changed_event_data_given_vehicle_data_and_availabilities(
        vehicle_event_data: VehicleEventData,
        bspp_availability: Availability,
        area_availability: Availability,
    ):
        return make_availability_changed_event_data(
            timestamp=snapshot_timestamp,
            area_availability=area_availability,
            bspp_availability=bspp_availability,
            home_area=vehicle_event_data.home_area,
            latest_event_data=vehicle_event_data,
            raw_vehicle_id=vehicle_event_data.raw_vehicle_id,
            role=vehicle_event_data.role,
            previous_raw_operation_id=vehicle_event_data.raw_operation_id,
            availability_changed=True,
        )

    expected_availablity_datas: List[AvailabilityChangedEventData] = [
        make_availability_changed_event_data_given_vehicle_data_and_availabilities(
            vehicle_event_data=data_1,
            area_availability=vsav_stou_availability,
            bspp_availability=vsav_bspp_availability,
        ),
        make_availability_changed_event_data_given_vehicle_data_and_availabilities(
            vehicle_event_data=data_2,
            area_availability=vsav_stou_availability,
            bspp_availability=vsav_bspp_availability,
        ),
        make_availability_changed_event_data_given_vehicle_data_and_availabilities(
            vehicle_event_data=data_3,
            area_availability=pse_stou_availability,
            bspp_availability=pse_bspp_availability,
        ),
        make_availability_changed_event_data_given_vehicle_data_and_availabilities(
            vehicle_event_data=data_4,
            area_availability=vsav_chpt_availability,
            bspp_availability=vsav_bspp_availability,
        ),
        make_availability_changed_event_data_given_vehicle_data_and_availabilities(
            vehicle_event_data=data_5,
            area_availability=epa_chpt_availability,
            bspp_availability=epa_bspp_availability,
        ),
    ]
    assert len(availablitiy_repo.availability_datas) == 5
    assert sorted(
        availablitiy_repo.availability_datas, key=lambda data: data.raw_vehicle_id
    ) == sorted(expected_availablity_datas, key=lambda data: data.raw_vehicle_id)


def make_couple_vehicle_event_datas(
    couple_id: int,
    vsav_status: VehicleStatus,
    pse_status: VehicleStatus,
    pse_role: VehicleRole,
    home_area: Area = "STOU",
):
    snapshot_timestamp = DateStr("2020-01-01T12:00:00.000Z")
    return [
        make_vehicle_event_data(
            raw_vehicle_id=f"omni_{couple_id}_vsav",
            role="vsav_omni",
            status=vsav_status,
            timestamp=snapshot_timestamp,
            home_area=home_area,
            omnibus=OmnibusInfos(
                partner_raw_vehicle_id=RawVehicleId(f"omni_{couple_id}_pse")
            ),
        ),
        make_vehicle_event_data(
            raw_vehicle_id=f"omni_{couple_id}_pse",
            role=pse_role,
            status=pse_status,
            timestamp=snapshot_timestamp,
            home_area=home_area,
            omnibus=OmnibusInfos(
                partner_raw_vehicle_id=RawVehicleId(f"omni_{couple_id}_vsav")
            ),
        ),
    ]


def create_omnibus_vehicle_events():
    return [
        *make_couple_vehicle_event_datas(
            couple_id=1,
            vsav_status="arrived_at_home",
            pse_status="arrived_at_home",
            pse_role="pse_omni_san",
        ),
        *make_couple_vehicle_event_datas(
            couple_id=2,
            vsav_status="arrived_at_home",
            pse_status="departed_to_intervention",
            pse_role="pse_omni_san",
        ),
        *make_couple_vehicle_event_datas(
            couple_id=3,
            vsav_status="arrived_at_hospital",
            pse_status="transport_to_hospital",
            pse_role="pse_omni_san",
        ),
        *make_couple_vehicle_event_datas(
            couple_id=4,
            vsav_status="unavailable_omnibus",
            pse_status="transport_to_hospital",
            pse_role="pse_omni_pump",
        ),
        *make_couple_vehicle_event_datas(
            couple_id=5,
            vsav_status="arrived_at_hospital",
            pse_status="misc_available",
            pse_role="pse_omni_san",
            home_area="CHPT",
        ),
        *make_couple_vehicle_event_datas(
            couple_id=6,
            vsav_status="arrived_on_intervention",
            pse_status="unavailable_lacks_staff",
            pse_role="pse_omni_pump",
            home_area="CHPT",
        ),
        make_vehicle_event_data(
            raw_vehicle_id="vsav_solo_7",
            role="vsav_solo",
            status="arrived_at_home",
            home_area="CHPT",
        ),
    ]


async def test_compute_omnibus_details_and_add_them_to_repo():
    # Prepare
    (
        use_case,
        vehicle_repo,
        availablitiy_repo,
        omnibus_repo,
        operation_repo,
        ongoingops_repo,
        converters_gateway,
        _,
    ) = await prepare_use_case()

    # Snapshot says :
    # - Affected to STOU
    #   - omni couple 1: vsav_1, pse_1 as pse_omni_san both available at home                # both_available
    #   - omni couple 2: vsav_2 at home, pse_2 as pse_omni_san on operation                  # pse_on_inter_san
    #   - omni couple 3: vsav_3 on operation, pse_3 as pse_omni_san on operation             # both_on_inter_san
    #   - omni couple 4: vsav_4 on unavailable omnibus, pse_4 as pse_omni_pump on operation  # pse_on_inter_fire

    # - Affected to CHPT
    #   - omni couple 5: vsav_5 on operation, pse_5 as pse_omni_san available                # vsav_on_inter_san
    #   - omni couple 6: vsav_6 on operation, pse_6 as pse_omni_san unavailable              # dangling
    #   - vsav_solo : vsav_7 at home (should not interfer )

    snapshot_vehicle_event_datas = create_omnibus_vehicle_events()

    converters_gateway.set_snapshot(
        ConvertersSnapshot(snapshot_vehicle_event_datas, [])
    )

    (
        data_1_vsav,
        data_1_pse,
        data_2_vsav,
        data_2_pse,
        data_3_vsav,
        data_3_pse,
        data_4_vsav,
        data_4_pse,
        data_5_vsav,
        data_5_pse,
        data_6_vsav,
        data_6_pse,
        _,
    ) = snapshot_vehicle_event_datas

    # Act
    await use_case.execute()

    # Assert
    omibus_details = omnibus_repo.get_omnibus_details()
    assert len(omibus_details) == 6
    assert omibus_details == [
        OmnibusDetail(
            vsav_id=data_1_vsav.raw_vehicle_id,
            pse_id=data_1_pse.raw_vehicle_id,
            area="STOU",
            balance_kind="both_available",
        ),
        OmnibusDetail(
            vsav_id=data_2_vsav.raw_vehicle_id,
            pse_id=data_2_pse.raw_vehicle_id,
            area="STOU",
            balance_kind="pse_on_inter_san",
        ),
        OmnibusDetail(
            vsav_id=data_3_vsav.raw_vehicle_id,
            pse_id=data_3_pse.raw_vehicle_id,
            area="STOU",
            balance_kind="both_on_inter_san",
        ),
        OmnibusDetail(
            vsav_id=data_4_vsav.raw_vehicle_id,
            pse_id=data_4_pse.raw_vehicle_id,
            area="STOU",
            balance_kind="pse_on_inter_fire",
        ),
        OmnibusDetail(
            vsav_id=data_5_vsav.raw_vehicle_id,
            pse_id=data_5_pse.raw_vehicle_id,
            area="CHPT",
            balance_kind="vsav_on_inter_san",
        ),
        OmnibusDetail(
            vsav_id=data_6_vsav.raw_vehicle_id,
            pse_id=data_6_pse.raw_vehicle_id,
            area="CHPT",
            balance_kind="dangling",
        ),
    ]


async def test_compute_ongoing_ops_and_add_them_to_repo():
    # Prepare
    (
        use_case,
        _,
        _,
        _,
        _,
        ongoingops_repo,
        converters_gateway,
        _,
    ) = await prepare_use_case()

    snapshot_timestamp = DateStr("2020-01-01T12:00:00.000Z")

    # Snapshot says :
    # - STOU
    #    - 1 victim_rescue ongoing (with vsav_1 and vsav_2 affected)
    #    - 1 fire opened (without any affected vehicles)
    # - CHPT : 1 VSAV available, 1 EPA not available
    #    - 1 fire ongoing (with pse_1 affected)

    # STOU : 1 victim_rescue ongoing (with vsav_1 and vsav_2 affected)
    opening_info_kwargs = dict(
        latitude=1,
        longitude=2,
        departure_criteria="some urgent stuff",
        raw_procedure="R",
        raw_cause="explained cause",
        address="1 place jules renard 75017 Paris",
    )

    id_ope_1 = RawOperationId("ope_1")
    ope_1 = make_operation_event_data(
        timestamp=snapshot_timestamp,
        raw_operation_id=id_ope_1,
        status="opened",
        address_area="STOU",
        cause="victim",
        **opening_info_kwargs,
    )
    veh1_ope_1 = make_vehicle_event_data(
        raw_vehicle_id="vsav_1",
        timestamp=snapshot_timestamp,
        status="arrived_on_intervention",
        raw_operation_id=id_ope_1,
    )
    veh2_ope_1 = make_vehicle_event_data(
        raw_vehicle_id="vsav_2",
        timestamp=snapshot_timestamp,
        status="arrived_on_intervention",
        raw_operation_id=id_ope_1,
    )

    veh3_ope_1 = make_vehicle_event_data(
        raw_vehicle_id="vsav_3",
        timestamp=snapshot_timestamp,
        status="arrived_at_home",
        raw_operation_id=id_ope_1,
    )

    # STOU : 1 fire opened (without any affected vehicles)
    ope_2 = make_operation_event_data(
        timestamp=snapshot_timestamp,
        raw_operation_id="op_2",
        status="opened",
        address_area="STOU",
        cause="fire",
        **opening_info_kwargs,
    )

    # CHPT : 1 fire ongoing (with pse_1 affected)
    id_ope_3 = RawOperationId("op_3")
    ope_3 = make_operation_event_data(
        timestamp=snapshot_timestamp,
        raw_operation_id=id_ope_3,
        status="opened",
        address_area="CHPT",
        cause="fire",
        **opening_info_kwargs,
    )
    veh1_ope_3 = make_vehicle_event_data(
        raw_vehicle_id="pse_1",
        timestamp=snapshot_timestamp,
        status="departed_to_intervention",
        raw_operation_id=id_ope_3,
    )

    converters_gateway.set_snapshot(
        ConvertersSnapshot(
            operations=[ope_1, ope_2, ope_3],
            vehicles=[veh1_ope_1, veh2_ope_1, veh3_ope_1, veh1_ope_3],
        )
    )

    # Act
    await use_case.execute()

    # Assert
    ongoing_op_datas = ongoingops_repo._ongoing_op_datas
    assert len(ongoing_op_datas) == 3

    expected_ongoing_op_datas = [
        make_ongoing_ops_changed_event_data(
            raw_operation_id=id_ope_1,
            timestamp=snapshot_timestamp,
            cause="victim",
            address_area="STOU",
            status="some_affected_vehicle_changed",
            affected_vehicles=[veh1_ope_1.raw_vehicle_id, veh2_ope_1.raw_vehicle_id],
            opening_timestamp=snapshot_timestamp,  # ??
            **opening_info_kwargs,
        ),
        make_ongoing_ops_changed_event_data(
            raw_operation_id=ope_2.raw_operation_id,
            timestamp=snapshot_timestamp,
            cause="fire",
            address_area="STOU",
            status="opened",
            affected_vehicles=[],
            opening_timestamp=snapshot_timestamp,  # ??
            **opening_info_kwargs,
        ),
        make_ongoing_ops_changed_event_data(
            raw_operation_id=id_ope_3,
            timestamp=snapshot_timestamp,
            cause="fire",
            address_area="CHPT",
            status="first_vehicle_affected",
            affected_vehicles=[veh1_ope_3.raw_vehicle_id],
            opening_timestamp=snapshot_timestamp,  # ??
            **opening_info_kwargs,
        ),
    ]
    assert ongoing_op_datas == expected_ongoing_op_datas


async def test_raises_an_event_when_execution_finishes():
    # Prepare
    (use_case, _, _, _, _, _, _, domain_event_bus) = await prepare_use_case()

    actual_events = await spy_on_topic(
        domain_event_bus, "set_repositories_from_snapshot_executed"
    )
    await use_case.execute()
    assert len(actual_events) == 1
