import asyncio

from tests.utils.prepare_update_omnibus import *

from shared.data_transfert_objects.vehicle_event_data import OmnibusInfos
from shared.factories.vehicle_event_factory import make_vehicle_event

loop = asyncio.get_event_loop()


def test_vehicle_not_omnibus():

    (
        update_omnibus,
        published_events,
        domain_event_bus,
        omnibus_repo,
        _,
    ) = loop.run_until_complete(prepare_update_omnibus())
    event = make_vehicle_event(omnibus=None)
    asyncio.run(update_omnibus.execute(event))
    assert len(published_events) == 0


def test_pse_affected_as_pse():
    initial_vehicle_events = [vsav_lacks_staff_omnibus_event, pse_as_pse_home_event]
    vehicle_event = pse_affected_as_pump_event

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        initial_omnibus_details=[],
        vehicle_event=vehicle_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_fire"
            )
        ],
    )


def test_pse_affected_as_only_pse_on_inter():
    initial_vehicle_events = [vsav_lacks_staff_event, pse_as_pse_home_event]
    vehicle_event = pse_affected_as_pump_event

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        initial_omnibus_details=[],
        vehicle_event=vehicle_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "only_pse_on_inter_fire",
            )
        ],
    )


def test_vsav_lacks_staff_omnibus_while_pse_on_inter():
    initial_vehicle_events = [
        vsav_back_home_event,
        pse_affected_as_pump_event,
    ]
    vehicle_event = vsav_lacks_staff_omnibus_event

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        initial_omnibus_details=[],
        vehicle_event=vehicle_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_fire"
            )
        ],
    )


def test_vsav_affected_but_pse_not_in_cache():
    vehicle_event = make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_raw_vehicle_id,
        ),
        status="selected",
    )

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[],
        initial_omnibus_details=[],
        vehicle_event=vehicle_event,
        expected_omnibus_details=[
            OmnibusDetail(vsav_raw_vehicle_id, None, "STOU", "dangling")
        ],
    )


def test_vsav_becomes_omnibus_and_is_affected_while_pse_at_home():

    vsav_no_omnibus = make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_solo",
        status="misc_unavailable",
        omnibus=None,
    )

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_no_omnibus, pse_as_vsav_home_event],
        initial_omnibus_details=[],
        vehicle_event=vsav_affected_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "vsav_on_inter_san"
            )
        ],
    )


def test_pse_downgraded_because_vsav_has_been_affected():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_as_pse_home_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="dangling",
                area="STOU",
            )
        ],
        vehicle_event=pse_downgraded_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "vsav_on_inter_san"
            )
        ],
    )


def test_pse_affected_on_inter_san():

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_as_vsav_home_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="vsav_on_inter_san",
                area="STOU",
            )
        ],
        vehicle_event=pse_to_inter_san_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "both_on_inter_san"
            )
        ],
    )


def test_vsav_home_while_pse_still_on_inter_san():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_to_inter_san_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="both_on_inter_san",
                area="STOU",
            )
        ],
        vehicle_event=vsav_back_home_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_san"
            )
        ],
    )


def test_pse_joins_vsav_back_home():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_back_home_event, pse_to_inter_san_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="pse_on_inter_san",
                area="STOU",
            )
        ],
        vehicle_event=pse_san_back_home_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "both_available"
            )
        ],
    )


def test_pse_only_available_for_fire():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_back_home_event, pse_pump_back_home_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="both_available",
                area="STOU",
            )
        ],
        vehicle_event=vsav_lacks_staff_event,
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "only_pse_available"
            )
        ],
    )


vsav_detached_event = make_vehicle_event(
    role="vsav_solo", raw_vehicle_id=vsav_raw_vehicle_id
)


def test_vsav_detached():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_back_home_event, pse_pump_back_home_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="both_available",
                area="STOU",
            )
        ],
        vehicle_event=vsav_detached_event,
        expected_omnibus_details=[
            OmnibusDetail(None, pse_raw_vehicle_id, "STOU", "dangling")
        ],
    )


pse_detached_event = make_vehicle_event(
    role="pse_solo", raw_vehicle_id=pse_raw_vehicle_id
)


def test_pse_detached():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_back_home_event, pse_pump_back_home_event],
        initial_omnibus_details=[
            OmnibusDetail(
                vsav_id=vsav_raw_vehicle_id,
                pse_id=pse_raw_vehicle_id,
                balance_kind="both_available",
                area="STOU",
            )
        ],
        vehicle_event=pse_detached_event,
        expected_omnibus_details=[
            OmnibusDetail(vsav_raw_vehicle_id, None, "STOU", "dangling")
        ],
    )
