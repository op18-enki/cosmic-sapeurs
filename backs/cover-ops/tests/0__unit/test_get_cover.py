from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.use_cases.get_cover import GetCover
from tests.utils.availability_changed_event_factory import (
    make_availability,
    make_availability_changed_event_data,
)

from shared.helpers.date import DateStr


def test_get_cover_if_empty():
    counts_events_repo = InMemoryAvailabilityEventsRepository(purge=True)
    counts_events_repo.availability_datas = []
    getCover = GetCover(
        availability_events_repo=counts_events_repo,
    )
    counts_entities = getCover.execute()
    assert len(counts_entities) == 0


def test_get_cover():
    counts_events_repo = InMemoryAvailabilityEventsRepository(purge=True)
    vsav_1_stou_19_sept = make_availability_changed_event_data(
        area_availability=make_availability(available=8),
        raw_vehicle_id="1",
        home_area="STOU",
        timestamp=DateStr("2020-09-19T13:00:00.000Z"),
        role="vsav_solo",
    )
    vsav_2_stou_26_sept = make_availability_changed_event_data(
        area_availability=make_availability(on_operation=3),
        raw_vehicle_id="2",
        timestamp=DateStr("2020-09-26T13:00:00.000Z"),
        home_area="PARM",
        role="vsav_solo",
    )

    pse_3_CHPT_20_sept = make_availability_changed_event_data(
        area_availability=make_availability(available=2),
        raw_vehicle_id="3",
        timestamp=DateStr("2020-09-20T12:00:00.000Z"),
        home_area="CHPT",
        role="pse_solo",
    )
    vsav_1_STOU_20_sept = make_availability_changed_event_data(
        area_availability=make_availability(available=12),
        raw_vehicle_id="1",
        timestamp=DateStr("2020-09-20T13:00:00.000Z"),
        home_area="STOU",
        role="vsav_solo",
    )
    vsav_1_STOU_25_sept = make_availability_changed_event_data(
        area_availability=make_availability(available=3),
        raw_vehicle_id="1",
        timestamp=DateStr("2020-09-25T13:00:00.000Z"),
        home_area="STOU",
        role="vsav_solo",
    )

    counts_events_repo.availability_datas = [
        vsav_2_stou_26_sept,
        vsav_1_stou_19_sept,
        vsav_1_STOU_25_sept,
        vsav_1_STOU_20_sept,
        pse_3_CHPT_20_sept,
    ]
    getCover = GetCover(availability_events_repo=counts_events_repo)
    actual_availability_changed_event_data = getCover.execute()

    expected_availability_changed_event_data = [
        pse_3_CHPT_20_sept,
        vsav_1_STOU_25_sept,
        vsav_2_stou_26_sept,
    ]
    assert len(actual_availability_changed_event_data) == len(
        expected_availability_changed_event_data
    )
    assert (
        actual_availability_changed_event_data
        == expected_availability_changed_event_data
    )
