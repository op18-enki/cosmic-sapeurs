import asyncio
from typing import Optional

from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.domain_event_bus import InMemoryDomainEventBus
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.notify_when_vehicle_availability_changed_affects_operation import (
    NotifyWhenVehicleAvailabilityChangedAffectsOperation,
)
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event,
    make_availability_changed_event_data,
)

from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.data_transfert_objects.vehicle_event_data import (
    VehicleEventData,
    VehicleStatus,
)
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr
from shared.helpers.uuid import CustomUuid, uuid4
from shared.sapeurs_events import AvailabilityChangedEvent
from shared.test_utils.spy_on_topic import spy_on_topic

expected_notification_uuid_1 = "notification_uuid_1"
expected_notification_uuid_2 = "notification_uuid_2"
expected_notification_uuid_3 = "notification_uuid_3"
expected_dispatched_at = DateStr("2021-01-03T13:00:00.000Z")
operation_1_id = RawOperationId("operation_1")
vehicle_A_id = RawVehicleId("vehicle_A")
vehicle_B_id = RawVehicleId("vehicle_B")

loop = asyncio.get_event_loop()


async def prepare_use_case():
    custom_clock = CustomClock()
    custom_uuid = CustomUuid()
    event_bus = InMemoryDomainEventBus(custom_clock)
    availability_repo = InMemoryAvailabilityEventsRepository(purge=True)
    operation_repo = InMemoryOperationEventsRepository(purge=True)

    notify_when_vehicle_availability_changed_affects_operation = (
        NotifyWhenVehicleAvailabilityChangedAffectsOperation(
            domain_event_bus=event_bus,
            uuid=custom_uuid,
            availability_repo=availability_repo,
            operation_repo=operation_repo,
        )
    )
    custom_uuid.set_next_uuids(
        [
            expected_notification_uuid_1,
            expected_notification_uuid_2,
            expected_notification_uuid_3,
        ]
    )
    custom_clock.set_next_date(expected_dispatched_at)

    published_events = await spy_on_topic(event_bus, "operation_changed_status")
    return (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    )


def make_availability_changed_event_with_specified_vehicle_status(
    timestamp: DateStr,
    raw_vehicle_id: RawVehicleId,
    raw_operation_id: Optional[RawOperationId],
    previous_raw_operation_id: Optional[RawOperationId],
    status: VehicleStatus,
    availability_changed: bool = True,
) -> AvailabilityChangedEvent:
    latest_event_data = make_vehicle_event_data(
        raw_vehicle_id=raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        timestamp=timestamp,
    )
    return make_availability_changed_event(
        timestamp=timestamp,
        latest_event_data=latest_event_data,
        previous_raw_operation_id=previous_raw_operation_id,
        availability_changed=availability_changed,
    )


def test_vehicle_became_on_operation_with_new_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_selected_timestamp = DateStr("2020-01-01T12:00:00.000Z")
    availability_vehicle_selected_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=None,
            status="selected",
            timestamp=vehicle_selected_timestamp,
            previous_raw_operation_id=None,
        )
    )
    vehicle_departed_timestamp = DateStr("2020-01-01T13:00:00.000Z")
    availability_vehicle_departed_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="departed_to_intervention",
            timestamp=vehicle_departed_timestamp,
            previous_raw_operation_id=None,
        )
    )
    # ensure availability repo is up to date (which is update_cover's job)
    availability_repo.add([availability_vehicle_departed_event.data])

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_selected_event
        )
    )
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_departed_event
        )
    )
    assert len(published_events) == 1
    assert published_events[0] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        status="first_vehicle_affected",
        timestamp=vehicle_departed_timestamp,
        affected_vehicles=[vehicle_A_id],
    )


def test_last_affected_vehicle_not_on_operation_anymore_with_known_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_arrived_at_home_timestamp = DateStr("2020-01-01T13:00:00.000Z")
    availability_vehicle_arrived_at_home_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="arrived_at_home",
            timestamp=vehicle_arrived_at_home_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )
    # ensure availability_repo is up to date (which is update_cover's job)
    availability_repo.add([availability_vehicle_arrived_at_home_event.data])

    notify_when_vehicle_availability_changed_affects_operation.operation_repo.set_operations_notified_from_vehicle_changes(
        {operation_1_id}
    )

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_arrived_at_home_event
        )
    )
    assert len(published_events) == 1
    assert published_events[0] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_arrived_at_home_timestamp,
        status="all_vehicles_released",
    )


def test_last_affected_vehicle_on_operation_but_with_an_other_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_departed_to_intervention_timestamp = DateStr("2020-01-01T13:00:00.000Z")
    new_operation_id = RawOperationId("new_operation")
    availability_vehicle_arrived_at_home_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=new_operation_id,
            status="departed_to_intervention",
            timestamp=vehicle_departed_to_intervention_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )
    # ensure availability_repo is up to date (which is update_cover's job)
    availability_repo.add([availability_vehicle_arrived_at_home_event.data])

    notify_when_vehicle_availability_changed_affects_operation.operation_repo.set_operations_notified_from_vehicle_changes(
        {operation_1_id}
    )

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_arrived_at_home_event
        )
    )

    assert len(published_events) == 2
    assert published_events[0] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_departed_to_intervention_timestamp,
        status="all_vehicles_released",
    )
    assert published_events[1] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_2,
        source="sapeurs_notify",
        raw_operation_id=new_operation_id,
        timestamp=vehicle_departed_to_intervention_timestamp,
        status="first_vehicle_affected",
        affected_vehicles=[vehicle_A_id],
    )


def test_last_vehicle_became_selected_with_operation_id_is_None():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_selected_timestamp = DateStr("2020-01-01T12:00:00.000Z")
    availability_vehicle_selected_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=None,
            status="selected",
            timestamp=vehicle_selected_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )

    # ensure availability_repo is up to date (which is update_cover's job)
    availability_repo.add([availability_vehicle_selected_event.data])

    notify_when_vehicle_availability_changed_affects_operation.operation_repo.set_operations_notified_from_vehicle_changes(
        {operation_1_id}
    )

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_selected_event
        )
    )

    assert len(published_events) == 1
    assert published_events[0] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        status="all_vehicles_released",
        timestamp=vehicle_selected_timestamp,
    )


def test_one_vehicle_amongst_others_not_on_operation_anymore_with_known_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_broken_timestamp = DateStr("2020-01-01T13:00:00.000Z")
    availability_vehicle_broken_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="broken",
            timestamp=vehicle_broken_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )

    # ensure availability_repo is up to date (which is update_cover's job)
    availability_repo.add([availability_vehicle_broken_event.data])
    availability_repo.add(
        [
            make_availability_changed_event_data(
                raw_vehicle_id=vehicle_B_id,
                latest_event_data=make_vehicle_event_data(
                    status="arrived_on_intervention",
                    raw_operation_id=operation_1_id,
                    raw_vehicle_id=vehicle_B_id,
                ),
            )
        ]
    )

    operation_repo.set_operations_notified_from_vehicle_changes({operation_1_id})

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_broken_event
        )
    )

    assert len(published_events) == 1
    assert published_events[0] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_broken_timestamp,
        status="some_affected_vehicle_changed",
        affected_vehicles=[vehicle_B_id],
    )
    assert operation_repo.get_operations_notified_from_vehicle_changes() == {
        operation_1_id
    }
    # now same vehicle changes status but not availability : Should not publish any new event
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            make_availability_changed_event_with_specified_vehicle_status(
                raw_vehicle_id=vehicle_A_id,
                raw_operation_id=operation_1_id,
                status="misc_unavailable",
                timestamp=vehicle_broken_timestamp,
                previous_raw_operation_id=operation_1_id,
                availability_changed=False,
            )
        )
    )
    assert len(published_events) == 1


def test_last_vehicle_unaffected_and_then_reaffected():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        vehicle_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_departed_to_intervention_timestamp = DateStr("2020-01-01T13:27:00.000Z")
    availability_vehicle_departed_to_intervention_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="departed_to_intervention",
            timestamp=vehicle_departed_to_intervention_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )
    vehicle_undefined_timestamp = DateStr("2020-01-01T13:32:00.000Z")
    availability_vehicle_undefined_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="undefined",  # for example, asking for address
            timestamp=vehicle_undefined_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )

    vehicle_arrived_on_intervention_timestamp = DateStr("2020-01-01T13:33:00.000Z")
    availability_vehicle_arrived_on_intervention_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="arrived_on_intervention",
            timestamp=vehicle_arrived_on_intervention_timestamp,
            previous_raw_operation_id=operation_1_id,
        )
    )

    # vehicle in service and affected to this intervention
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_departed_to_intervention_event
        )
    )
    assert len(published_events) == 1
    expected_operation_event_first_affected = make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_1,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_departed_to_intervention_timestamp,
        status="first_vehicle_affected",
        affected_vehicles=[vehicle_A_id],
    )
    assert published_events[0] == expected_operation_event_first_affected

    notify_when_vehicle_availability_changed_affects_operation.operation_repo.set_operations_notified_from_vehicle_changes(
        {operation_1_id}
    )

    # vehicle not in service anymore
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_undefined_event
        )
    )
    assert len(published_events) == 2
    expected_operation_event_all_released = make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_2,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_undefined_timestamp,
        status="all_vehicles_released",
    )
    assert published_events[1] == expected_operation_event_all_released

    notify_when_vehicle_availability_changed_affects_operation.operation_repo.set_operations_notified_from_vehicle_changes(
        set()
    )

    # vehicle back to in service and affected to this intervention
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            availability_vehicle_arrived_on_intervention_event
        )
    )

    assert len(published_events) == 3
    assert published_events[2] == make_operation_event(
        dispatched_at=expected_dispatched_at,
        uuid=expected_notification_uuid_3,
        source="sapeurs_notify",
        raw_operation_id=operation_1_id,
        timestamp=vehicle_arrived_on_intervention_timestamp,
        status="first_vehicle_affected",
        affected_vehicles=[vehicle_A_id],
    )


def test_available_vehicle_affected_to_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        vehicle_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_available_but_affected_timestamp_event = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="misc_available",
            timestamp=DateStr("2020-01-01T13:33:00.000Z"),
            previous_raw_operation_id=operation_1_id,
        )
    )

    # vehicle back to in service and affected to this intervention
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            vehicle_available_but_affected_timestamp_event
        )
    )
    assert len(published_events) == 0


def test_available_vehicle_changes_operation():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        availability_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_changes_from_operation_1_to_2 = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=RawOperationId("operation_2"),
            status="arrived_on_intervention",
            timestamp=DateStr("2020-01-01T15:33:00.000Z"),
            previous_raw_operation_id=RawOperationId("operation_1"),
        )
    )
    # Vehicle A first arrived on operation 1
    availability_repo.add([vehicle_changes_from_operation_1_to_2.data])

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            vehicle_changes_from_operation_1_to_2
        )
    )
    assert len(published_events) == 1
    assert published_events[-1].data.status == "first_vehicle_affected"


def test_raw_inter_id_turns_None():
    (
        notify_when_vehicle_availability_changed_affects_operation,
        vehicle_repo,
        operation_repo,
        published_events,
    ) = loop.run_until_complete(prepare_use_case())

    vehicle_affected_to_operation_1 = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="arrived_on_intervention",
            timestamp=DateStr("2020-01-01T15:33:00.000Z"),
            previous_raw_operation_id=None,
        )
    )

    vehicle_changes_from_operation_1_to_None = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=None,
            status="arrived_on_intervention",
            timestamp=DateStr("2020-01-01T16:33:00.000Z"),
            previous_raw_operation_id=operation_1_id,
        )
    )
    vehicle_on_operation_1 = (
        make_availability_changed_event_with_specified_vehicle_status(
            raw_vehicle_id=vehicle_A_id,
            raw_operation_id=operation_1_id,
            status="arrived_on_intervention",
            timestamp=DateStr("2020-01-01T17:33:00.000Z"),
            previous_raw_operation_id=None,
        )
    )
    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            vehicle_affected_to_operation_1
        )
    )
    assert len(published_events) == 1
    assert published_events[-1].data.status == "first_vehicle_affected"

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            vehicle_changes_from_operation_1_to_None
        )
    )
    assert len(published_events) == 2
    assert published_events[-1].data.status == "all_vehicles_released"

    asyncio.run(
        notify_when_vehicle_availability_changed_affects_operation.execute(
            vehicle_on_operation_1
        )
    )
    assert len(published_events) == 3
    assert published_events[-1].data.status == "first_vehicle_affected"
