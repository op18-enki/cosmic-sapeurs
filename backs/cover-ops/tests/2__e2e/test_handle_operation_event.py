import asyncio
import json

from cover_ops.entrypoints.server import route_converted_event
from tests.utils.post_and_sleep import post_and_sleep
from tests.utils.prepare_test_config import reset_test_config

# fixture, appears like it was unused, but it is !
from tests.utils.server_fixture import make_server_fixture

from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_publish_operation_event_to_bus(server_fixture):
    reset_test_config(config)
    operation_event_as_dict = {
        "topic": "operation_changed_status",
        "uuid": "some_operation_id",
        "source": "converters_online",
        "data": {
            "timestamp": "",
            "raw_operation_id": "2",
            "status": "opened",
            "address": "161 rue guillaume tell",
            "address_area": "CHPT",
            "cause": "victim",
            "raw_cause": "Feu de ...",
            "raw_procedure": "red",
        },
    }

    published_events = await spy_on_topic(
        config.domain_event_bus, "operation_changed_status"
    )

    data = json.dumps(operation_event_as_dict)
    resp = await post_and_sleep(server_fixture, route_converted_event, data=data)

    assert resp.status == 200
    resp_json = await resp.json()

    assert resp_json == {"success": "true"}
    assert len(published_events) == 1
    assert published_events[0].uuid == "some_operation_id"
