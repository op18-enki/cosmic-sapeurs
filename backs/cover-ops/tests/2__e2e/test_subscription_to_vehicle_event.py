import asyncio

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.entrypoints.subscribe_to_domain_events import (
    subscribe_to_vehicle_changed_status,
)
from tests.utils.availability_changed_event_factory import (
    make_availability,
    make_availability_changed_event_entity,
)
from tests.utils.prepare_update_cover import prepare_update_cover

from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.helpers.uuid import RealUuid, uuid4
from shared.sapeurs_events import VehicleEvent
from shared.test_utils.spy_on_topic import spy_on_topic

loop = asyncio.get_event_loop()


def test_event_availability_is_updated():
    (
        vehicle_repo,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        availability_published_events,
    ) = loop.run_until_complete(prepare_update_cover())
    omnibus_repo = InMemoryOmnibusEventsRepository()
    ongoing_op_repo = InMemoryOngoingOpEventsRepository(purge=True)
    operation_repo = InMemoryOperationEventsRepository(purge=True)
    update_omnibus = UpdateOmnibus(
        domain_event_bus=domain_event_bus,
        uuid=RealUuid(),
        omnibus_repo=omnibus_repo,
        vehicle_repo=vehicle_repo,
    )
    add_events_to_repositories = AddEventsToRepositories(
        operation_events_repo=operation_repo,
        vehicle_events_repo=vehicle_repo,
        availability_events_repo=availability_repo,
        ongoing_op_events_repo=ongoing_op_repo,
    )
    loop.run_until_complete(
        subscribe_to_vehicle_changed_status(
            domain_event_bus, update_cover, update_omnibus, add_events_to_repositories
        )
        # domain_event_bus.subscribe(
        #     topic="vehicle_changed_status", callback=subscribe_to_vehicle_changed_status
        # )
    )
    availability_events = loop.run_until_complete(
        spy_on_topic(domain_event_bus, "availabilityChanged")
    )

    def on_event_assert_availability(
        event: VehicleEvent, expected_availability: AvailabilityChangedEventEntity
    ):
        custom_uuid.set_next_uuid(uuid4())
        asyncio.run(domain_event_bus.publish(event))
        assert availability_events[-1].data == expected_availability.data

    dispatched_at = DateStr("2020-11-30T12:00:00.000Z")
    custom_clock.set_next_date(dispatched_at)

    # First event
    timestamp_1 = DateStr("2020-10-01T12:00:00.000Z")
    victim_rescue_1_became_available_event = make_vehicle_event(
        role="vsav_solo",
        raw_vehicle_id="1",
        status="misc_available",
        timestamp=timestamp_1,
        home_area="STOU",
    )

    on_event_assert_availability(
        victim_rescue_1_became_available_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_1,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=make_availability(available=1),
            bspp_availability=make_availability(available=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_1_became_available_event.data,
        ),
    )

    # Second event : vehicle #2 as pump became available
    timestamp_2 = DateStr("2020-10-01T13:00:00.000Z")
    pump_2_became_available_event = make_vehicle_event(
        role="pse_solo",
        raw_vehicle_id="2",
        home_area="STOU",
        status="misc_available",
        timestamp=timestamp_2,
    )
    on_event_assert_availability(
        pump_2_became_available_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_2,
            uuid=uuid4(),
            role="pse_solo",
            home_area="STOU",
            area_availability=make_availability(available=1),
            bspp_availability=make_availability(available=1),
            raw_vehicle_id="2",
            latest_event_data=pump_2_became_available_event.data,
        ),
    )

    # Third event : vehicle #1 as victim_rescue was selected
    timestamp_3 = DateStr("2020-10-01T13:03:00.000Z")
    victim_rescue_1_arrived_at_home = make_vehicle_event(
        role="vsav_solo",
        raw_vehicle_id="1",
        home_area="STOU",
        status="selected",
        timestamp=timestamp_3,
    )
    on_event_assert_availability(
        victim_rescue_1_arrived_at_home,
        make_availability_changed_event_entity(
            timestamp=timestamp_3,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=make_availability(on_operation=1),
            bspp_availability=make_availability(on_operation=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_1_arrived_at_home.data,
        ),
    )

    # Fourth event : vehicle #3 as other is new and in sport
    timestamp_4 = DateStr("2020-10-01T14:00:00.000Z")
    other_3_is_new_and_in_sport_event = make_vehicle_event(
        role="other",
        raw_vehicle_id="3",
        home_area="STOU",
        status="recoverable_within_15_minutes",
        timestamp=timestamp_4,
    )

    assert len(availability_events) == 3
    custom_uuid.set_next_uuid(uuid4())
    asyncio.run(domain_event_bus.publish(other_3_is_new_and_in_sport_event))

    assert len(availability_events) == 3

    # Fifth event : vehicle #1 as victim_rescue arrived at home
    timestamp_5 = DateStr("2020-10-01T20:09:00.000Z")
    victim_rescue_1_arrived_at_home = make_vehicle_event(
        role="vsav_solo",
        home_area="STOU",
        raw_vehicle_id="1",
        status="arrived_at_home",
        timestamp=timestamp_5,
    )

    on_event_assert_availability(
        victim_rescue_1_arrived_at_home,
        make_availability_changed_event_entity(
            timestamp=timestamp_5,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=make_availability(available=1),
            bspp_availability=make_availability(available=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_1_arrived_at_home.data,
        ),
    )

    # Sixth event : Vehicle #5 as vsav_solo is new and arrived at home
    timestamp_6 = DateStr("2020-10-01T20:10:00.000Z")
    victim_rescue_5_arrived_at_home = make_vehicle_event(
        role="vsav_solo",
        home_area="CHPT",
        raw_vehicle_id="5",
        status="arrived_at_home",
        timestamp=timestamp_6,
    )
    on_event_assert_availability(
        victim_rescue_5_arrived_at_home,
        make_availability_changed_event_entity(
            timestamp=timestamp_6,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="CHPT",
            area_availability=make_availability(available=1),
            bspp_availability=make_availability(available=2),
            raw_vehicle_id="5",
            latest_event_data=victim_rescue_5_arrived_at_home.data,
        ),
    )
