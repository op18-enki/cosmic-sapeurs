import json

from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)
from tests.utils.post_and_sleep import post_and_sleep
from tests.utils.prepare_test_config import reset_test_config
from tests.utils.server_fixture import make_server_fixture

from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.routes import make_url, sapeurs_routes
from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()

route_handle_converted_events = make_url(
    sapeurs_routes.prefix, sapeurs_routes.converted_event
)

raw_procedure_r = "R"
departure_criteria = "301 (default)"
longitude = 60000.25
latitude = 60000.25
address = "1 place jules renard 75017 Paris"
raw_cause = "Feu de ..."


async def test_subscriptions_to_cover_ops(server_fixture):
    reset_test_config(config)
    operation_changed_status_published_events = await spy_on_topic(
        config.domain_event_bus, "operation_changed_status"
    )
    ongoing_op_changed_published_events = await spy_on_topic(
        config.domain_event_bus, "ongoingOpChanged"
    )
    # Operation 1 opened
    operation_1_id = RawOperationId("operation_1")
    timestamp_opening = DateStr("2021-01-02T12:00:00.000Z")
    operation_1_opened = make_operation_event(
        status="opened",
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_opening,
        raw_procedure=raw_procedure_r,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        address=address,
        raw_cause=raw_cause,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(operation_1_opened),
    )

    assert resp.status == 200
    assert len(operation_changed_status_published_events) == 1
    assert len(ongoing_op_changed_published_events) == 1
    assert ongoing_op_changed_published_events[
        -1
    ].data == make_ongoing_ops_changed_event_data(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="opened",
        timestamp=timestamp_opening,
        raw_procedure=raw_procedure_r,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        address=address,
        raw_cause=raw_cause,
        opening_timestamp=timestamp_opening,
        affected_vehicles=[],
    )

    # Vehicle A became selected
    timestamp_vehicle_selected = DateStr("2021-01-02T12:01:00.000Z")
    vehicle_A_home = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_A"),
        status="selected",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=None,
        timestamp=timestamp_vehicle_selected,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(vehicle_A_home),
    )

    assert len(ongoing_op_changed_published_events) == 1

    # Vehicle A departed to intervention
    timestamp_vehicle_affected = DateStr("2021-01-02T12:01:00.000Z")
    vehicle_id_a = RawVehicleId("vehicle_A")
    vehicle_A_departed_to_intervention = make_vehicle_event(
        raw_vehicle_id=vehicle_id_a,
        status="departed_to_intervention",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_affected,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(vehicle_A_departed_to_intervention),
    )

    assert resp.status == 200
    assert len(ongoing_op_changed_published_events) == 2
    assert ongoing_op_changed_published_events[
        -1
    ].data == make_ongoing_ops_changed_event_data(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="first_vehicle_affected",
        timestamp=timestamp_vehicle_affected,
        raw_procedure=raw_procedure_r,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        address=address,
        raw_cause=raw_cause,
        opening_timestamp=timestamp_opening,
        affected_vehicles=[vehicle_id_a],
    )

    # Vehicle B also affected to same operation
    timestamp_vehicle_selected = DateStr("2021-01-02T12:03:00.000Z")
    vehicle_id_b = RawVehicleId("vehicle_B")
    vehicle_B_affected = make_vehicle_event(
        raw_vehicle_id=vehicle_id_b,
        status="arrived_on_intervention",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_affected,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(vehicle_B_affected),
    )

    assert resp.status == 200
    assert len(ongoing_op_changed_published_events) == 3

    expected_published_data = make_ongoing_ops_changed_event_data(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="some_affected_vehicle_changed",
        timestamp=timestamp_vehicle_affected,
        raw_procedure=raw_procedure_r,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        address=address,
        raw_cause=raw_cause,
        opening_timestamp=timestamp_opening,
        affected_vehicles=[vehicle_id_a, vehicle_id_b],
    )
    assert ongoing_op_changed_published_events[-1].data == expected_published_data

    # Vehicle A became available
    timestamp_vehicle_A_home = DateStr("2021-01-02T13:05:00.000Z")
    vehicle_A_home = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_A"),
        status="arrived_at_home",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_A_home,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(vehicle_A_home),
    )
    assert resp.status == 200
    assert len(ongoing_op_changed_published_events) == 4
    assert ongoing_op_changed_published_events[-1].data.affected_vehicles == [
        vehicle_id_b
    ]

    # Vehicle B became available
    timestamp_vehicle_B_available = DateStr("2021-01-02T14:03:00.000Z")

    vehicle_id_b = RawVehicleId("vehicle_B")
    vehicle_B_affected = make_vehicle_event(
        raw_vehicle_id=vehicle_id_b,
        status="misc_available",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_B_available,
    ).as_dict()

    resp = await post_and_sleep(
        server_fixture,
        route_handle_converted_events,
        data=json.dumps(vehicle_B_affected),
    )

    assert resp.status == 200

    assert len(ongoing_op_changed_published_events) == 5

    assert ongoing_op_changed_published_events[
        -1
    ].data == make_ongoing_ops_changed_event_data(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="all_vehicles_released",
        timestamp=timestamp_vehicle_B_available,
        raw_procedure=raw_procedure_r,
        departure_criteria=departure_criteria,
        longitude=longitude,
        latitude=latitude,
        address=address,
        raw_cause=raw_cause,
        opening_timestamp=timestamp_opening,
        affected_vehicles=[],
    )
