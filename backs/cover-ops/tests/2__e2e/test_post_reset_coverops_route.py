import json

from tests.utils.post_and_sleep import post_and_sleep
from tests.utils.prepare_test_config import reset_test_config
from tests.utils.server_fixture import make_server_fixture

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory
from shared.routes import make_url, sapeurs_routes

server_fixture, config = make_server_fixture()


async def test_post_reset_coverops_route(server_fixture):
    posted_victim_history = [
        {"timestamp": "2020-01-01", "value": 32, "variable": "sap"}
    ]
    posted_fire_history = [{"timestamp": "2020-01-02", "value": 2, "variable": "ep"}]
    post_data = json.dumps(
        {
            "victim": posted_victim_history,
            "fire": posted_fire_history,
        }
    )
    reset_test_config(config)

    resp = await post_and_sleep(
        server_fixture,
        route=make_url(sapeurs_routes.prefix, sapeurs_routes.reset),
        data=post_data,
    )

    assert resp.status == 200
    resp_json = await resp.json()

    assert config.front_lines_history.get_front_lines_history() == FrontLinesHistory(
        posted_victim_history, posted_fire_history
    )
