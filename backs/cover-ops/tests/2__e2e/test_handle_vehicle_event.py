import json

from cover_ops.entrypoints.server import route_converted_event
from tests.utils.post_and_sleep import post_and_sleep
from tests.utils.prepare_test_config import reset_test_config

# fixture, appears like it was unused, but it is !
from tests.utils.server_fixture import make_server_fixture

from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_publish_vehicle_event_to_bus(server_fixture):
    reset_test_config(config)
    vehicle_changed_status_event_as_dict = {
        "data": {
            "raw_vehicle_id": "1",
            "raw_operation_id": "3",
            "role": "vsav_solo",
            "raw_status": "parti",
            "status": "departed_to_intervention",
            "timestamp": "2020-12-10T12:30:00.853Z",
            "home_area": "STOU",
            "vehicle_name": "VSAV 102",
        },
        "uuid": "some_vehicle_id",
        "topic": "vehicle_changed_status",
        "source": "converters_online",
    }

    published_events = await spy_on_topic(
        config.domain_event_bus, "vehicle_changed_status"
    )

    resp = await post_and_sleep(
        server_fixture,
        route_converted_event,
        data=json.dumps(vehicle_changed_status_event_as_dict),
    )

    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(published_events) == 1
    assert published_events[0].uuid == "some_vehicle_id"
