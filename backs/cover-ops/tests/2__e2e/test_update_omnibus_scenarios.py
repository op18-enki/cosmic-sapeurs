import asyncio
from typing import List, Optional

from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.ongoing_op_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.entrypoints.subscribe_to_domain_events import (
    subscribe_to_vehicle_changed_status,
)
from tests.utils.prepare_update_omnibus import *

from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.helpers.uuid import RealUuid
from shared.sapeurs_events import VehicleEvent


async def prepare_scenarios(
    initial_vehicle_datas: Optional[List[VehicleEventData]] = None,
    initial_omnibus_details: Optional[List[OmnibusDetail]] = None,
):
    (
        update_omnibus,
        published_events,
        domain_event_bus,
        omnibus_repo,
        vehicle_repo,
    ) = await prepare_update_omnibus(
        initial_vehicle_datas,
        initial_omnibus_details,
    )
    availability_repo = InMemoryAvailabilityEventsRepository(purge=True)

    update_cover = UpdateCover(
        domain_event_bus=domain_event_bus,
        availability_repo=availability_repo,
        vehicle_repo=vehicle_repo,
        uuid=RealUuid(),
    )
    add_events_to_repositories = AddEventsToRepositories(
        operation_events_repo=InMemoryOperationEventsRepository(purge=False),
        vehicle_events_repo=vehicle_repo,
        availability_events_repo=InMemoryAvailabilityEventsRepository(purge=False),
        ongoing_op_events_repo=InMemoryOngoingOpEventsRepository(purge=False),
    )

    await subscribe_to_vehicle_changed_status(
        domain_event_bus, update_cover, update_omnibus, add_events_to_repositories
    )

    def on_event_assert_omnibus_balance(
        event: VehicleEvent,
        expected_details: Optional[List[OmnibusDetail]],
    ):
        asyncio.run(domain_event_bus.publish(event))

        if expected_details is None:
            return

        expected_omnibus_event_data = OmnibusBalanceChangedEventData(
            timestamp=event.data.timestamp,
            details=expected_details,
        )

        assert published_events[-1].data == expected_omnibus_event_data
        assert omnibus_repo.get_omnibus_details() == expected_omnibus_event_data.details

    return on_event_assert_omnibus_balance


loop = asyncio.get_event_loop()


def test_omnibus_is_updated_on_vehicle_changed_status():

    on_event_assert_omnibus_balance = loop.run_until_complete(
        prepare_scenarios(
            initial_vehicle_datas=[
                make_vsav_event("arrived_at_home").data,
                make_pse_event("arrived_at_home", role="pse_omni_pump").data,
            ],
            initial_omnibus_details=[
                OmnibusDetail(
                    vsav_raw_vehicle_id,
                    pse_raw_vehicle_id,
                    "STOU",
                    "both_available",
                )
            ],
        )
    )

    on_event_assert_omnibus_balance(
        vsav_affected_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "dangling",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_downgraded_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "vsav_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_to_inter_san_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "both_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        vsav_back_home_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "pse_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_as_vsav_home_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "both_available",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_to_inter_fire_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "dangling",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        vsav_lacks_staff_omnibus_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "pse_on_inter_fire",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_as_pse_home_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "only_pse_available",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        vsav_lacks_staff_event,
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "only_pse_available",
            )
        ],
    )


vsav_1_id = RawVehicleId("vsav_1")
vsav_2_id = RawVehicleId("vsav_2")
pse_1_id = RawVehicleId("pse_1")
pse_2_id = RawVehicleId("pse_2")

vsav_1_omnibus_at_home = make_vehicle_event_data(
    raw_vehicle_id=vsav_1_id,
    status="arrived_at_home",
    role="vsav_omni",
    omnibus=OmnibusInfos(partner_raw_vehicle_id=pse_1_id),
)


pse_1_omnibus_and_at_home = make_vehicle_event_data(
    raw_vehicle_id=pse_1_id,
    status="arrived_at_home",
    role="pse_omni_pump",
    omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
)


def test_omnibus_couple_changes_vsav_while_both_home():
    vsav_2_at_home = make_vehicle_event_data(
        raw_vehicle_id=vsav_2_id, status="arrived_at_home", omnibus=None
    )

    on_event_assert_omnibus_balance = loop.run_until_complete(
        prepare_scenarios(
            initial_vehicle_datas=[
                vsav_2_at_home,
                vsav_1_omnibus_at_home,
                pse_1_omnibus_and_at_home,
            ],
            initial_omnibus_details=[
                OmnibusDetail(
                    vsav_1_id,
                    pse_1_id,
                    "STOU",
                    "both_available",
                )
            ],
        )
    )

    vsav_1_detached = make_vehicle_event(
        raw_vehicle_id=vsav_1_id,
        role="vsav_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    vsav_2_attached = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="misc_unavailable",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )
    vsav_2_attached_and_ready = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="arrived_at_home",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )

    vsav_2_is_affected = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="selected",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )

    pse_1_downgraded = make_vehicle_event(
        raw_vehicle_id=pse_1_id,
        status="arrived_at_home",
        role="pse_omni_san",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_2_id),
    )

    on_event_assert_omnibus_balance(
        vsav_1_detached,
        expected_details=[
            OmnibusDetail(
                None,
                pse_1_id,
                "STOU",
                "dangling",
            )
        ],
    )
    on_event_assert_omnibus_balance(
        vsav_2_attached,
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "only_pse_available",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        vsav_2_attached_and_ready,
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "both_available",
            )
        ],
    )
    on_event_assert_omnibus_balance(
        vsav_2_is_affected,
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "dangling",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_1_downgraded,
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "vsav_on_inter_san",
            )
        ],
    )


def test_omnibus_couple_changes_pse_while_both_home():

    pse_2_at_home = make_vehicle_event_data(
        raw_vehicle_id=pse_2_id, status="arrived_at_home", omnibus=None
    )

    on_event_assert_omnibus_balance = loop.run_until_complete(
        prepare_scenarios(
            initial_vehicle_datas=[
                pse_2_at_home,
                vsav_1_omnibus_at_home,
                pse_1_omnibus_and_at_home,
            ],
            initial_omnibus_details=[
                OmnibusDetail(
                    vsav_1_id,
                    pse_1_id,
                    "STOU",
                    "both_available",
                )
            ],
        ),
    )

    pse_1_detached = make_vehicle_event(
        raw_vehicle_id=pse_1_id,
        role="pse_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    pse_2_attached = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="misc_unavailable",
        role="pse_omni_pump",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )
    pse_2_attached_and_ready = make_vehicle_event(
        raw_vehicle_id=pse_2_id,
        status="arrived_at_home",
        role="pse_omni_pump",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )

    pse_2_is_affected = make_vehicle_event(
        raw_vehicle_id=pse_2_id,
        status="selected",
        role="pse_omni_san",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )

    on_event_assert_omnibus_balance(
        pse_1_detached,
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                None,
                "STOU",
                "dangling",
            )
        ],
    )

    on_event_assert_omnibus_balance(pse_2_attached, None)

    on_event_assert_omnibus_balance(
        pse_2_attached_and_ready,
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                pse_2_id,
                "STOU",
                "both_available",
            )
        ],
    )
    on_event_assert_omnibus_balance(
        pse_2_is_affected,
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                pse_2_id,
                "STOU",
                "pse_on_inter_san",
            )
        ],
    )


def test_omnibus_couple_removed():

    on_event_assert_omnibus_balance = loop.run_until_complete(
        prepare_scenarios(
            initial_vehicle_datas=[
                vsav_1_omnibus_at_home,
                pse_1_omnibus_and_at_home,
            ],
            initial_omnibus_details=[
                OmnibusDetail(
                    vsav_1_id,
                    pse_1_id,
                    "STOU",
                    "both_available",
                )
            ],
        )
    )

    pse_1_detached = make_vehicle_event(
        raw_vehicle_id=pse_1_id,
        role="pse_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    vsav_1_detached = make_vehicle_event(
        raw_vehicle_id=vsav_1_id,
        role="vsav_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    on_event_assert_omnibus_balance(
        pse_1_detached,
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                None,
                "STOU",
                "dangling",
            )
        ],
    )
    on_event_assert_omnibus_balance(vsav_1_detached, expected_details=[])
