from dataclasses import asdict

from tests.utils.prepare_test_config import reset_test_config
from tests.utils.server_fixture import make_server_fixture

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory
from shared.routes import make_url, sapeurs_routes

server_fixture, config = make_server_fixture()


async def test_get_history_route(server_fixture):
    cached_front_lines_history = FrontLinesHistory(
        [{"timestamp": "2020-01-01", "value": 32, "variable": "sap"}],
        [{"timestamp": "2020-01-02", "value": 2, "variable": "vsav"}],
    )
    config.front_lines_history.set_front_lines_history(cached_front_lines_history)

    reset_test_config(config)
    resp = await server_fixture.get(
        path=make_url(sapeurs_routes.prefix, sapeurs_routes.get_front_history),
    )
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == asdict(cached_front_lines_history)
