from pathlib import Path
from typing import Optional

from cover_ops.adapters.csv_events_repositories import CsvAvailabilityEventsRepository
from cover_ops.adapters.redis.redis_availability_events_repository import (
    RedisAvailabilityEventsRepository,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from tests.utils.assert_helpers import assert_lists_equal
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_data,
)

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.custom_types import RawOperationId, RawVehicleId
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.csv import delete_file
from shared.helpers.date import DateStr


def prepare_redis_repo() -> AbstractAvailabilityEventsRepository:
    r = make_redis_instance()
    r.flushdb()
    return RedisAvailabilityEventsRepository(purge=True, redis_instance=r)


def prepare_csv_repo() -> AbstractAvailabilityEventsRepository:
    folder_path = Path("tests/1__integration/temp_data")
    csv_path = folder_path / "repo.csv"
    delete_file(csv_path)
    return CsvAvailabilityEventsRepository(csv_path=csv_path, purge=True)


data_0_0900 = make_availability_changed_event_data(
    raw_vehicle_id=RawVehicleId("0"),
    timestamp=DateStr("2021-01-01T09:00:00.000Z"),
    home_area="CHPT",
    role="vsav_omni",
    area_availability=Availability(1, 1, 3, 1, 0),
    bspp_availability=Availability(10, 10, 3, 10, 0),
)
data_0959 = make_availability_changed_event_data(
    raw_vehicle_id=RawVehicleId("1"),
    timestamp=DateStr("2021-01-01T09:59:00.000Z"),
    home_area="STOU",
    role="vsav_omni",
    area_availability=Availability(1, 1, 3, 1, 0),
    bspp_availability=Availability(10, 10, 3, 10, 0),
)
data_1001 = make_availability_changed_event_data(
    raw_vehicle_id=RawVehicleId("1"),
    timestamp=DateStr("2021-01-01T10:01:00.000Z"),
)
data_1600 = make_availability_changed_event_data(
    raw_vehicle_id=RawVehicleId("1"),
    timestamp=DateStr("2021-01-01T16:00:00.000Z"),
)


def assert_scenario_add_and_retrieve(repo: AbstractAvailabilityEventsRepository):
    repo.add([data_0_0900])
    repo.add([data_0959])

    latest_availability_event_data_by_vehicle_from_repo = (
        repo.list_latest_availability_event_data_by_vehicle_from_repo()
    )
    assert set(
        [
            latest_event_data.timestamp
            for latest_event_data in latest_availability_event_data_by_vehicle_from_repo
        ]
    ) == {data_0_0900.timestamp, data_0959.timestamp}

    assert repo.get_availability_event_data_since() == [
        data_0_0900,
        data_0959,
    ]
    assert repo.get_bspp_availability_stored_infos_for_role(
        "vsav_omni"
    ) == AvailabilityStoredInfos(
        data_0959.bspp_availability, data_0959.latest_event_data
    )
    assert repo.get_availability_stored_infos_for_area_for_role(
        "CHPT", "vsav_omni"
    ) == AvailabilityStoredInfos(
        data_0_0900.area_availability, data_0_0900.latest_event_data
    )
    assert (
        repo.get_bspp_availability_stored_infos_for_role(
            "vi_nrbc",
        )
        == AvailabilityStoredInfos(Availability(0, 0, 0, 0, 0))
    )

    assert repo.get_availability_stored_infos_for_area_for_role(
        "PARM", "vsav_omni"
    ) == AvailabilityStoredInfos(Availability(0, 0, 0, 0, 0))

    repo.add([data_1001])
    assert repo.list_latest_availability_event_data_by_vehicle_from_repo() == [
        data_0_0900,
        data_1001,
    ]
    assert repo.get_availability_event_data_since() == [
        data_0_0900,
        data_0959,
        data_1001,
    ]

    repo.add([data_1600])
    # Events from 09:59 for vehicle #1 should have been purged
    assert_lists_equal(
        repo.get_availability_event_data_since(),
        [
            data_0_0900,
            data_1001,
            data_1600,
        ],
    )

    repo.reset()
    assert repo.get_availability_event_data_since() == []
    assert repo.get_bspp_availability_stored_infos_for_role(
        role="vsav_omni"
    ) == AvailabilityStoredInfos(
        availability=Availability(
            available=0,
            unavailable=0,
            on_operation=0,
            recoverable=0,
            unavailable_omnibus=0,
        ),
        latest_event_data=None,
    )


def make_affected_availability(
    raw_vehicle_id: str, raw_operation_id: Optional[str], hour: int
):
    timestamp = DateStr(f"2021-01-01T{hour}:00:00.000Z")
    return make_availability_changed_event_data(
        raw_vehicle_id=raw_vehicle_id,
        timestamp=timestamp,
        previous_raw_operation_id=RawOperationId(raw_operation_id)
        if raw_operation_id
        else None,
        latest_event_data=make_vehicle_event_data(
            raw_vehicle_id=raw_vehicle_id,
            raw_operation_id=raw_operation_id,
            timestamp=timestamp,
        ),
    )


def test_csv_repo_assert_scenario_add_and_retrieve():
    repo = prepare_csv_repo()
    assert_scenario_add_and_retrieve(repo)


def test_redis_repo_assert_scenario_add_and_retrieve():
    repo = prepare_redis_repo()
    assert_scenario_add_and_retrieve(repo)


def assert_affected_to_operation_id_add_and_remove(
    repo: AbstractAvailabilityEventsRepository,
):
    vehicle_A_affected_to_operation_1 = make_affected_availability("A", "1", 15)
    vehicle_B_affected_to_operation_1 = make_affected_availability("B", "1", 16)
    vehicle_B_still_affected_to_operation_1 = make_affected_availability("B", "1", 17)
    vehicle_A_affected_to_no_operation = make_affected_availability("A", None, 18)
    vehicle_B_affected_to_operation_2 = make_affected_availability("B", "2", 19)

    repo.add([vehicle_A_affected_to_operation_1])
    assert repo.get_latest_vehicle_event_data_affected_to_operation_id(
        RawOperationId("1")
    ) == [vehicle_A_affected_to_operation_1.latest_event_data]

    repo.add([vehicle_B_affected_to_operation_1])

    assert repo.get_latest_vehicle_event_data_affected_to_operation_id(
        RawOperationId("1")
    ) == [
        vehicle_A_affected_to_operation_1.latest_event_data,
        vehicle_B_affected_to_operation_1.latest_event_data,
    ]

    repo.add([vehicle_B_still_affected_to_operation_1])
    assert repo.get_latest_vehicle_event_data_affected_to_operation_id(
        RawOperationId("1")
    ) == [
        vehicle_A_affected_to_operation_1.latest_event_data,
        vehicle_B_still_affected_to_operation_1.latest_event_data,
    ]

    repo.add([vehicle_A_affected_to_no_operation])
    assert repo.get_latest_vehicle_event_data_affected_to_operation_id(
        RawOperationId("1")
    ) == [
        vehicle_B_still_affected_to_operation_1.latest_event_data,
    ]

    repo.add([vehicle_B_affected_to_operation_2])
    assert (
        repo.get_latest_vehicle_event_data_affected_to_operation_id(RawOperationId("1"))
        == []
    )
    assert repo.get_latest_vehicle_event_data_affected_to_operation_id(
        RawOperationId("2")
    ) == [
        vehicle_B_affected_to_operation_2.latest_event_data,
    ]


def test_csv_assert_affected_to_operation_id_add_and_remove():
    repo = prepare_csv_repo()
    assert_affected_to_operation_id_add_and_remove(repo)


def test_redis_assert_affected_to_operation_id_add_and_remove():
    repo = prepare_redis_repo()
    assert_affected_to_operation_id_add_and_remove(repo)
