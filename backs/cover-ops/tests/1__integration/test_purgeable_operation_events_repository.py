from pathlib import Path

from cover_ops.adapters.csv_events_repositories import CsvOperationEventsRepository
from cover_ops.adapters.redis.redis_operation_events_repository import (
    RedisOperationEventsRepository,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from tests.utils.assert_helpers import assert_lists_equal

from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OperationOpeningInfos,
)
from shared.factories.operation_event_factory import (
    make_opening_infos_kwargs,
    make_operation_event_data,
)
from shared.helpers.csv import delete_file
from shared.helpers.date import DateStr


def prepare_redis_repo() -> AbstractOperationEventsRepository:
    r = make_redis_instance()
    r.flushdb()
    return RedisOperationEventsRepository(purge=True, redis_instance=r)


def prepare_csv_repo() -> AbstractOperationEventsRepository:
    folder_path = Path("tests/1__integration/temp_data")
    csv_path = folder_path / "repo.csv"
    delete_file(csv_path)
    return CsvOperationEventsRepository(csv_path=csv_path, purge=True)


snapshot_opening_info_kwargs = make_opening_infos_kwargs(
    cause="victim", address_area="CHPT"
)
data_snapashot = make_operation_event_data(  # mimic data from snapshot, that is opening info for status != just_opened
    raw_operation_id=RawOperationId("snapshot-id"),
    status="first_vehicle_affected",
    timestamp=DateStr("2021-01-01T09:50:00.000Z"),
    **snapshot_opening_info_kwargs
)

ope_2_opening_info_kwargs = make_opening_infos_kwargs(
    cause="victim", address_area="STOU"
)
data_2_0957 = make_operation_event_data(
    raw_operation_id=RawOperationId("2"),
    status="opened",
    timestamp=DateStr("2021-01-01T09:57:00.000Z"),
    **ope_2_opening_info_kwargs
)

data_2_0958 = make_operation_event_data(
    raw_operation_id=RawOperationId("2"),
    status="first_vehicle_affected",
    timestamp=DateStr("2021-01-01T09:58:00.000Z"),
)

data_1_0959 = make_operation_event_data(
    raw_operation_id=RawOperationId("1"),
    status="first_vehicle_affected",
    timestamp=DateStr("2021-01-01T09:59:00.000Z"),
)
data_1_1001 = make_operation_event_data(
    raw_operation_id=RawOperationId("1"),
    status="some_affected_vehicle_changed",
    timestamp=DateStr("2021-01-01T10:01:00.000Z"),
)
data_1_1600 = make_operation_event_data(
    raw_operation_id=RawOperationId("1"),
    status="all_vehicles_released",
    timestamp=DateStr("2021-01-01T16:00:00.000Z"),
)


def assert_scenario_add_purge_and_retrieve(
    repo: AbstractOperationEventsRepository,
):
    # Act :Snapshot
    repo.add([data_snapashot])

    # Assert
    actual_operation_opening_infos_for_operation_from_snapshot = (
        repo.get_operation_opening_infos_for_id(data_snapashot.raw_operation_id)
    )
    assert actual_operation_opening_infos_for_operation_from_snapshot is not None
    expected_opening_infos = OperationOpeningInfos(
        **snapshot_opening_info_kwargs, opening_timestamp=data_snapashot.timestamp
    )
    assert (
        actual_operation_opening_infos_for_operation_from_snapshot
        == expected_opening_infos
    )

    # Operation 2 opens
    repo.add([data_2_0957])

    # Assert
    actual_operation_opening_infos_for_operation_2 = (
        repo.get_operation_opening_infos_for_id(RawOperationId("2"))
    )
    assert actual_operation_opening_infos_for_operation_2 is not None
    expected_opening_infos = OperationOpeningInfos(
        **ope_2_opening_info_kwargs, opening_timestamp=data_2_0957.timestamp
    )
    assert actual_operation_opening_infos_for_operation_2 == expected_opening_infos

    # First vehicle affected to operation 2
    repo.add([data_2_0958])
    assert_lists_equal(
        repo.get_datas_for_id_with_status_in(RawOperationId("2")),
        [data_2_0957, data_2_0958],
    )

    # First vehicle affected to operation 1
    repo.add([data_1_0959])
    assert repo.get_datas_for_id_with_status_in(
        RawOperationId("2"), ["first_vehicle_affected"]
    ) == [data_2_0958]

    assert repo.get_datas_for_id_with_status_in(
        RawOperationId("1"), ["first_vehicle_affected"]
    ) == [data_1_0959]

    assert_lists_equal(
        repo.get_ongoing_operation_datas_with_relevant_status(),
        [data_snapashot, data_2_0957, data_2_0958, data_1_0959],
    )
    assert_lists_equal(
        repo.get_datas_for_id_with_status_in(RawOperationId("1")), [data_1_0959]
    )

    # Some vehicles affectation changed on operation 1
    repo.add([data_1_1001])
    assert_lists_equal(
        repo.get_ongoing_operation_datas_with_relevant_status(),
        [
            data_snapashot,
            data_2_0957,
            data_2_0958,
            data_1_0959,
            data_1_1001,
        ],
    )
    assert_lists_equal(
        repo.get_datas_for_id_with_status_in(
            RawOperationId("1"), statuses=["some_affected_vehicle_changed"]
        ),
        [
            data_1_1001,
        ],
    )
    assert (
        repo.get_latest_event_data_for_operation_id(RawOperationId("1")) == data_1_1001
    )
    assert (
        repo.get_latest_event_data_for_operation_id(RawOperationId("2")) == data_2_0958
    )

    # # At 16:00 : operation 1 has all released
    repo.add([data_1_1600])
    assert repo.get_ongoing_operation_datas_with_relevant_status() == [
        data_snapashot,
        data_2_0957,
        data_2_0958,
    ]
    # entity 09:59 should have been purged
    assert_lists_equal(
        repo.get_datas_for_id_with_status_in(RawOperationId("1")),
        [
            data_1_1001,
            data_1_1600,
        ],
    )
    assert_lists_equal(
        repo.get_datas_for_id_with_status_in(RawOperationId("2")),
        [data_2_0957, data_2_0958],
    )

    # reset repo and check it's all empty
    repo.reset()
    assert repo.get_datas_for_id_with_status_in(RawOperationId("1")) == []
    assert repo.get_latest_event_data_for_operation_id(RawOperationId("1")) == None
    assert repo.get_ongoing_operation_datas_with_relevant_status() == []
    assert repo.get_operation_opening_infos_for_id(RawOperationId("1")) == None
    assert repo.get_operations_notified_from_vehicle_changes() == set()


def test_csv_repo_assert_scenario_add_and_purge_and_retrieve():
    repo = prepare_csv_repo()
    assert_scenario_add_purge_and_retrieve(repo)


def test_redis_repo_assert_scenario_add_and_purge_and_retrieve():
    repo = prepare_redis_repo()
    assert_scenario_add_purge_and_retrieve(repo)


def test_redis_set_and_get_operations_notified_from_vehicle_changes():
    repo = prepare_redis_repo()
    repo.set_operations_notified_from_vehicle_changes(
        {RawOperationId("1"), RawOperationId("2")}
    )
    assert repo.get_operations_notified_from_vehicle_changes() == {
        RawOperationId("1"),
        RawOperationId("2"),
    }
