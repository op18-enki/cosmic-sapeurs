import asyncio
from typing import Dict, List

from cover_ops.adapters.rabbitmq_domain_event_bus import RabbitMqDomainEventBus
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event,
)
from tests.utils.ongoing_ops_changed_event_factory import make_ongoing_op_changed_event

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
    OmnibusDetail,
)
from shared.event_bus import EventCallback
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr
from shared.sapeurs_events import Event, OmnibusBalanceChangedEvent


async def make_spy_for_topic(topic: CoverOpsDomainTopic):
    published_events = []

    async def spy(event: Event):
        published_events.append(event)

    return published_events, spy


async def async_spy_on_topic(bus: RabbitMqDomainEventBus, topic: CoverOpsDomainTopic):
    published_events, spy = await make_spy_for_topic(topic)
    await bus.subscribe(topic, spy)
    return published_events


async def dummy_subscription(event: Event):
    pass


dispatched_at = DateStr("2021-05-04T22:20:09.840Z")
loop = asyncio.get_event_loop()


async def prepare_rabbit_mq_subscribe_and_publish(
    loop,
    events_to_publish: List[Event],
    subscriptions: Dict[CoverOpsDomainTopic, List[EventCallback]],
    unsubscriptions: Dict[CoverOpsDomainTopic, List[EventCallback]],
    topics_to_spy: List[CoverOpsDomainTopic],
):
    clock = CustomClock()
    clock.set_next_date(dispatched_at)

    bus = RabbitMqDomainEventBus(clock)
    await bus.start_on_loop(loop)
    # Subscribe and spy topics
    published_events_by_topic: Dict[CoverOpsDomainTopic, List[Event]] = {}
    for topic, callbacks in subscriptions.items():
        for callback in callbacks:
            await bus.subscribe(topic, callback)

    for topic, callbacks in unsubscriptions.items():
        for callback in callbacks:
            await bus.unsubscribe(topic, callback)

    for topic in topics_to_spy:
        published_events_by_topic[topic] = await async_spy_on_topic(bus, topic)

    # Publish events
    for event in events_to_publish:
        await bus.publish(event)

    return published_events_by_topic


def test_rabbit_mq_can_pub_and_spy_operation_and_vehicle_event():
    vehicle_event_0 = make_vehicle_event(
        uuid="uuid_vehicle_event_0", dispatched_at=dispatched_at
    )
    vehicle_event_1 = make_vehicle_event(
        uuid="uuid_vehicle_event_1", dispatched_at=dispatched_at
    )
    operation_event = make_operation_event(dispatched_at=dispatched_at)
    availability_event = make_availability_changed_event(dispatched_at=dispatched_at)
    omnibus_event = OmnibusBalanceChangedEvent(
        data=OmnibusBalanceChangedEventData(
            timestamp=DateStr("2020-01-01T12:00:00.000Z"),
            details=[
                OmnibusDetail(
                    vsav_id=RawVehicleId("vsav_1"),
                    pse_id=RawVehicleId("pse_1"),
                    area="STOU",
                    balance_kind="both_available",
                )
            ],
        ),
        uuid="omni_uuid",
        source="sapeurs_update_omnibus",
    )
    ongoing_op_event = make_ongoing_op_changed_event(dispatched_at=dispatched_at)

    subscriptions: Dict[CoverOpsDomainTopic, List[EventCallback]] = {
        "vehicle_changed_status": [
            dummy_subscription
        ],  # just to check that we can do multiple subscriptions on the same channel (ie. topic)
    }
    published_events_by_topic = loop.run_until_complete(
        prepare_rabbit_mq_subscribe_and_publish(
            loop,
            subscriptions=subscriptions,
            unsubscriptions={},
            events_to_publish=[
                vehicle_event_0,
                vehicle_event_1,
                operation_event,
                availability_event,
                omnibus_event,
                ongoing_op_event,
            ],
            topics_to_spy=[
                "vehicle_changed_status",
                "operation_changed_status",
                "availabilityChanged",
                "ongoingOpChanged",
                "omnibusBalanceChanged",
            ],
        )
    )
    assert len(published_events_by_topic["vehicle_changed_status"]) == 2
    assert published_events_by_topic["vehicle_changed_status"] == [
        vehicle_event_0,
        vehicle_event_1,
    ]
    assert published_events_by_topic["operation_changed_status"] == [operation_event]
    assert published_events_by_topic["availabilityChanged"] == [availability_event]
    assert published_events_by_topic["ongoingOpChanged"] == [ongoing_op_event]
    assert published_events_by_topic["omnibusBalanceChanged"] == [
        OmnibusBalanceChangedEvent(
            data=omnibus_event.data,
            uuid=omnibus_event.uuid,
            source=omnibus_event.source,
            dispatched_at=dispatched_at,
        )
    ]


def test_rabbit_mq_can_subscribe_then_unsubscribe():
    event = make_vehicle_event()

    published_events = []

    async def spy(event: Event):
        published_events.append(event)

    subscriptions: Dict[CoverOpsDomainTopic, List[EventCallback]] = {event.topic: [spy]}
    unsubscriptions: Dict[CoverOpsDomainTopic, List[EventCallback]] = {
        event.topic: [spy]
    }
    _ = loop.run_until_complete(
        prepare_rabbit_mq_subscribe_and_publish(
            loop,
            subscriptions=subscriptions,
            unsubscriptions=unsubscriptions,
            events_to_publish=[event],
            topics_to_spy=[],
        )
    )
    assert published_events == []
