from cover_ops.adapters.postgres.db import vehicle_events_table
from cover_ops.adapters.postgres.pg_vehicle_events_repository import (
    PgVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_data,
)
from sqlalchemy.sql.expression import text
from tests.utils.prepare_db import insert_in_table, prepare_db

from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import DateStr, to_datetime


def pass_test_pg_vehicle_events_repo_add_item():
    engine = prepare_db()
    repo = PgVehicleEventsRepository(
        purge=False,
        engine=engine,
    )

    vehicle_event_data = make_vehicle_event_data()

    repo.add([vehicle_event_data])

    query = text("SELECT * FROM vehicle_events")
    records = engine.connect().execute(query).all()

    assert len(records) == 1
    (uuid, source, topic, data, event_timestamp, event_id) = records[0]
    assert event_timestamp == to_datetime(vehicle_event_data.timestamp)
    stored_data = dataclass_to_dict(vehicle_event_data)
    assert data == stored_data


def pass_test_pg_vehicle_events_repo_get_latest_event_by_raw_vehicle_id():
    engine = prepare_db()
    repo = PgVehicleEventsRepository(
        purge=False,
        engine=engine,
    )
    vehicle_data_1_on_inter = make_vehicle_event_data(
        raw_vehicle_id="1",
        status="arrived_on_intervention",
        timestamp=DateStr("2021-03-01T12:00:00.000Z"),
    )
    vehicle_data_1_selected = make_vehicle_event_data(
        raw_vehicle_id="1",
        status="selected",
        timestamp=DateStr("2021-03-01T10:00:00.000Z"),
    )
    vehicle_data_2 = make_vehicle_event_data(
        raw_vehicle_id="2",
        status="selected",
    )

    event_datas = [
        vehicle_data_1_on_inter,
        vehicle_data_1_selected,
        vehicle_data_2,
    ]

    insert_in_table(engine, vehicle_events_table, event_datas)

    result = repo.get_latest_data_by_raw_vehicle_id()
    assert result == {
        "1": vehicle_data_1_on_inter,
        "2": vehicle_data_2,
    }
