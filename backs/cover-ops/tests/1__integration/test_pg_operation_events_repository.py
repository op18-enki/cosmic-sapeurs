from cover_ops.adapters.postgres.db import operation_events_table
from cover_ops.adapters.postgres.pg_operation_events_repository import (
    PgOperationEventsRepository,
)
from cover_ops.helpers.factories.operation_event_entity_factory import (
    make_operation_event_data,
)
from sqlalchemy.sql.expression import text
from tests.utils.prepare_db import insert_in_table, prepare_db

from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import DateStr, to_datetime

operation_1_opened_data = make_operation_event_data(
    raw_operation_id="1",
    status="opened",
    cause="victim",
    address_area="STOU",
    timestamp=DateStr("2021-03-01T12:00:00.0"),
)

operation_1_first_vehicle_affected_data = make_operation_event_data(
    raw_operation_id="1",
    status="first_vehicle_affected",
    cause=None,
    address_area=None,
    timestamp=DateStr("2021-03-01T13:00:00.0"),
)
operation_1_reinforced_data = make_operation_event_data(
    raw_operation_id="1",
    status="reinforced",
    cause="victim",
    address_area="STOU",
    timestamp=DateStr("2021-03-01T14:00:00.0"),
)
operation_1_all_vehicles_released_event = make_operation_event_data(
    raw_operation_id="1",
    status="all_vehicles_released",
    cause="victim",
    address_area="STOU",
    timestamp=DateStr("2021-03-01T15:00:00.0"),
)
operation_2_opened_data = make_operation_event_data(
    raw_operation_id="2",
    status="opened",
    timestamp=DateStr("2021-03-01T15:00:00.0"),
)

operation_2_first_affected_data = make_operation_event_data(
    raw_operation_id="2",
    status="first_vehicle_affected",
    timestamp=DateStr("2021-03-01T14:00:00.0"),
)


def prepare_operation_repo():
    engine = prepare_db()
    repo = PgOperationEventsRepository(engine=engine, purge=False)
    event_entities = [
        operation_1_opened_data,
        operation_1_first_vehicle_affected_data,
        operation_1_reinforced_data,
        operation_1_all_vehicles_released_event,
        operation_2_opened_data,
        operation_2_first_affected_data,
    ]

    insert_in_table(engine, operation_events_table, event_entities)

    return repo


def pass_test_pg_operation_events_repo_get_events_for_id_with_status_in():
    repo = prepare_operation_repo()

    result_with_all_status = repo.get_datas_for_id_with_status_in(
        raw_operation_id=RawOperationId("1"), statuses=None
    )
    assert result_with_all_status == [
        operation_1_opened_data,
        operation_1_first_vehicle_affected_data,
        operation_1_reinforced_data,
        operation_1_all_vehicles_released_event,
    ]

    result_with_relevant_status = repo.get_datas_for_id_with_status_in(
        raw_operation_id=RawOperationId("1"),
        statuses=["opened", "first_vehicle_affected"],
    )
    assert result_with_relevant_status == [
        operation_1_opened_data,
        operation_1_first_vehicle_affected_data,
    ]


def pass_test_pg_operation_events_repo_get_ongoing_operation_events_with_relevant_status():
    repo = prepare_operation_repo()
    fetched = repo.get_ongoing_operation_datas_with_relevant_status()
    assert len(fetched) == 2
    assert fetched == [
        operation_2_opened_data,
        operation_2_first_affected_data,
    ]


def pass_test_pg_operation_events_repo_get_ongoing_operation_events_with_relevant_status_when_repo_is_empty():
    engine = prepare_db()
    repo = PgOperationEventsRepository(engine=engine, purge=False)
    fetched = repo.get_ongoing_operation_datas_with_relevant_status()
    assert len(fetched) == 0


def pass_test_pg_operation_events_add():
    engine = prepare_db()
    repo = PgOperationEventsRepository(engine=engine, purge=False)
    operation_event_data = make_operation_event_data()

    repo.add([operation_event_data])

    query = text("SELECT * FROM operation_events")
    records = engine.connect().execute(query).all()

    assert len(records) == 1
    (uuid, source, topic, data, event_timestamp, event_id) = records[0]
    assert event_timestamp == to_datetime(operation_event_data.timestamp)
    assert data == dataclass_to_dict(operation_event_data)
