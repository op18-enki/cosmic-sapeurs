from cover_ops.adapters.redis.redis_api_state_cache import RedisApiStateCache
from cover_ops.adapters.redis.redis_utils import make_redis_instance


def prepare_redis_cache() -> RedisApiStateCache:
    r = make_redis_instance()
    r.flushdb()
    api_state_cache = RedisApiStateCache(redis_instance=r)
    return api_state_cache


def test_is_ready_is_True_at_init():
    api_state_cache = prepare_redis_cache()
    assert api_state_cache.get_is_ready() == True


def test_can_set_and_get_is_ready():
    api_state_cache = prepare_redis_cache()
    api_state_cache.set_is_ready(False)
    assert api_state_cache.get_is_ready() == False

    api_state_cache = prepare_redis_cache()
    api_state_cache.set_is_ready(True)
    assert api_state_cache.get_is_ready() == True
