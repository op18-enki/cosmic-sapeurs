from cover_ops.adapters.postgres.db import availability_changed_events_table
from cover_ops.adapters.postgres.pg_availability_events_repository import (
    PgAvailabilityEventsRepository,
)
from sqlalchemy.sql.expression import text
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_data,
)
from tests.utils.prepare_db import insert_in_table, prepare_db

from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.date import DateStr, to_datetime

availability_vehicle_3_before_11 = make_availability_changed_event_data(
    timestamp=DateStr("2021-01-01T09:00:00.000Z"), raw_vehicle_id="3"
)
availability_vehicle_1_before_11 = make_availability_changed_event_data(
    timestamp=DateStr("2021-01-01T10:59:00.000Z"), raw_vehicle_id="1"
)
availability_vehicle_1_after_11 = make_availability_changed_event_data(
    timestamp=DateStr("2021-01-01T11:01:00.000Z"), raw_vehicle_id="1"
)
availability_vehicle_2_after_11 = make_availability_changed_event_data(
    timestamp=DateStr("2021-01-01T14:00:00.000Z"), raw_vehicle_id="2"
)


def prepare_operation_repo():
    engine = prepare_db()
    repo = PgAvailabilityEventsRepository(
        engine=engine, purge=False
    )  # purge not implemented for PG repo
    event_datas = [
        availability_vehicle_3_before_11,
        availability_vehicle_1_before_11,
        availability_vehicle_1_after_11,
        availability_vehicle_2_after_11,
    ]

    insert_in_table(engine, availability_changed_events_table, event_datas)

    return repo


def pass_test_pg_availability_events_add():
    engine = prepare_db()
    repo = PgAvailabilityEventsRepository(engine=engine, purge=False)
    availability_changed_event_data = make_availability_changed_event_data()

    repo.add([availability_changed_event_data])

    query = text("SELECT * FROM availability_changed_events")
    records = engine.connect().execute(query).all()

    assert len(records) == 1

    (data, event_timestamp) = records[0]
    assert event_timestamp == to_datetime(availability_changed_event_data.timestamp)
    assert data == dataclass_to_dict(availability_changed_event_data)


def pass_test_pg_availability_repo_list_latest_availability_event_data_by_vehicle_from_repo():
    repo = prepare_operation_repo()

    latest_availability_for_each_vehicle_id = (
        repo.list_latest_availability_event_data_by_vehicle_from_repo()
    )
    assert len(latest_availability_for_each_vehicle_id) == 3
    assert latest_availability_for_each_vehicle_id == [
        availability_vehicle_3_before_11,
        availability_vehicle_1_after_11,
        availability_vehicle_2_after_11,
    ]
