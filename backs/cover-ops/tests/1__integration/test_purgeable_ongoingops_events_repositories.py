from pathlib import Path

from cover_ops.adapters.csv_events_repositories import CsvOngoingOpEventsRepository
from cover_ops.adapters.redis.redis_ongoing_op_events_repository import (
    RedisOngoingOpEventsRepository,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance
from cover_ops.domain.ports.ongoing_op_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from tests.utils.assert_helpers import assert_lists_equal
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)

from shared.data_transfert_objects.custom_types import RawOperationId
from shared.helpers.csv import delete_file
from shared.helpers.date import DateStr

folder_path = Path("tests/1__integration/temp_data")
csv_path = folder_path / "repo.csv"


def prepare_redis_repo() -> AbstractOngoingOpEventsRepository:
    r = make_redis_instance()
    r.flushdb()
    return RedisOngoingOpEventsRepository(purge=True, redis_instance=r)


def prepare_csv_repo() -> AbstractOngoingOpEventsRepository:
    folder_path = Path("tests/1__integration/temp_data")
    csv_path = folder_path / "repo.csv"
    delete_file(csv_path)
    return CsvOngoingOpEventsRepository(csv_path=csv_path, purge=True)


def assert_repo_get_methods(
    repo: AbstractOngoingOpEventsRepository,
):
    op_A_opened = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="opA",
        status="opened",
    )
    op_A_closed = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="opA",
        status="closed",
    )

    op_OTHER_opened = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="other",
        raw_operation_id="opO",
    )

    repo.add([op_A_opened])

    assert repo.get_all_ongoing_ops() == [op_A_opened]
    assert (
        repo.get_latest_event_data_for_area_for_cause(area="STOU", cause="victim")
        == op_A_opened
    )

    repo.add([op_A_closed])

    # test previous events of closed operation A have been purged
    assert repo.get_all_ongoing_ops() == []
    assert repo.get_latest_event_data_for_all_area_for_cause_fire_and_victim() == [
        op_A_closed
    ]

    assert (
        repo.get_latest_event_data_for_area_for_cause(area="STOU", cause="victim")
        == op_A_closed
    )

    repo.add([op_OTHER_opened])
    assert (
        repo.get_latest_event_data_for_area_for_cause(area="STOU", cause="other")
        == op_OTHER_opened
    )
    assert repo.get_latest_event_data_for_all_area_for_cause_fire_and_victim() == [
        op_A_closed
    ]

    assert (
        repo.get_latest_event_data_for_operation_id(RawOperationId("opO"))
        == op_OTHER_opened
    )
    assert (
        repo.get_latest_event_data_for_operation_id(RawOperationId("opA"))
        == op_A_closed
    )

    # reset repo and check it's all empty
    repo.reset()
    assert repo.get_all_ongoing_ops() == []
    assert repo.get_latest_event_data_for_all_area_for_cause_fire_and_victim() == []
    assert (
        repo.get_latest_event_data_for_area_for_cause(area="STOU", cause="other")
        == None
    )
    assert repo.get_latest_event_data_for_operation_id(RawOperationId("opA")) == None


def test_csv_repo():
    repo = prepare_csv_repo()
    assert_repo_get_methods(repo)


def test_redis_repo():
    repo = prepare_redis_repo()
    assert_repo_get_methods(repo)


def assert_repo_can_purge(repo: AbstractOngoingOpEventsRepository):

    op_P_closed_0957 = make_ongoing_ops_changed_event_data(
        address_area="PARM",
        cause="victim",
        raw_operation_id="opP",
        timestamp=DateStr("2021-01-01T09:57:00.000Z"),
        status="closed",
    )
    op_A_closed_0958 = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="opA",
        timestamp=DateStr("2021-01-01T09:58:00.000Z"),
        status="closed",
    )
    op_B_first_vehicule_0959 = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="opB",
        timestamp=DateStr("2021-01-01T09:59:00.000Z"),
        status="first_vehicle_affected",
    )

    # Operations triggering purge (+6h)
    op_C_opened_1600 = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        cause="victim",
        raw_operation_id="opC",
        timestamp=DateStr("2021-01-01T16:00:00.000Z"),
        status="opened",
    )
    repo.add(
        [op_P_closed_0957, op_A_closed_0958, op_B_first_vehicule_0959, op_C_opened_1600]
    )

    # test op A (stou, vic) closed at 09:58 has been purged
    assert_lists_equal(
        repo.get_all_ongoing_ops(),
        [
            op_B_first_vehicule_0959,
            op_C_opened_1600,
        ],
    )
    assert (
        repo.get_latest_event_data_for_area_for_cause(area="STOU", cause="victim")
        == op_C_opened_1600
    )
    assert (
        repo.get_latest_event_data_for_area_for_cause(area="PARM", cause="victim")
        == op_P_closed_0957
    )


def test_csv_repo_can_purge():
    repo = prepare_csv_repo()
    assert_repo_can_purge(repo)


def test_redis_repo_can_purge():
    repo = prepare_redis_repo()
    assert_repo_can_purge(repo)
