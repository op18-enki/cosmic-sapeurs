from cover_ops.adapters.redis.redis_front_lines_history_cache import (
    AbstractFrontLinesHistoryCache,
    RedisFrontLinesHistoryCache,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance

from shared.data_transfert_objects.front_lines_history import FrontLinesHistory


def prepare_redis_cache() -> AbstractFrontLinesHistoryCache:
    r = make_redis_instance()
    r.flushdb()
    return RedisFrontLinesHistoryCache(redis_instance=r)


def test_can_set_and_retrieve_front_lines_history_cache():
    cache = prepare_redis_cache()

    setted_history = FrontLinesHistory(
        [{"value": 32, "timestamp": "2020-01-01", "type": "vsav"}],
        [{"value": 42, "timestamp": "2020-01-02", "type": "ep"}],
    )
    cache.set_front_lines_history(setted_history)

    retrieved_history = cache.get_front_lines_history()

    assert setted_history == retrieved_history
