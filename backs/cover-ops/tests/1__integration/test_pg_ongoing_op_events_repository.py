from cover_ops.adapters.postgres.db import ongoing_events_table
from cover_ops.adapters.postgres.pg_ongoing_op_events_repository import (
    PgOngoingOpEventsRepository,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)
from tests.utils.prepare_db import insert_in_table, prepare_db

from shared.helpers.date import DateStr

ongoing_op_3_before_11 = make_ongoing_ops_changed_event_data(
    timestamp=DateStr("2021-01-01T09:00:00.000Z"),
    raw_operation_id="3",
    address_area="CHPT",
    cause="fire",
)
ongoing_op_1_before_11 = make_ongoing_ops_changed_event_data(
    timestamp=DateStr("2021-01-01T10:59:00.000Z"),
    raw_operation_id="1",
    address_area="STOU",
    cause="victim",
)
ongoing_op_1_after_11 = make_ongoing_ops_changed_event_data(
    timestamp=DateStr("2021-01-01T11:01:00.000Z"),
    raw_operation_id="1",
    address_area="STOU",
    cause="victim",
)
ongoing_op_2_after_11 = make_ongoing_ops_changed_event_data(
    timestamp=DateStr("2021-01-01T14:00:00.000Z"),
    raw_operation_id="2",
    address_area="POIS",
    cause="fire",
)


def prepare_operation_repo():
    engine = prepare_db()
    repo = PgOngoingOpEventsRepository(engine=engine, purge=False)
    event_datas = [
        ongoing_op_3_before_11,
        ongoing_op_1_before_11,
        ongoing_op_1_after_11,
        ongoing_op_2_after_11,
    ]

    insert_in_table(engine, ongoing_events_table, event_datas)
    return repo


def pass_test_pg_get_ongoing_ops_since():
    repo = prepare_operation_repo()

    ongoing_ops_for_past_hours = repo.get_all_ongoing_ops()
    assert len(ongoing_ops_for_past_hours) == 3
    assert ongoing_ops_for_past_hours == [
        ongoing_op_1_after_11,
        ongoing_op_2_after_11,
        ongoing_op_3_before_11,
    ]


def pass_test_pg_get_latest_event_data_for_area_for_cause():
    repo = prepare_operation_repo()
    latest_event_data_for_STOU_for_victim = (
        repo.get_latest_event_data_for_area_for_cause("STOU", "victim")
    )
    latest_event_data_for_PARM_for_fire = repo.get_latest_event_data_for_area_for_cause(
        "PARM", "fire"
    )
    assert latest_event_data_for_STOU_for_victim == ongoing_op_1_after_11
    assert latest_event_data_for_PARM_for_fire is None


def pass_test_get_latest_event_data_for_all_area_for_cause_fire_and_victim():
    repo = prepare_operation_repo()
    actual_event_datas = (
        repo.get_latest_event_data_for_all_area_for_cause_fire_and_victim()
    )
    assert len(actual_event_datas) == 3
    assert actual_event_datas == [
        ongoing_op_3_before_11,
        ongoing_op_2_after_11,
        ongoing_op_1_after_11,
    ]
