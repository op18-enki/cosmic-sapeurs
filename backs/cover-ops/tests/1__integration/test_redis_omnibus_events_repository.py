from cover_ops.adapters.redis.redis_omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
    RedisOmnibusEventsRepository,
)
from cover_ops.adapters.redis.redis_utils import make_redis_instance

from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusDetail,
)


def prepare_redis_repo() -> AbstractOmnibusEventsRepository:
    r = make_redis_instance()
    r.flushdb()
    return RedisOmnibusEventsRepository(redis_instance=r)


def test_add_omnibus_then_get_omnibus_details_then_removes_omnibus():
    repo = prepare_redis_repo()
    vsav_id = RawVehicleId("vsav_id")
    pse_id = RawVehicleId("pse_id")
    omni1_detail = OmnibusDetail(
        area="STOU",
        balance_kind="vsav_on_inter_san",
        pse_id=pse_id,
        vsav_id=vsav_id,
    )
    omni2_detail = OmnibusDetail(
        area="PARM",
        balance_kind="pse_on_inter_san",
        pse_id=RawVehicleId("pse_2_id"),
        vsav_id=RawVehicleId("vsav_2_id"),
    )
    repo.add_omnibus(omnibus_detail=omni1_detail)
    repo.add_omnibus(omnibus_detail=omni2_detail)

    omni1_retrieved_from_vsav = repo.get_omnibus_detail(raw_vehicle_id=vsav_id)
    assert omni1_retrieved_from_vsav == omni1_detail

    omni1_retrieved_from_pse = repo.get_omnibus_detail(raw_vehicle_id=pse_id)
    assert omni1_retrieved_from_pse == omni1_detail

    details = repo.get_omnibus_details()
    assert len(details) == 2
    assert omni1_detail in details
    assert omni2_detail in details

    repo.remove_omnibus(omni1_detail)
    details_after_remove = repo.get_omnibus_details()
    assert details_after_remove == [omni2_detail]

    repo.reset()
    assert repo.get_omnibus_details() == []


def test_add_omnibus_then_get_omnibus_details_then_removes_omnibus_for_dissociated_couple():
    repo = prepare_redis_repo()
    pse_id = RawVehicleId("pse_id")
    omni1_detail = OmnibusDetail(
        area="STOU",
        balance_kind="vsav_on_inter_san",
        pse_id=pse_id,
        vsav_id=None,
    )
    repo.add_omnibus(omnibus_detail=omni1_detail)

    assert repo.get_omnibus_detail(raw_vehicle_id=pse_id) == omni1_detail

    details = repo.get_omnibus_details()
    assert details == [omni1_detail]

    repo.remove_omnibus(omni1_detail)
    details_after_remove = repo.get_omnibus_details()
    assert details_after_remove == []
