from pathlib import Path

from cover_ops.adapters.csv_events_repositories import CsvVehicleEventsRepository
from cover_ops.adapters.redis.redis_utils import make_redis_instance
from cover_ops.adapters.redis.redis_vehicle_events_repository import (
    RedisVehicleEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.data_transfert_objects.custom_types import RawVehicleId
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.csv import delete_file
from shared.helpers.date import DateStr

data_2_0900 = make_vehicle_event_data(
    raw_vehicle_id=RawVehicleId("2"), timestamp=DateStr("2021-01-01T09:00:00.000Z")
)
data_0959 = make_vehicle_event_data(
    raw_vehicle_id=RawVehicleId("1"), timestamp=DateStr("2021-01-01T09:59:00.000Z")
)

data_1001 = make_vehicle_event_data(
    raw_vehicle_id=RawVehicleId("1"), timestamp=DateStr("2021-01-01T10:01:00.000Z")
)
data_1600 = make_vehicle_event_data(
    raw_vehicle_id=RawVehicleId("1"), timestamp=DateStr("2021-01-01T16:00:00.000Z")
)


def prepare_redis_repo() -> AbstractVehicleEventsRepository:
    r = make_redis_instance()
    r.flushdb()
    return RedisVehicleEventsRepository(purge=True, redis_instance=r)


def prepare_csv_repo() -> AbstractVehicleEventsRepository:
    folder_path = Path("tests/1__integration/temp_data")
    csv_path = folder_path / "repo.csv"
    delete_file(csv_path)
    return CsvVehicleEventsRepository(csv_path=csv_path, purge=True)


def assert_scenario_add_retrieve_and_reset(repo: AbstractVehicleEventsRepository):
    repo.add([data_2_0900])
    repo.add([data_0959])
    assert repo.get_latest_data_by_raw_vehicle_id() == {
        "2": data_2_0900,
        "1": data_0959,
    }
    assert repo.get_latest_data_for_vehicle_id(RawVehicleId("2")) == data_2_0900
    assert repo.get_latest_data_for_vehicle_id(RawVehicleId("1")) == data_0959

    repo.add([data_1001])
    assert repo.get_latest_data_by_raw_vehicle_id() == {
        "1": data_1001,
        "2": data_2_0900,
    }

    repo.add([data_1600])
    assert repo.get_latest_data_by_raw_vehicle_id() == {
        "1": data_1600,
        "2": data_2_0900,
    }
    repo.reset()
    assert repo.get_latest_data_by_raw_vehicle_id() == {}


def test_csv_repo_assert_scenario_add_and_retrieve():
    repo = prepare_csv_repo()
    assert_scenario_add_retrieve_and_reset(repo)


def test_redis_assert_scenario_add_and_retrieve():
    repo = prepare_redis_repo()
    assert_scenario_add_retrieve_and_reset(repo)
