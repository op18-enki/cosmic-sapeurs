# Cover-Ops 🇬🇧

This is a Python application, where all the domain code (ie. business logic) happens. For example :

- Update Cover when a vehicle changes status
- Update Ongoing Operation Count when an operation is opened or closed
- Save the processed event in the repositories
- ...

## Pre-Requisite

Go to the right folder:

```
cd ./backs/cover-ops

```

- Python 3

## Install

Create a virtual environment, install the packages and initialize a .env file.

```
python3 -m venv venv
source venv/bin/activate
pip install -e ../shared
pip install -e .
cp .env.sample .env
```

## Launch the tests

```
pip install -r tests-requirements.txt
pytest tests
```

## Launch the app

```
python cover_ops/entrypoints/server.py
```
