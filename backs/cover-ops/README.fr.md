# Cover-ops 🇫🇷

C'est une application Python, où tout le code du domaine (c'est-à-dire la logique métier) se produit. Par exemple:

- Mettre à jour la couverture opérationnelle lorsqu'un véhicule change de statut
- Mettre à jour le nombre d'opérations en cours lorsqu'une opération est ouverte ou fermée
- Enregistrer les évènements dans les dépôts
- ...

## Prérequis

Aller dans le dossier :

```
cd backs/cover-ops
```

## Installation

Créer un environnement virtuel, installez les pacquets et initialisez le .env.

```
python3 -m venv venv
source venv/bin/activate
pip install -e ../shared
pip install -e .
cp .env.sample .env
```

## Lancer les tests !

```
pip install -r tests-requirements.txt
pytest tests
```

## Lancer l'application

```
python cover_ops/entrypoints/server.py
```
