# Offline

This service allows one to replay at full-speed some events and use the sapeurs services (converters, cover-ops) offline. 

## Install 

Create a virtual environment, install the packages and initialize a .env file. 
```
python3 -m venv venv 
source venv/bin/activate 
pip install -e ../shared
pip install -e ../cover-ops
pip install -e ../converters
pip install -e .
```
## Launch the service

### 1. Convert Adagio CSV in Sapeurs CSV 

- Paste the CSV files corresponding to CsvAdagioVehicleRepo and/or CsvAdagioOperationRepo in directory `offline/data/adagio_input` (NB : If you are using the app on real-data with Docker, those CSV files are located in `docker-data/converters`. Those are the same files we use in replayers service ) 
- Launch : 
```
python offline/entrypoints/convert.py --help
```
There are two commands (`adagio-vehicles` and `adagio-operations`);  each having three options (`--force` , `--input-path` , `--output-path`) 
- This will create new files in  `offline/data/sapeurs_input` with same basenames and `.sapeurs.csv` suffix.

### 2. Covers-Ops from Sapeurs CSV files 
- Either paste CSV files corresponding to CsvVehicleRepo and/or CsvOperationRepo in directory `offline/data/sapeurs_input` ; Or follow step 1 to convert events from adagio and use the generated files 
- Launch the following script : 
```
python offline/entrypoints/cover-ops.py
```
- This will create new files in  `offline/data/sapeurs_output` with sapeurs processed events : availabilities, ongoing_ops and omnibus_balance

NB : ```python offline/entrypoints/cover-ops.py --help```will give you the options you can choose (input/output folders to consider, adapters choices such as repositories, cache, domain event bus, how many hours to replay, ..). By default, it's all in memory and in csv. 