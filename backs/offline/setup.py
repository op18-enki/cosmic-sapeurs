import pathlib

from setuptools import find_packages, setup

packages = find_packages(exclude=[])


setup(name="offline", version="0.1", packages=packages, install_requires=["click"])
