import abc
from typing import List, Union

from shared.sapeurs_events import OperationEvent, VehicleEvent


class AbstractOfflineQuery(abc.ABC):
    @abc.abstractmethod
    def get_initial_vehicle_events(self) -> List[VehicleEvent]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_events_to_replay(self) -> List[Union[VehicleEvent, OperationEvent]]:
        raise NotImplementedError
