from cover_ops.entrypoints.config import Config

from offline.domain.ports.offline_query import AbstractOfflineQuery
from shared.sapeurs_events import ConvertersCatchesUpEvent, ConvertersVehiclesInitEvent


class ExecuteCoverOpsOffline:
    def __init__(
        self,
        cover_ops_config: Config,
        offline_query: AbstractOfflineQuery,
    ):

        self.event_bus = cover_ops_config.domain_event_bus
        self.offline_query = offline_query
        self.uuid = cover_ops_config.uuid
        self.cover_ops_config = cover_ops_config

    async def execute(self):
        await self.cover_ops_config.bus_subscribes_to_domain_events()

        vehicle_init_event = ConvertersVehiclesInitEvent(
            data=self.offline_query.get_initial_vehicle_events(), uuid=self.uuid.make()
        )
        await self.event_bus.publish(vehicle_init_event)

        catch_up_event = ConvertersCatchesUpEvent(
            data=self.offline_query.get_events_to_replay(),
            uuid=self.uuid.make(),
        )
        await self.event_bus.publish(catch_up_event)
