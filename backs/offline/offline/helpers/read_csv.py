import csv
import sys
from pathlib import Path
from typing import Dict, List

csv.field_size_limit(sys.maxsize)


def read_csv(csv_path: Path) -> List[Dict]:
    with csv_path.open("r") as f:
        reader = csv.DictReader(f)
        rows = [row for row in reader]
    return rows
