import datetime
import json
from pathlib import Path
from typing import Dict, List, Optional, Tuple

from offline.domain.ports.offline_query import AbstractOfflineQuery
from offline.helpers.read_csv import read_csv
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData
from shared.helpers.date import DateStr, to_datetime
from shared.helpers.logger import logger
from shared.sapeurs_events import OperationEvent, VehicleEvent


def timedelta_is_greater_than(t0: DateStr, t1: DateStr, hours: int) -> bool:
    return (to_datetime(t1) - to_datetime(t0)) > datetime.timedelta(hours=hours)


class CsvOfflineQuery(AbstractOfflineQuery):
    def __init__(
        self,
        vehicle_csv_path: Optional[Path],
        operation_csv_path: Optional[Path],
        initial_date: DateStr,
        hours_to_replay: Optional[int] = None,
    ):
        super().__init__()
        vehicle_rows = read_csv(vehicle_csv_path) if vehicle_csv_path else []
        operation_rows = read_csv(operation_csv_path) if operation_csv_path else []
        (
            self._vehicle_initial_events,
            self._vehicle_replay_events,
        ) = self._infer_vehicle_events_from_rows(
            vehicle_rows, initial_date, hours_to_replay
        )
        self._operation_events = self._infer_operation_events_from_rows(
            operation_rows, hours_to_replay
        )

    def get_initial_vehicle_events(self) -> List[VehicleEvent]:
        return self._vehicle_initial_events

    def get_events_to_replay(self) -> List[VehicleEvent]:
        # TODO : Should we drop duplicated events ?
        # TODO : Allow NOT sorting events (or only sort based on the `dispatched_at` instead of `timestamp`)
        all_events = self._vehicle_replay_events + self._operation_events  # type: ignore
        return sorted(all_events, key=lambda d: d.data.timestamp)  #

    @staticmethod
    def _infer_vehicle_events_from_rows(
        rows: List[Dict], initial_date: DateStr, hours_to_replay: Optional[int]
    ) -> Tuple[List[VehicleEvent], List[VehicleEvent]]:
        initial_events = []
        replay_events = []
        t0 = None
        for row in rows:
            try:
                deserialized_data = json.loads(row["data"])
                event_data = VehicleEventData.from_dict(deserialized_data)
                event = VehicleEvent(data=event_data, uuid=row["uuid"])
                timestamp = event.data.timestamp

                if timestamp == initial_date:
                    initial_events.append(event)
                    continue
                if not t0:
                    t0 = timestamp  # first event
                elif hours_to_replay and timedelta_is_greater_than(
                    t0, timestamp, hours_to_replay
                ):
                    logger.info(
                        f"Will replay {len(replay_events)} vehicle events out of {len(rows)} (corresponding to {hours_to_replay} hours)"
                    )
                    return initial_events, replay_events
                else:
                    replay_events.append(event)
            except Exception as e:
                logger.warn(
                    f"Could not construct vehicle data from row {row}: \n {str(e)}. Skipping."
                )
        return initial_events, replay_events

    @staticmethod
    def _infer_operation_events_from_rows(
        rows: List[Dict], hours_to_replay: Optional[int]
    ) -> List[OperationEvent]:
        events = []
        t0 = None
        for row in rows:
            try:
                deserialized_data = json.loads(row["data"])
                event_data = OperationEventData.from_dict(deserialized_data)
                timestamp = event_data.timestamp
                if not t0:
                    t0 = timestamp
                event = OperationEvent(data=event_data, uuid=row["uuid"])
                if hours_to_replay and timedelta_is_greater_than(
                    t0, timestamp, hours_to_replay
                ):
                    logger.info(
                        f"Will replay {len(events)} operation events out of {len(rows)} (corresponding to {hours_to_replay} hours)"
                    )
                    return events
                events.append(event)
            except Exception as e:
                logger.warn(
                    f"Could not construct operation data from row {row}: \n {str(e)}. Skipping."
                )
        return events
