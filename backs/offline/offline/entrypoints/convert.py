import ast
import json
import os
from pathlib import Path
from typing import Dict, Optional

import click
from tqdm import tqdm

from converters.adapters.adagio.adagio_operation_event_mapper import (
    AdagioOperationEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from offline.helpers.read_csv import read_csv
from shared.helpers.csv import writerow
from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.decorators import timeit
from shared.helpers.find_file_matching import find_file_matching
from shared.helpers.logger import logger
from shared.helpers.uuid import RealUuid


def convert_and_writerow(
    mapper: AdagioVehicleEventMapper, event_as_dict: Dict, output_path: Path
):
    try:
        sapeurs_event = mapper.convert(event_as_dict)
        writerow(
            output_path,
            [
                sapeurs_event.uuid,
                sapeurs_event.topic,
                json.dumps(dataclass_to_dict(sapeurs_event.data)),
            ],
        )
    except Exception as e:
        logger.warn(f"Could not convert event {event_as_dict} in sapeurs: \n {str(e)}")


@timeit("adagio_vehicles_to_sapeurs_csv")
def adagio_vehicles_to_sapeurs_csv(input_path: Path, output_path: Path):
    mapper = AdagioVehicleEventMapper(uuid=RealUuid())
    input_rows = read_csv(input_path)
    writerow(output_path, ["uuid", "topic", "data"])
    print(
        f"[Offline] Converting {len(input_rows)} rows from adagio vehicles to sapeurs"
    )
    for row in tqdm(input_rows):
        row["data"] = ast.literal_eval(row["data"])
        if row["topic"] == "provided_last_status_of_all_vehicles":
            initial_events_as_dict = row["data"]
            print(
                f"[Offline] Converting {len(initial_events_as_dict)} initial events from adagio vehicles to sapeurs"
            )
            for event_as_dict in tqdm(initial_events_as_dict):
                convert_and_writerow(mapper, event_as_dict, output_path)
        elif row["topic"] == "adagio_vehicle_changed_status":
            convert_and_writerow(mapper, row, output_path)


@timeit("adagio_operations_to_sapeurs_csv")
def adagio_operations_to_sapeurs_csv(input_path: Path, output_path: Path):
    mapper = AdagioOperationEventMapper(uuid=RealUuid())
    input_rows = read_csv(input_path)
    writerow(output_path, ["uuid", "topic", "data"])
    print(
        f"[Offline] Converting {len(input_rows)} rows from adagio operations to sapeurs"
    )
    for row in tqdm(input_rows):
        row["data"] = ast.literal_eval(row["data"])
        convert_and_writerow(mapper, row, output_path)


@click.group(chain=True)
def cli():
    pass


@cli.command("adagio-operations")
@click.option(
    "--input-path",
    type=str,
    help="Path to input CSV file",
    required=False,
    default=None,
)
@click.option(
    "--output-path",
    type=str,
    help="Path to output CSV file",
    required=False,
    default=None,
)
@click.option("--force", is_flag=True)
def convert_adagio_operations(
    input_path: Optional[str], output_path: Optional[str], force: bool = False
):
    click.echo("Convert adagio operations")
    input_path = (
        Path(input_path)
        if input_path
        else find_file_matching(Path("./data/adagio_input"), "*operation*.csv")
    )
    output_path = (
        Path(output_path)
        if output_path
        else Path("./data/sapeurs_input")
        / input_path.name.replace(".csv", ".sapeurs.csv")
    )
    if os.path.exists(output_path) and not force:
        click.echo(
            f"Output file {output_path} already exists and force is false. Exit. "
        )
        return
    if os.path.exists(output_path):
        os.remove(output_path)
    adagio_operations_to_sapeurs_csv(input_path, output_path)


@cli.command("adagio-vehicles")
@click.option(
    "--input-path",
    type=str,
    help="Path to input CSV file",
    required=False,
    default=None,
)
@click.option(
    "--output-path",
    type=str,
    help="Path to output CSV file",
    required=False,
    default=None,
)
@click.option("--force", is_flag=True)
def convert_adagio_vehicles(
    input_path: Optional[str], output_path: Optional[str], force: bool = False
):
    click.echo("Convert adagio vehicles")
    input_path = (
        Path(input_path)
        if input_path
        else find_file_matching(Path("./data/adagio_input"), "*vehicle*.csv")
    )
    output_path = (
        Path(output_path)
        if output_path
        else Path("./data/sapeurs_input")
        / input_path.name.replace(".csv", ".sapeurs.csv")
    )
    if os.path.exists(output_path) and not force:
        click.echo(
            f"Output file {output_path} already exists and force is false. Exit. "
        )
        return
    adagio_vehicles_to_sapeurs_csv(input_path, output_path)


if __name__ == "__main__":
    cli()
