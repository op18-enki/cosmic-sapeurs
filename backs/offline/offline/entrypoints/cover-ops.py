import asyncio
import shutil
from pathlib import Path
from typing import Optional

import click
from cover_ops.entrypoints.config import Config
from cover_ops.entrypoints.environment_variables import (
    CacheOption,
    DomainEventBusOption,
    EnvironmentVariables,
    EventsRepositories,
    cache_options,
    domain_event_bus_options,
    events_repositories_options,
)

from offline.adapters.csv_offline_query import CsvOfflineQuery
from offline.domain.use_cases.execute_cover_ops_offline import ExecuteCoverOpsOffline
from shared.helpers.date import DateStr
from shared.helpers.decorators import ExceptionMode
from shared.helpers.find_file_matching import find_file_matching


def clear_path(path: Path):
    shutil.rmtree(path, ignore_errors=True)


async def main(
    input_path: Path,
    output_path: Path,
    env: EnvironmentVariables,
    hours_to_replay: Optional[int],
):
    clear_path(output_path)

    cover_ops_config = Config(env, csv_base_path=Path("./data/sapeurs_output"))

    input_vehicle_path = find_file_matching(input_path, "*vehicle*.csv")
    input_operation_path = find_file_matching(input_path, "*operation*.csv")

    offline_query = CsvOfflineQuery(
        vehicle_csv_path=input_vehicle_path,
        operation_csv_path=input_operation_path,
        initial_date=DateStr("2000-01-01T00:00:00.000Z"),
        hours_to_replay=hours_to_replay,
    )
    execute_cover_ops_offline = ExecuteCoverOpsOffline(
        cover_ops_config=cover_ops_config,
        offline_query=offline_query,
    )
    await execute_cover_ops_offline.execute()


@click.command()
@click.option(
    "--hours-to-replay",
    type=int,
    help="Number of hours to replay",
    required=False,
    default=None,
)
@click.option(
    "--events-repositories",
    type=click.Choice(events_repositories_options),
    help="Event repositories option",
    required=False,
    default="CSV",
)
@click.option(
    "--exception-mode",
    type=click.Choice(["RAISE", "PASS"]),
    help="Exception mode option",
    required=False,
    default="RAISE",
)
@click.option(
    "--domain_event_bus",
    type=click.Choice(domain_event_bus_options),
    help="Domain event bus option",
    required=False,
    default="IN_MEMORY",
)
@click.option(
    "--cache",
    type=click.Choice(cache_options),
    help="Cache option",
    required=False,
    default="IN_MEMORY",
)
@click.option(
    "--input-folder",
    type=str,
    help="Path to folder with sapeurs input CSV files (vehicles, operations)",
    required=False,
    default=None,
)
@click.option(
    "--output-folder",
    type=str,
    help="Path to folder to save output CSV files (availability, ongoing ops, omnibus balance) ",
    required=False,
    default=None,
)
def cli(
    input_folder: Optional[str],
    output_folder: Optional[str],
    hours_to_replay: Optional[int],
    cache: CacheOption,
    domain_event_bus: DomainEventBusOption,
    exception_mode: ExceptionMode,
    events_repositories: EventsRepositories,
):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    input_path = Path(input_folder or "./data/sapeurs_input")
    output_path = Path(output_folder or "./data/sapeurs_output")
    env = EnvironmentVariables(
        events_repositories=events_repositories,
        exception_mode=exception_mode,
        cache=cache,
        domain_event_bus=domain_event_bus,
    )
    _ = loop.run_until_complete(main(input_path, output_path, env, hours_to_replay))
    loop.close()


if __name__ == "__main__":
    cli()
