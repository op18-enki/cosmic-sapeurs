# Replayers 🇫🇷

Ce service nous permet de "rejouer" des événements à partir de fichiers CSV, afin de simuler un système externe. On peut choisir la vitesse de
relecture. Notons que si le stockage des convertisseurs est en mode CSV, les fichiers à rejouer sont directement les fichiers CSV stockés.

## Prérequis

Aller dans le dossier :

```
cd backs/replayers
```

- Python 3
- Les fichiers CSV avec les évènements à rejouer doivent être : `data/adagio_vehicle_events_to_replay.csv` et
  `data/adagio_intervention_events_to_replay.csv`. Vous pouvez copier les fichiers d'exemple avec la commande suivante :

```
cp data_sample/adagio_vehicle_events_to_replay.sample.csv data/adagio_vehicle_events_to_replay.csv

cp data_sample/adagio_intervention_events_to_replay.sample.csv data/adagio_intervention_events_to_replay.csv
```

## Installation

Créer un environnement virtuel, installez les pacquets et initialisez le .env.

```
python3 -m venv venv
source venv/bin/activate
pip install ../shared
pip install -e ./src
cp .env.sample .env
```

## Lancer l'application

```
python src/entrypoints/server.py
```

## Tests

```
pip install -r tests-requirements.txt
pytest tests
```
