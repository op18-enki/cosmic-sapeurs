from domain.ports.publisher import AbstractPublisher

from shared.external_event import ExternalEvent
from shared.sapeurs_events import operation_changed_status, vehicle_changed_status


class SapeursPublishers(AbstractPublisher):
    def __init__(
        self,
        vehicle_publisher: AbstractPublisher,
        intervention_publisher: AbstractPublisher,
    ):
        self.vehicle_publisher = vehicle_publisher
        self.intervention_publisher = intervention_publisher

    def publish_to_converters(self, event: ExternalEvent) -> None:
        """Publish event to converters app depending on topic"""
        if event.topic == operation_changed_status:
            self.intervention_publisher.publish_to_converters(event)
        elif event.topic in vehicle_changed_status:
            self.vehicle_publisher.publish_to_converters(event)
        else:
            print(f"Cannot publish event with topic {event.topic}")
