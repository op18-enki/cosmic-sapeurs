from pathlib import Path
from typing import Callable, Dict, Union

import pandas as pd
from adapters.scheduled_event_bus import ReplayedEventBus, prepare_df_to_replay
from domain.ports.publisher import AbstractPublisher

from shared.external_event import ExternalEvent
from shared.helpers.clock import AbstractClock
from shared.sapeurs_events import Event


class ReplayedEventBuses(ReplayedEventBus):
    def __init__(
        self,
        clock: AbstractClock,
        publisher: AbstractPublisher,
        *,
        event_dict_to_event: Union[
            Callable[[Dict], Event], Callable[[Dict], ExternalEvent]
        ],
        vehicle_csv_path: Path,
        operation_csv_path: Path,
        time_step: float = 1.0,
        speed: float = 1,
        resync: bool = False,
    ):

        df_vehicle = pd.read_csv(vehicle_csv_path)
        df_vehicle_prepared = prepare_df_to_replay(df_vehicle)
        latest_vehicle_timestamp = df_vehicle_prepared.timestamp.min()

        df_operation = pd.read_csv(operation_csv_path)
        if df_operation.empty:
            self.df = df_vehicle_prepared.sort_values(by="timestamp")
            last_published_timestamp = latest_vehicle_timestamp
        else:
            df_operation_prepared = prepare_df_to_replay(df_operation)
            latest_operation_timestamp = df_operation_prepared.timestamp.min()

            self.df = pd.concat(
                [df_vehicle_prepared, df_operation_prepared], axis=0
            ).sort_values(by="timestamp")
            last_published_timestamp = max(
                latest_vehicle_timestamp, latest_operation_timestamp
            )

        ReplayedEventBus.__init__(
            self,
            clock,
            publisher=publisher,
            df=self.df,
            time_step=time_step,
            speed=speed,
            resync=resync,
            event_dict_to_event=event_dict_to_event,
        )

        self.set_last_published_timestamp(last_published_timestamp)
