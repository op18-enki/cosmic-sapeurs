from typing import Tuple

from adapters.abstract_scheduled_event_buses import AbstractScheduledEventBuses
from adapters.scheduled_event_bus import ReplayedEventBus, ScheduledEventBus
from domain.ports.publisher import AbstractPublisher
from pandas.core.frame import DataFrame

from shared.helpers.clock import AbstractClock
from shared.sapeurs_events import OperationEvent, VehicleEvent


class ReplayedSapeursEventBuses(AbstractScheduledEventBuses):
    def _instantiate_event_buses(
        self,
        clock: AbstractClock,
        vehicle_publisher: AbstractPublisher,
        operation_publisher: AbstractPublisher,
        *,
        vehicle_df: DataFrame = None,
        operation_df: DataFrame = None,
        vehicle_csv_path: str = None,
        operation_csv_path: str = None,
        time_step: float = 1.0,
        speed: float = 1,
        resync: bool = False,
    ) -> Tuple[ScheduledEventBus, ScheduledEventBus]:

        vehicle_events_bus = ReplayedEventBus(
            clock=clock,
            csv_path=vehicle_csv_path,
            df=vehicle_df,
            time_step=time_step,
            speed=speed,
            resync=resync,
            publisher=vehicle_publisher,
            event_dict_to_event=VehicleEvent.from_dict,
        )
        operation_events_bus = ReplayedEventBus(
            clock=clock,
            csv_path=operation_csv_path,
            df=operation_df,
            time_step=time_step,
            speed=speed,
            resync=resync,
            publisher=operation_publisher,
            event_dict_to_event=OperationEvent.from_dict,
        )
        return vehicle_events_bus, operation_events_bus
