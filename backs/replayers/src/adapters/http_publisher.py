import json

import requests
from domain.ports.publisher import AbstractPublisher

from shared.sapeurs_events import Event


class HttpPublisher(AbstractPublisher):
    def __init__(self, http_client_url: str) -> None:
        super().__init__()
        self.http_client_url = http_client_url

    def publish_to_converters(self, event: Event) -> None:
        event_as_json = json.dumps(event.as_dict())
        requests.post(self.http_client_url, data=event_as_json)
