import json
from pathlib import Path

import pandas as pd
from adapters.scheduled_event_bus import deserialize_data_column

raw_operation_csv = Path("data/original_raw_operation_events.csv")
raw_vehicle_csv = Path("data/original_raw_vehicle_events.csv")


df_veh: pd.DataFrame = pd.read_csv(raw_vehicle_csv)  # type: ignore
deserialize_data_column(df_veh, inplace=True)


# Get latest snapshot
snapshot_row = df_veh[df_veh.topic == "snapshot"].iloc[-1]
snapshot_index = snapshot_row.name
snapshot = snapshot_row.data
snapshot_timestamp = snapshot["vehicles"][0]["timestamp"]

# Truncate vehicles data
df_veh_post_snapshot = df_veh[snapshot_index + 1 :]

# Truncate operations data
df_ope: pd.DataFrame = pd.read_csv(raw_operation_csv)  # type: ignore
deserialize_data_column(df_ope, inplace=True)


first_index_post_snapshot = df_ope[
    df_ope.data.apply(lambda s: s.get("timestamp")) >= snapshot_timestamp
].index[0]


snap_op_time = [ope.get("timestamp") for ope in snapshot["operations"]]
last_index_pre_snapshot = df_ope[
    df_ope.data.apply(lambda s: s.get("timestamp")) <= max(snap_op_time)
].index[-1]
print("Lost operation events : ", first_index_post_snapshot - last_index_pre_snapshot)

df_ope_post_snapshot = df_ope[first_index_post_snapshot:]

# Save all those stuffs
date = snapshot_timestamp[:10]
df_ope_post_snapshot.to_csv(Path(f"data/{date}_raw_operation_events.csv"), index=False)
df_veh_post_snapshot.to_csv(Path(f"data/{date}_raw_vehicle_events.csv"), index=False)

with open(
    f"../converters/data/fake_adagio_responses/{date}_snapshot.json",
    "w",
) as file:
    json.dump({"data": snapshot, "topic": "adagio_snapshot"}, file)
