# This file is for debug purposes. It filters fire operation from snapshot and replayed data and saves them in readable csv.

import json
from pathlib import Path

import numpy as np
import pandas as pd
from adapters.scheduled_event_bus import deserialize_data_column
from pandas import json_normalize

date = "2022-02-09"
raw_operation_csv = Path(f"data/{date}_raw_operation_events.csv")
raw_vehicle_csv = Path(f"data/{date}_raw_vehicle_events.csv")

reset_snapshot_json = Path(
    f"../converters/data/fake_adagio_responses/{date}_snapshot.json"
)

with open(reset_snapshot_json, "r") as file:
    snapshot_data = json.load(file)

useful_op_columns = [
    "timestamp",
    "OdeResume.IdIntervention",
    "OdeResume.LibelleMotif",
    "OdeResume.EtatIntervention",
    "OdeResume.Cstc.Libelle",
]


useful_veh_columns = [
    "timestamp",
    "IdMMA",
    "IdIntervention",
    "AbregeStatutOperationnel",
    "AbregeFamilleMMAOriginelle",
    "AbregeFamilleMMAOperationnelle",
    "Cstc",
    "EnDelestage",
    "Omnibus",
    "InterventionEncours",
    "InformationsOmnibus.IdMMAOmnibus",
    "InformationsOmnibus.EstOmnibusEpm6Classique",
]

# PROCESS SNAPSHOT OPERATIONS
snapshot_fire_operations = [
    {"data": operation}
    for operation in snapshot_data["data"]["operations"]
    if str(operation.get("OdeResume", {}).get("Motif", 000))[0] == "1"
]
df_ope_snapshot_data = json_normalize(pd.DataFrame(snapshot_fire_operations).data)[
    useful_op_columns
]
df_ope_snapshot_data.to_csv(Path(f"data/{date}_FIRE_SNAP_operation_events.csv"))


# PROCESS SNAPSHOT VEHICLES
snapshot_fire_vehicles = [
    {"data": vehicle}
    for vehicle in snapshot_data["data"]["vehicles"]
    if vehicle.get("IdIntervention")
    in df_ope_snapshot_data["OdeResume.IdIntervention"].values
]

df_veh_snapshot_data = json_normalize(pd.DataFrame(snapshot_fire_vehicles).data)[
    useful_veh_columns
]

infos_by_inter = {}
for IdIntervention, group in df_veh_snapshot_data.groupby("IdIntervention"):
    infos_by_inter[IdIntervention] = group[
        ["IdMMA", "AbregeStatutOperationnel"]
    ].to_dict(orient="list")

df_veh_snapshot_data = (
    pd.DataFrame(infos_by_inter)
    .T.reset_index()
    .rename(columns={"index": "IdIntervention"})
)

df_veh_snapshot_data.to_csv(Path(f"data/{date}_FIRE_SNAP_vehicle_events.csv"))


# PROCESS REPLAY OPERATIONS
snapshot_op_ids = set(
    df_ope_snapshot_data["OdeResume.IdIntervention"].astype(float).values
)

df_ope: pd.DataFrame = pd.read_csv(raw_operation_csv)  # type: ignore
deserialize_data_column(df_ope, inplace=True)
df_ope_data = json_normalize(df_ope.data)
useless_statuses = {
    "Réception",
    "Validation automatique",
    "Envoi",
    "Validation manuelle",
}
df_ope_data = df_ope_data[
    ~df_ope_data["StatutIntervention.LibelleStatutIntervention"].isin(useless_statuses)
]
df_ope_data["is_fire"] = df_ope_data["OdeResume.Motif"].apply(
    lambda motif: str(motif)[0] == "1" if motif else None
)

id_fire_interventions = set(
    df_ope_data[df_ope_data.is_fire]["OdeResume.IdIntervention"].values  # type: ignore
).union(snapshot_op_ids)

df_ope_data_fire = (
    pd.concat(
        [
            df_ope_data[
                df_ope_data["OdeResume.IdIntervention"].isin(id_fire_interventions)
            ],
            df_ope_data[
                df_ope_data["Intervention.IdIntervention"].isin(id_fire_interventions)
            ],
        ],
        axis=0,
    )
    .sort_index()
    .drop_duplicates()
)[useful_op_columns]

df_ope_data_fire["topic"] = df_ope.loc[df_ope_data_fire.index].topic
df_ope_data_fire.to_csv(Path(f"data/{date}_FIRE_operation_events.csv"))


df_veh: pd.DataFrame = pd.read_csv(raw_vehicle_csv)  # type: ignore
deserialize_data_column(df_veh, inplace=True)
df_veh_data = json_normalize(df_veh.data)
df_veh_data_fire = df_veh_data[
    df_veh_data["IdIntervention"].isin(id_fire_interventions)
]

df_veh_data_fire[useful_veh_columns].to_csv(
    Path(f"data/{date}_FIRE_vehicle_events.csv")
)
