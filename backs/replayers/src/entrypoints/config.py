from pathlib import Path
from typing import Callable, Dict, Optional, Union

from adapters.adagio_publishers import AdagioPublishers
from adapters.http_publisher import HttpPublisher
from adapters.in_memory_publisher import InMemoryPublisher
from adapters.random_event_buses import RandomEventBuses
from adapters.replayed_event_buses import ReplayedEventBuses
from adapters.scheduled_event_bus import ReplayedEventBus, ScheduledEventBus
from domain.ports.publisher import AbstractPublisher
from entrypoints.environment_variables import (
    EnvironmentVariables,
    PublisherToConvertersOption,
    ScheduledEventBusOption,
)

from shared.adagio_event_dict_to_event import adagio_event_dict_to_event
from shared.factories.adagio.adagio_intervention_event_factory import (
    make_adagio_intervention_created_event,
)
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)
from shared.helpers.clock import RealClock
from shared.helpers.find_file_matching import find_file_matching
from shared.helpers.logger import logger
from shared.routes import route_external_operation_event, route_external_vehicle_event

clock = RealClock()


def prepare_env_var_to_scheduled_event_bus(
    vehicle_publisher: AbstractPublisher,
    operation_publisher: AbstractPublisher,
    replay_speed: int = 30,
    date: Optional[str] = None,
) -> Dict[
    ScheduledEventBusOption,
    Union[Callable[[], ReplayedEventBus], Callable[[], RandomEventBuses]],
]:
    vehicle_csv_path = find_file_matching(
        path=Path("data"), pattern=f"*{date or ''}*raw_vehicle*"
    )
    operation_csv_path = find_file_matching(
        path=Path("data"), pattern=f"*{date or ''}*raw_operation*"
    )

    env_var_to_scheduled_event_bus: Dict[
        ScheduledEventBusOption,
        Union[Callable[[], ReplayedEventBus], Callable[[], RandomEventBuses]],
    ] = dict(
        RANDOM_ADAGIO=lambda: RandomEventBuses(
            clock=clock,
            min_time_step=0.1,
            max_time_step=0.5,
            make_operation_event=make_adagio_intervention_created_event,
            make_vehicle_event=make_adagio_vehicle_event,
            vehicle_publisher=vehicle_publisher,
            operation_publisher=operation_publisher,
        ),
        REPLAYED_ADAGIO=lambda: ReplayedEventBuses(
            clock=clock,
            publisher=AdagioPublishers(
                vehicle_publisher=vehicle_publisher,
                intervention_publisher=operation_publisher,
            ),
            vehicle_csv_path=vehicle_csv_path,
            operation_csv_path=operation_csv_path,
            time_step=4.0,
            resync=False,
            speed=replay_speed,
            event_dict_to_event=adagio_event_dict_to_event,
        ),
    )

    return env_var_to_scheduled_event_bus


class Config:
    replayed_event_bus: ScheduledEventBus

    def __init__(
        self, env: EnvironmentVariables, replay_speed: int, date: Optional[str]
    ) -> None:

        self.ENV = env
        logger.info("Env variables : ", self.ENV)

        env_var_to_vehicle_publisher: Dict[
            PublisherToConvertersOption, Callable[[], AbstractPublisher]
        ] = dict(
            IN_MEMORY=lambda: InMemoryPublisher(),
            HTTP=lambda: HttpPublisher(
                f"{self.ENV.converters_url}/{route_external_vehicle_event}"
            ),
        )

        env_var_to_operation_publisher: Dict[
            PublisherToConvertersOption, Callable[[], AbstractPublisher]
        ] = dict(
            IN_MEMORY=lambda: InMemoryPublisher(),
            HTTP=lambda: HttpPublisher(
                f"{self.ENV.converters_url}/{route_external_operation_event}"
            ),
        )

        vehicle_publisher = env_var_to_vehicle_publisher[
            self.ENV.publisher_to_converters
        ]()
        operation_publisher = env_var_to_operation_publisher[
            self.ENV.publisher_to_converters
        ]()

        self.replayed_event_bus = prepare_env_var_to_scheduled_event_bus(
            vehicle_publisher, operation_publisher, replay_speed=replay_speed, date=date
        )[self.ENV.scheduled_event_bus]()
