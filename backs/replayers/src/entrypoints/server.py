import asyncio
from asyncio.events import AbstractEventLoop
from typing import Optional

import click
from entrypoints.config import Config
from entrypoints.environment_variables import EnvironmentVariables, get_env_variables


def main(
    speed: int, date: Optional[str], env: EnvironmentVariables, loop: AbstractEventLoop
):
    config = Config(env=env, replay_speed=speed, date=date)
    replayed_event_bus = config.replayed_event_bus
    replayed_event_bus.start()
    loop.create_task(replayed_event_bus.async_loop())


@click.command()
@click.option("--speed", default=30, help="Replay speed. Default 30.")
@click.option("--date", required=False, help="Date of the file to replay.")
def cli(speed: int, date: Optional[str] = None):
    env = get_env_variables()
    loop = asyncio.get_event_loop()
    main(speed, date, env, loop)
    loop.run_forever()


if __name__ == "__main__":
    # execute only if run as a script
    cli()
