import os
from dataclasses import dataclass
from typing import List, Literal, Optional

from dotenv import load_dotenv

from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

load_dotenv()

ScheduledEventBusOption = Literal[
    "RANDOM_ADAGIO", "REPLAYED_ADAGIO"
]  # coming soon => "RANDOM_ADAGIO", "REPLAYED_ADAGIO", # coming some day => "RANDOM_NEXSIS"
scheduled_event_bus_options: List[ScheduledEventBusOption] = [
    "RANDOM_ADAGIO",
    "REPLAYED_ADAGIO",
]

PublisherToConvertersOption = Literal["IN_MEMORY", "HTTP"]
publisher_to_converters_options: List[PublisherToConvertersOption] = [
    "IN_MEMORY",
    "HTTP",
]


@dataclass
class EnvironmentVariables:
    scheduled_event_bus: ScheduledEventBusOption
    publisher_to_converters: PublisherToConvertersOption
    converters_url: Optional[str]

    def __str__(self):
        return f"\
            scheduled_event_bus: {self.scheduled_event_bus} \n \
            publisher_to_converters: {self.publisher_to_converters} \n \
            converters_url : {self.converters_url} "


get_env_variable = prepare_get_env_variable(os.environ.get)


def get_env_variables() -> EnvironmentVariables:
    scheduled_event_bus = get_env_variable(
        "SCHEDULED_EVENT_BUS", scheduled_event_bus_options
    )

    publisher_to_converters = get_env_variable(
        "PUBLISHER_TO_CONVERTERS", publisher_to_converters_options
    )
    converters_url = None
    if publisher_to_converters == "HTTP":
        converters_url = os.environ.get("CONVERTERS_URL")
        if not converters_url:
            raise Exception("When CONVERTERS_URL is HTTP, a CONVERTERS_URL is needed")

    return EnvironmentVariables(
        scheduled_event_bus=scheduled_event_bus,
        publisher_to_converters=publisher_to_converters,
        converters_url=converters_url,
    )
