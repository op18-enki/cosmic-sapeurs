from setuptools import setup

setup(
    name="replayers",
    version="0.1",
    packages=["."],
    install_requires=["requests", "pandas", "python-dotenv"],
)
