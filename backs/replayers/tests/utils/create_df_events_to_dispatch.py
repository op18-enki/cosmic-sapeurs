from typing import Callable, List, Union

import pandas as pd

from shared.external_event import ExternalEvent
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import Event
from shared.test_utils.reset_path import reset_path


def event_to_series(event: Union[Event, ExternalEvent]) -> pd.Series:
    event_dict = event.as_dict()
    return pd.Series(event_dict)


def create_df_events_to_dispatch(
    timestamps: Union[List[DateStr], List[str]], make_event: Callable
):
    list_series = []
    for timestamp in timestamps:
        event = make_event(timestamp=timestamp, uuid=uuid4())
        list_series.append(event_to_series(event))

    df = pd.DataFrame(list_series)
    return df


def create_csv_events_to_dispatch(
    path: str, timestamps: Union[List[DateStr], List[str]], make_event: Callable
):
    reset_path(path)
    df = create_df_events_to_dispatch(timestamps, make_event=make_event)
    df.to_csv(path)
    return df
