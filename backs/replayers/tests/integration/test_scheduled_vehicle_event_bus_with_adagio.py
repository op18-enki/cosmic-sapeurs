import asyncio

import pandas as pd
from adapters.in_memory_publisher import InMemoryPublisher
from adapters.scheduled_event_bus import ReplayedEventBus
from tests.utils.create_df_events_to_dispatch import event_to_series

from shared.adagio_event_dict_to_event import adagio_vehicle_event_dict_to_event
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr


async def _async_test_one_step(
    event_bus, published_events, expected_dispatched_events_timestamps
):
    # dispatch first event
    clock = event_bus.clock
    awaken_event = asyncio.Event()
    looping_task = asyncio.create_task(event_bus.async_loop())
    clock.set_awaken_event(awaken_event)
    await clock.wake_up()
    await looping_task

    assert [
        str(event.data.timestamp) for event in published_events
    ] == expected_dispatched_events_timestamps


timestamps = [
    "2020-10-15T17:46:09.850Z",
    "2020-10-15T17:47:00.850Z",
    "2020-10-15T17:47:10.850Z",
]
test_start_time = DateStr("2020-11-02T15:00:00.000Z")


def prepare_vehicle_event_bus_and_spy(
    resync: bool, time_step: float, speed: float, clock: CustomClock = None
):
    list_series = []
    for timestamp in timestamps:
        event = make_adagio_vehicle_event(timestamp=timestamp, id_intervention=-1)
        list_series.append(event_to_series(event))
    df = pd.DataFrame(list_series)

    clock = clock or CustomClock(test_start_time)

    publisher = InMemoryPublisher()

    event_bus = ReplayedEventBus(
        clock,
        df=df,
        resync=resync,
        time_step=time_step,
        speed=speed,
        publisher=publisher,
        event_dict_to_event=adagio_vehicle_event_dict_to_event,
    )
    published_events = publisher.published_events

    event_bus.start()
    return event_bus, published_events, clock


def test_provided_adagio_vehicle_event_get_dispatched_correctly():
    speed = 1
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_vehicle_event_bus_and_spy(
        resync, time_step, speed
    )

    expected_dispatched_events_timestamps = [timestamps[0]]
    clock.add_seconds(1)
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )


def test_streaming_speed_with_adagio():
    speed = 60
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_vehicle_event_bus_and_spy(
        resync, time_step, speed
    )

    clock.add_seconds(1)  # equivalent to 60 seconds

    expected_dispatched_events_timestamps = [timestamps[0], timestamps[1]]
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )


# TODO : Decide if we get rid of resync for adagio ?
# => The only problem is when setting the date back in the event data after having resync

# def test_resync_with_adagio():
#     speed = 1
#     time_step = 1
#     resync = True

#     event_bus, published_events, clock = prepare_vehicle_event_bus_and_spy(
#         resync, time_step, speed
#     )
#     clock.add_seconds(1)

#     expected_dispatched_events_timestamps = ["2020-11-02T15:00:00.000Z"]
#     asyncio.run(
#         _async_test_one_step(
#             event_bus,
#             published_events,
#             expected_dispatched_events_timestamps,
#         )
#     )
