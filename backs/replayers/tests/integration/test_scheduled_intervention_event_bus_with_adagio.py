import asyncio

import pandas as pd
from adapters.in_memory_publisher import InMemoryPublisher
from adapters.scheduled_event_bus import ReplayedEventBus
from tests.utils.create_df_events_to_dispatch import event_to_series

from shared.adagio_event_dict_to_event import adagio_intervention_event_dict_to_event
from shared.factories.adagio.adagio_intervention_event_factory import (
    make_adagio_intervention_created_event,
)
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr


async def _async_test_one_step(
    event_bus, published_events, expected_dispatched_events_timestamps
):
    # dispatch first event
    clock = event_bus.clock
    awaken_event = asyncio.Event()
    looping_task = asyncio.create_task(event_bus.async_loop())
    clock.set_awaken_event(awaken_event)
    await clock.wake_up()
    await looping_task

    assert [
        event.data.OdeResume.EtatIntervention for event in published_events
    ] == expected_dispatched_events_timestamps


intervention_timestamps = [
    "2020-10-09T17:35:24.000Z",
    "2020-10-09T17:36:20.000Z",
    "2020-10-09T17:36:30.000Z",
]
test_start_time = DateStr("2020-11-02T15:00:00.000Z")


def prepare_intervention_event_bus_and_spy(
    resync: bool, time_step: float, speed: float, clock: CustomClock = None
):
    list_series = []
    for intervention_timestamp in intervention_timestamps:
        event = make_adagio_intervention_created_event(
            timestamp=DateStr(intervention_timestamp), id_intervention=-1
        )
        list_series.append(event_to_series(event))
    df = pd.DataFrame(list_series)
    clock = clock or CustomClock(test_start_time)

    publisher = InMemoryPublisher()

    event_bus = ReplayedEventBus(
        clock,
        df=df,
        resync=resync,
        time_step=time_step,
        speed=speed,
        publisher=publisher,
        event_dict_to_event=adagio_intervention_event_dict_to_event,
    )
    published_events = publisher.published_events

    event_bus.start()
    return event_bus, published_events, clock


def test_provided_adagio_intervention_event_get_dispatched_correctly():
    speed = 1
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_intervention_event_bus_and_spy(
        resync, time_step, speed
    )

    expected_dispatched_events_timestamps = [intervention_timestamps[0]]
    clock.add_seconds(1)
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )
