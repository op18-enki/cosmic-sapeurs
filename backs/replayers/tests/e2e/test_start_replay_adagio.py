import asyncio

from entrypoints.environment_variables import EnvironmentVariables
from entrypoints.server import main


async def skip_test_start_replay_adagio():
    env = EnvironmentVariables(
        scheduled_event_bus="REPLAYED_ADAGIO",
        publisher_to_converters="IN_MEMORY",
        converters_url="",
    )
    loop = asyncio.get_event_loop()
    main(10, None, env, loop)
    await asyncio.sleep(2)
    loop.stop()
