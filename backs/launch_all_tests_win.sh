#!/bin/bash  
echo "------- Testing cover-ops and shared packages..."
cd cover-ops
source venv/Scripts/activate
pytest ../shared/tests
pytest tests
deactivate 

echo "------- Testing converters package..."
cd ../converters
source venv/Scripts/activate
pytest tests
deactivate 

echo "------- Testing replayers package..."
cd ../replayers
source venv/Scripts/activate
pytest tests


echo "------- Black..."
cd ../
black --check .

deactivate 
echo "All good !"
