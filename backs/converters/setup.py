import pathlib

from setuptools import find_packages, setup

packages = find_packages(exclude=[])

HERE = pathlib.Path(__file__).parent
INSTALL_REQUIRES = (HERE / "requirements.txt").read_text().splitlines()
TESTS_REQUIRE = (HERE / "tests-requirements.txt").read_text().splitlines()

setup(
    name="converters",
    version="0.1",
    packages=packages,
    install_requires=INSTALL_REQUIRES,
    tests_require=TESTS_REQUIRE,
)
