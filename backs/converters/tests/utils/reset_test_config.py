from pathlib import Path

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.adapters.adagio.postgres.db import reset_db
from converters.entrypoints.config import Config
from shared.helpers.remove_folder_content import remove_folder_content


def reset_test_config(config: Config):
    if isinstance(config.raw_vehicle_events_repo, AdagioCsvEventsRepository):
        remove_folder_content(Path("data"))
    elif isinstance(config.raw_vehicle_events_repo, AdagioPgEventsRepository):
        reset_db(config.raw_vehicle_events_repo.connection)
