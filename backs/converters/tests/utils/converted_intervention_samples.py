from shared.data_transfert_objects.custom_types import RawOperationId
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.helpers.date import DateStr

example_adagio_intervention_data_1 = {
    "OdeResume": {
        "LibelleMotif": "Accident matériel de la circulation (hors mission BSPP)",
        "Delestage": 0,
        "Procedure": "H",
        "Precurseur": False,
        "AlerteCoordination": False,
        "RenfortMoyenDeleste": False,
        "RenfortMoyenNonDeleste": False,
        "Emetteur": {"Libelle": ""},
        "EtatIntervention": "14/02/2017 18:02:40",
        "Cstc": {"Libelle": "STOU"},
        "Motif": "215",
        "NbEnginsDepart": 0,
        "ListeEnginsDepart": None,
        "NbEnginsPartis": 0,
        "NbEnginsPresentes": 0,
        "Adresse": {
            "IdObjetGeo": 4258879,
            "CodeTypeReseau": "ROUTIER",
            "CodeTypeAdresse": "ADR_PRINCIPALE",
            "Commentaire": None,
            "X": 662622.375,
            "Y": 6875119,
            "LibelleComplet": " AUTOROUTE A1 W 95277 GONESSE",
            "TypeInfrastructure": None,
            "CategorieFonctionnelle": "AUTOROUTE",
        },
        "IdIntervention": 11619855,
    },
    "Intervention": {
        "ObservationMotifAlerte": "CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
        "ObservationAdresse": "AL14R(PAU) TRONCON COMMUN",
        "ObservationMMA": "MMA manquants : ",
        "ObservationECTS": "",
        "IdIntervention": 11619855,
        "RenseignementComplementaire": "appel du COL TROHEL // peugeot 3008 noire en panne sur la voie de droite // entre la jonction A1/A104 et la jonction A1/N2 // sens province Paris // CRS prévenus ==> envoi d'une patrouille ",
    },
    "timestamp": "2017-02-14T17:02:41.120Z",
}

expected_converted_operation_data_1 = OperationEventData(
    timestamp=DateStr("2017-02-14T17:02:41.120Z"),
    raw_operation_id=RawOperationId("11619855"),
    status="opened",
    address=" AUTOROUTE A1 W 95277 GONESSE",
    address_area="STOU",
    cause="accident",
    raw_cause="Accident matériel de la circulation (hors mission BSPP)",
    raw_procedure="H",
    departure_criteria="215",
    longitude=2.4894227,
    latitude=48.9749251,
    affected_vehicles=[],
)
