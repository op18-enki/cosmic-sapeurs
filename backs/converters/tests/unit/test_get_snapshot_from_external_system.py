import asyncio
from converters.adapters.adagio.adagio_resource_event_mapper import (
    AdagioRessourceEventMapper,
)

from tests.utils.converted_intervention_samples import (
    example_adagio_intervention_data_1,
    expected_converted_operation_data_1,
)
from tests.utils.converted_resource_samples import (
    example_adagio_resource_event_data_0,
    expected_vehicle_event_converted_from_resource_exemple_0,
)
from tests.utils.converted_vehicle_samples import (
    example_adagio_initialization_vehicle_event_1_converted,
    example_adagio_vehicle_event_0,
    example_adagio_vehicle_event_1,
    example_initialization_adagio_vehicle_event_0_converted,
)

from converters.adapters.adagio.adagio_operation_event_mapper import (
    AdagioOperationEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_events_repository import (
    InMemoryAdagioVehicleEventsRepository,
)
from converters.domain.ports.external_system_gateway import (
    CustomExternalSystemGateway,
    ExternalSystemGatewaySnapshot,
)
from converters.domain.use_cases.get_snapshot_from_external_system import (
    GetSnapshotFromExternalSystem,
)
from shared.converters_snapshot import ConvertersSnapshot
from shared.helpers.uuid import CustomUuid


async def _async_test_get_snapshot_from_external_system():
    vehicle_mapper = AdagioVehicleEventMapper(uuid=CustomUuid())
    operation_mapper = AdagioOperationEventMapper(uuid=CustomUuid())
    resource_mapper = AdagioRessourceEventMapper(uuid=CustomUuid())
    external_sytem_gateway = CustomExternalSystemGateway()
    adagio_vehicle_event_repo = InMemoryAdagioVehicleEventsRepository(purge=False)

    get_last_status_of_all_vehicles = GetSnapshotFromExternalSystem(
        external_system_gateway=external_sytem_gateway,
        vehicle_mapper=vehicle_mapper,
        operation_mapper=operation_mapper,
        resource_mapper=resource_mapper,
        raw_vehicle_events_repo=adagio_vehicle_event_repo,
    )

    adagio_vehicle_datas_as_dict = [
        example_adagio_vehicle_event_0["data"],
        example_adagio_vehicle_event_1["data"],
    ]

    snapshot = ExternalSystemGatewaySnapshot(
        adagio_vehicle_datas_as_dict,
        [example_adagio_intervention_data_1],
        [example_adagio_resource_event_data_0],
    )

    external_sytem_gateway.set_snapshot(
        {
            "data": {
                "vehicles": snapshot.vehicles,
                "operations": snapshot.operations,
                "resources": snapshot.resources,
            }
        }
    )

    expected_snapshot = ConvertersSnapshot(
        vehicles=[
            example_initialization_adagio_vehicle_event_0_converted.data,
            example_adagio_initialization_vehicle_event_1_converted.data,
            expected_vehicle_event_converted_from_resource_exemple_0.data,
        ],
        operations=[expected_converted_operation_data_1],
    )

    actual_snapshot = await get_last_status_of_all_vehicles.execute()
    assert actual_snapshot == expected_snapshot


def test_get_snapshot_from_external_system():
    asyncio.run(_async_test_get_snapshot_from_external_system())
