import copy
import json
from dataclasses import asdict

import pytest

from converters.entrypoints.config import Config

# from tests.e2e.adagio_server_fixture import make_adagio_server_fixture
from converters.entrypoints.environment_variables import EnvironmentVariables
from converters.entrypoints.server import make_app, route_external_vehicle_event
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)
from shared.test_utils.test_pg_url import test_pg_url

test_env = EnvironmentVariables(
    external_events_source="ADAGIO",
    publisher_to_cover_ops="IN_MEMORY",
    cover_ops_url="",
    converters_port=8081,
    external_system_gateway="CUSTOM",
    external_system_gateway_vehicles_url="foo:8080/vehicles",
    external_system_gateway_snapshot_url="foo:8080/snapshot",
    repositories="PG",
    pg_url=test_pg_url,
)


config = Config(test_env)


@pytest.fixture
def adagio_server_fixture(loop, aiohttp_client):
    """Start server for end-to-end tests"""
    app, loop = make_app(config)
    return loop.run_until_complete(aiohttp_client(app))


adagio_vehicle_event = make_adagio_vehicle_event(
    id_mma=1,
    abrege_famille_mma_operationnelle="PSE",
    abrege_statut_operationnel="PARTI",
    timestamp="2020-12-10T12:30:00.853Z",
    id_intervention=3,
    libelle_affectation_operationnelle="STOU",
)
adagio_vehicle_event_as_dict = asdict(adagio_vehicle_event)


async def test_convert_from_adagio_and_publish_vehicle_event_to_cover_ops(
    adagio_server_fixture,
):
    # reset publisher events
    config.publisher.__init__()

    resp = await adagio_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(adagio_vehicle_event_as_dict)
    )

    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(config.publisher.published_events) == 1  # type: ignore (AbstractPublisher does not have this property)


async def test_fails_when_event_from_adagio_with_wrong_data_is_provided(
    adagio_server_fixture,
):
    wrong_external_event = copy.copy(adagio_vehicle_event_as_dict)
    del wrong_external_event["data"]["Associe"]
    resp = await adagio_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(wrong_external_event)
    )

    assert resp.status == 400
    resp_json = await resp.json()
    assert resp_json["success"] == "false"
    assert resp_json["reason"] == "{'Associe': ['Missing data for required field.']}"
