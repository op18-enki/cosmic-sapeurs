import copy
import json
from dataclasses import asdict

import pytest

from converters.entrypoints.config import Config

# from tests.e2e.adagio_server_fixture import make_adagio_server_fixture
from converters.entrypoints.environment_variables import EnvironmentVariables
from converters.entrypoints.server import make_app, route_external_resource_event
from shared.factories.adagio.adagio_resource_event_factory import (
    make_adagio_resource_event,
)
from shared.test_utils.test_pg_url import test_pg_url

test_env = EnvironmentVariables(
    external_events_source="ADAGIO",
    publisher_to_cover_ops="IN_MEMORY",
    cover_ops_url="",
    converters_port=8081,
    external_system_gateway="CUSTOM",
    external_system_gateway_vehicles_url="foo:8080/vehicles",
    external_system_gateway_snapshot_url="foo:8080/snapshot",
    repositories="PG",
    pg_url=test_pg_url,
)


config = Config(test_env)


@pytest.fixture
def adagio_server_fixture(loop, aiohttp_client):
    """Start server for end-to-end tests"""
    app, loop = make_app(config)
    return loop.run_until_complete(aiohttp_client(app))


adagio_resource_event = make_adagio_resource_event(
    id_intervention=4234,
    abrege_famille_ressources_partagees="bamr",
    timestamp="2020-12-09T17:32:18.897Z",
    immatriculation_bspp="BAMR 123",
    id_resource_partagee=123,
    abrege_statut_operationnel="RENTRE",
    libelle_statut_operationnel="Rentré",
    libelle_affectation_operationnelle="LAND",
)
adagio_resource_event_as_dict = asdict(adagio_resource_event)


async def test_convert_from_adagio_and_publish_resource_event_to_cover_ops(
    adagio_server_fixture,
):
    # reset publisher events
    config.publisher.__init__()

    resp = await adagio_server_fixture.post(
        route_external_resource_event, data=json.dumps(adagio_resource_event_as_dict)
    )

    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(config.publisher.published_events) == 1  # type: ignore (AbstractPublisher does not have this property)


async def test_fails_when_event_from_adagio_with_wrong_data_is_provided(
    adagio_server_fixture,
):
    wrong_external_event = copy.copy(adagio_resource_event_as_dict)
    del wrong_external_event["data"]["AbregeFamilleRessourcesPartagees"]
    resp = await adagio_server_fixture.post(
        route_external_resource_event, data=json.dumps(wrong_external_event)
    )

    assert resp.status == 400
    resp_json = await resp.json()
    assert resp_json["success"] == "false"
    assert (
        resp_json["reason"]
        == "{'AbregeFamilleRessourcesPartagees': ['Missing data for required field.']}"
    )
