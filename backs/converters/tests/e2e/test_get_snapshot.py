import json
import os

import pytest

from converters.entrypoints.config import Config
from converters.entrypoints.environment_variables import EnvironmentVariables
from converters.entrypoints.server import make_app
from shared.converters_snapshot import ConvertersSnapshot
from shared.routes import route_cover_ops_snapshot
from shared.test_utils.test_pg_url import test_pg_url

test_env = EnvironmentVariables(
    external_events_source="ADAGIO",
    publisher_to_cover_ops="IN_MEMORY",
    cover_ops_url="",
    converters_port=8081,
    external_system_gateway="CUSTOM",
    external_system_gateway_vehicles_url="foo:8080/vehicles",
    external_system_gateway_snapshot_url="foo:8080/snapshot",
    repositories="CSV",
    pg_url=test_pg_url,
)


config = Config(test_env)


@pytest.fixture
def identity_server_fixture(loop, aiohttp_client):
    """Start server for end-to-end tests"""

    app, loop = make_app(config)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.mark.asyncio
async def skip_test_offline_test_get_snapshot(  # remove "offline" preffix to run the test (cannot run on gitlab since we don't commit the JSON file)
    identity_server_fixture,
):

    resp = await identity_server_fixture.get(
        route_cover_ops_snapshot,
    )
    resp_json = await resp.json()

    with open("./data/converted_snapshot.json", "w") as file:
        json.dump(resp_json, file)

    assert resp.status == 200

    # Check it can be converted to ConvertersSnapshot
    snapshot = ConvertersSnapshot.from_dict(resp_json)

    # Check majority datas could be converted
    with open(os.getenv("JSON_CUSTOM_EXTERNAL_SYSTEM_GATEWAY", ""), "r") as file:
        adagio_response = json.load(file)

    # Check more that half of events could be converted
    assert len(snapshot.vehicles) > len(adagio_response["data"]["vehicles"]) / 2
    assert len(snapshot.operations) > len(adagio_response["data"]["operations"]) / 2
