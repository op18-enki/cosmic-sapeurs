from converters.adapters.http_publisher import HttpPublisher


def test_can_publish():
    http_publisher = HttpPublisher("http://dummy_url.com")
    assert http_publisher.can_publish() == False
