import json
import os
from pathlib import Path

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from shared.helpers.csv import read_csv
from shared.helpers.date import DateStr


def empty_folder(path):
    if os.path.exists(path):
        for file in os.listdir(path):
            os.remove(os.path.join(path, file))


path = os.path.join("data", "tests")
empty_folder(path)


def test_adagio_csv_events_repo_for_serializable_event():
    now = "2020-12-15T11:30:00.000Z"
    suffix = "test"
    filename = f"{now.replace(':','-')}_{suffix}.csv"
    csv_path = Path(os.path.join(path, filename))

    repo = AdagioCsvEventsRepository(csv_path=csv_path, purge=False)
    event_1 = dict(
        topic="foo_topic",
        data={
            "idEvent": 1,
            "status": {"id": 1, "label": "PARTI"},
            "timeGMT": "2020/01/03 23:03",
        },
    )
    event_2 = dict(
        topic="foo_topic",
        data={
            "idEvent": 2,
            "status": {"id": 9, "label": "RENTRE"},
            "timeGMT": "2020/03/03 23:03",
        },
    )
    repo.add(event_1)
    repo.add(event_2)
    headers, rows = read_csv(csv_path)
    assert len(rows) == 2
    assert json.loads(rows[1][1]) == {
        "idEvent": 2,
        "status": {"id": 9, "label": "RENTRE"},
        "timeGMT": "2020/03/03 23:03",
    }


def test_adagio_csv_events_repo_for_unserializable_event():
    now = "2020-12-15T11:30:00.000Z"
    suffix = "test_error"
    filename = f"{now.replace(':','-')}_{suffix}.csv"
    csv_path = Path(os.path.join(path, filename))
    repo = AdagioCsvEventsRepository(csv_path=csv_path, purge=False)

    class FooClass:
        pass

    unserializable_class = FooClass()
    event = dict(
        topic="foo_topic",
        data={
            "wrong_field": unserializable_class,
        },
    )

    repo.add(event)
    headers, rows = read_csv(csv_path)
    assert rows[0][1] == "Object of type FooClass is not JSON serializable"
