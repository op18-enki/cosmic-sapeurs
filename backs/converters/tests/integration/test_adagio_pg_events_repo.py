from sqlalchemy.engine import create_engine
from sqlalchemy.sql import select

from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.adapters.adagio.postgres.db import (
    drop_db,
    initialize_db,
    raw_operation_events_table,
)
from shared.test_utils.test_pg_url import test_pg_url


def prepare_db():
    engine = create_engine(
        test_pg_url,
        isolation_level="REPEATABLE READ",
    )
    drop_db(engine)
    initialize_db(engine)
    return engine


def test_adagio_pg_events_repo_for_serializable_event():
    engine = prepare_db()
    repo = AdagioPgEventsRepository(
        engine=engine, table=raw_operation_events_table, purge=False
    )
    event_1 = dict(
        topic="foo_topic",
        data={
            "idEvent": 1,
            "status": {"id": 1, "label": "PARTI"},
            "timeGMT": "2020/01/03 23:03",
        },
    )

    repo.add(event_1)

    s = select([raw_operation_events_table])
    records = engine.connect().execute(s).fetchall()

    assert len(records) == 1
    assert records[0][0] == "foo_topic"
    assert (
        records[0][1]
        == '{"idEvent": 1, "status": {"id": 1, "label": "PARTI"}, "timeGMT": "2020/01/03 23:03"}'
    )
