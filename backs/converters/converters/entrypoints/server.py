import asyncio
import os
from asyncio.events import AbstractEventLoop
from dataclasses import asdict
from typing import Tuple

import aiohttp_cors
import click
from aiohttp import web
from aiohttp.web_app import Application

from converters.entrypoints.config import Config
from converters.entrypoints.environment_variables import get_env_variables
from shared.exceptions import SapeursError
from shared.helpers.logger import logger
from shared.routes import (
    route_cover_ops_snapshot,
    route_external_operation_event,
    route_external_vehicle_event,
    route_external_resource_event,
)
from shared.wait_for_pg import wait_for_pg

external_operation_events_count = 0
external_vehicle_events_count = 0
external_resource_events_count = 0


def handle_sapeurs_error(function_description: str, e: SapeursError):
    logger.warn(f"Exception raised when {function_description} : {str(e)}")
    return web.json_response(
        {"success": "false", "reason": str(e)},
        status=400,
        reason="Incorrect format",
    )


def make_app(
    config: Config,
) -> Tuple[Application, AbstractEventLoop]:
    (
        convert_to_sapeurs_event,
        get_snapshot_from_external_system,
    ) = config.use_cases()
    routes = web.RouteTableDef()

    # @routes.get(make_url(route_get_last_status_of_all_vehicles))
    # async def get_last_status_of_all_vehicles_endpoint(request):
    #     try:
    #         vehicle_events = await get_last_status_of_all_vehicles.execute()
    #         # return web.json_response(vehicle_events_as_dict)
    #     except SapeursError as e:
    #         return handle_sapeurs_error("getting last status of all vehicles", e)

    @routes.get(f"/{route_cover_ops_snapshot}")
    async def get_converters_snapshot(request):
        try:
            converters_snapshot = await get_snapshot_from_external_system.execute()
            # return web.json_response(vehicle_events_as_dict)
            return web.json_response(asdict(converters_snapshot))
        except SapeursError as e:
            return handle_sapeurs_error("getting last status of all vehicles", e)

    @routes.post(f"/{route_external_vehicle_event}")
    async def convert_vehicle_and_publish_to_cover_ops(request):
        global external_vehicle_events_count
        external_vehicle_events_count += 1

        if request.body_exists:
            external_raw_event = await request.json()
            try:
                convert_to_sapeurs_event.execute_for_raw_vehicle_event(
                    external_raw_event
                )
                return web.json_response({"success": "true"})
            except SapeursError as e:
                return handle_sapeurs_error("converting vehicle event", e)
        return web.json_response(
            {"success": "false", "reason": "Missing body"},
            status=400,
            reason="Missing body",
        )

    @routes.post(f"/{route_external_resource_event}")
    async def convert_resource_and_publish_to_cover_ops(request):
        global external_resource_events_count
        external_resource_events_count += 1

        if request.body_exists:
            external_raw_event = await request.json()
            try:
                convert_to_sapeurs_event.execute_for_raw_resource_event(
                    external_raw_event
                )
                return web.json_response({"success": "true"})
            except SapeursError as e:
                return handle_sapeurs_error("converting resource event", e)
        return web.json_response(
            {"success": "false", "reason": "Missing body"},
            status=400,
            reason="Missing body",
        )

    @routes.post(f"/{route_external_operation_event}")
    async def convert_operation_and_publish_to_cover_ops(request):
        if request.body_exists:
            external_raw_event = await request.json()

            global external_operation_events_count
            external_operation_events_count += 1

            try:
                convert_to_sapeurs_event.execute_for_raw_operation_event(
                    external_raw_event
                )
                return web.json_response({"success": "true"})
            except SapeursError as e:
                return handle_sapeurs_error("converting operation event", e)
        return web.json_response(
            {"success": "false", "reason": "Missing body"},
            status=400,
            reason="Missing body",
        )

    loop = asyncio.get_event_loop()
    app = web.Application()
    app.add_routes(routes)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
        },
    )

    for route in list(app.router.routes()):
        cors.add(route)

    return app, loop


@click.command()
@click.option("--wait-for-services", is_flag=True)
def main(wait_for_services: bool = False):
    if wait_for_services:
        pg_url = os.getenv("PG_URL")
        if pg_url:
            wait_for_pg(pg_url)
    config = Config(env=get_env_variables())
    app, loop = make_app(config)
    # loop.create_task(get_last_status_of_all_vehicles.execute())
    runner = web.AppRunner(app)
    loop.run_until_complete(runner.setup())
    site = web.TCPSite(runner, port=config.ENV.converters_port)
    loop.run_until_complete(site.start())

    loop.run_forever()


if __name__ == "__main__":
    # execute only if run as a script
    main()
