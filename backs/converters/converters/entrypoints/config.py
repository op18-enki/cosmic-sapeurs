import os
from pathlib import Path
from typing import Callable, Dict, Tuple

from converters.adapters.adagio.adagio_external_system_gateway import (
    AdagioExternalSystemGateway,
)
from converters.adapters.adagio.adagio_intervention_events_repository import (
    CsvAdagioInterventionEventsRepository,
)
from converters.adapters.adagio.adagio_operation_event_mapper import (
    AdagioOperationEventMapper,
)
from converters.adapters.adagio.adagio_resource_event_mapper import (
    AdagioRessourceEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_events_repository import (
    CsvAdagioVehicleEventsRepository,
)
from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.adapters.adagio.postgres.db import (
    initialize_db,
    raw_operation_events_table,
    raw_vehicle_events_table,
    raw_resource_events_table,
)
from converters.adapters.http_publisher import HttpPublisher
from converters.adapters.in_memory_publisher import InMemoryPublisher
from converters.domain.ports.accumulated_events_repo import (
    AbstractAccumulatedEventsRepository,
    InMemoryAccumulatedEventsRepository,
)
from converters.domain.ports.event_mapper import (
    AbstractResourceEventMapper,
    AbstractOperationEventMapper,
    AbstractVehicleEventMapper,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from converters.domain.ports.external_system_gateway import (
    AbstractExternalSystemGateway,
    CustomExternalSystemGateway,
)
from converters.domain.ports.publisher import AbstractPublisher
from converters.domain.use_cases.convert_to_sapeurs_event import ConvertToSapeursEvent
from converters.domain.use_cases.get_snapshot_from_external_system import (
    GetSnapshotFromExternalSystem,
)
from converters.entrypoints.environment_variables import (
    EnvironmentVariables,
    ExternalEventsSourceOption,
    ExternalSystemGatewayOption,
    PublisherToCoverOpsOption,
    Repositories,
)
from shared.helpers.clock import RealClock
from shared.helpers.logger import logger
from shared.helpers.uuid import RealUuid

uuid = RealUuid()
server_started_at = (
    RealClock().get_now().replace(":", " ")
)  # replace ":" by " " for Windows compatibility

env_var_to_publisher: Dict[
    PublisherToCoverOpsOption,
    Callable[[str], AbstractPublisher],
] = {
    "IN_MEMORY": lambda url: InMemoryPublisher(),
    "HTTP": lambda url: HttpPublisher(url),
}

env_var_to_vehicle_mapper: Dict[
    ExternalEventsSourceOption, Callable[[], AbstractVehicleEventMapper]
] = {
    "ADAGIO": lambda: AdagioVehicleEventMapper(uuid=uuid),
}

env_var_to_operation_mapper: Dict[
    ExternalEventsSourceOption, Callable[[], AbstractOperationEventMapper]
] = {
    "ADAGIO": lambda: AdagioOperationEventMapper(uuid=uuid),
}

env_var_to_resource_mapper: Dict[
    ExternalEventsSourceOption, Callable[[], AbstractResourceEventMapper]
] = {
    "ADAGIO": lambda: AdagioRessourceEventMapper(uuid=uuid),
}

env_var_to_external_system_gateway: Dict[
    ExternalSystemGatewayOption, Callable[[str], AbstractExternalSystemGateway]
] = {
    "CUSTOM": lambda _: CustomExternalSystemGateway(),
    "ADAGIO": lambda url: AdagioExternalSystemGateway(url, url),
}


def get_csv_repositories(
    *_,
) -> Tuple[
    AbstractExternalEventsRepository,
    AbstractExternalEventsRepository,
    AbstractExternalEventsRepository,
]:
    raw_vehicle_events_repo = CsvAdagioVehicleEventsRepository(
        csv_path=Path(
            os.path.join(
                "data",
                "raw_events_repo",
                f"{server_started_at}_raw_vehicle_events_repo.csv",
            )
        ),
        purge=False,
    )
    raw_operation_events_repo = CsvAdagioInterventionEventsRepository(
        csv_path=Path(
            os.path.join(
                "data",
                "raw_events_repo",
                f"{server_started_at}_raw_operation_events_repo.csv",
            )
        ),
        purge=False,
    )
    raw_resource_events_repo = CsvAdagioInterventionEventsRepository(
        csv_path=Path(
            os.path.join(
                "data",
                "raw_events_repo",
                f"{server_started_at}_raw_resource_events_repo.csv",
            )
        ),
        purge=False,
    )
    return raw_vehicle_events_repo, raw_operation_events_repo, raw_resource_events_repo


def get_pg_repositories(
    pg_url: str,
) -> Tuple[
    AbstractExternalEventsRepository,
    AbstractExternalEventsRepository,
    AbstractExternalEventsRepository,
]:
    from sqlalchemy.engine import create_engine

    engine = create_engine(
        pg_url,
        isolation_level="REPEATABLE READ",
    )
    initialize_db(engine)

    raw_vehicle_events_repo = AdagioPgEventsRepository(
        engine=engine, table=raw_vehicle_events_table, purge=False
    )
    raw_operation_events_repo = AdagioPgEventsRepository(
        engine=engine, table=raw_operation_events_table, purge=False
    )
    raw_resource_events_repo = AdagioPgEventsRepository(
        engine=engine, table=raw_resource_events_table, purge=False
    )
    return raw_vehicle_events_repo, raw_operation_events_repo, raw_resource_events_repo


env_var_to_repositories: Dict[
    Repositories,
    Callable[
        [str],
        Tuple[
            AbstractExternalEventsRepository,
            AbstractExternalEventsRepository,
            AbstractExternalEventsRepository,
        ],
    ],
] = {"PG": get_pg_repositories, "CSV": get_csv_repositories}


class Config:
    raw_vehicle_events_repo: AbstractExternalEventsRepository
    raw_operation_events_repo: AbstractExternalEventsRepository
    raw_resource_events_repo: AbstractExternalEventsRepository
    publisher: AbstractPublisher
    vehicle_mapper: AbstractVehicleEventMapper
    operation_mapper: AbstractOperationEventMapper
    resource_mapper: AbstractResourceEventMapper
    external_event_repo: AbstractExternalEventsRepository
    accumulated_events_repo: AbstractAccumulatedEventsRepository

    def __init__(self, env: EnvironmentVariables) -> None:

        self.ENV = env
        logger.info(self.ENV)
        self.publisher = env_var_to_publisher[self.ENV.publisher_to_cover_ops](
            f"{self.ENV.cover_ops_url}"
        )
        self.vehicle_mapper = env_var_to_vehicle_mapper[
            self.ENV.external_events_source
        ]()
        self.operation_mapper = env_var_to_operation_mapper[
            self.ENV.external_events_source
        ]()
        self.resource_mapper = env_var_to_resource_mapper[
            self.ENV.external_events_source
        ]()
        (
            self.raw_vehicle_events_repo,
            self.raw_operation_events_repo,
            self.raw_resource_events_repo,
        ) = env_var_to_repositories[self.ENV.repositories](
            self.ENV.pg_url or "No URL PROVIDED"
        )

        self.external_system_gateway = env_var_to_external_system_gateway[
            self.ENV.external_system_gateway
        ](self.ENV.external_system_gateway_vehicles_url or "No URL PROVIDED")
        self.accumulated_events_repo = InMemoryAccumulatedEventsRepository()

    def use_cases(self):
        return (
            ConvertToSapeursEvent(
                raw_vehicle_events_repo=self.raw_vehicle_events_repo,
                vehicle_mapper=self.vehicle_mapper,
                raw_operation_events_repo=self.raw_operation_events_repo,
                raw_resource_events_repo=self.raw_resource_events_repo,
                operation_mapper=self.operation_mapper,
                resource_mapper=self.resource_mapper,
                publisher=self.publisher,
                uuid=RealUuid(),
                accumulated_events_repo=self.accumulated_events_repo,
            ),
            GetSnapshotFromExternalSystem(
                raw_vehicle_events_repo=self.raw_vehicle_events_repo,
                vehicle_mapper=self.vehicle_mapper,
                operation_mapper=self.operation_mapper,
                resource_mapper=self.resource_mapper,
                external_system_gateway=self.external_system_gateway,
            ),
        )
