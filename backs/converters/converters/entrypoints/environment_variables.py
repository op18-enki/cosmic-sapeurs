import os
from dataclasses import dataclass
from typing import List, Literal, Optional

from dotenv import load_dotenv

from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

PublisherToCoverOpsOption = Literal["IN_MEMORY", "HTTP"]
publisher_to_cover_ops_options: List[PublisherToCoverOpsOption] = ["IN_MEMORY", "HTTP"]

ExternalEventsSourceOption = Literal["ADAGIO"]  #  "NEXSIS" : not yet implemented
external_events_source_options: List[ExternalEventsSourceOption] = ["SAPEURS", "ADAGIO"]

ExternalSystemGatewayOption = Literal[
    "CUSTOM", "ADAGIO"  #  "NEXSIS" : not yet implemented
]
external_system_gateway_options: List[ExternalSystemGatewayOption] = [
    "CUSTOM",
    "ADAGIO",
]
Repositories = Literal["CSV", "PG"]
repositories_options: List[Repositories] = ["CSV", "PG"]


@dataclass
class EnvironmentVariables:
    external_events_source: ExternalEventsSourceOption
    external_system_gateway: ExternalSystemGatewayOption
    external_system_gateway_vehicles_url: Optional[str]
    external_system_gateway_snapshot_url: Optional[str]
    publisher_to_cover_ops: PublisherToCoverOpsOption
    cover_ops_url: Optional[str]
    converters_port: int
    repositories: Repositories
    pg_url: Optional[str] = None

    def __str__(self):
        return f"\
            external_events_source: {self.external_events_source} \n\
            external_system_gateway: {self.external_system_gateway} \n\
            external_system_gateway_vehicles_url: {self.external_system_gateway_vehicles_url} \n\
            external_system_gateway_snapshot_url: {self.external_system_gateway_snapshot_url} \n\
            publisher_to_cover_ops: {self.publisher_to_cover_ops} \n\
            cover_ops_url: {self.cover_ops_url} \n\
            converters_port: {self.converters_port} \n\
            repositories: {self.repositories} \n\
            pg_url: {self.pg_url} "


def get_env_variables() -> EnvironmentVariables:

    load_dotenv()

    get_env_variable = prepare_get_env_variable(os.environ.get)

    external_events_source = get_env_variable(
        "EXTERNAL_EVENTS_SOURCE", external_events_source_options
    )
    external_system_gateway = get_env_variable(
        "EXTERNAL_SYSTEM_GATEWAY", external_system_gateway_options
    )
    publisher_to_cover_ops = get_env_variable(
        "PUBLISHER_TO_COVER_OPS", publisher_to_cover_ops_options
    )
    repositories = get_env_variable("REPOSITORIES", repositories_options)

    pg_url: Optional[str] = None
    if repositories == "PG":
        pg_url = os.environ.get("PG_URL")
        if not pg_url:
            raise Exception("When REPOSITORIES is PG, a PG_URL is needed")

    cover_ops_url: Optional[str] = None
    if publisher_to_cover_ops == "HTTP":
        cover_ops_url = os.environ.get("COVER_OPS_URL")
        if not cover_ops_url:
            raise Exception(
                "When PUBLISHER_TO_COVER_OPS is HTTP, a COVER_OPS_URL is needed"
            )

    external_system_gateway = get_env_variable(
        "EXTERNAL_SYSTEM_GATEWAY", external_system_gateway_options
    )
    external_system_gateway_vehicles_url: Optional[str] = None
    external_system_gateway_snapshot_url: Optional[str] = None
    if external_system_gateway == "ADAGIO":
        external_system_gateway_vehicles_url = os.environ.get(
            "EXTERNAL_SYSTEM_GATEWAY_VEHICLES_URL"
        )
        if not external_system_gateway_vehicles_url:
            raise Exception(
                "When EXTERNAL_SYSTEM_GATEWAY is ADAGIO, a EXTERNAL_SYSTEM_GATEWAY_VEHICLES_URL is needed"
            )
        external_system_gateway_snapshot_url = os.environ.get(
            "EXTERNAL_SYSTEM_GATEWAY_SNAPSHOT_URL"
        )
        if not external_system_gateway_snapshot_url:
            raise Exception(
                "When EXTERNAL_SYSTEM_GATEWAY is ADAGIO, a EXTERNAL_SYSTEM_GATEWAY_SNAPSHOT_URL is needed"
            )
    port = os.environ.get("PORT")
    if not port:
        raise Exception("Missing  environment variable 'PORT'")

    return EnvironmentVariables(
        publisher_to_cover_ops=publisher_to_cover_ops,
        cover_ops_url=cover_ops_url,
        external_events_source=external_events_source,
        converters_port=int(port),
        external_system_gateway=external_system_gateway,
        external_system_gateway_vehicles_url=external_system_gateway_vehicles_url,
        external_system_gateway_snapshot_url=external_system_gateway_snapshot_url,
        repositories=repositories,
        pg_url=pg_url,
    )
