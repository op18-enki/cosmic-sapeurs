import sys

from shared.wait_for_pg import wait_for_pg

if __name__ == "__main__":
    pg_url = sys.argv[1]
    wait_for_pg(pg_url)
