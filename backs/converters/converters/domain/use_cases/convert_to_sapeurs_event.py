from typing import Dict, Union

from converters.domain.ports.accumulated_events_repo import (
    AbstractAccumulatedEventsRepository,
)
from converters.domain.ports.event_mapper import (
    AbstractOperationEventMapper,
    AbstractResourceEventMapper,
    AbstractVehicleEventMapper,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from converters.domain.ports.publisher import AbstractPublisher
from shared.data_transfert_objects.operation_event_data import operation_just_opened
from shared.external_event import ExternalEventAsDict
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import ConvertersCatchesUpEvent, Event


class ConvertToSapeursEvent:
    def __init__(
        self,
        raw_vehicle_events_repo: AbstractExternalEventsRepository,
        vehicle_mapper: AbstractVehicleEventMapper,
        raw_operation_events_repo: AbstractExternalEventsRepository,
        raw_resource_events_repo: AbstractExternalEventsRepository,
        operation_mapper: AbstractOperationEventMapper,
        resource_mapper: AbstractResourceEventMapper,
        accumulated_events_repo: AbstractAccumulatedEventsRepository,
        publisher: AbstractPublisher,
        uuid: AbstractUuid,
    ):
        self.raw_vehicle_events_repo = raw_vehicle_events_repo
        self.raw_operation_events_repo = raw_operation_events_repo
        self.raw_resource_events_repo = raw_resource_events_repo
        self.vehicle_mapper = vehicle_mapper
        self.operation_mapper = operation_mapper
        self.resource_mapper = resource_mapper
        self.publisher = publisher
        self.was_able_to_publish = True
        self.accumulated_events_repo = accumulated_events_repo
        self.uuid = uuid

    def execute_for_raw_vehicle_event(
        self, raw_event_as_dict: Union[Dict, ExternalEventAsDict]
    ):
        self.raw_vehicle_events_repo.add(raw_event_as_dict)
        sapeurs_vehicle_event = self.vehicle_mapper.convert(
            raw_event_as_dict, source="converters_online"
        )
        self.handle_publication(sapeurs_vehicle_event)

    def execute_for_raw_operation_event(
        self, raw_event_as_dict: Union[Dict, ExternalEventAsDict]
    ):
        self.raw_operation_events_repo.add(raw_event_as_dict)
        sapeurs_operation_event = self.operation_mapper.convert(
            raw_event_as_dict, source="converters_online"
        )
        if sapeurs_operation_event.data.status == operation_just_opened:
            self.handle_publication(sapeurs_operation_event)

    def execute_for_raw_resource_event(
        self, raw_event_as_dict: Union[Dict, ExternalEventAsDict]
    ):
        self.raw_resource_events_repo.add(raw_event_as_dict)
        sapeurs_vehicle_event_from_resource = self.resource_mapper.convert(
            raw_event_as_dict, source="converters_online"
        )
        self.handle_publication(sapeurs_vehicle_event_from_resource)

    def handle_publication(self, event: Event):
        if self.was_able_to_publish:
            try:
                self.publisher.publish_to_cover_ops(event)
            except:
                logger.info("CoverOps is not available, accumulating events...")
                self.was_able_to_publish = False
        if not self.was_able_to_publish:
            event.source = "converters_catch_up"  # TODO / Question : Not quite sure of this one. Is it necessary to distinguish the source here from `conveters_online`?
            self.accumulated_events_repo.add(event)
            is_able_to_publish = self.publisher.can_publish()
            if is_able_to_publish:
                accumulated_events = (
                    self.accumulated_events_repo.pop_accumulated_events()
                )
                catch_up_event = ConvertersCatchesUpEvent(
                    uuid=self.uuid.make(),
                    data=accumulated_events,
                )
                logger.info(
                    f"Publishing catch_up event, {len(accumulated_events)} events"
                )
                self.publisher.publish_to_cover_ops(catch_up_event)

            self.was_able_to_publish = is_able_to_publish
