import json
from collections import Counter
from dataclasses import asdict
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Tuple

from converters.domain.ports.event_mapper import (
    AbstractOperationEventMapper,
    AbstractResourceEventMapper,
    AbstractVehicleEventMapper,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from converters.domain.ports.external_system_gateway import (
    AbstractExternalSystemGateway,
    ExternalSystemGatewaySnapshot,
)
from shared.converters_snapshot import ConvertersSnapshot
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData
from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger
from shared.helpers.mkdir_if_relevent import mkdir_if_relevant


class GetSnapshotFromExternalSystem:
    def __init__(
        self,
        external_system_gateway: AbstractExternalSystemGateway,
        vehicle_mapper: AbstractVehicleEventMapper,
        resource_mapper: AbstractResourceEventMapper,
        operation_mapper: AbstractOperationEventMapper,
        raw_vehicle_events_repo: AbstractExternalEventsRepository,
    ) -> None:
        self.external_system_gateway = external_system_gateway
        self.vehicle_data_mapper = vehicle_mapper
        self.operation_mapper = operation_mapper
        self.ressource_mapper = resource_mapper
        self.raw_vehicle_events_repo = raw_vehicle_events_repo

    async def execute(self) -> ConvertersSnapshot:
        # 1. Get snapshot and save to repo
        external_system_response = self.external_system_gateway.get_snapshot()
        external_system_snapshot = ExternalSystemGatewaySnapshot(
            external_system_response["data"]["vehicles"],
            external_system_response["data"]["operations"],
            external_system_response["data"]["resources"],
        )
        self.raw_vehicle_events_repo.add(
            {"topic": "snapshot", "data": asdict(external_system_snapshot)}  # type: ignore
        )

        # 2. Convert vehicle events and report errors
        (
            converted_vehicle_datas,
            vehicle_conversion_errors,
        ) = self.convert_raw_vehicle_datas_as_dict(external_system_snapshot.vehicles)

        logger.info(
            f"Converted {len(converted_vehicle_datas)} vehicle events out of {len(external_system_snapshot.vehicles)}"
            + f"\n The errors were : \n {str(vehicle_conversion_errors)}"
        )

        # 2.bis Convert ressource events and report errors
        (
            converted_vehicle_datas_from_resources,
            resource_conversion_errors,
        ) = self.convert_raw_ressource_datas_as_dict(external_system_snapshot.resources)

        logger.info(
            f"Converted {len(converted_vehicle_datas_from_resources)} resource events out of {len(external_system_snapshot.resources)}"
            + f"\n The errors were : \n {str(resource_conversion_errors)}"
        )

        # 3. Convert operations events and report errors

        (
            converted_operations_datas,
            operations_conversion_errors,
        ) = self.convert_raw_intervention_datas_as_dict(
            external_system_snapshot.operations
        )

        logger.info(
            f"Converted {len(converted_operations_datas)} operations events out of {len(external_system_snapshot.operations)}"
            + f"\n The errors were : {str(operations_conversion_errors)}"
            if operations_conversion_errors
            else ""
        )
        self.dump_conversion_errors(
            vehicle_conversion_errors, operations_conversion_errors
        )

        return ConvertersSnapshot(
            vehicles=converted_vehicle_datas + converted_vehicle_datas_from_resources,
            operations=converted_operations_datas,
        )

    def convert_raw_vehicle_datas_as_dict(
        self,
        raw_vehicle_datas_as_dict: List[Dict],
    ) -> Tuple[List[VehicleEventData], Dict]:
        converted_datas = []
        errors = []
        unknown_AbregeFamilleMMA = []
        unknown_cstcs = []
        for data_as_dict in raw_vehicle_datas_as_dict:
            try:
                converted_data = self.vehicle_data_mapper.convert_data(data_as_dict)
                if converted_data.role != "other":  # type: ignore
                    converted_datas.append(converted_data)
            except InvalidEventFormatError as e:
                # logger.warn(
                #     f"\n \n Received event with invalid format: \n {str(e)}. \n Skipping event: \n {data_as_dict},"
                # )
                errors.append(str(e))
                if "AbregeFamilleMMAOperationnelle" in str(e):
                    unknown_AbregeFamilleMMA.append(
                        data_as_dict["AbregeFamilleMMAOperationnelle"]
                    )
                elif "AbregeFamilleMMAOriginelle" in str(e):
                    unknown_AbregeFamilleMMA.append(
                        data_as_dict["AbregeFamilleMMAOperationnelle"]
                    )
                elif "LibelleAffectationOperationnelle" in str(e):
                    unknown_cstcs.append(
                        data_as_dict["LibelleAffectationOperationnelle"]
                    )

        return converted_datas, {
            "errors": dict(Counter(errors)),
            "unknown_cstcs": dict(Counter(unknown_cstcs)),
            "unknown_AbregeFamilleMMA": dict(Counter(unknown_AbregeFamilleMMA)),
        }

    def convert_raw_ressource_datas_as_dict(
        self,
        raw_ressource_datas_as_dict: List[Dict],
    ) -> Tuple[List[VehicleEventData], Dict]:
        converted_datas = []
        errors = []
        unknown_AbregeFamilleRessourcesPartagees = []
        unknown_cstcs = []
        for data_as_dict in raw_ressource_datas_as_dict:
            try:
                converted_data = self.ressource_mapper.convert_data(data_as_dict)
                if converted_data.role != "other":  # type: ignore
                    converted_datas.append(converted_data)
            except InvalidEventFormatError as e:
                errors.append(str(e))
                if "AbregeFamilleRessourcesPartagees" in str(e):
                    unknown_AbregeFamilleRessourcesPartagees.append(
                        data_as_dict["AbregeFamilleRessourcesPartagees"]
                    )
                elif "LibelleAffectationOperationnelle" in str(e):
                    unknown_cstcs.append(
                        data_as_dict["LibelleAffectationOperationnelle"]
                    )

        return converted_datas, {
            "errors": dict(Counter(errors)),
            "unknown_cstcs": dict(Counter(unknown_cstcs)),
            "unknown_AbregeFamilleRessourcesPartagees": dict(
                Counter(unknown_AbregeFamilleRessourcesPartagees)
            ),
        }

    def convert_raw_intervention_datas_as_dict(
        self, raw_intervention_datas_as_dict: List[Dict]
    ) -> Tuple[List[OperationEventData], Dict]:
        converted_datas = []
        errors = []
        unknown_cstcs = []
        for data_as_dict in raw_intervention_datas_as_dict:
            try:
                converted_data = self.operation_mapper.convert_data(
                    data_as_dict,
                    topic="adagio_intervention_snapshot",
                )
                converted_datas.append(converted_data)
            except InvalidEventFormatError as e:
                errors.append(str(e))
                if (
                    str(e)
                    == "{'OdeResume': {'Cstc': {'Libelle': ['Must be one of: ... ]}}}"
                ):
                    unknown_cstcs.append(data_as_dict["OdeResume"]["Cstc"]["Libelle"])

        return converted_datas, {
            "errors": dict(Counter(errors)),
            "unknown_cstcs": dict(Counter(unknown_cstcs)),
        }

    @staticmethod
    def dump_conversion_errors(vehicles: Dict, operation: Dict):
        now = datetime.now().strftime("%d-%m-%YT%H.%M.%S")
        mkdir_if_relevant(Path("./data/logs"))
        with open(f"./data/logs/{now}__reset_conversion_errors.json", "a") as file:
            json.dump({"vehicles": vehicles, "operation": operation}, file)
