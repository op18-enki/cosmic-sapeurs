import abc
from typing import Dict

from shared.ports.abstract_events_repository import AbstractEventsRepository


class AbstractExternalEventsRepository(AbstractEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)

    def add(self, event: Dict) -> None:
        self._add(event)

    @abc.abstractclassmethod
    def _add(self, event: Dict) -> None:
        raise NotImplementedError

    def purge_old_similar_events(self, event):
        if self._should_purge:
            raise NotImplementedError
