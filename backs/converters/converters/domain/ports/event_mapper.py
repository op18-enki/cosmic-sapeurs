import abc
from typing import Dict, Union

from shared.event_source import EventSource
from shared.events.event_entity import EventData
from shared.external_event import ExternalEventAsDict
from shared.sapeurs_events import OperationEvent, VehicleEvent


class AbstractVehicleEventMapper(abc.ABC):
    def __init__(self):
        pass

    def convert(
        self, raw_event: Union[Dict, ExternalEventAsDict], source: EventSource
    ) -> VehicleEvent:
        raise NotImplementedError

    def convert_data(self, raw_data: Dict) -> EventData:
        raise NotImplementedError


class AbstractOperationEventMapper(abc.ABC):
    def __init__(self):
        pass

    def convert(
        self, raw_event: Union[Dict, ExternalEventAsDict], source: EventSource
    ) -> OperationEvent:
        raise NotImplementedError

    def convert_data(self, raw_data: Dict, topic: str) -> EventData:
        raise NotImplementedError


class AbstractResourceEventMapper(abc.ABC):
    def __init__(self):
        pass

    def convert(
        self, raw_event: Union[Dict, ExternalEventAsDict], source: EventSource
    ) -> VehicleEvent:
        raise NotImplementedError

    def convert_data(self, raw_data: Dict) -> EventData:
        raise NotImplementedError
