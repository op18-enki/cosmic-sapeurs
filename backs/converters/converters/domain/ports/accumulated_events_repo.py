import abc
from typing import List, Optional

from shared.sapeurs_events import Event


class AlreadyExistingEventUuid(Exception):
    pass


class AbstractAccumulatedEventsRepository(abc.ABC):
    def add(self, event: Event) -> None:
        if self._match_uuid(event.uuid):
            raise AlreadyExistingEventUuid(f"Duplicated uuid : {event.uuid}")
        self._add(event)

    @abc.abstractmethod
    def _add(self, event: Event) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def _match_uuid(self, uuid: str) -> Optional[Event]:
        raise NotImplementedError

    @abc.abstractmethod
    def pop_accumulated_events(self) -> List[Event]:
        raise NotImplementedError


class InMemoryAccumulatedEventsRepository(AbstractAccumulatedEventsRepository):
    def __init__(self):
        self._events: List[Event] = []

    def _add(self, event: Event):
        self._events.append(event)

    def _match_uuid(self, uuid: str) -> Optional[Event]:
        _matches = [event for event in self._events if event.uuid == uuid]
        return _matches[0] if _matches else None

    def pop_accumulated_events(self) -> List[Event]:
        accumulated_events = self._events
        self._events = []
        return accumulated_events

    # next methods are only for test purposes
    @property
    def events(self) -> List[Event]:
        return self._events

    @events.setter
    def events(self, events: List[Event]):
        self._events = events
