import abc
import json
import os
from dataclasses import asdict, dataclass
from typing import Dict, List

from dotenv import load_dotenv

from shared.helpers.logger import logger


@dataclass
class ExternalSystemGatewaySnapshot:
    vehicles: List[Dict]
    operations: List[Dict]
    resources: List[Dict]


class AbstractExternalSystemGateway(abc.ABC):
    @abc.abstractmethod
    def get_snapshot() -> Dict:
        raise NotImplementedError()


class CustomExternalSystemGateway(AbstractExternalSystemGateway):
    def __init__(self) -> None:
        load_dotenv()
        json_path = os.getenv("JSON_CUSTOM_EXTERNAL_SYSTEM_GATEWAY")
        if json_path is None:
            self._shapshot = asdict(ExternalSystemGatewaySnapshot([], [], []))
            return

        try:
            with open(json_path) as file:
                response = json.load(file)
            self._shapshot = response
        except Exception as e:
            logger.warn(f"Could not load json from {json_path} : {str(e)}")
            self._shapshot = asdict(ExternalSystemGatewaySnapshot([], [], []))

    def get_snapshot(self) -> Dict:
        return self._shapshot

    # For test purpose only
    def set_snapshot(self, snapshot: Dict) -> None:
        self._shapshot = snapshot
