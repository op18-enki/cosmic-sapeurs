import abc
from typing import List

from shared.sapeurs_events import Event


class AbstractPublisher(abc.ABC):
    @abc.abstractmethod
    def publish_to_cover_ops(self, event: Event) -> None:
        """Publish event to cover-ops app"""
        raise NotImplementedError

    @abc.abstractmethod
    def can_publish(self) -> bool:
        raise NotImplementedError
