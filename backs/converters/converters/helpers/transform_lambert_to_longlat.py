from typing import Tuple

from pyproj import Proj, Transformer

transformer = Transformer.from_proj(Proj("epsg:2154"), Proj("epsg:4326"))


def transform_lambert_to_latlong(
    lambert_coordinates: Tuple[float, float]
) -> Tuple[float, float]:
    # Note: one can find the epsg references in the following site: spatialreference.
    # For example 2154 for lambert 93 and 4326 for the World Geodetic System.
    latitude, longitude = transformer.transform(*lambert_coordinates)
    return round(latitude, 7), round(longitude, 7)
