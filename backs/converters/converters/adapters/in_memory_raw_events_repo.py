from typing import List

from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.external_event import ExternalEvent


class InMemoryExternalEventRepository(AbstractExternalEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self.raw_events: List[ExternalEvent] = []

    def _add(self, raw_event: ExternalEvent):
        self.raw_events.append(raw_event)
