from pathlib import Path
from typing import List

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.adagio_vehicle_events import AdagioVehicleChangedStatusEvent


class InMemoryAdagioVehicleEventsRepository(AbstractExternalEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge=purge)
        self._adagio_events: List[AdagioVehicleChangedStatusEvent] = []

    def _add(self, event: AdagioVehicleChangedStatusEvent) -> None:
        self._adagio_events.append(event)

    # For test purpose only
    @property
    def adagio_events(self):
        return self._adagio_events

    def purge_old_similar_events(self, event: AdagioVehicleChangedStatusEvent):
        pass


class CsvAdagioVehicleEventsRepository(AdagioCsvEventsRepository):
    def __init__(self, purge: bool, csv_path: Path):
        super().__init__(purge, csv_path)
