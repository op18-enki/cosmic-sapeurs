from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)


class PgAdagioInterventionEventsRepository(AdagioPgEventsRepository):
    pass
