from sqlalchemy import Column, MetaData, String, Table
from sqlalchemy.dialects.postgresql import JSONB

metadata = MetaData()

raw_vehicle_events_table = Table(
    "raw_vehicle_events",
    metadata,
    Column("topic", String(255)),
    Column("data", JSONB),
)

raw_operation_events_table = Table(
    "raw_operation_events",
    metadata,
    Column("topic", String(255)),
    Column("data", JSONB),
)

raw_resource_events_table = Table(
    "raw_resource_events",
    metadata,
    Column("topic", String(255)),
    Column("data", JSONB),
)


def initialize_db(engine):
    metadata.create_all(engine)


def drop_db(engine):
    metadata.drop_all(engine)


def reset_db(connection):
    connection.execute(
        """
        DELETE FROM raw_vehicle_events;
        """
    )
    connection.execute(
        """
        DELETE FROM raw_operation_events;
        """
    )
