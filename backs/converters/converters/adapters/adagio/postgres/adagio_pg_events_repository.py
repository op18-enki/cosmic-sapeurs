import json
from typing import Dict

from sqlalchemy import Table
from sqlalchemy.engine.base import Engine

from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)


class AdagioPgEventsRepository(AbstractExternalEventsRepository):
    def __init__(
        self,
        purge: bool,
        engine: Engine,
        table: Table,
    ):
        super().__init__(purge)
        self.connection = engine.connect()
        self.table = table

    def _add(self, event: Dict) -> None:
        topic = event["topic"]
        try:
            serialized_data = json.dumps(event["data"])
        except Exception as e:
            serialized_data = str(e)
        insertion = self.table.insert().values(topic=topic, data=serialized_data)
        self.connection.execute(insertion)
