from typing import Dict, List

import requests

from converters.domain.ports.external_system_gateway import (
    AbstractExternalSystemGateway,
)
from shared.external_event import ExternalEvent
from shared.helpers.logger import logger


class AdagioExternalSystemGateway(AbstractExternalSystemGateway):
    def __init__(
        self,
        external_system_gateway_vehicles_url: str,
        external_system_gateway_snapshot_url: str,
    ) -> None:
        super().__init__()
        self.external_system_gateway_vehicles_url = external_system_gateway_vehicles_url
        self.external_system_gateway_snapshot_url = external_system_gateway_snapshot_url

    def get_last_status_of_all_vehicles(self) -> List[ExternalEvent]:
        logger.info(
            f"\nGetting last status of all vehicles, from {self.external_system_gateway_vehicles_url}..."
        )
        r = requests.get(self.external_system_gateway_vehicles_url)
        logger.info(f"Response status was : {r.status_code}")
        body = r.json()
        if r.status_code == 200:
            return body

        logger.warn("Failed to fetch data from Adagio : ", body)
        return []

    def get_snapshot(self) -> Dict:
        logger.info(
            f"\nGetting snapshot, from {self.external_system_gateway_snapshot_url}..."
        )
        r = requests.get(self.external_system_gateway_snapshot_url)
        logger.info(f"Response status was : {r.status_code}")
        body = r.json()
        if r.status_code == 200:
            return body

        logger.warn("Failed to fetch data from Adagio : ", body)
        return {"vehicles": [], "interventions": []}
