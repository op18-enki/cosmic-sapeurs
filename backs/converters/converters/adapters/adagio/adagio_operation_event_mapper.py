from typing import Dict, Literal, Optional, Tuple, Union, cast

from converters.adapters.adagio.mapping_dictionnaries.departure_criteria_to_cause import (
    departure_criteria_to_cause,
)
from converters.domain.ports.event_mapper import AbstractOperationEventMapper
from converters.helpers.transform_lambert_to_longlat import transform_lambert_to_latlong
from shared.adagio_event_dict_to_event import adagio_intervention_dict_to_data
from shared.adagio_intervention_events import (
    AdagioInterventionChangedStatusEventData,
    AdagioInterventionCreatedEventData,
    AdagioInterventionReinforcedEventData,
    OdeResume,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    RawOperationId,
)
from shared.data_transfert_objects.vehicle_event_data import VehicleStatus
from shared.event_source import EventSource
from shared.exceptions import InvalidEventFormatError
from shared.helpers.date import DateStr
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import OperationEvent

from .mapping_dictionnaries.libelleStatutIntervention_to_OperationStatus import (
    libelleStatutIntervention_to_OperationStatus,
)


def ode_resume_to_latlong(
    ode_resume: OdeResume,
) -> Union[Tuple[float, float], Tuple[None, None]]:
    return (
        transform_lambert_to_latlong((ode_resume.Adresse.X, ode_resume.Adresse.Y))
        if ode_resume.Adresse
        and (ode_resume.Adresse.X is not None and ode_resume.Adresse.Y is not None)
        else (None, None)  # TODO / Question : Should it NOT be optional ?
    )


# TODO : check where it's used : did not break any tests despite the fact that event.data may not be of right type
class AdagioOperationEventMapper(AbstractOperationEventMapper):
    def __init__(self, uuid: AbstractUuid):
        super().__init__()
        self.uuid = uuid

    def convert(
        self, raw_event_as_dict: Dict, source: EventSource
    ) -> Optional[OperationEvent]:
        topic = raw_event_as_dict["topic"]
        data = self.convert_data(raw_event_as_dict["data"], topic)
        return OperationEvent(data=data, source=source, uuid=self.uuid.make())

    def convert_data(self, raw_data_as_dict: Dict, topic) -> OperationEventData:

        raw_data = adagio_intervention_dict_to_data(raw_data_as_dict, topic)

        if topic in ["adagio_intervention_created", "adagio_intervention_snapshot"]:
            return self.on_intervention_with_opening_infos(
                cast(AdagioInterventionCreatedEventData, raw_data), topic
            )
        elif topic == "adagio_intervention_reinforced":
            return self.on_intervention_reinforced(
                cast(AdagioInterventionReinforcedEventData, raw_data)
            )
        elif topic == "adagio_intervention_changed_status":
            return self.on_intervention_changed_status(
                cast(AdagioInterventionChangedStatusEventData, raw_data)
            )
        else:
            messages = f"Received intervention event with unknown topic {topic}"
            logger.warn(messages)
            raise InvalidEventFormatError(messages)

    def on_intervention_with_opening_infos(
        self,
        event_data: AdagioInterventionCreatedEventData,
        topic: Literal["adagio_intervention_created", "adagio_intervention_snapshot"],
    ) -> OperationEventData:
        adagio_procedure = event_data.OdeResume.Procedure
        ode_resume = event_data.OdeResume
        raw_cause = ode_resume.LibelleMotif
        departure_criteria = ode_resume.Motif
        latitude, longitude = ode_resume_to_latlong(ode_resume)

        cause = departure_criteria_to_cause(departure_criteria)

        if topic == "adagio_intervention_created":
            status: VehicleStatus = "opened"
        else:
            status = (
                "opened" if ode_resume.NbEnginsPartis == 0 else "all_vehicles_released"
            )
        return OperationEventData(
            raw_cause=raw_cause,
            raw_operation_id=RawOperationId(str(ode_resume.IdIntervention)),
            address=ode_resume.Adresse.LibelleComplet if ode_resume.Adresse else None,
            address_area=ode_resume.Cstc.Libelle,
            cause=cause,
            status=status,
            timestamp=DateStr(event_data.timestamp),
            raw_procedure=adagio_procedure,
            departure_criteria=departure_criteria,
            latitude=latitude,
            longitude=longitude,
        )

    def on_intervention_reinforced(
        self, event_data: AdagioInterventionReinforcedEventData
    ) -> OperationEventData:
        adagio_procedure = event_data.OdeResume.Procedure
        ode_resume = event_data.OdeResume
        raw_cause = event_data.OdeResume.LibelleMotif

        latitude, longitude = ode_resume_to_latlong(ode_resume)

        departure_criteria = ode_resume.Motif
        cause = departure_criteria_to_cause(departure_criteria)

        return OperationEventData(
            raw_cause=raw_cause,
            raw_operation_id=RawOperationId(str(ode_resume.IdIntervention)),
            address=ode_resume.Adresse.LibelleComplet if ode_resume.Adresse else None,
            address_area=ode_resume.Cstc.Libelle,
            cause=cause,
            status="reinforced",
            timestamp=DateStr(event_data.timestamp),
            raw_procedure=adagio_procedure,
            departure_criteria=departure_criteria,
            latitude=latitude,
            longitude=longitude,
        )

    def on_intervention_changed_status(
        self, event_data: AdagioInterventionChangedStatusEventData
    ) -> OperationEventData:
        intervention = event_data.Intervention
        libelle_statut_intervention = (
            event_data.StatutIntervention.LibelleStatutIntervention
        )

        return OperationEventData(
            raw_cause=None,
            raw_operation_id=RawOperationId(str(intervention.IdIntervention)),
            address=None,
            address_area=None,
            cause=None,
            status=libelleStatutIntervention_to_OperationStatus[
                libelle_statut_intervention
            ],
            timestamp=DateStr(event_data.timestamp),
            raw_procedure=None,
            departure_criteria=None,
            latitude=None,
            longitude=None,
        )
