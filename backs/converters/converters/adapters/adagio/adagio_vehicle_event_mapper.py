from typing import Dict

from converters.domain.ports.event_mapper import AbstractVehicleEventMapper
from shared.adagio_event_dict_to_event import adagio_vehicle_event_dict_to_event_data
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    OmnibusInfos,
    RawVehicleId,
    VehicleEventData,
)
from shared.event_source import EventSource
from shared.helpers.date import DateStr
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import VehicleEvent

from .mapping_dictionnaries.abregeFamilleMMA_to_VehicleRole import (
    abregeFamille_to_VehicleRole,
)
from .mapping_dictionnaries.abregeStatutOperationnel_to_VehicleStatus import (
    AbregeStatutOperationnel_to_VehicleStatus,
)
from .mapping_dictionnaries.libelleObjetGeo_to_area import libelleObjetGeo_to_area


class AdagioVehicleEventMapper(AbstractVehicleEventMapper):
    def __init__(self, uuid: AbstractUuid):
        super().__init__()
        self.uuid = uuid

    def convert(self, raw_event_as_dict: Dict, source: EventSource) -> VehicleEvent:
        converted_data = self.convert_data(raw_event_as_dict["data"])
        return VehicleEvent(uuid=self.uuid.make(), source=source, data=converted_data)

    def convert_data(self, raw_data_as_dict: Dict) -> VehicleEventData:
        raw_data = adagio_vehicle_event_dict_to_event_data(raw_data_as_dict)
        libelleStatutOperationnel = raw_data.LibelleStatutOperationnel.strip()
        raw_operation_id_as_string = str(raw_data.IdIntervention)

        if raw_operation_id_as_string in ["-1", "0", "None"]:
            raw_operation_id = None
        else:
            raw_operation_id = RawOperationId(raw_operation_id_as_string)

        is_ep6_omnibus = (
            raw_data.Omnibus and raw_data.InformationsOmnibus.EstOmnibusEpm6Classique
        )
        # --------------------------------------------

        home_area = libelleObjetGeo_to_area[raw_data.LibelleAffectationOperationnelle]

        role = abregeFamille_to_VehicleRole(
            raw_data.AbregeFamilleMMAOperationnelle,
            raw_data.AbregeFamilleMMAOriginelle,
            is_ep6_omnibus,
            home_area,
        )

        return VehicleEventData(
            home_area=home_area,
            raw_operation_id=raw_operation_id,
            raw_status=libelleStatutOperationnel,
            raw_vehicle_id=RawVehicleId(str(raw_data.IdMMA)),
            role=role,
            status=AbregeStatutOperationnel_to_VehicleStatus[
                raw_data.AbregeStatutOperationnel
            ],
            timestamp=raw_data.timestamp,
            vehicle_name=raw_data.ImmatriculationBSPP,
            omnibus=OmnibusInfos(
                partner_raw_vehicle_id=RawVehicleId(
                    str(raw_data.InformationsOmnibus.IdMMAOmnibus)
                ),
            )
            if is_ep6_omnibus
            else None,
        )
