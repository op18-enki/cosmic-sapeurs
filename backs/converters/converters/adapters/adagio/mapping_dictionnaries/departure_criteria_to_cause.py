import re
from typing import Dict, cast

from shared.data_transfert_objects.operation_event_data import OperationCause

family_key_to_cause: Dict[str, OperationCause] = {
    "1": "fire",
    "2": "accident",
    "3": "victim",
    "4": "people_protection",
    "5": "animal",
    "6": "property_protection",
    "7": "water_gaz_electricity",
    "8": "pollution",
    "9": "exploration",
}


def departure_criteria_to_cause(departure_criteria: str) -> OperationCause:
    for family_key, cause in family_key_to_cause.items():
        regex = "^" + family_key + "\d{2}"
        if re.findall(regex, departure_criteria):
            return cause
    return cast(OperationCause, "other")
