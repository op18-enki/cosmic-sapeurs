from typing import Dict

from shared.adagio_vehicle_events import AdagioArea
from shared.data_transfert_objects.area import Area

libelleObjetGeo_to_area: Dict[AdagioArea, Area] = {
    "STMR": "STMR",
    "ANTO": "ANTO",
    "CHPY": "CHPY",
    "PLCL": "PLCL",
    "STCL": "STCL",
    "CHLY": "CHLY",
    "POIS": "POIS",
    "ROUS": "ROUS",
    "CHTO": "CHTO",
    "STOU": "STOU",
    "PARM": "PARM",
    "VIJF": "VIJF",
    "PIER": "PIER",
    "SEVI": "SEVI",
    "CHPT": "CHPT",
    "PCDG": "PCDG",
    "LEVA": "LEVA",
    "AUBE": "AUBE",
    "VISG": "VISG",
    "BOUL": "BOUL",
    "NOIS": "NOIS",
    "MALF": "MALF",
    "SUCY": "SUCY",
    "VILC": "VILC",
    "CHOI": "CHOI",
    "VITR": "VITR",
    "STDE": "STDE",
    "LACO": "LACO",
    "MTRL": "MTRL",
    "STHO": "STHO",
    "VIMB": "VIMB",
    "NEUI": "NEUI",
    "COBI": "COBI",
    "CLIC": "CLIC",
    "PLAI": "PLAI",
    "AULN": "AULN",
    "MALA": "MALA",
    "BLME": "BLME",
    "DRAN": "DRAN",
    "LAND": "LAND",
    "MENI": "MENI",
    "CHAR": "CHAR",
    "PANT": "PANT",
    "BITC": "BITC",
    "BLAN": "BLAN",
    "MTMA": "MTMA",
    "PROY": "PROY",
    "AUTE": "AUTE",
    "DAUP": "DAUP",
    "MTGE": "MTGE",
    "GREN": "GREN",
    "ISSY": "ISSY",
    "MEUD": "MEUD",
    "BGLR": "BGLR",
    "CLAM": "CLAM",
    "NATI": "NATI",
    "CBVE": "CBVE",
    "PUTX": "PUTX",
    "GENN": "GENN",
    "MASS": "MASS",
    "ASNI": "ASNI",
    "IVRY": "IVRY",
    "RUEI": "RUEI",
    "VINC": "VINC",
    "NANT": "NANT",
    "COBE": "COBE",
    "NOGT": "NOGT",
    "CRET": "CRET",
    "TREM": "TREM",
    "RUNG": "RUNG",
    "ORLY": "ORLY",
    "BOND": "BOND",
    "BSLT": "BSLT",
    "BES": "SDIS",
    "CEP": "SDIS",
    "COU": "SDIS",
    "ENG": "SDIS",
    "ERA": "SDIS",
    "FRA": "SDIS",
    "GLG": "SDIS",
    "GOU": "SDIS",
    "MEV": "SDIS",
    "MLC": "SDIS",
    "MCY": "SDIS",
    "NLV": "SDIS",
    "SUR": "SDIS",
    "TAV": "SDIS",
    "ARG": "SDIS",
    "VLB": "SDIS",
    "EVRY": "SDIS",
    "MEREV": "SDIS",
    "VERLG": "SDIS",
    "RIS": "SDIS",
    "MENNE": "SDIS",
    "CHALO": "SDIS",
    "VIRY": "SDIS",
    "MAISS": "SDIS",
    "BMT": "SDIS",
    "CEV": "SDIS",
    "CHM": "SDIS",
    "AIN": "SDIS",
    "PER": "SDIS",
    "HER": "SDIS",
    "LIA": "SDIS",
    "VIG": "SDIS",
    "PRE": "SDIS",
    "NEU": "SDIS",
    "GON": "SDIS",
    "VIA": "SDIS",
    "DOM": "SDIS",
    "GRA": "SDIS",
    "EAU": "SDIS",
    "MAR": "SDIS",
    "CHA": "SDIS",
    "BEL": "SDIS",
    "BEZ": "SDIS",
    "LOU": "SDIS",
    "SAN": "SDIS",
    "ROI": "SDIS",
    "OSN": "SDIS",
    "MER": "SDIS",
    "WISOU": "SDIS",
    "ANGER": "SDIS",
    "ATHIS": "SDIS",
    "BALIN": "SDIS",
    "BALLA": "SDIS",
    "BOUTI": "SDIS",
    "CHILY": "SDIS",
    "CUTTE": "SDIS",
    "DRAVE": "SDIS",
    "EPYNO": "SDIS",
    "GIF": "SDIS",
    "JUVIS": "SDIS",
    "LONGJ": "SDIS",
    "MILLY": "SDIS",
    "MONTG": "SDIS",
    "MONTL": "SDIS",
    "PUISE": "SDIS",
    "SAVIG": "SDIS",
    "VDY": "SDIS",
    "MONDE": "SDIS",
    "CERNY": "SDIS",
    "ETAMP": "SDIS",
    "MASSY": "SDIS",
    "SOS": "SDIS",
    "BIEVR": "SDIS",
    "ULIS": "SDIS",
    "SACLA": "SDIS",
    "LISSE": "SDIS",
    "ETREC": "SDIS",
    "BREUI": "SDIS",
    "ARPAJ": "SDIS",
    "MAROL": "SDIS",
    "LARDY": "SDIS",
    "STCHE": "SDIS",
    "PUSAY": "SDIS",
    "SGB": "SDIS",
    "LIMOU": "SDIS",
    "DOURD": "SDIS",
    "BRETI": "SDIS",
    "MARCO": "SDIS",
    "BRUYE": "SDIS",
    "PALAI": "SDIS",
    "CORBE": "SDIS",
    "MPS": "SDIS",
    "MLB": "SDIS",
    "SLG": "SDIS",
    "CHE": "SDIS",
    "PSY": "SDIS",
    "AUB": "SDIS",
    "CSH": "SDIS",
    "HOD": "SDIS",
    "MES": "SDIS",
    "LOU": "SDIS",
    "MOA": "SDIS",
    "CLV": "SDIS",
    "MTS": "SDIS",
    "RAM": "SDIS",
    "VES": "SDIS",
    "BRE": "SDIS",
    "STA": "SDIS",
    "CYR": "SDIS",
    "BON": "SDIS",
    "SEP": "SDIS",
    "GRC": "SDIS",
    "CSB": "SDIS",
    "GUY": "SDIS",
    "MAL": "SDIS",
    "LIM": "SDIS",
    "ACH": "SDIS",
    "PLA": "SDIS",
    "GGV": "SDIS",
    "ABL": "SDIS",
    "CRO": "SDIS",
    "CSC": "SDIS",
    "ESS": "SDIS",
    "MAG": "SDIS",
    "VRS": "SDIS",
    "VRS": "SDIS",
    "MLF": "SDIS",
    "CHA": "SDIS",
    "VIR": "SDIS",
    "BOI": "SDIS",
    "VLY": "SDIS",
    "TRI": "SDIS",
    "SGL": "SDIS",
    "HOI": "SDIS",
    "VIL": "SDIS",
    "MAR": "SDIS",
    "LMX": "SDIS",
    "VRN": "SDIS",
    "SPT": "SDIS",
    "CPS": "SDIS",
    "CNS": "SDIS",
    "CSI": "SDIS",
    "CSL": "SDIS",
    "CLM": "SDIS",
    "CLC": "SDIS",
    "DML": "SDIS",
    "DMG": "SDIS",
    "DMD": "SDIS",
    "FRM": "SDIS",
    "FTN": "SDIS",
    "JLC": "SDIS",
    "LCR": "SDIS",
    "LFG": "SDIS",
    "LFJ": "SDIS",
    "LGP": "SDIS",
    "LGM": "SDIS",
    "CTB": "SDIS",
    "LRB": "SDIS",
    "MSR": "SDIS",
    "MLN": "SDIS",
    "MTM": "SDIS",
    "MSC": "SDIS",
    "NGS": "SDIS",
    "CHL": "SDIS",
    "FTB": "SDIS",
    "TPT": "SDIS",
    "LGS": "SDIS",
    "CTL": "SDIS",
    "GGR": "SDIS",
    "VLX": "SDIS",
    "MUX": "SDIS",
    "BLR": "SDIS",
    "MLR": "SDIS",
    "VLP": "SDIS",
    "BSS": "SDIS",
    "FRB": "SDIS",
    "LZO": "SDIS",
    "PVS": "SDIS",
    "SPL": "SDIS",
    "EGV": "SDIS",
    "MMT": "SDIS",
    "MRL": "SDIS",
    "RBS": "SDIS",
    "BMG": "SDIS",
    "BRM": "SDIS",
    "BCR": "SDIS",
    "MTR": "SDIS",
    "NMS": "SDIS",
    "OZF": "SDIS",
    "PTC": "SDIS",
    "SFP": "SDIS",
    "SGM": "SDIS",
    "SLT": "SDIS",
    "TNB": "SDIS",
    "VRM": "SDIS",
    "VLP": "SDIS",
    "VLG": "SDIS",
    "VLS": "SDIS",
    "RZB": "SDIS",
    "CCT": "LSO",
    "CASJ": "LSO",
    "CASC": "LSO",
    "MONN": "LSO",
    "CCT": "LSO",
    "SEVR": "LSO",
    "EMGAS": "LSO",
    "CASS": "LSO",
    "JOIN": "LSO",
    "NBCP": "LSO",
    "NBCL": "LSO",
    "NBCR": "LSO",
    "BALA": "LSO",
    "STEC": "LSL",
    "SAMU": "LSL",
    "CCL1": "LSL",
    "CCL2": "LSL",
    "CCL3": "LSL",
    "CIR": "LSL",
    "CFC": "LSL",
    "LVV": "LSL",
    "STEC": "LSL",
    "POUC": "LSL",
    "CDS2": "LSL",
    "CMAI": "LSL",
    "GPOR": "LSL",
    # "CSC": "LSL",
    "CSC": "CSC",
    "LIME": "LSL",
}
