from pathlib import Path
from typing import List

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.adagio_intervention_events import AdagioInterventionEvent


class InMemoryAdagioInterventionEventsRepository(AbstractExternalEventsRepository):
    def __init__(self, purge: bool):
        super().__init__(purge)
        self._adagio_events: List[AdagioInterventionEvent] = []

    def _add(self, event: AdagioInterventionEvent) -> None:
        self._adagio_events.append(event)

    # For test purpose only
    @property
    def adagio_events(self):
        return self._adagio_events

    def purge_old_similar_events(self, event: AdagioInterventionEvent):
        pass


class CsvAdagioInterventionEventsRepository(AdagioCsvEventsRepository):
    def __init__(self, purge: bool, csv_path: Path):
        super().__init__(purge, csv_path)
