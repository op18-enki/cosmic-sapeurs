from typing import Dict
from converters.adapters.adagio.mapping_dictionnaries.abregeStatutOperationnel_to_VehicleStatus import (
    AbregeStatutOperationnel_to_VehicleStatus,
)

from converters.domain.ports.event_mapper import (
    AbstractResourceEventMapper,
)
from shared.adagio_event_dict_to_event import adagio_ressource_event_dict_to_event_data
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
)
from shared.event_source import EventSource
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import VehicleEvent
from shared.adagio_ressource_events import AdagioResourceChangedStatusEventData

from .mapping_dictionnaries.libelleObjetGeo_to_area import libelleObjetGeo_to_area


class AdagioRessourceEventMapper(AbstractResourceEventMapper):
    def __init__(self, uuid: AbstractUuid):
        super().__init__()
        self.uuid = uuid

    def convert(self, raw_event_as_dict: Dict, source: EventSource) -> VehicleEvent:
        converted_data = self.convert_data(raw_event_as_dict["data"])
        return VehicleEvent(uuid=self.uuid.make(), source=source, data=converted_data)

    def convert_data(self, raw_data_as_dict: Dict) -> VehicleEventData:
        raw_data: AdagioResourceChangedStatusEventData = (
            adagio_ressource_event_dict_to_event_data(raw_data_as_dict)
        )
        libelleStatutOperationnel = raw_data.LibelleStatutOperationnel.strip()
        raw_operation_id_as_string = str(raw_data.IdIntervention)

        if raw_operation_id_as_string in ["-1", "0", "None"]:
            raw_operation_id = None
        else:
            raw_operation_id = RawOperationId(raw_operation_id_as_string)

        # --------------------------------------------

        home_area = libelleObjetGeo_to_area[raw_data.LibelleAffectationOperationnelle]

        role = raw_data.AbregeFamilleRessourcesPartagees.replace(" ", "_")

        return VehicleEventData(
            home_area=home_area,
            raw_operation_id=raw_operation_id,
            raw_status=libelleStatutOperationnel,
            raw_vehicle_id=RawVehicleId(
                "r_" + str(raw_data.IdRessourcesPartagee)
            ),  # Adding "r_" as a prefix to prevent collisions with other vehicle ids
            role=role,
            status=AbregeStatutOperationnel_to_VehicleStatus[
                raw_data.AbregeStatutOperationnel
            ],
            timestamp=raw_data.timestamp,
            vehicle_name=raw_data.ImmatriculationBSPP,
            omnibus=None,
        )
