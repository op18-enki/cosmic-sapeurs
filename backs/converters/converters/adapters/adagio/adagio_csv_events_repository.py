import json
from pathlib import Path
from typing import Dict

from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.helpers.csv import writerow
from shared.helpers.mkdir_if_relevent import mkdir_if_relevant


class AdagioCsvEventsRepository(AbstractExternalEventsRepository):
    def __init__(self, purge: bool, csv_path: Path):
        super().__init__(purge=purge)
        self.csv_path = csv_path
        mkdir_if_relevant(self.csv_path.parent)
        writerow(self.csv_path, ["topic", "data"], mode="w")

    def _add(self, event: Dict):
        topic = event["topic"]
        try:
            serialized_data = json.dumps(event["data"])
        except Exception as e:
            serialized_data = str(e)
        writerow(
            self.csv_path,
            [
                topic,
                serialized_data,
            ],
            mode="a",
        )
