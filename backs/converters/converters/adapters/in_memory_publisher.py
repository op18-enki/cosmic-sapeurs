from typing import List, Optional

from converters.domain.ports.publisher import AbstractPublisher
from shared.sapeurs_events import Event


class InMemoryPublisher(AbstractPublisher):
    def __init__(self) -> None:
        super().__init__()
        self._published_events: List[Event] = []
        self.error_message: Optional[str] = None

    def publish_to_cover_ops(self, event: Event) -> None:
        if self.error_message:
            raise Exception(self.error_message)
        self._published_events.append(event)

    def can_publish(self) -> bool:
        return self.error_message is None

    # For test purposes only
    @property
    def published_events(self) -> List[Event]:
        return self._published_events

    def set_error(self, error_message: Optional[str]) -> None:
        self.error_message = error_message
