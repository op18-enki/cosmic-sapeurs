import json

import requests

from converters.domain.ports.publisher import AbstractPublisher
from shared.helpers.logger import logger
from shared.routes import sapeurs_routes
from shared.sapeurs_events import Event


class CoverOpsNotReadyError(Exception):
    pass


class HttpPublisher(AbstractPublisher):
    def __init__(self, http_client_url: str) -> None:
        super().__init__()
        self.http_client_url = http_client_url

    def publish_to_cover_ops(self, event: Event) -> None:
        event_as_json = json.dumps(event.as_dict())
        response = requests.post(
            f"{self.http_client_url}/{sapeurs_routes.converted_event}",
            data=event_as_json,
        )
        if response.status_code != 200:
            logger.info(
                f"Cannot publish converted-event to cover-ops. \nReceived status {response.status_code}, reason {response.reason}"
            )
            raise CoverOpsNotReadyError

    def can_publish(self) -> bool:
        try:
            response = requests.get(
                f"{self.http_client_url}/{sapeurs_routes.ping}",
            )
            response_json = response.json()
            is_ready = response_json["is_ready"]
            logger.info(f"Cover-Ops is up, and is_ready is {is_ready}")
            return is_ready
        except requests.exceptions.ConnectionError:
            return False
