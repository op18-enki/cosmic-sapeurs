# Converters 🇬🇧

This service aims at converting the raw data (coming from the external system) into sapeurs-format. This is an independent Python application,
deployed independently from Sapeurs-Backend. The converters stores the raw events before converting them and sending them to Sapeurs-Backend.

## Pre-Requisite

Go to the right folder :

```
cd backs/converters
```

- Python 3

## Install

Create a virtual environment, install the packages and initialize a .env file.

```
python3 -m venv venv
source venv/bin/activate
pip install ../shared
pip install -e .
cp .env.sample .env
```

## Up test docker-compose and launch the tests

```
pip install -r tests-requirements.txt
docker-compose -f ../../docker-compose.test.yml up --build
pytest tests
```

## Launch the app

```
python converters/entrypoints/server.py
```
